/*shaokai*/
package com.android.dfish;

import android.util.Log;
import com.mediatek.common.MediatekClassFactory;
import com.mediatek.common.hdmi.IHDMINative;
/*
	TYPE_MIN = 0,
	TYPE_CHARGE = 0,
	TYPE_CALL = 1
	TYPE_MSG =2
	TYPE_NOTIFY=3
	TYPE_TOP= 9

	enable = 1 0r 0

	breath = 1 or 0

	i = 1 0r 2 or 3

*/
public class AWLedControl {
    private static final String TAG = "AWLedControl";
    //private IHDMINative mHdmiNative = null; 
	public static String LAST_STATUS = "NONE";
	
	private static String CURRENT_STATUS = "NONE";
	
	
	
    public static void isAwRedLedEnable(int type,int enable,int breath,int i) {
		IHDMINative HdmiNative = null;
		HdmiNative = MediatekClassFactory.createInstance(IHDMINative.class);
		HdmiNative.isEnableRedLed(type,enable,breath,i);
	
		if(enable==0){
			CURRENT_STATUS = "NONE";
		}else{
			if(!LAST_STATUS.equals(CURRENT_STATUS))
				LAST_STATUS = CURRENT_STATUS;
			CURRENT_STATUS = "RED";
		}
    }

    public static void isAwGreenLedEnable(int type,int enable,int breath,int i) {
		IHDMINative HdmiNative = null;
		HdmiNative = MediatekClassFactory.createInstance(IHDMINative.class);
		HdmiNative.isEnableGreenLed(type,enable,breath,i);
		
		if(enable==0){
			CURRENT_STATUS = "NONE";
		}else{
			if(!LAST_STATUS.equals(CURRENT_STATUS))
				LAST_STATUS = CURRENT_STATUS;
			CURRENT_STATUS = "GREEN";
		}
    }

    public static void isAwBlueLedEnable(int type,int enable,int breath,int i) {
		IHDMINative HdmiNative = null;
		HdmiNative = MediatekClassFactory.createInstance(IHDMINative.class);
		HdmiNative.isEnableBlueLed(type,enable,breath,i);
		
		if(enable==0){
			CURRENT_STATUS = "NONE";
		}else{
			if(!LAST_STATUS.equals(CURRENT_STATUS))
				LAST_STATUS = CURRENT_STATUS;
			CURRENT_STATUS = "BLUE";
		}
    }

    public static void isAwWhiteEnable(int type,int enable,int breath,int i) {
		IHDMINative HdmiNative = null;
		HdmiNative = MediatekClassFactory.createInstance(IHDMINative.class);
		HdmiNative.isEnableWhiteLed(type,enable,breath,i);
		
		if(enable==0){
			CURRENT_STATUS = "NONE";
		}else{
			if(!LAST_STATUS.equals(CURRENT_STATUS))
				LAST_STATUS = CURRENT_STATUS;
			CURRENT_STATUS = "WHITE";
		}
    }

	    public static void isAwStandByEnable(int type,int enable,int breath,int i) {
		IHDMINative HdmiNative = null;
		HdmiNative = MediatekClassFactory.createInstance(IHDMINative.class);
		HdmiNative.isEnableStandByLed(type,enable,breath,i);
		
		if(enable==0){
			CURRENT_STATUS = "NONE";
		}else{
			if(!LAST_STATUS.equals(CURRENT_STATUS))
				LAST_STATUS = CURRENT_STATUS;
			CURRENT_STATUS = "STANDBY";
		}
    }
}

