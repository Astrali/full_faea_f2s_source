
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_ARM_MODE := arm
LOCAL_SRC_FILES := macwrite.c


LOCAL_SHARED_LIBRARIES := libc libnvram libcutils libfile_op

LOCAL_MODULE_PATH := $(TARGET_OUT_BIN)

LOCAL_C_INCLUDES := \
  $(MTK_PATH_SOURCE)/external/nvram/libnvram \
  $(MTK_PATH_SOURCE)/external/nvram/libfile_op \
  $(MTK_PATH_CUSTOM)/cgen/cfgfileinc \
  $(MTK_PATH_CUSTOM)/cgen/cfgdefault \
  $(MTK_PATH_CUSTOM)/cgen/inc

LOCAL_MODULE := macwrite
LOCAL_MODULE_TAGS := optional
include $(BUILD_EXECUTABLE)


