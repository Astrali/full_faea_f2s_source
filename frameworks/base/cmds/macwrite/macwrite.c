#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <fcntl.h>
#include <linux/serial.h> 

/* use nvram */
//#include "CFG_WIFI_File.h"
#include "libfile_op.h"
#include "Custom_NvRam_LID.h"
#include "CFG_WIFI_Default.h"
#include "CFG_file_lid.h"
#include "libnvram.h"


#define MAX_RETRY_COUNT 20

static void GetRandomValue(UINT_8 string[6])
{
    int iRandom = 0;
    int fd = 0;
    unsigned long seed;


    
    /* initialize random seed */
    srand (time(NULL));
    iRandom = rand();
    
    //string[1] = (((iRandom>>24|iRandom>>16) & (0xFE)) | (0x02));
    string[1] = ((iRandom>>8) &0xff);
    
    /* second seed */
    struct timeval tv;
    gettimeofday(&tv, NULL);
    srand (tv.tv_usec);
    iRandom = rand();
    
    string[2] = ((iRandom>>8) &0xff);
    
    /* third seed */
    fd = open("/dev/urandom", O_RDONLY);
    if (fd > 0){
        if (read(fd, &seed, sizeof(unsigned long)) > 0){
            srand (seed);
            iRandom = rand();
        }
        close(fd);
    }
    
    string[5] = (iRandom &0xff);

}

int main(void)
{
 
	UINT_8 addr[6];
	int i =0;
	F_ID  fid ;                                                                                                   
	int rec_size = 0;                                                                                               
	int rec_num = 0;
	int file_lid = AP_CFG_RDEB_FILE_WIFI_LID;
	bool isread = true;//ture for read
	WIFI_CFG_PARAM_STRUCT * pstWifiCfg_value  = NULL;
#if 1
	/*make sure nvram restore form bin complete*/
	int read_nvram_ready_retry = 0;
        char nvram_init_val[32] = {0};
	while(read_nvram_ready_retry < MAX_RETRY_COUNT)
	{
		read_nvram_ready_retry++;
		property_get("nvram_init",nvram_init_val,NULL);
	if(strcmp(nvram_init_val,"Ready") == 0)
	{
		break;
	}
	else
	{
		usleep(500*1000);
	}
	}
	if(read_nvram_ready_retry >= MAX_RETRY_COUNT)
	{
		printf("Get nvram restore ready faild\n");
	}
#endif	
	pstWifiCfg_value =( WIFI_CFG_PARAM_STRUCT *) malloc(sizeof(WIFI_CFG_PARAM_STRUCT));
	if(pstWifiCfg_value == NULL)
	return -1;                                                                                                  
	fid = NVM_GetFileDesc(file_lid, &rec_size, &rec_num, isread );                                     
	if(fid.iFileDesc <0)
	return -1;
	if(rec_size != read(fid.iFileDesc, pstWifiCfg_value,rec_size))
	{
		free(pstWifiCfg_value);
		return -1;
	}
	else
	{
		if((pstWifiCfg_value->aucMacAddress[0] == 0x00) && (pstWifiCfg_value->aucMacAddress[3] == 0xDE) && (pstWifiCfg_value->aucMacAddress[4] == 0xDF))
		{
			//already write
			printf("already write......\n");
			free(pstWifiCfg_value);
			return 0;
		}
	}

	if(!NVM_CloseFileDesc(fid))                                                                                 
		return -1;

	fid = NVM_GetFileDesc(file_lid, &rec_size, &rec_num, 0);
	if(fid.iFileDesc <0)
	{
		return -1;
	}
	
	if(rec_num != CFG_FILE_WIFI_REC_TOTAL)
	{
		NVM_CloseFileDesc(fid);
		return -1;
	}

	if(rec_size != CFG_FILE_WIFI_REC_SIZE)
	{
		NVM_CloseFileDesc(fid);
		return -1;
	}
	
	lseek(fid.iFileDesc,sizeof(UINT_16)*2,0);
	#if 1
	addr[0]=0x00;
	//addr[1]=0x11;
	//addr[2]=0x22;
	addr[3]=0xDE;
	addr[4]=0xDF;
	//addr[5]=0x55;
	#endif
	GetRandomValue(addr);//generate mac 1 2 5
	printf("%2x,%2x,%2x,%2x,%2x,%2x\n",addr[0],addr[1],addr[2],addr[3],addr[4],addr[5]);
	//write mac
	if(write(fid.iFileDesc,addr,6)<0)
	{
		NVM_CloseFileDesc(fid);
		return -1;
	}
	NVM_CloseFileDesc(fid);
	
	while(1)
	{
		bool back_res = false;
		back_res=FileOp_BackupToBinRegion_All();
		if(back_res)
		{
			FileOp_SetCleanBootFlag(true);
			printf("backup sussess\n");
			break;
		}
		else
		{
			printf("backup fail\n");
		}
		//if(FileOp_BackupToBinRegion_All())
			//break;
	}
	return 0;
}
