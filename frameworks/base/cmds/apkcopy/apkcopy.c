/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <arpa/inet.h>


#define LOG_TAG "apkcopy"
#include <cutils/log.h>
#include <cutils/sockets.h>
#include <private/android_filesystem_config.h>

#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>

char apk_list[20][128];

void List( char *path, char apk_array[20][128]  )
{
    struct dirent* ent = NULL;
    DIR *pDir;
    char dir[512];
    int i=0;
    struct stat statbuf; 
    if( (pDir=opendir(path))==NULL )
    {
        fprintf( stderr, "Cannot open directory:%s\n", path );
        return;
    }
    
    while( (ent=readdir(pDir))!=NULL )
    {

        snprintf( dir, 512,"%s/%s", path, ent->d_name ); 

        lstat(dir, &statbuf);

        if( S_ISDIR(statbuf.st_mode) )
        {

            if(strcmp( ".",ent->d_name) == 0 || 
                strcmp( "..",ent->d_name) == 0)
                 continue;

            printf( "AA %s:%s/\n", path, ent->d_name );

           // List( dir, ); 
           continue;
        }
        else
        {
            printf( "BB %s : %s\n", path, ent->d_name );
            strcpy(apk_array[i++], ent->d_name);
        } 
    }//while
    closedir(pDir);
}


int main(int argc, char **argv)
{
        const char copy_lan[] = "/data/app/app_copy_flag.text";
        int  lan_flag = 0;//1:exists  0:not exists
        int  lan_fd = -1;
        char buff[128] ={0};
        char buff1[128] ={0};
        char apk_name[512] = {0};
        int i;

        lan_fd = open(copy_lan,O_RDONLY, 0);
        if (lan_fd < 0) {
            //LOGD("switch_lan: copy_lan not exists!!! \n");
        }else{
            lan_flag = 1;
            close(lan_fd);
        }

        //test system("cat /system/thirdapps/QQ.apk > /data/app/QQ.apk");
        memset(apk_list, 0x00, sizeof(apk_list));
            
        if(lan_flag==0){
            List("/system/thirdapps", apk_list);
            if(strlen(apk_list[0]) == 0) return 0;
            for(i=0; i<20 && strlen(apk_list[i])>0; i++)
            {
				memset(apk_name, 0, sizeof(apk_name));
				strcpy(apk_name, apk_list[i]);
				str_replace(apk_name, ".apk_", ".apk");
            
                sprintf(buff, "cat /system/thirdapps/%s > /data/app/%s", apk_list[i], apk_name/*apk_list[i]*/);
                system(buff);
                sprintf(buff1, "chmod 666 /data/app/%s", apk_name);
                system(buff1);
            }
        }

        //LOGD("switch_lan: 11111111111 not exists!!! \n");
        return 0;
}

int str_replace(char* str,char* str_src, char* str_des){
    char *ptr=NULL;
    char buff[256];
    char buff2[256];
    int i = 0;
   
    if(str != NULL){
        strcpy(buff2, str);
    }else{
        printf("str_replace err!/n");
        return -1;
    }
    memset(buff, 0x00, sizeof(buff));
    while((ptr = strstr( buff2, str_src)) !=0){
        if(ptr-buff2 != 0) memcpy(&buff[i], buff2, ptr - buff2);
        memcpy(&buff[i + ptr - buff2], str_des, strlen(str_des));
        i += ptr - buff2 + strlen(str_des);
        strcpy(buff2, ptr + strlen(str_src));
    }
    strcat(buff,buff2);
    strcpy(str,buff);
    return 0;
}
