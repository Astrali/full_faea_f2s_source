
PRODUCT_PACKAGES := \
    GooglePartnerSetup.apk \
    GoogleServicesFramework.apk \
    NetworkLocation.apk \
    GooglePlay.apk \
    GooglePlayService.apk \
    GoogleLoginService.apk \
    GoogleMaps.apk \
    Security.apk \
    XunFeiInput.apk \
    Gmail.apk \
    CompassCY \

#    SouGouInput.apk \
#    FMRadio
#    MyTube \
#    VideoPlayer
#    GoogleVoice.apk \


$(call inherit-product, $(SRC_TARGET_DIR)/product/common.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/telephony.mk)

# Overrides
PRODUCT_BRAND  := alps
PRODUCT_NAME   := $(TARGET_PRODUCT)
PRODUCT_DEVICE := $(TARGET_PRODUCT)

# This is for custom project language configuration.
PRODUCT_LOCALES := $(MTK_PRODUCT_LOCALES)

# Third Apk
# APP_COPY_FLAG FOR UNINSTALL
PRODUCT_COPY_FILES += third_apk/app_copy_flag.text:system/thirdapps/app_copy_flag.text

# APP
##################
# Baidu apk
ifeq ($(strip $(CUSTOM_THIRD_APP_BAIDUMAP)), yes)
	PRODUCT_COPY_FILES += third_apk/BaiduMapV5.0.0.apk_:system/thirdapps/BaiduMap.apk_
endif
ifeq ($(strip $(CUSTOM_THIRD_APP_BAIDUMUSIC)), yes)
	PRODUCT_COPY_FILES += third_apk/BaiduMusicV3.4.1.apk_:system/thirdapps/BaiduMusic.apk_
endif	

##################
# Tencent apk
ifeq ($(strip $(CUSTOM_THIRD_APP_QQ)), yes)
	PRODUCT_COPY_FILES += third_apk/MobileQQ2013V4.1.1.apk_:system/thirdapps/MobileQQ.apk_
endif
ifeq ($(strip $(CUSTOM_THIRD_APP_QQSYNC)), yes)
	PRODUCT_COPY_FILES += third_apk/QQSyncV4.2.apk_:system/thirdapps/QQSync.apk_
endif	
ifeq ($(strip $(CUSTOM_THIRD_APP_QZONE)), yes)
	PRODUCT_COPY_FILES += third_apk/QzoneV3.6.2.687.apk_:system/thirdapps/Qzone.apk_
endif	
ifeq ($(strip $(CUSTOM_THIRD_APP_TENCENTWEIBO)), yes)
	PRODUCT_COPY_FILES += third_apk/TencentWeiboV3.9.apk_:system/thirdapps/TencentWeibo.apk_
endif	
ifeq ($(strip $(CUSTOM_THIRD_APP_WEIXIN)), yes)
	PRODUCT_COPY_FILES += third_apk/WeiXinV4.5.1.apk_:system/thirdapps/WeiXin.apk_
endif	

##################
# Netease apk
ifeq ($(strip $(CUSTOM_THIRD_APP_NETEASENEWS)), yes)
	PRODUCT_COPY_FILES += third_apk/NetEaseNewsV3.4.0.apk_:system/thirdapps/NetEaseNews.apk_
endif	
ifeq ($(strip $(CUSTOM_THIRD_APP_YOUDAONOTE)), yes)
	PRODUCT_COPY_FILES += third_apk/YoudaoNoteV3.2.2.apk_:system/thirdapps/YoudaoNote.apk_
endif	
ifeq ($(strip $(CUSTOM_THIRD_APP_NETEASEALBUM)), yes)
	PRODUCT_COPY_FILES += third_apk/NetEaseCloudAlbumV4.0.6.apk_:system/thirdapps/NetEaseCloudAlbum.apk_
endif	

##################
# Sina apk
ifeq ($(strip $(CUSTOM_THIRD_APP_SINAWEIBO)), yes)
	#PRODUCT_COPY_FILES += third_apk/SinaWeiboV3.5.2.apk_:system/thirdapps/SinaWeibo.apk_
	PRODUCT_COPY_FILES += third_apk/SinaWeiboV3.3.0_295_f2.apk_:system/thirdapps/SinaWeibo.apk_
endif	
ifeq ($(strip $(CUSTOM_THIRD_APP_TIANQITONG)), yes)
	PRODUCT_COPY_FILES += third_apk/TianQiTongV2.4.apk_:system/thirdapps/TianQiTong.apk_
endif	

PRODUCT_COPY_FILES += gmaps/com.google.android.maps.jar:system/framework/com.google.android.maps.jar
PRODUCT_COPY_FILES += gmaps/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml
