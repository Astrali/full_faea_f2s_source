/*
 * gConfig.h
 *
 *  Created on: Jan 25, 2013
 *      Author: Gao @ QST corp
 */

#ifndef GCONFIG_H_
#define GCONFIG_H_


#define G_USE_IOCTL
#define M_USE_IOCTL
#define G_USE_POLL
#define M_USE_POLL

#define VERSION_INFO "QST sensor daemon - version is: v1.0"

//Notice Please Carefully Enable This !!! Only for self debug (With out completion in driver) !
//#define SELF_DEBUG_MODE

#define QMC5983_IOCTL_BASE 'm'
/* The following define the IOCTL command values via the ioctl macros */
#define QMC5983_SET_RANGE		_IOW(QMC5983_IOCTL_BASE, 1, int)
#define QMC5983_SET_MODE		_IOW(QMC5983_IOCTL_BASE, 2, int)
#define QMC5983_SET_BANDWIDTH	_IOW(QMC5983_IOCTL_BASE, 3, int)
#define QMC5983_READ_MAGN_XYZ	_IOR(QMC5983_IOCTL_BASE, 4, char *)
#define QMC5983_SET_REGISTER_A	_IOW(QMC5983_IOCTL_BASE, 5, char *)
#define QMC5983_SELF_TEST	   _IOWR(QMC5983_IOCTL_BASE, 6, char *)

#define QMC5983_WRITE_ACC_XYZ	_IOW(QMC5983_IOCTL_BASE, 11, short[3])
#define QMC5983_WRITE_QMC_XYZ	_IOW(QMC5983_IOCTL_BASE, 12, short[4])
#define QMC5983_WRITE_ORI_XYZ	_IOW(QMC5983_IOCTL_BASE, 13, short[4])

#define QMC5983_READ_ACC_XYZ	_IOR(QMC5983_IOCTL_BASE, 21, short[3])
#define QMC5983_READ_QMC_XYZ	_IOR(QMC5983_IOCTL_BASE, 22, short[4])
#define QMC5983_READ_ORI_XYZ	_IOR(QMC5983_IOCTL_BASE, 23, short[4])
#define QMC5983_READ_ALL		_IOR(QMC5983_IOCTL_BASE, 25, short[10])

//M sensor Layout
#define QMC_X_CONVERT	1
#define QMC_Y_CONVERT	1
#define QMC_Z_CONVERT	1

//
#define ACCEL_DATA	""
#define QMC_INPUT	""
#define QMC_IOCTL	""




/*****************************************************************************/

#define SENSOR_DBG

#ifdef SENSOR_DBG
#define  D(...)  LOGD(__VA_ARGS__)
#else
#define  D(...)  ((void)0)
#endif

#define  E(...)  LOGE(__VA_ARGS__)

/*****************************************************************************/

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#define ID_A  (0)
#define ID_M  (1)
#define ID_O  (2)
#define ID_L  (3)
#define ID_P  (4)
#define ID_GY (5)
#define ID_PR (6)
#define ID_T  (7)

/*****************************************************************************/

/*
 * The SENSORS Module
 */

#define NSEC_PER_SEC    1000000000L
static inline int64_t timespec_to_ns(const struct timespec *ts)
{
    return ((int64_t) ts->tv_sec * NSEC_PER_SEC) + ts->tv_nsec;
}

/*****************************************************************************/
#define GRAVITY 9.80665f

#define USE_ABS
#ifdef USE_REL
#define EVENT_TYPE_ACCEL_X          REL_X
#define EVENT_TYPE_ACCEL_Y          REL_Y
#define EVENT_TYPE_ACCEL_Z          REL_Z
#endif

#ifdef USE_ABS
#define EVENT_TYPE_ACCEL_X          REL_X
#define EVENT_TYPE_ACCEL_Y          REL_Y
#define EVENT_TYPE_ACCEL_Z          REL_Z
#endif

#define EVENT_TYPE_YAW              REL_RX
#define EVENT_TYPE_PITCH            REL_RY
#define EVENT_TYPE_ROLL             REL_RZ
#define EVENT_TYPE_ORIENT_STATUS    REL_WHEEL

#define EVENT_TYPE_MAGV_X           REL_DIAL
#define EVENT_TYPE_MAGV_Y           REL_HWHEEL
#define EVENT_TYPE_MAGV_Z           REL_MISC

#define EVENT_TYPE_M_O_X			REL_X
#define EVENT_TYPE_M_O_Y			REL_Y
#define EVENT_TYPE_M_O_Z			REL_Z

// 64 LSB = 1G for KR3DM
#define LSB                         (64.0f)
#define NUMOFACCDATA                (8.0f)

/*****************************************************************************/


#endif /* GCONFIG_H_ */
