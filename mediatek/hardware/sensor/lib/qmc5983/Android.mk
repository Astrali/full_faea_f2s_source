

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_LDFLAGS := $(CURDIR)/mediatek/hardware/sensor/lib/qmc5983/ical.a

LOCAL_SRC_FILES:=   src/main.cpp


LOCAL_C_INCLUDES += \
	$(JNI_H_INCLUDE) \
	$(LOCAL_PATH)/android/graphics \
	$(call include-path-for, bluedroid) \
	$(call include-path-for, libhardware)/hardware \
	$(call include-path-for, libhardware_legacy)/hardware_legacy \
        $(LOCAL_PATH)/include

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	libnetutils

LOCAL_CFLAGS := -W -Wall

#LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= qmc5983

include $(BUILD_EXECUTABLE)

