
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <poll.h>
#include <dlfcn.h> 
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/ipc.h>
#include <sys/ioctl.h>
#include <string.h>
#include <syslog.h>
#include <signal.h>
#include <stdarg.h>
#include <time.h>

#include <linux/input.h>

#include <cutils/log.h>

#include <hardware/sensors.h>
#include <gConfig.h>

#include <linux/sensors_io.h>

#define DEBUG			1



#define QMCD_LOGBUFSZ	256			/*log buffer size*/
#define QMCD_LOGFILE		"/data/misc/sensors/qmcd.log"	/*log filename*/


#define SENSORS_ACCELERATION		(1 << ID_A)
#define SENSORS_MAGNETIC_FIELD		(1 << ID_M)
#define SENSORS_ORIENTATION		(1 << ID_O)

#define DAEMON_POLLING_INTV		20			// ms

/*------------------------------------for log file-----------------*/

static int qmcd_log(const char *fmt, ...){
	va_list args;
	char buf[QMCD_LOGBUFSZ];
	int save_errno;
	struct tm *tm;
	time_t current_time;
	int fd_log;
	save_errno = errno;
	va_start(args, fmt);
	(void)time (&current_time);
	/* Get current time */
	tm = localtime (&current_time);
	sprintf(buf, "%02d/%02d %02d:%02d:%02d ",tm->tm_mon + 1,tm->tm_mday,tm->tm_hour,tm->tm_min,tm->tm_sec);
	vsprintf(buf + strlen(buf), fmt, args);	va_end(args);
	fd_log = open(QMCD_LOGFILE, O_WRONLY | O_CREAT | O_APPEND, 0664);
	write(fd_log, buf, strlen(buf));
	close(fd_log);
	errno = save_errno;
	return 0;
}
static int qmcd_init(void){
	pid_t pid;
	/* parent exits , child continues */
	if ((pid = fork()) < 0) {
		return -1;
	}
	else if (pid != 0) {
		exit(0);
	}
	setsid();
	/* become session leader */
	#if (!DEBUG)
	close(0);	/* close STDIN */
	close(1);	/* close STDOUT */
	close(2);	/* close STDERR */
	#endif
	umask(0); /* clear file mode creation mask */
	return 0;
}
static void qmcd_abort(void){
	qmcd_log("qmcd abort\n");
}
static void qmcd_sigterm(int signo){
	qmcd_abort();
	qmcd_log("signal: %d\n", signo);	/* catched signal */
	qmcd_log("qmcd stopped\n");
	exit(0);
}



/*--------------------------------------------------global values-----------------------------------------------*/

static int64_t timevalToNano(timeval const& t) {
	return t.tv_sec * 1000000000LL + t.tv_usec * 1000;
}

static int control_read_sensors_state(int fd){
	if (fd < 0) {
		return 0;
	}
	short flags;
	int sensors = 0;
	// read the actual value of all sensors
	if(ioctl(fd, ECOMPASS_IOC_GET_MFLAG, &flags)<0){
		qmcd_log("ECOMPASS_IOC_GET_MFLAG failed!\n");
	}
	else{
		if(flags){
			sensors |= SENSORS_MAGNETIC_FIELD;
		}
		else{
			sensors &= ~SENSORS_MAGNETIC_FIELD;
		}
	}
	if(ioctl(fd, ECOMPASS_IOC_GET_OFLAG, &flags)<0){
		qmcd_log("ECOMPASS_IOC_GET_OFLAG failed!\n");
	}
	else{
		if(flags)
		{			sensors |= SENSORS_ORIENTATION;
		}
		else{
			sensors &= ~SENSORS_ORIENTATION;
		}
	}
	return sensors;
}


extern void mcal();
extern int push2mcal(sensors_event_t* data, int count);
extern bool getReady();
extern int get(sensors_event_t* data);
extern int ori_init();
extern void dori();
extern int process(sensors_event_t* mMagneticEvent);

/*-------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------MAIN------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------*/
sensors_event_t mAccelEvent;
sensors_event_t mMagneticEvent;
sensors_event_t mOrientationEvent;

static int64_t get_time_in_nanosec(void){
	struct timeval tv;
	int64_t t;
	gettimeofday(&tv, NULL);
	t = tv.tv_sec * 1000000000LL + tv.tv_usec * 1000LL;
	return t;
}
static int mediatek_read_gsensor(int fd, sensors_event_t *AccelEvent){
	char buf[64];
	int tmp[3], i;
	qmcd_log("mediatek_read_gsensor IN\n");
	if (ioctl(fd, GSENSOR_IOCTL_READ_SENSORDATA, (void*)buf) < 0) {
		return -1;
	}

	#if 0
	AccelEvent.version = sizeof(sensors_event_t);
    AccelEvent.sensor = ID_A;
    AccelEvent.type = SENSOR_TYPE_ACCELEROMETER;
    AccelEvent.acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;
	#endif
	
	sscanf(buf, "%x %x %x", &tmp[0], &tmp[1], &tmp[2]);
	for (i = 0; i < 3; i++) {
		AccelEvent->data[i] = tmp[i] / 1000.0;
	}
	
//add for changhua debug start.............	
#if 0
AccelEvent->data[0]=0;
AccelEvent->data[1]=0;
AccelEvent->data[2]=9.8;
#endif
//add for changhua debug end.............	

	AccelEvent->timestamp = get_time_in_nanosec();
	AccelEvent->acceleration.status = 3;
	
	qmcd_log("mediatek_read_gsensor OUT[%.3f][%.3f][%.3f]\n", AccelEvent->data[0], AccelEvent->data[1], AccelEvent->data[2]);
	return 0;
	}


static int mediatek_read_msensor(int fd, sensors_event_t *MagneticEvent){
	char buf[64];
	int tmp[3], i;
	qmcd_log("mediatek_read_msensor IN\n");
	
	if (ioctl(fd, MSENSOR_IOCTL_READ_SENSORDATA, (void*)buf) < 0) {
		return -1;
	}
	#if 0
	MagneticEvent.version = sizeof(sensors_event_t);
    MagneticEvent.sensor = ID_A;
    MagneticEvent.type = SENSOR_TYPE_MAGNETIC_FIELD;
    MagneticEvent.magnetic.status = SENSOR_STATUS_ACCURACY_HIGH;
	#endif
	
	sscanf(buf, "%x %x %x", &tmp[0], &tmp[1], &tmp[2]);
	for (i = 0; i < 3; i++) {
		MagneticEvent->data[i] = tmp[i];
	}
	
	MagneticEvent->timestamp = get_time_in_nanosec();
	MagneticEvent->magnetic.status = 3;
	
	qmcd_log("mediatek_read_msensor OUT[%.3f][%.3f][%.3f]\n", MagneticEvent->data[0], MagneticEvent->data[1], MagneticEvent->data[2]);
	return 0;
	}


int main(){
	
	 qmcd_log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
	 int stat_curr = 0, stat_prev = 0;	int sensor_stat = 0;

	unsigned int delay_timer = DAEMON_POLLING_INTV;
	int i_ret;
	int value = 0;
	
	static int val[12] ={0,0,0,0,0,0,0,0,0,0,0,0};//for write back to driver
	short acc[4];
	short mag[4];
	short ori[4];

	if (qmcd_init() == -1) {
		qmcd_log("can't fork self\n");
		exit(0);
	}
	
	qmcd_log("qmcd started. \n");
	signal(SIGTERM,qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGHUP, qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGINT, qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGQUIT, qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGTRAP, qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGABRT, qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGKILL, qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGPIPE, qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGSTOP, qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGTSTP, qmcd_sigterm); /* arrange to catch the signal */
	signal(SIGURG, qmcd_sigterm); /* arrange to catch the signal */

	
	int g_fd = open("/dev/gsensor", O_RDWR);
	if(g_fd == -1){
		qmcd_log("Cannot open gsensor!!!\n");
		return -1;
	}
	int qmc_fd = open("/dev/msensor", O_RDWR);
	if(qmc_fd == -1){
		close(g_fd);
		qmcd_log("Cannot open msensor!!!\n");
		return -1;
	}
	
    mcal();

    mAccelEvent.version = sizeof(sensors_event_t);
    mAccelEvent.sensor = ID_A;
    mAccelEvent.type = SENSOR_TYPE_ACCELEROMETER;
    mAccelEvent.acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;
	memset(mAccelEvent.data, 0, sizeof(mAccelEvent.data));

	mMagneticEvent.version = sizeof(sensors_event_t);
	mMagneticEvent.sensor = ID_M;
	mMagneticEvent.type = SENSOR_TYPE_MAGNETIC_FIELD;
	mMagneticEvent.magnetic.status = SENSOR_STATUS_ACCURACY_HIGH;
	memset(mMagneticEvent.data, 0, sizeof(mMagneticEvent.data));

	mOrientationEvent.version = sizeof(sensors_event_t);
	mOrientationEvent.sensor = ID_O;
	mOrientationEvent.type = SENSOR_TYPE_ORIENTATION;
	mOrientationEvent.magnetic.status = SENSOR_STATUS_ACCURACY_HIGH;
	memset(mOrientationEvent.data, 0, sizeof(mOrientationEvent.data));

    while(true){
		struct timeval tv_begin;
		struct timeval tv_now;
		unsigned int usec_elapse = 0;	/* usec */ 
		gettimeofday (&tv_begin, NULL);
		
		stat_curr = control_read_sensors_state(qmc_fd);
		if(stat_curr == 0){
			// wait for m or o sensor open again
			qmcd_log("waitting for enable m or o sensor\n");
			if(ioctl(qmc_fd, ECS_IOCTL_GET_OPEN_STATUS, &sensor_stat)<0){
				qmcd_log("ECOMPASS_IOC_GET_OPEN_STATUS failed\n");
			}
		}

		if(ioctl(qmc_fd, ECS_IOCTL_GET_DELAY, &value)<0){
			qmcd_log("ECS_IOCTL_GET_DELAY failed\n");
			delay_timer = DAEMON_POLLING_INTV;
		}
		else if(value < 0 || value > 20){
			delay_timer = DAEMON_POLLING_INTV;
		}
		else{
			delay_timer = value;
		}
		
		if (stat_curr) {

			mediatek_read_gsensor(g_fd,&mAccelEvent);

			mAccelEvent.data[0] = ((float) mAccelEvent.data[0]) / 20 * GRAVITY;   //value * CONVERT_A_X;
			mAccelEvent.data[1] = ((float) mAccelEvent.data[1]) / 20 * GRAVITY;   //value * CONVERT_A_Y;
			mAccelEvent.data[2] = ((float) mAccelEvent.data[2]) / 20 * GRAVITY;   //value * CONVERT_A_Z;
			//printf("Accel - {%f, %f, %f}\n", mAccelEvent.data[0], mAccelEvent.data[1], mAccelEvent.data[2]);
						
						acc[0] = (short)(mAccelEvent.data[0]*1000);
						acc[1] = (short)(mAccelEvent.data[1]*1000);
						acc[2] = (short)(mAccelEvent.data[2]*1000);
						acc[3] = mAccelEvent.acceleration.status;
						qmcd_log("Accel after /20*GRAVITY*1000- {%d, %d, %d}\n", acc[0], acc[1], acc[2]);
						//i_ret = ioctl(qmc_fd, QMC5983_WRITE_ACC_XYZ, acc);
			//printf("i_acc_ret = %d, (errno = %d, str = %s)", i_ret, errno, strerror(errno));
						push2mcal(&mAccelEvent, 1);


			mediatek_read_msensor(qmc_fd,&mMagneticEvent);
			#if 0
		    mMagneticEvent.magnetic.x = value * QMC_X_CONVERT;//modify here,if we change the layout in the driver instead of daemon
			mMagneticEvent.magnetic.y = value * QMC_Y_CONVERT;
			mMagneticEvent.magnetic.z = value * QMC_Z_CONVERT;
			#endif
						process(&mMagneticEvent);
						
						
						mag[0] = (short)mMagneticEvent.data[0];
						mag[1] = (short)mMagneticEvent.data[1];
						mag[2] = (short)mMagneticEvent.data[2];
						mag[3] = (short)mMagneticEvent.magnetic.status;
//						printf("Qmc - {%f, %f, %f}\n", mMagneticEvent.data[0], mMagneticEvent.data[1], mMagneticEvent.data[2]);
						qmcd_log("Qmc - after process(){%d, %d, %d} - %d\n", mag[0], mag[1], mag[2], mag[3]);
						//i_ret = ioctl(qmc_fd, QMC5983_WRITE_QMC_XYZ, mag);
//						printf("i_qmc_ret = %d, (errno = %d, str = %s)", i_ret, errno, strerror(errno));
						push2mcal(&mMagneticEvent, 1);
					}

    	//process
		get(&mOrientationEvent);
		
		ori[0] = (short)mOrientationEvent.data[0];
		ori[1] = (short)mOrientationEvent.data[1];
		ori[2] = (short)mOrientationEvent.data[2];
		ori[3] = (short)mOrientationEvent.orientation.status;
		//i_ret = ioctl(qmc_fd, QMC5983_WRITE_ORI_XYZ, ori);

		qmcd_log("Qmc - Orientation{%d, %d, %d} - %d\n", ori[0], ori[1], ori[2], ori[3]);
		
		val[0]=acc[0];
		val[1]=acc[1];
		val[2]=acc[2];
		val[3]=acc[3];
		val[4]=mag[0];
		val[5]=mag[1];
		val[6]=mag[2];
		val[7]=mag[3];
		val[8]=ori[0];
		val[9]=ori[1];
		val[10]=ori[2];
		val[11]=ori[3];

		if(ioctl(qmc_fd,ECS_IOCTL_SET_YPR,val)<0){
			qmcd_log("ECS_IOCTL_SET_YPR failed!\n");
		}
	gettimeofday (&tv_now, NULL);
	usec_elapse = (tv_now.tv_sec - tv_begin.tv_sec) * 1000000 + tv_now.tv_usec - tv_begin.tv_usec;
	if(usec_elapse >= delay_timer * 1000){
		//	usleep(1000);		
	}
	else{
		usleep(delay_timer * 1000 - usec_elapse);
	}
	
  }
    close(qmc_fd);
    return 0;
}


