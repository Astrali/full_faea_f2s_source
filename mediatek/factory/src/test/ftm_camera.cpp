/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
/*******************************************************************************
 * $Log: AcdkMain.cpp $
 * Brif       : factory mode
 *******************************************************************************/
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/mount.h>
#include <sys/statfs.h>
#include "strobe_drv.h"







extern "C" {
#include "common.h"
#include "miniui.h"
#include "ftm.h"
}

//#ifdef FEATURE_FTM_MAIN_CAMERA

#include "cct_feature.h"
#include "AcdkCommon.h"
#include "AcdkIF.h"
//#include "msdk_flash_light_exp.h"
//#include "camera_custom_flashlight.h"

#define TAG                  "[CAMERA] "

#define DEBUG_FTM_CAMERA
#ifdef DEBUG_FTM_CAMERA
#define FTM_CAMERA_DBG(fmt, arg...) LOGD(fmt, ##arg)
#define FTM_CAMERA_ERR(fmt, arg...)  LOGE("Err: %5d:, "fmt, __LINE__, ##arg)
#else
#define FTM_CAMERA_DBG(a,...)
#define FTM_CAMERA_ERR(a,...)
#endif

/*******************************************************************************
*
********************************************************************************/
enum
{
    ITEM_PASS,
    ITEM_FAIL,
    ITEM_RETURN,
    ITEM_CAMERA_TEST,
//    ITEM_CAPTURE,
//    ITEM_STROBE,
};

//
/*******************************************************************************
*
********************************************************************************/
static item_t camera_items[] = {
    {ITEM_CAMERA_TEST, uistr_camera_preview,   0},
    {ITEM_PASS, uistr_pass, 0},
    {ITEM_FAIL, uistr_fail, 0},
    {-1, NULL,   0},
};

/*******************************************************************************
*
********************************************************************************/
struct camera
{
    char  info[1024];
    bool  exit_thd;

    text_t    title;
    text_t    text;
    text_t    left_btn;
    text_t    center_btn;
    text_t    right_btn;

    //text_t    cap_left_btn;
    //text_t    cap_center_btn;
    //text_t    cap_right_btn;

    //pthread_t update_button_thd;
    //pthread_t update_cap_thd;
    struct ftm_module *mod;
    struct textview tv;
    struct itemview *iv;
//    struct imageview imv;
//    struct paintview pv;
};

#define mod_to_camera(p)     (struct camera*)((char*)(p) + sizeof(struct ftm_module))

#define FREEIF(p)   do { if(p) free(p); (p) = NULL; } while(0)

pthread_t camera_flash_thread_handle = 0;
//FLASHLIGHT_TYPE_ENUM eFlashSupport = FLASHLIGHT_NONE;
UINT8 srcDev = 0;
static int g_previewStop = FALSE;


/*******************************************************************************
*
********************************************************************************/
static bool bSendDataToACDK(ACDK_CCT_FEATURE_ENUM	FeatureID,
                                     UINT8*  pInAddr,
                                     UINT32  nInBufferSize,
                                     UINT8*  pOutAddr,
                                     UINT32  nOutBufferSize,
                                     UINT32* pRealOutByeCnt)
{
    ACDK_FEATURE_INFO_STRUCT rAcdkFeatureInfo;

    rAcdkFeatureInfo.puParaIn     = pInAddr;
    rAcdkFeatureInfo.u4ParaInLen  = nInBufferSize;
    rAcdkFeatureInfo.puParaOut    = pOutAddr;
    rAcdkFeatureInfo.u4ParaOutLen = nOutBufferSize;
    rAcdkFeatureInfo.pu4RealParaOutLen = pRealOutByeCnt;

    return (MDK_IOControl(FeatureID, &rAcdkFeatureInfo));
}

/*******************************************************************************
*
********************************************************************************/
enum
{
    CAMERA_STATE_NONE,
    CAMERA_STATE_IDLE,
    CAMERA_STATE_PREVIEW,
};

#define MEDIA_PATH "/data/"
static unsigned int g_u4ImgCnt = 0;
static bool bCapDone = FALSE;
static int camera_state = CAMERA_STATE_NONE;
static char szFileName[256];

/////////////////////////////////////////////////////////////////////////
//
//   camera_preview_test () -
//!
//!  brief for camera preview test
//
/////////////////////////////////////////////////////////////////////////
static int camera_preview_test()
{
    FTM_CAMERA_DBG("%s ,Enter\n", __FUNCTION__);

    ACDK_CCT_CAMERA_PREVIEW_STRUCT rCCTPreviewConfig;
    rCCTPreviewConfig.fpPrvCB = NULL;
    rCCTPreviewConfig.u2PreviewWidth = 320;
    rCCTPreviewConfig.u2PreviewHeight = 240;
    rCCTPreviewConfig.eOperaMode    = ACDK_OPT_FACTORY_MODE;
    unsigned int u4RetLen = 0;

    bool bRet = bSendDataToACDK(ACDK_CCT_OP_PREVIEW_LCD_START,
                                (unsigned char *)&rCCTPreviewConfig,
                                sizeof(ACDK_CCT_CAMERA_PREVIEW_STRUCT),
                                NULL,
                                0,
                                &u4RetLen);

    if (!bRet)
    {
        FTM_CAMERA_ERR("ACDK_CCT_OP_PREVIEW_LCD_START Fail\n");
        return 1;
    }

    camera_state = CAMERA_STATE_PREVIEW;

    FTM_CAMERA_DBG("%s ,Exit\n", __FUNCTION__);

    return 0;
}

/////////////////////////////////////////////////////////////////////////
//
//   camera_flash_test_thread () -
//!
//!  brief for camera flash test thread
//
/////////////////////////////////////////////////////////////////////////

static void *camera_flash_test_thread(void *priv)
{
	/*
    unsigned int u4RetLen = 0;
    ACDK_FLASH_CONTROL flashCtrl;

    memset (&flashCtrl, 0, sizeof(ACDK_FLASH_CONTROL));
    flashCtrl.level = 32;       //max is 32, min is 0
    flashCtrl.duration = 50000;



    FTM_CAMERA_DBG("ACDK_CCT_OP_FLASH_CONTROL ON \n");
    */


#if defined(MTK_FACTORY_AUTO_FLASH_SUPPORT)
	struct camera *cam = (struct camera *)priv;
	while (!cam->exit_thd)
	{
		FTM_CAMERA_DBG("[camera_flash_test_thread auto flash] \n");
		usleep(500000);
	}
#else


    struct camera *cam = (struct camera *)priv;

		StrobeDrv*  pStrobe;
		pStrobe = StrobeDrv::createInstance();
		pStrobe->init(1);

		//pStrobe->setDuty(0);

    while (!cam->exit_thd) {
        //bSendDataToACDK(ACDK_CCT_OP_FLASH_CONTROL, (UINT8*)&flashCtrl, (UINT32)sizeof(int),NULL, 0, &u4RetLen);
        FTM_CAMERA_DBG("[camera_flash_test_thread] \n");

        pStrobe->setOnOff(1);
        usleep(100000);//300ms
        pStrobe->setOnOff(0);
        usleep(800000);
    }
    pStrobe->uninit();

#endif

    return NULL;
}

/////////////////////////////////////////////////////////////////////////
//
//   mrSaveJPEGImg () -
//!
//!  brief for geneirc function to save image file
//
/////////////////////////////////////////////////////////////////////////
bool bSaveJPEGImg(char *a_pBuf,  unsigned int a_u4Size)
{
    sprintf(szFileName, "%s//%04d.jpg" , MEDIA_PATH, g_u4ImgCnt);

    FILE *pFp = fopen(szFileName, "wb");

    if (NULL == pFp )
    {
        FTM_CAMERA_ERR("Can't open file to save Image\n");
        return FALSE;
    }


    int i4WriteCnt = fwrite(a_pBuf, 1, a_u4Size , pFp);

    FTM_CAMERA_DBG("Save image file name:%s\n", szFileName);

    fclose(pFp);
    sync();
    return TRUE;
}


/////////////////////////////////////////////////////////////////////////
//
//   vCapCb () -
//!
//!  brief for capture callback
//
/////////////////////////////////////////////////////////////////////////
static void vCapCb(void *a_pParam)
{
    FTM_CAMERA_DBG("[vCapCb] - E\n");

    /*
    ImageBufInfo *pImgBufInfo = (ImageBufInfo *)a_pParam;

    if (pImgBufInfo->eImgType == JPEG_TYPE)
    {
        FTM_CAMERA_DBG("[vCapCb] Size:%d\n", pImgBufInfo->rCapBufInfo.u4ImgSize);
        FTM_CAMERA_DBG("[vCapCb] Width:%d\n", pImgBufInfo->rCapBufInfo.u2ImgXRes);
        FTM_CAMERA_DBG("[vCapCb] Height:%d\n", pImgBufInfo->rCapBufInfo.u2ImgYRes);

        bSaveJPEGImg((char*)pImgBufInfo->rCapBufInfo.pucImgBuf,
                            pImgBufInfo->rCapBufInfo.u4ImgSize);
    }
    else
    {
        FTM_CAMERA_DBG("[vCapCb] UnKnow Format \n");
        bCapDone = FALSE;
        return;
    }
    */

    bCapDone = TRUE;
    g_u4ImgCnt ++;
}

/////////////////////////////////////////////////////////////////////////
//
//   camera_capture_test () -
//!
//!  brief for camera capture test
//
/////////////////////////////////////////////////////////////////////////
static int camera_capture_test()
{

    FTM_CAMERA_DBG("Camera_Capture_Test-E\n");

    unsigned int u4RetLen = 0;
    bool bRet;

#if 1   //JPEG capture

#if 0
    ACDK_CCT_SENSOR_RESOLUTION_STRUCT  SensorResolution;

    memset(&SensorResolution,0,sizeof(ACDK_CCT_SENSOR_RESOLUTION_STRUCT));

    FTM_CAMERA_DBG("[camera_capture_test] Get Sensor Resolution Info\n");

    bRet = bSendDataToACDK(ACDK_CCT_V2_OP_GET_SENSOR_RESOLUTION,
                           NULL,
                           0,
                           (UINT8 *)&SensorResolution,
                           sizeof(ACDK_CCT_SENSOR_RESOLUTION_STRUCT),
                           &u4RetLen);


    if (!bRet)
    {
        FTM_CAMERA_DBG("[camera_capture_test]Get Sensor Resolution Fail \n");
        return 1;
    }

    FTM_CAMERA_DBG("SensorFullWidth(%u),SensorFullHeight(%u)\n",SensorResolution.SensorFullWidth,SensorResolution.SensorFullHeight);
    FTM_CAMERA_DBG("JPEGEncWidth(%u),JPEGEncHeight(%u)\n",rCCTStillCapConfig.u2JPEGEncWidth,rCCTStillCapConfig.u2JPEGEncHeight);
#endif

    ACDK_CCT_STILL_CAPTURE_STRUCT rCCTStillCapConfig;

    rCCTStillCapConfig.eCameraMode   = CAPTURE_MODE;
    rCCTStillCapConfig.eOperaMode    = ACDK_OPT_FACTORY_MODE;
    rCCTStillCapConfig.eOutputFormat = OUTPUT_JPEG;

    rCCTStillCapConfig.u2JPEGEncWidth =  0;     //will be alignd to 16x
    rCCTStillCapConfig.u2JPEGEncHeight =  0;    //will be alignd to 16x
    rCCTStillCapConfig.fpCapCB = vCapCb;
    rCCTStillCapConfig.i4IsSave = 0;    // 0-no save, 1-save

#else   //RAW capture

    ACDK_CCT_STILL_CAPTURE_STRUCT rCCTStillCapConfig;

    rCCTStillCapConfig.eCameraMode   = CAPTURE_MODE;
    rCCTStillCapConfig.eOperaMode    = ACDK_OPT_FACTORY_MODE;
    rCCTStillCapConfig.eOutputFormat = OUTPUT_PURE_RAW10;

    rCCTStillCapConfig.u2JPEGEncWidth  =  0;    //will be alignd to 16x
    rCCTStillCapConfig.u2JPEGEncHeight =  0;    //will be alignd to 16x
    rCCTStillCapConfig.fpCapCB = vCapCb;
    rCCTStillCapConfig.i4IsSave = 1;    // 0-no save, 1-save

#endif

    bCapDone = FALSE;
    bRet = bSendDataToACDK(ACDK_CCT_OP_SINGLE_SHOT_CAPTURE_EX,
                           (unsigned char *)&rCCTStillCapConfig,
                           sizeof(ACDK_CCT_STILL_CAPTURE_STRUCT),
                           NULL,
                           0,
                           &u4RetLen);

    //wait capture done

    if(bRet == TRUE)
    {
        FTM_CAMERA_DBG("[camera_capture_test] wait capture done\n");
        while(!bCapDone)
        {
            usleep(1000);
        }
    }
    else
    {
        FTM_CAMERA_DBG("[camera_capture_test] capture fail\n");
        return 1;
    }

    FTM_CAMERA_DBG("[camera_capture_test] X\n");
    return 0;
}


/////////////////////////////////////////////////////////////////////////
//
//   camera_preview_stop () -
//!
//!  brief for camera preview stop
//
/////////////////////////////////////////////////////////////////////////
static int camera_preview_stop(void)
{
    if(camera_state != CAMERA_STATE_PREVIEW)
    {
        FTM_CAMERA_ERR("[camera_preview_stop] camera_state != CAMERA_STATE_PREVIEW\n");
        return 0;
    }

    FTM_CAMERA_DBG("[camera_preview_stop] Stop Camera Preview\n");

    unsigned int u4RetLen = 0;
    bool bRet = bSendDataToACDK(ACDK_CCT_OP_PREVIEW_LCD_STOP, NULL, 0, NULL, 0, &u4RetLen);

    if (!bRet)
    {
        return 1;
    }

    camera_state = CAMERA_STATE_IDLE;
    g_previewStop = TRUE;

    FTM_CAMERA_DBG("[camera_preview_stop] X\n");
    return 0;
}


/////////////////////////////////////////////////////////////////////////
//
//   camera_reset_layer_buffer () -
//!
//!  brief for camera reset camera preview layer buffer
//
/////////////////////////////////////////////////////////////////////////
static int camera_reset_layer_buffer(void)
{
    unsigned int u4RetLen = 0;
    bool bRet = 0;
    ACDK_FEATURE_INFO_STRUCT rAcdkFeatureInfo;

    rAcdkFeatureInfo.puParaIn = NULL;
    rAcdkFeatureInfo.u4ParaInLen = 0;
    rAcdkFeatureInfo.puParaOut = NULL;
    rAcdkFeatureInfo.u4ParaOutLen = 0;
    rAcdkFeatureInfo.pu4RealParaOutLen = &u4RetLen;

    bRet = MDK_IOControl(ACDK_CCT_FEATURE_RESET_LAYER_BUFFER, &rAcdkFeatureInfo);
    if (!bRet)
    {
        return 1;
    }
    return 0;
}

/////////////////////////////////////////////////////////////////////////
//
//   vQuickViewCallback () -
//!
//!  brief for camera quick view callback
//
/////////////////////////////////////////////////////////////////////////

static volatile int quickViewDone = FALSE;
VOID vQuickViewCallback(VOID *a_pArg)
{
    FTM_CAMERA_DBG("Quick View the image callback \n");

    quickViewDone = TRUE;
}

/////////////////////////////////////////////////////////////////////////
//
//   camera_show_image () -
//!
//!  brief for camera show image on the screen
//
/////////////////////////////////////////////////////////////////////////
static int camera_show_image(void)
{
#if 0   //ACDK will show QV automatic
    if(camera_state != CAMERA_STATE_IDLE)
    {
        FTM_CAMERA_ERR("[camera_show_image] camera_state != CAMERA_STATE_IDLE\n");
        return 0;
    }

    FTM_CAMERA_DBG("QuickView the Image\n");

    MUINT32 u4RetLen = 0;
    ACDK_FEATURE_INFO_STRUCT rAcdkFeatureInfo;
    rAcdkFeatureInfo.puParaIn =  (MUINT8 *)szFileName;
    rAcdkFeatureInfo.u4ParaInLen = 256;
    rAcdkFeatureInfo.puParaOut = (MUINT8 *)vQuickViewCallback;
    rAcdkFeatureInfo.u4ParaOutLen = 0;
    rAcdkFeatureInfo.pu4RealParaOutLen =&u4RetLen;

    bool bRet = MDK_IOControl(ACDK_CCT_FEATURE_QUICK_VIEW_IMAGE, &rAcdkFeatureInfo);

    if (!bRet)
    {
        FTM_CAMERA_ERR("[camera_show_image] QV fail\n");
        return 1;
    }

    FTM_CAMERA_DBG("[camera_show_image]-X\n");
#endif
    quickViewDone = TRUE;
    return 0;
}



/*******************************************************************************
*
********************************************************************************/
static volatile int capture_done = FALSE;
static int camera_cap_key_handler(int key, void *priv)
{
    int err = 0;
    int handled = 0, exit = 0;
    struct camera *cam = (struct camera *)priv;
    struct textview *tv = &cam->tv;
    struct ftm_module *fm = cam->mod;

    FTM_CAMERA_DBG("%s: Enter\n", __FUNCTION__);

    switch (key)
    {
    case UI_KEY_CONFIRM:
        FTM_CAMERA_DBG("Key_Confirm Click - Capture \n");
        err = camera_capture_test();
        if(err == 0)
        {
            capture_done = TRUE;
        }
        else
        {
            FTM_CAMERA_DBG("Capture Fail\n");
        }
        exit = 1;
        break;
    case UI_KEY_BACK:
        FTM_CAMERA_DBG("Back Button Click - Back\n");
        exit = 1;
        break;
    default:
        handled = -1;
        break;
    }
    if (exit)
    {
        FTM_CAMERA_DBG( "%s: Exit thead\n", __FUNCTION__);
        cam->exit_thd = true;
        tv->exit(tv);
    }
    return handled;
}

/*******************************************************************************
*
********************************************************************************/
#if 0
static int camera_preview_key_handler(int key, void *priv)
{
    int handled = 0, exit = 0;
    struct camera *cam = (struct camera *)priv;
    struct textview *tv = &cam->tv;
    struct ftm_module *fm = cam->mod;

    switch (key) {
    case UI_KEY_CONFIRM:
        FTM_CAMERA_DBG("Key Confirm press \n");
        cam->mod->test_result = FTM_TEST_PASS;
        exit = 1;
        break;
    case UI_KEY_BACK:
        FTM_CAMERA_DBG("Key back press \n");
        cam->mod->test_result = FTM_TEST_FAIL;
        exit = 1;
        break;
    default:
        handled = -1;
        break;
    }
    if (exit) {
        FTM_CAMERA_DBG( "%s: Exit thead\n", __FUNCTION__);
        cam->exit_thd = true;
        tv->exit(tv);
    }
    return handled;
}
#endif

/*******************************************************************************
*
********************************************************************************/
static void *camera_update_capture_tv_thread(void *priv)
{
    struct camera *cam = (struct camera *)priv;
    struct textview *tv = &cam->tv;
    struct statfs stat;
    int count = 1, chkcnt = 5;
    int key;

    //tv = &cam ->tv;
    ui_init_textview(tv, camera_cap_key_handler, (void*)cam );
    tv->set_title(tv, &cam->title);
    tv->set_text(tv, &cam->text);
    tv->set_btn(tv, &cam->left_btn, &cam->center_btn, &cam->right_btn);
    tv->redraw(tv);

    FTM_CAMERA_DBG("%s: Start\n", __FUNCTION__);

    camera_preview_test();

    FTM_CAMERA_DBG("PREVIEW_Start\n");

    cam->exit_thd = false;

#ifdef CONSTANT_FLASHLIGHT
	FTM_CAMERA_DBG("camera_update_capture_tv_thread line=%d\n",__LINE__);

    if(srcDev==1) //only for main sensor with strobe led (srcDev==1 => main sensor)
    {
    	FTM_CAMERA_DBG("camera_update_capture_tv_thread line=%d\n",__LINE__);
        if (pthread_create(&camera_flash_thread_handle, NULL, camera_flash_test_thread, cam))
        {
            FTM_CAMERA_DBG("Create flash test thread fail \n");
            return (0);
        }
    }
#else
	FTM_CAMERA_DBG("camera_update_capture_tv_thread line=%d\n",__LINE__);
#endif


    while (1)
    {
        key = ui_wait_key();
        usleep(200000);
        chkcnt--;
        tv->m_khandler(key, tv->m_priv);
        if (cam ->exit_thd)
            break;

        if (chkcnt > 0)
            continue;

        chkcnt = 5;
    }
    FTM_CAMERA_DBG("%s: Exit\n", __FUNCTION__);

#if 0 // strobe will be independent item

    if (eFlashSupport != FLASHLIGHT_NONE)
    {
        pthread_join(camera_flash_thread_handle, NULL);
        camera_flash_thread_handle = 0;
    }
#endif

    //pthread_exit(NULL);
    return NULL;
}

/*******************************************************************************
*
********************************************************************************/
static int camera_cap_result_key_handler(int key, void *priv)
{
    int handled = 0, exit = 0;
    struct camera *cam = (struct camera *)priv;
    struct textview *tv = &cam->tv;
    struct ftm_module *fm = cam->mod;

    switch (key)
    {
    case UI_KEY_BACK:
        FTM_CAMERA_DBG("Back Button Click\n");
        exit = 1;
        break;
    default:
        handled = -1;
        break;
    }

    if (exit)
    {
        FTM_CAMERA_DBG("%s: Exit thead\n", __FUNCTION__);
        cam->exit_thd = true;
        tv->exit(tv);
    }
    return handled;
}


/*******************************************************************************
*
********************************************************************************/
static void *camera_update_showImg_tv_thread(void *priv)
{
    struct camera *cam = (struct camera *)priv;
    struct textview *tv =  &cam->tv;
    struct statfs stat;
    int count = 1, chkcnt = 5, key;

    FTM_CAMERA_DBG("%s: Start\n", __FUNCTION__);

    /* Initial the title info. */
    init_text(&cam ->left_btn, "", COLOR_YELLOW);
    init_text(&cam ->center_btn, "", COLOR_YELLOW);
    init_text(&cam ->right_btn, uistr_camera_back, COLOR_YELLOW);

    /* Initial the paintview function pointers */
    ui_init_textview(tv, camera_cap_result_key_handler,  (void*)cam);
    tv->set_title(tv, &cam->title);
    tv->set_text(tv, &cam->text);
    tv->set_btn(tv, &cam ->left_btn, &cam ->center_btn, &cam ->right_btn);
    camera_show_image();

    while (!quickViewDone)
    {
        usleep(200);
    }

    tv->redraw(tv);
    cam->exit_thd = false;

    while (1)
    {
        key = ui_wait_key();
        usleep(200000);
        chkcnt--;
        tv->m_khandler(key, tv->m_priv);
        if (cam ->exit_thd)
            break;

        if (chkcnt > 0)
            continue;

        chkcnt = 5;
    }

    FTM_CAMERA_DBG("%s: Exit\n", __FUNCTION__);
    return NULL;
}

static int acdkIFInit()
{
    FTM_CAMERA_DBG("%s : Open ACDK\n",__FUNCTION__);

    //====== Local Variable ======

    ACDK_FEATURE_INFO_STRUCT rAcdkFeatureInfo;
    bool bRet;
    unsigned int u4RetLen;

    //====== Create ACDK Object ======
    if (MDK_Open() == FALSE)
    {
        FTM_CAMERA_ERR("MDK_Open() Fail \n");
        return -1;
    }

    //====== Select Camera Sensor ======

    rAcdkFeatureInfo.puParaIn = (UINT8*)&srcDev;
    rAcdkFeatureInfo.u4ParaInLen = sizeof(int);
    rAcdkFeatureInfo.puParaOut = NULL;
    rAcdkFeatureInfo.u4ParaOutLen = 0;
    rAcdkFeatureInfo.pu4RealParaOutLen = &u4RetLen;

    FTM_CAMERA_DBG("%s : srcDev:%d\n",__FUNCTION__,srcDev);
    bRet = MDK_IOControl(ACDK_CCT_FEATURE_SET_SRC_DEV, &rAcdkFeatureInfo);
    if (!bRet)
    {
        FTM_CAMERA_DBG("ACDK_FEATURE_SET_SRC_DEV Fail: %d\n",srcDev);
        return -1;
    }

    //====== Initialize ACDK ======

    FTM_CAMERA_DBG("%s : Init ACDK\n",__FUNCTION__);
    if(MDK_Init() == FALSE)
    {
        return -1;
    }

    return 0;
}


/*******************************************************************************
*
********************************************************************************/
int camera_entry(struct ftm_param *param, void *priv)
{
    char *ptr;
    int chosen;
    bool exit = false;
    int index = 0;
    struct camera *cam = (struct camera *)priv;
    struct textview *tv = NULL ;
    struct itemview *iv = NULL ;
    struct statfs stat;
//    PFLASH_LIGHT_FUNCTION_STRUCT pstFlash;
    int isTestDone = 0;
    int initDone = 1;

    FTM_CAMERA_DBG("%s\n", __FUNCTION__);

    init_text(&cam ->title, param->name, COLOR_YELLOW);
    init_text(&cam ->text, &cam->info[0], COLOR_YELLOW);

    cam->exit_thd = false;

    if (!cam->iv)
    {
        iv = ui_new_itemview();
        if (!iv)
        {
            FTM_CAMERA_DBG("No memory");
            return -1;
        }
        cam->iv = iv;
    }

    iv = cam->iv;
    iv->set_title(iv, &cam->title);

#if 0
    //init camera items
    eFlashSupport = FlashLightInit(&pstFlash);
    if (initDone)
    {
        if (eFlashSupport != FLASHLIGHT_NONE )
        {
            //init camera items with support flash
            camera_items[index].id = ITEM_CAMERA_TEST;
            camera_items[index].name = uistr_camera_preview_strobe;
        }
        else
        {
            //init camera items without support flash
            camera_items[index].id = ITEM_CAMERA_TEST;
            camera_items[index].name = uistr_camera_preview;
        }
        index++;
        //ITEM PASS
        camera_items[index].id = ITEM_PASS;
        camera_items[index].name = uistr_pass;
        index++;
    }
#endif

    if(initDone)
    {
        //init camera items without support flash
        camera_items[index].id = ITEM_CAMERA_TEST;

        if(srcDev == 0x02)  //sub camera
        {
            //camera_items[index].name = uistr_camera_preview;
            camera_items[index].name =  uistr_camera_prv_cap;
        }
        else
        {
        #ifdef CONSTANT_FLASHLIGHT
            //camera_items[index].name = uistr_camera_prv_strobe;
            camera_items[index].name = uistr_camera_prv_cap_strobe;
        #else
            //camera_items[index].name = uistr_camera_preview;
            camera_items[index].name = uistr_camera_prv_cap;
        #endif

        }
        index++;

        //ITEM PASS
        camera_items[index].id = ITEM_PASS;
        camera_items[index].name = uistr_pass;
        index++;
    }

    //ITEM FAIL
    camera_items[index].id = ITEM_FAIL;
    camera_items[index].name = uistr_fail;
    index++;

    camera_items[index].id = -1;
    camera_items[index].name = NULL;

    iv->set_items(iv, camera_items, 0);
    iv->set_text(iv, &cam->text);

    camera_state = CAMERA_STATE_IDLE;

    do
    {
        chosen = iv->run(iv, &exit);

        switch (chosen)
        {
        case ITEM_CAMERA_TEST:
            if(camera_state == CAMERA_STATE_IDLE)
            {
                capture_done = FALSE;
                g_previewStop = FALSE;

                if(0 != acdkIFInit())
                {
                    FTM_CAMERA_ERR("acdkIFInit() Fail\n");

                    MDK_DeInit();
                    MDK_Close();

                    memset(cam->info, 0, 1024);
                    sprintf(cam->info, "acdkIFInit Fail\n");
                    iv->set_text(iv, &cam->text);
                    iv->redraw(iv);

                    camera_state = CAMERA_STATE_NONE;
                }
                else
                {
                    init_text(&cam ->left_btn, "", COLOR_YELLOW);
                    init_text(&cam ->center_btn, uistr_camera_capture, COLOR_YELLOW);
                    //init_text(&cam ->center_btn, "", COLOR_YELLOW);
                    init_text(&cam ->right_btn, uistr_camera_back, COLOR_YELLOW);
                    camera_update_capture_tv_thread(priv);
                    camera_preview_stop();
                    if (capture_done == TRUE)
                    {
                        camera_update_showImg_tv_thread(priv);
                    }
                    camera_reset_layer_buffer();

                    memset(cam->info, 0, 1024);
                    iv->set_text(iv, &cam->text);
                    iv->redraw(iv);

                    isTestDone = 1;
                }
            }

            if(g_previewStop == TRUE)
            {
                FTM_CAMERA_DBG("Uninit ACDK object\n");
                MDK_DeInit();
                MDK_Close();
            }
            break;
        case ITEM_PASS:
            if(isTestDone)
            {
                cam->mod->test_result = FTM_TEST_PASS;
                exit = true;
            }
            else
            {
                memset(cam->info, 0, 1024);
                sprintf(cam->info, "Not test done !! \n");
                iv->set_text(iv, &cam->text);
                iv->redraw(iv);
            }
            break;
        case ITEM_FAIL:
            if(isTestDone)
            {
                cam->mod->test_result = FTM_TEST_FAIL;
                exit = true;
            }
            else
            {
                memset(cam->info, 0, 1024);
                sprintf(cam->info, "Not test done !! \n");
                iv->set_text(iv, &cam->text);
                iv->redraw(iv);
            }
            break;
        }

        if(exit)
        {
            cam->exit_thd = true;
            break;
        }
    } while (1);

Exit:

    camera_state = CAMERA_STATE_NONE;

    return 0;
}


/*******************************************************************************
*
********************************************************************************/
int camera_main_preview_entry(struct ftm_param *param, void *priv)
{
    srcDev = 0x01; //main sensor
    return camera_entry(param,priv);
}

/*******************************************************************************
*
********************************************************************************/
int camera_main2_preview_entry(struct ftm_param *param, void *priv)
{
    srcDev = 0x08; //main2 sensor
    return camera_entry(param,priv);
}


/*******************************************************************************
*
********************************************************************************/
int camera_sub_preview_entry(struct ftm_param *param, void *priv)
{
    srcDev = 0x02; //sub sensor
    return camera_entry(param,priv);
}

/*******************************************************************************
*
********************************************************************************/
extern "C" int camera_main_init(void)
{
    int ret = 0;
    struct ftm_module *mod;
    struct camera *cam;

    FTM_CAMERA_DBG( "%s\n", __FUNCTION__);

    mod = ftm_alloc(ITEM_MAIN_CAMERA, sizeof(struct camera));
    cam  = mod_to_camera(mod);

    memset(cam, 0x0, sizeof(struct camera));

    /*NOTE: the assignment MUST be done, or exception happens when tester press Test Pass/Test Fail*/
    cam->mod = mod;

    if (!mod)
        return -ENOMEM;

    ret = ftm_register(mod, camera_main_preview_entry, (void*)cam);

    return ret;
}

/*******************************************************************************
*
********************************************************************************/
extern "C" int camera_main2_init(void)
{
    int ret = 0;
    struct ftm_module *mod;
    struct camera *cam;

    FTM_CAMERA_DBG( "%s\n", __FUNCTION__);

    mod = ftm_alloc(ITEM_MAIN2_CAMERA, sizeof(struct camera));
    cam  = mod_to_camera(mod);

    memset(cam, 0x0, sizeof(struct camera));

    /*NOTE: the assignment MUST be done, or exception happens when tester press Test Pass/Test Fail*/
    cam->mod = mod;

    if (!mod)
        return -ENOMEM;

    ret = ftm_register(mod, camera_main2_preview_entry, (void*)cam);

    return ret;
}


/*******************************************************************************
*
********************************************************************************/
extern "C" int camera_sub_init(void)
{
    int ret = 0;
    struct ftm_module *mod;
    struct camera *cam;

    FTM_CAMERA_DBG("%s\n", __FUNCTION__);

    mod = ftm_alloc(ITEM_SUB_CAMERA, sizeof(struct camera));
    cam  = mod_to_camera(mod);

    memset(cam, 0x0, sizeof(struct camera));

    /*NOTE: the assignment MUST be done, or exception happens when tester press Test Pass/Test Fail*/
    cam->mod = mod;

    if (!mod)
        return -ENOMEM;

    ret = ftm_register(mod, camera_sub_preview_entry, (void*)cam);
    return ret;
}



//#endif
