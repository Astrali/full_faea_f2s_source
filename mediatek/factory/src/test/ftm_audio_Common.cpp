/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
/*******************************************************************************
 *
 * Filename:
 * ---------
 *   ftm_audio_Common.cpp
 *
 * Project:
 * --------
 *   MT6575/MT6573 + Android
 *
 * Description:
 * ------------
 *   Factory Mode Audio Test
 *
 * Author:
 * -------
 *
 *
 *------------------------------------------------------------------------------
 * $Revision:$
 * $Modtime:$
 * $Log:$
 *
 * 01 30 2013 ning.feng
 * [ALPS00456420] [JB2][MT6589][mATV][Must Resolve]����Audio pathΪanalog���л�Ƶ���б���
 * .
 *
 * 01 19 2013 ning.feng
 * [ALPS00367096] [Need Patch] [Volunteer Patch]mt6589 matv/libstagefright enhancement
 * .
 *
 * 01 11 2013 changqing.yan
 * [ALPS00435826] [Factory Mode] AutoTest Fail in Loopback(Spk-Mic)
 * .
 *
 * 01 09 2013 changqing.yan
 * [ALPS00435826] [Factory Mode] AutoTest Fail in Loopback(Spk-Mic)
 * .
 *
 * 01 03 2013 changqing.yan
 * [ALPS00434013] [Need Patch] [Volunteer Patch]Receiver/Headset auto test on factory mode
 * .
 *
 * 12 26 2012 changqing.yan
 * [ALPS00428915] [Power Management] Factory mode suspend current is greater than flight mode suspend
 * .
 *
 * 11 08 2012 changqing.yan
 * [ALPS00390142] [Need Patch] [Volunteer Patch]Factory mode ringtone auto test.
 * .
 *
 * 01 30 2012 donglei.ji
 * [ALPS00106007] [Need Patch] [Volunteer Patch]DMNR acoustic loopback feature
 * .
 *
 * 01 18 2012 donglei.ji
 * [ALPS00106007] [Need Patch] [Volunteer Patch]DMNR acoustic loopback feature
 * set dual mic input.
 *
 * 01 11 2012 donglei.ji
 * [ALPS00106007] [Need Patch] [Volunteer Patch]DMNR acoustic loopback feature
 * DMNR acoustic loopback check in.
 *
 * 12 27 2011 donglei.ji
 * [ALPS00106007] [Need Patch] [Volunteer Patch]DMNR acoustic loopback feature
 * change mic volume setting
 *
 * 12 26 2011 donglei.ji
 * [ALPS00106007] [Need Patch] [Volunteer Patch]DMNR acoustic loopback feature
 * DMNR Acoustic loopback check in.
 *
 * 12 14 2011 donglei.ji
 * [ALPS00101149] [Need Patch] [Volunteer Patch]AudioPlayer, AMR/AWB Playback ICS migration
 * Audio factory mode migration- remove mt6516 code.
 *
 * 10 12 2011 donglei.ji
 * [ALPS00079849] [Need Patch] [Volunteer Patch][Factory Mode] TF card test disturbs Ringtone test
 * adjust file handler correctly..
 *
 *
 *******************************************************************************/

#include "cust.h"
#include <fcntl.h>

#ifndef FEATURE_DUMMY_AUDIO

/*****************************************************************************
*                E X T E R N A L   R E F E R E N C E S
******************************************************************************
*/
#include "ftm_audio_Common.h"
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/syscall.h>
#include <time.h>
#include <sched.h>
#include <pthread.h>
#include "SineWave_156Hz.h"


/*****************************************************************************
*                     C O M P I L E R   F L A G S
******************************************************************************
*/



/*****************************************************************************
*                          C O N S T A N T S
******************************************************************************
*/

/*****************************************************************************
*                         D A T A   T Y P E S
******************************************************************************
*/


/*****************************************************************************
*                   G L O B A L      V A R I A B L E
******************************************************************************
*/
#if 0
//#if defined(MT6575) || defined(MT6577)

static int bMetaAudioInited = false;

// for gen wave data
static bool WavegenInit = false;
static pthread_mutex_t WaveGenMutex;
static pthread_t hWabeGenThread ;
static int32 Vibratetime =0;
static bool VibrateTreadExit = false;

#ifndef MTK_DUAL_MIC_SUPPORT
#define MAX_RECEIVER_GAIN_DB 12
#endif
/*****************************************************************************
*                        F U N C T I O N   D E F I N I T I O N
******************************************************************************
*/
#ifdef __cplusplus
extern "C" {
#endif

    void Audio_Set_Speaker_Vol(int level);
    void Audio_Set_Speaker_On(int Channel);
    void Audio_Set_Speaker_Off(int Channel);
    void Audio_Set_HeadPhone_On(int Channel);
    void Audio_Set_HeadPhone_Off(int Channel);
    void Audio_Set_Earpiece_On(void);
    void Audio_Set_Earpiece_Off(void);
    int Audio_Write_Vibrate_On(int millisecond);
    int Audio_Write_Vibrate_Off(void);

    static void *WaveGen_function(void *ptr)
    {
        ALOGD("WaveGen_function");
        pthread_mutex_lock( &WaveGenMutex );
        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();
        mAsmReg->Afe_Set_Stream_Attribute(1, 0x4|0x8,8000);  //PCM_16_BIT , CHANNEL_OUT_STEREO , 8k Hz
        mAsmReg->Afe_Set_Stream_Gain(0xffff);
        mAsmReg->Afe_Set_Timer(IRQ1_MCU, 1024); // time = 1024 sample (1024*2*2 = 4096 bytes/interrupt)
        mAsmReg->Afe_DL_Mute(AFE_MODE_DAC);
        mAsmReg->Afe_DL_Start(AFE_MODE_DAC);
        mAudFtm->Meta_Open_Analog(AUDIO_PATH);
        Audio_Set_Speaker_On(Channel_Stereo);
        mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();
        pthread_mutex_unlock( &WaveGenMutex );
        ALOGD("AFE prepare finish");

        while( (!VibrateTreadExit) || (Vibratetime <= 0))
        {
            int bytes = ::write(mFd, Sine_156Hz, WaveSize);
            ALOGD("VibrateTreadExit Vibratetime=%d",Vibratetime);
            usleep(250 * 1000);		// 250 ms
            pthread_mutex_lock( &WaveGenMutex);
            Vibratetime  -= 250;
            pthread_mutex_unlock( &WaveGenMutex);
            ALOGD("WaveGen_function write bytes=%d, Vibratetime=%d",bytes,Vibratetime);
        }

        // handle for thread exit,close asm analog
        pthread_mutex_lock( &WaveGenMutex);
        VibrateTreadExit = false;
        WavegenInit = false;
        Vibratetime = 0;

        Audio_Set_Speaker_Off(Channel_Stereo);
        mAsmReg->Afe_DL_Stop(AFE_MODE_DAC);
        mAudFtm->Meta_Close_Analog();
        pthread_mutex_unlock( &WaveGenMutex);
        return 0;
    }

//start vibrator with millicseond
    int Audio_Write_Vibrate_On(int millisecond)
    {
        int ret =0;
        ALOGD("Audio_Write_Vibrate_On with millisecond = %d",millisecond);
        pthread_mutex_lock( &WaveGenMutex );
        //create track and start output
        if(WavegenInit == false)
        {
            Vibratetime = millisecond;
            ALOGD("WavegenInit == false  Vibratetime = %d",Vibratetime);
            pthread_mutex_unlock( &WaveGenMutex );
            Vibratetime += millisecond;
            WavegenInit = true;
            // create a thread for write to asm
            ret = pthread_create(&hWabeGenThread,NULL,WaveGen_function,NULL);
            pthread_mutex_lock( &WaveGenMutex );
        }
        //update for time need to vibrate.
        else
        {
            Vibratetime += millisecond;
            ALOGD("WavegenInit == true Vibratetime = %d",Vibratetime);
        }
        pthread_mutex_unlock( &WaveGenMutex );
        return true;
    }
    int Audio_Write_Vibrate_Off(void)
    {
        ALOGD("Audio_Write_Vibrate_Off");
        pthread_mutex_lock( &WaveGenMutex );
        if(WavegenInit == true)
        {
            VibrateTreadExit = true;
        }
        pthread_mutex_unlock( &WaveGenMutex );
        return true;
    }

    void Audio_Set_Speaker_Vol(int level)
    {
        mAudFtm->Audio_Set_Speaker_Vol(level);
    }

    void Audio_Set_Speaker_On(int Channel)
    {
        mAudFtm->Audio_Set_Speaker_On(Channel);
    }

    void Audio_Set_Speaker_Off(int Channel)
    {
        mAudFtm->Audio_Set_Speaker_Off(Channel);
    }

    void Audio_Set_HeadPhone_On(int Channel)
    {
        mAudFtm->Audio_Set_HeadPhone_On(Channel);
    }

    void Audio_Set_HeadPhone_Off(int Channel)
    {
        mAudFtm->Audio_Set_HeadPhone_Off(Channel);
    }

    void Audio_Set_Earpiece_On()
    {
        mAudFtm->Audio_Set_Earpiece_On();
    }

    void Audio_Set_Earpiece_Off()
    {
        mAudFtm->Audio_Set_Earpiece_Off();
    }

    int Common_Audio_init(void)
    {

        if(bMetaAudioInited == true)
        {
            ALOGD("Common_Audio_init bMetaAudioInited == true, already init!!");

            if(mAnaReg != NULL)
            {
                //Request Analog clock before access analog hw
                mAnaReg->AnalogAFE_Request_ANA_CLK();
                mAnaReg->SetAnaReg(AUDIO_NCP0,0x102B,0xffff);
                mAnaReg->SetAnaReg(AUDIO_NCP1,0x0600,0xffff);
                mAnaReg->SetAnaReg(AUDIO_LDO0,0x1030,0xffff);
                mAnaReg->SetAnaReg(AUDIO_LDO1,0x3010,0xffff);
                mAnaReg->SetAnaReg(AUDIO_LDO2,0x0013,0xffff);
                mAnaReg->SetAnaReg(AUDIO_GLB0,0x2920,0xffff);
                mAnaReg->SetAnaReg(AUDIO_GLB1,0x0000,0xffff);
                mAnaReg->SetAnaReg(AUDIO_REG1,0x0001,0xffff);
                //Release Analog clock before after analog hw
                mAnaReg->AnalogAFE_Release_ANA_CLK();
            }

            ioctl(mFd,START_DL1_STREAM,0);  // init memory
            return true;
        }
        ALOGD("Common_Audio_init bMetaAudioInited = %d",bMetaAudioInited);

        mFd = open("/dev/eac", O_RDWR);

        int err =0;
        /*  Init asm */
        mAsmReg = new android::AudioAfe(NULL);
        err = mAsmReg->Afe_Init(mFd);

        if(err == false)
        {
            ALOGD("Afe_Init error");
            return false;
        }

        /* init analog  */
        mAnaReg = new android::AudioAnalog(NULL);
        err = mAnaReg->AnalogAFE_Init(mFd);

        if(err == false)
        {
            ALOGD("AnalogAFE_Init error");
            return false;
        }

#ifdef  AUD_DL1_USE_SLAVE
        ioctl(mFd,SET_DL1_SLAVE_MODE, 1);  //Set Audio DL1 slave mode
        ioctl(mFd,INIT_DL1_STREAM,0x2700);    // init AFE
#else
        ioctl(mFd,SET_DL1_SLAVE_MODE, 0);  //Set Audio DL1 master mode
        ioctl(mFd,INIT_DL1_STREAM,0x3000);    // init AFE
#endif

        ioctl(mFd,START_DL1_STREAM,0);  // init memory


// Disable LAD related code for MT65xx and later chip
// for AP processor, no need to use LAD. (No need to control modem side.)
// Make the audio factory mode free from Modem side.
#ifdef FEATURE_FTM_ACSLB
        if(mLad == NULL)
        {
           mLad = new android::LAD(NULL);
           if( !mLad->LAD_Initial() ){
              ALOGD("LAD_Initial error!");
              return false;
           }
           mLad->pCCCI->mFd = mFd;
           if(mLad->mHeadSetMessager->SetHeadInit() == false)
              ALOGE("Common_Audio_init SetHeadInit error");

           fcntl(mLad->pCCCI->GetRxFd(), F_SETFD, FD_CLOEXEC);
           fcntl(mLad->pCCCI->GetTxFd(), F_SETFD, FD_CLOEXEC);
           ALOGD("LAD create success!");
        }
#endif

        mAudFtm = new android::AudioFtm(NULL,mAsmReg,mAnaReg);
        err = mAudFtm->AudFtm_Init(mFd, mLad);

        mHeadSetMessager = new android::AudioYusuHeadSetMessager(NULL);
        if(mHeadSetMessager->SetHeadInit() == false)
            ALOGE("Common_Audio_init SetHeadInit error");

        /* new an volume controller */
        mVolumeController = new android::AudioYusuVolumeController(mFd,NULL,mAsmReg,mAnaReg);
        mVolumeController->InitVolumeController ();
        mVolumeController->ApplyGain(AUDIO_APPLY_BIG_GAIN,OUT_SPEAKER);  //apply volume
        bMetaAudioInited = true;

        // inint thread mutex
        err = pthread_mutex_init(&WaveGenMutex, NULL);
        if ( err != 0 )
        {
            return -1;
        }
        Audio_Set_Speaker_Vol(12);
        Audio_Set_Speaker_Off(Channel_Stereo);
        //Turn-on PMU here
        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();
        mAnaReg->SetAnaReg(AUDIO_NCP0,0x102B,0xffff);
        mAnaReg->SetAnaReg(AUDIO_NCP1,0x0600,0xffff);
        mAnaReg->SetAnaReg(AUDIO_LDO0,0x1030,0xffff);
        mAnaReg->SetAnaReg(AUDIO_LDO1,0x3010,0xffff);
        mAnaReg->SetAnaReg(AUDIO_LDO2,0x0013,0xffff);
        mAnaReg->SetAnaReg(AUDIO_GLB0,0x2920,0xffff);
        mAnaReg->SetAnaReg(AUDIO_GLB1,0x0000,0xffff);
        mAnaReg->SetAnaReg(AUDIO_REG1,0x0001,0xffff);
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();

        ALOGD("-Common_Audio_init");
        return true;
    }


    int Common_Audio_deinit(void)
    {
        if(bMetaAudioInited == true)
        {
            ALOGD("!Common_Audio_deinit bMetaAudioInited=%d",bMetaAudioInited);
            ioctl(mFd,STANDBY_DL1_STREAM,0);
            return true;
        }
        return true;
    }


    int PhoneMic_Receiver_Loopback(char echoflag)
    {
        return mAudFtm->PhoneMic_Receiver_Loopback(echoflag);
    }

    int PhoneMic_EarphoneLR_Loopback(char echoflag)
    {
        return mAudFtm->PhoneMic_EarphoneLR_Loopback(echoflag);
    }

    int PhoneMic_SpkLR_Loopback(char echoflag)
    {
        return mAudFtm->PhoneMic_SpkLR_Loopback(echoflag);
    }

    int HeadsetMic_EarphoneLR_Loopback(char bEnable, char bHeadsetMic)
    {
        return mAudFtm->HeadsetMic_EarphoneLR_Loopback(bEnable, bHeadsetMic);
    }

    int HeadsetMic_SpkLR_Loopback(char echoflag)
    {
        return mAudFtm->HeadsetMic_SpkLR_Loopback(echoflag);
    }

    int RecieverTest(char receiver_test)
    {
        ALOGD("RecieverTest echoflag=%d",receiver_test);
        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();
        if(receiver_test)
        {
            mAudFtm->FTM_AnaLpk_on();
            mAnaReg->SetAnaReg(AUDIO_CON3,0x01e0,0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON5,0x4400,0xffff); //Enable HS + HPL buffer
            mAnaReg->SetAnaReg(AUDIO_CON1,0x0404,0xffff); //adjust HS volume

            mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
            mAudFtm->Afe_Enable_SineWave(true);
            Audio_Set_Earpiece_On();
        }
        else
        {
            Audio_Set_Earpiece_Off();
            mAudFtm->Afe_Enable_SineWave(false);
            mAudFtm->FTM_AnaLpk_off();
        }
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();
        return true;
    }

    int LouderSPKTest(char left_channel, char right_channel)
    {
        ALOGD("LouderSPKTest left_channel=%d, right_channel=%d",left_channel,right_channel);
        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();
        int Speaker_Channel =0;
        if( left_channel == 0 && right_channel == 0)
        {
            mAudFtm->Afe_Enable_SineWave(false);
            mAudFtm->FTM_AnaLpk_off();
            Audio_Set_Speaker_Off(Channel_Stereo);
        }
        else
        {
            mAudFtm->FTM_AnaLpk_on();
            mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
            mAudFtm->Afe_Enable_SineWave(true);
            if(left_channel ==1 && right_channel == 1)
            {
                Audio_Set_Speaker_On(Channel_Stereo);
            }
            else if(right_channel ==1)
            {
                Audio_Set_Speaker_On(Channel_Right);
            }
            else if(left_channel == 1)
            {
                Audio_Set_Speaker_On(Channel_Left);
            }
        }
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();
        return true;
    }


#ifdef FEATURE_FTM_ACSLB
    int PhoneMic_Receiver_Acoustic_Loopback(int Acoustic_Type, int *Acoustic_Status_Flag, int bHeadset_Output)
    {
        ALOGD("PhoneMic_Receiver_Acoustic_Loopback Acoustic_Type=%d, headset_available=%d",Acoustic_Type, bHeadset_Output);
        /*  Acoustic loopback
        *   0: Dual mic (w/o DMNR)acoustic loopback off
        *   1: Dual mic (w/o DMNR)acoustic loopback
        *   2: Dual mic (w/  DMNR)acoustic loopback off
        *   3: Dual mic (w/  DMNR)acoustic loopback
        */
        bool retval = true;
        android::LadInPutDevice_Line inputSource = android::LADIN_Microphone1;
        static int acoustic_status = 0;
        switch(Acoustic_Type) {
        case ACOUSTIC_STATUS:
            *Acoustic_Status_Flag = acoustic_status;
            break;
        case DUAL_MIC_WITHOUT_DMNR_ACS_OFF:
            // close single mic acoustic loopback
            mAudFtm->SetLoopbackOff();
            acoustic_status = DUAL_MIC_WITHOUT_DMNR_ACS_OFF;
            break;
        case DUAL_MIC_WITHOUT_DMNR_ACS_ON:
            // open dual mic acoustic loopback (w/o DMNR)
            mAudFtm->SetLoopbackOn(android::MD_DUAL_MIC_ACOUSTIC_LOOPBACK_WITHOUT_DMNR, bHeadset_Output);
            acoustic_status = DUAL_MIC_WITHOUT_DMNR_ACS_ON;
            break;
        case DUAL_MIC_WITH_DMNR_ACS_OFF:
            // close dual mic acoustic loopback
            mAudFtm->SetLoopbackOff();
            acoustic_status = DUAL_MIC_WITH_DMNR_ACS_OFF;
            break;
        case DUAL_MIC_WITH_DMNR_ACS_ON:
            // open dual mic acoustic loopback (w/ DMNR)
            mAudFtm->SetLoopbackOn(android::MD_DUAL_MIC_ACOUSTIC_LOOPBACK_WITH_DMNR, bHeadset_Output);
            acoustic_status = DUAL_MIC_WITH_DMNR_ACS_ON;
            break;
        default:
            break;
    }

    ALOGD("PhoneMic_Receiver_Acoustic_Loopback out -");
    return retval;
    }
#endif

    int EarphoneTest(char bEnable)
    {
        ALOGD("EarphoneTest bEnable=%d",bEnable);

        Audio_Set_Speaker_Off(Channel_Stereo);
        if(bEnable)
        {
            mAudFtm->FTM_AnaLpk_on();
            mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
            mAudFtm->Afe_Enable_SineWave(true);
            Audio_Set_HeadPhone_On(Channel_Stereo);
        }
        else
        {
            mAudFtm->Afe_Enable_SineWave(false);
            mAudFtm->FTM_AnaLpk_off();
            Audio_Set_Speaker_Off(Channel_Stereo);
            Audio_Set_HeadPhone_Off(Channel_Stereo);
        }
        return true;
    }


    int FMLoopbackTest(char bEnable)
    {
        ALOGD("FMLoopbackTest bEnable=%d",bEnable);

        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();
        if(bEnable == true)
        {
            ::ioctl(mFd,START_DL1_STREAM,0);        // init DL1 Stream
            // enable Digital AFE
            // ...
            // enable Analog AFE
            mAnaReg->SetAnaReg(AUDIO_CON0,(0x4<<12),0xf000);
            mAnaReg->SetAnaReg(AUDIO_CON1,0x0C0C,MASK_ALL);
            mAnaReg->SetAnaReg(AUDIO_CON2,0x000C,MASK_ALL);
            mAnaReg->SetAnaReg(AUDIO_CON3,0x0070,MASK_ALL);  // enable voice buffer, audio left/right buffer
//      SetAnaReg(AUDIO_CON5,0x0220,MASK_ALL);  // FM mono playback (analog line in)
            mAnaReg->SetAnaReg(AUDIO_CON5,0x0110,MASK_ALL);  // FM stereo playback (analog line in)
#if 0
            //mAnaReg->AnalogAFE_Depop(FM_PATH_STEREO,true);
#else
            mAnaReg->SetAnaReg(AUDIO_NCP0,0x102B,0xffff);
            mAnaReg->SetAnaReg(AUDIO_NCP1,0x0600,0x0E00);
            mAnaReg->SetAnaReg(AUDIO_LDO0,0x1030,0x1fff);
            mAnaReg->SetAnaReg(AUDIO_LDO1,0x3010,0xffff);
            mAnaReg->SetAnaReg(AUDIO_LDO2,0x0013,0x0013);
            mAnaReg->SetAnaReg(AUDIO_GLB0,0x2920,0xffff);
            mAnaReg->SetAnaReg(AUDIO_GLB1,0x0000,0xffff);
            mAnaReg->SetAnaReg(AUDIO_REG1,0x0001,0x0001);

            mAnaReg->SetAnaReg(AUDIO_CON0,0x5000,0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON1,0x3f0C,0x3F3F);
            mAnaReg->SetAnaReg(AUDIO_CON2,0x000C,0x003F);
            mAnaReg->SetAnaReg(AUDIO_CON3,0x0030,0x01FF);
            mAnaReg->SetAnaReg(AUDIO_CON5,0x0110,MASK_ALL);

            mAnaReg->SetAnaReg(AUDIO_CON6,0x0533,0x0FFF);
            mAnaReg->SetAnaReg(AUDIO_CON7,0x003F,0x003F);
            mAnaReg->SetAnaReg(AUDIO_CON7,0x0000,0xC000);
            mAnaReg->SetAnaReg(AUDIO_CON8,0x0000,0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON9,0x0058,0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON14,0x0000,0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON17,0x0018,0xffff);
#endif
            mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
            Audio_Set_Speaker_On(Channel_Stereo);
        }
        else
        {
            mAsmReg->Afe_DL_Mute(AFE_MODE_DAC);
            //mAsmReg->ForceDisableSpeaker();
            Audio_Set_Speaker_Off(Channel_Stereo);
            // disable Digital AFE
            // ...
            // disable Analog AFE
            mAnaReg->SetAnaReg(AUDIO_CON0,0,0xf000);
            mAnaReg->SetAnaReg(AUDIO_CON3,0x0000,0x00f0);  // disable voice buffer, audio left/right buffer
            mAnaReg->SetAnaReg(AUDIO_CON5,0x0440,0x0ff0);  // disable FM mono playback (analog line in)

            mAnaReg->AnalogAFE_Depop(FM_PATH_STEREO,false);

            ::ioctl(mFd,STANDBY_DL1_STREAM,0);
        }
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();
        return true;
    }

    /*
    int EarphoneMicbiasEnable(int bMicEnable)
    {
       ALOGD("EarphoneMicbiasEnable bEnable=%d",bMicEnable);
       mLad->LAD_SwitchMicBias((int32)bMicEnable);
       return true;
    }
    */

    unsigned int ATV_AudioWrite(void* buffer, unsigned int bytes)
    {
        return 0;
    }

    int ATV_AudAnalogPath(char bEnable)
    {
        ALOGD("ATV_AudAnalogPath bEnable=%d",bEnable);
        FMLoopbackTest(bEnable);
        return true;
    }


    int ATV_AudPlay_On()
    {
        ALOGD("ATV_AudPlay_On");
        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();
        //mAnaReg->Meta_Open_Analog(AUDIO_PATH);
        //Temp solution
        mAnaReg->SetAnaReg(AUDIO_NCP0,0x102b,0xffff);
        mAnaReg->SetAnaReg(AUDIO_NCP1,0x0600,0xffff);
        //mAnaReg->SetAnaReg(AUDIO_NCP1,0x0000,0xffff);
        mAnaReg->SetAnaReg(AUDIO_LDO0,0x1030,0xffff);
        mAnaReg->SetAnaReg(AUDIO_LDO1,0x3010,0xffff);
        mAnaReg->SetAnaReg(AUDIO_LDO2,0x0013,0xffff);
        mAnaReg->SetAnaReg(AUDIO_GLB0,0x2920,0xffff);
        mAnaReg->SetAnaReg(AUDIO_GLB1,0x0000,0xffff);
        mAnaReg->SetAnaReg(AUDIO_REG1,0x0001,0xffff);
        mAnaReg->SetAnaReg(AUDIO_CON0,0x4000,0xffff); //Set Line-in gain
        mAnaReg->SetAnaReg(AUDIO_CON1,0x0c0c,0xffff); //Set HS/HPL gain
        mAnaReg->SetAnaReg(AUDIO_CON2,0x000c,0xffff); //Set HPR gain
        mAnaReg->SetAnaReg(AUDIO_CON3,0x01f0,0xffff); //Turn-on DAC and HP buffer
        mAnaReg->SetAnaReg(AUDIO_CON5,0x4440,0xffff); //Set HS/HPL/HPR MUX
        mAnaReg->SetAnaReg(AUDIO_CON6,0x0a44,0xffff); //Set buffer Ib/Iq current(thd=-91/-88dBc@16R/32R)
        mAnaReg->SetAnaReg(AUDIO_CON7,0x003f,0xffff); //Set ZCD bias current
        mAnaReg->SetAnaReg(AUDIO_CON8,0x0000,0xffff);
        mAnaReg->SetAnaReg(AUDIO_CON14,0x00c0,0xffff); //set DAC ref. gen.
        mAnaReg->SetAnaReg(AUDIO_CON17,0x0008,0xffff); //Enable HS pull-low
        mAnaReg->SetAnaReg(AUDIO_CON10,0x01a1,0xffff); //scrambler enable
        mAnaReg->SetAnaReg(AUDIO_CON9,0x0052,0xfffe);//AFIFO enable
        usleep(1);
        mAnaReg->SetAnaReg(AUDIO_CON9,0x0001,0x0001);
        //~Temp solution
        mAsmReg->Afe_DL_Start(AFE_MODE_FTM_I2S);
        //Temp solution
        mAsmReg->SetAfeReg(AFE_DAC_CON1,0x00000800,0x00000f00);//32kHz
        mAsmReg->SetAfeReg(AFE_DL_SRC2_1,0x60000000,0xf0000000);//32kHz
        //~Temp solution
        mAsmReg->Afe_Set_Stream_Gain(0xffff); // hardcore AFE gain
        mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
        Audio_Set_Speaker_On(Channel_Stereo);
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();
        return true;
    }

    int ATV_AudPlay_Off()
    {
        ALOGD("ATV_AudPlay_Off");
        Audio_Set_Speaker_Off(Channel_Stereo);
        mAsmReg->Afe_DL_Stop(AFE_MODE_FTM_I2S);
        mAudFtm->Meta_Close_Analog();
        return true;
    }


    int Audio_I2S_Play(int enable_flag)
    {
        ALOGD("Audio_I2S_Play : %d",enable_flag);
        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();

        if(enable_flag == true)
        {
            ALOGD("+Audio_I2S_Play true");
            //mAnaReg->Meta_Open_Analog(AUDIO_PATH);
            //Temp solution
            mAnaReg->SetAnaReg(AUDIO_NCP0,0x102b,0xffff);
            mAnaReg->SetAnaReg(AUDIO_NCP1,0x0600,0xffff);
            //mAnaReg->SetAnaReg(AUDIO_NCP1,0x0000,0xffff);
            mAnaReg->SetAnaReg(AUDIO_LDO0,0x1030,0xffff);
            mAnaReg->SetAnaReg(AUDIO_LDO1,0x3010,0xffff);
            mAnaReg->SetAnaReg(AUDIO_LDO2,0x0013,0xffff);
            mAnaReg->SetAnaReg(AUDIO_GLB0,0x2920,0xffff);
            mAnaReg->SetAnaReg(AUDIO_GLB1,0x0000,0xffff);
            mAnaReg->SetAnaReg(AUDIO_REG1,0x0001,0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON0,0x4000,0xffff); //Set Line-in gain
            mAnaReg->SetAnaReg(AUDIO_CON1,0x0c0c,0xffff); //Set HS/HPL gain
            mAnaReg->SetAnaReg(AUDIO_CON2,0x000c,0xffff); //Set HPR gain
            mAnaReg->SetAnaReg(AUDIO_CON3,0x01f0,0xffff); //Turn-on DAC and HP buffer
            mAnaReg->SetAnaReg(AUDIO_CON5,0x4440,0xffff); //Set HS/HPL/HPR MUX
            mAnaReg->SetAnaReg(AUDIO_CON6,0x0a44,0xffff); //Set buffer Ib/Iq current(thd=-91/-88dBc@16R/32R)
            mAnaReg->SetAnaReg(AUDIO_CON7,0x003f,0xffff); //Set ZCD bias current
            mAnaReg->SetAnaReg(AUDIO_CON8,0x0000,0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON14,0x00c0,0xffff); //set DAC ref. gen.
            mAnaReg->SetAnaReg(AUDIO_CON17,0x0008,0xffff); //Enable HS pull-low
            mAnaReg->SetAnaReg(AUDIO_CON10,0x01a1,0xffff); //scrambler enable
            mAnaReg->SetAnaReg(AUDIO_CON9,0x0052,0xfffe);  //AFIFO enable
            usleep(1);
            mAnaReg->SetAnaReg(AUDIO_CON9,0x0001,0x0001);
            //~Temp solution
            mAsmReg->Afe_DL_Start(AFE_MODE_FTM_I2S);
            //Temp solution
            mAsmReg->SetAfeReg(AFE_DAC_CON0,0x00000000,0x00000008);//Disable VUL
            mAsmReg->SetAfeReg(AFE_DAC_CON1,0x00000a00,0x00000f00);//48kHz
            mAsmReg->SetAfeReg(AFE_DL_SRC2_1,0x80000000,0xf0000000);//48kHz
            mAsmReg->SetAfeReg(AFE_CONN1,0x00200000,0xffffffff);//I00_O03_S
            mAsmReg->SetAfeReg(AFE_CONN2,0x00002000,0xffffffff);//I01_O04_S
            //~Temp solution
            mAsmReg->Afe_Set_Stream_Gain(0xffff); // hardcore AFE gain
            mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
            Audio_Set_Speaker_On(Channel_Stereo);
            ::ioctl(mFd,AUDDRV_SET_FM_I2S_GPIO,0);// enable FM use I2S
            ALOGD("-Audio_I2S_Play true");
        }
        else
        {
            ALOGD("-Audio_I2S_Play false");
            Audio_Set_Speaker_Off(Channel_Stereo);
            mAsmReg->Afe_DL_Stop(AFE_MODE_FTM_I2S);
            mAudFtm->Meta_Close_Analog();
            ::ioctl(mFd,AUDDRV_RESET_BT_FM_GPIO,0);// Reset GPIO pin mux
            ALOGD("-Audio_I2S_Play false");
        }
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();

        return true;
    }

    int Audio_FMTX_Play(bool Enable, unsigned int  Freq)
    {
        ALOGD("Audio_FMTX_Play : %d, Freq=%d ", Enable, Freq);
        ALOGD("Audio_FMTX_Play : Enable =%d, Freq=%d ", Enable, Freq);
        if(Enable)
        {
            mAudFtm->FTM_AnaLpk_on();
            mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
        }
        else
        {
            mAudFtm->FTM_AnaLpk_off();
            mAsmReg->Afe_DL_Mute(AFE_MODE_DAC);
        }

        return mAudFtm->WavGen_SW_SineWave(Enable, Freq, 0); // 0: FM-Tx, 1: HDMI

        return true;
    }


    int Audio_HDMI_Play(bool Enable, unsigned int Freq)
    {
        ALOGD("Audio_HDMI_Play(%d), Freq=%d ", Enable, Freq);
        ALOGD("Audio_HDMI_Play : Enable =%d, Freq=%d ", Enable, Freq);
        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();
        if(Enable)
        {
            mAnaReg->SetAnaReg(AUDIO_CON1, 0x000C, 0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON2, 0x000C, 0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON3, 0x01B0, 0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON9,0x0052,0xfffe);//AFIFO enable
            usleep(1);
            mAnaReg->SetAnaReg(AUDIO_CON9,0x0001,0x0001);
            mAnaReg->SetAnaReg(AUDIO_CON14, 0x00c0, 0xffff);
        }
        else
        {
            mAnaReg->SetAnaReg(AUDIO_CON1, 0, 0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON2, 0, 0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON3, 0, 0xffff);
        }
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();

        return mAudFtm->WavGen_SW_SineWave(Enable, Freq, 1); // 0: FM-Tx, 1: HDMI

        return true;
    }


    static void Audio_Wave_Afe_Set_Stereo(void)
    {
        uint32 reg_AFE_CONN1,reg_AFE_CONN2;
        ALOGD("!!! Audio_Wave_Afe_Set_Stereo \n");

        mAsmReg->SetAfeReg(AFE_CONN1,0x0,0xffffffff);  // Connect -- DL1_L to DAC_L, bit26: I05-O03
        mAsmReg->SetAfeReg(AFE_CONN2,0x0,0xffffffff);  // Connect -- DL1_L to DAC_L, bit26: I05-O03

        mAsmReg->SetAfeReg(AFE_CONN1,(1<<26),(1<<26));  // Connect -- DL1_L to DAC_L, bit26: I05-O03
        mAsmReg->SetAfeReg(AFE_CONN2,(1<<18),(1<<18));  // Connect -- DL1_R to DAC_R, bit18: I06-O04
        mAsmReg->GetAfeReg(AFE_CONN1, &reg_AFE_CONN1);
        mAsmReg->GetAfeReg(AFE_CONN2, &reg_AFE_CONN2);
        mAsmReg->SetAfeReg(AFE_DAC_CON1,0x000000,0x100000); // bit20: DL1_MONO (memoryinterface)
        ALOGD("Afe_Set_Stereo AFE_CONN1=%x, AFE_CONN2=%x,\n",reg_AFE_CONN1,reg_AFE_CONN2);
    }

//LCH: HW stereo to mono
//RCH: only RCH PCM
    static void Audio_Wave_Afe_Set_Mono(void)
    {

        uint32 reg_AFE_CONN1, reg_AFE_CONN2;
        ALOGD("!!! Audio_Wave_Afe_Set_Mono \n");
        // Mix the stereo to mono to LCH(O3)
        // Need to check with HW designer
        mAsmReg->SetAfeReg(AFE_CONN1,0x0,0xffffffff);  // Connect -- DL1_L to DAC_L, bit26: I05-O03
        mAsmReg->SetAfeReg(AFE_CONN1,(1<<26),(1<<26));  // Connect -- DL1_L to DAC_L, bit26: I05-O03

        mAsmReg->SetAfeReg(AFE_CONN2,0x0,0xffffffff);  // Connect -- DL1_L to DAC_L, bit26: I05-O03
        mAsmReg->SetAfeReg(AFE_CONN2,(1<<17),(1<<17));  // Connect -- DL1_R to DAC_R, bit18: I05-O04
        mAsmReg->GetAfeReg(AFE_CONN1, &reg_AFE_CONN1);
        mAsmReg->GetAfeReg(AFE_CONN2, &reg_AFE_CONN2);
        mAsmReg->SetAfeReg(AFE_DAC_CON1,0x100000,0x100000); // bit20: DL1_MONO (memoryinterface)
        ALOGD("HW S2M Afe_Set_Mono AFE_CONN1=%x, AFE_CONN2=%x,\n",reg_AFE_CONN1,reg_AFE_CONN2);
    }

// open file and write data to hardware
    static void *Audio_Wave_Playabck_routine(void *arg)
    {

        char *mAudioBuffer = NULL;
        short *mAudio8to16Buffer = NULL;
        short *mAudioFinalBuffer = NULL;
        int SizeByte=0;
        int Channels =0;
        int interruptinterval =0;

        WavePlayData *pWavePlaydata = (WavePlayData*)arg;
        if(pWavePlaydata == NULL || arg == NULL)
        {
            ALOGD("Audio_Wave_Playabck_routine Exit \n");
            pthread_exit(NULL); // thread exit
            return NULL;
        }

        printf("pWavePlaydata open file %s \n",pWavePlaydata->FileName);
        pWavePlaydata->pFile =fopen(pWavePlaydata->FileName,"rb");
        //pWavePlaydata->pFile =fopen("/sdcard/testpattern1.wav","rb");
        if(pWavePlaydata->pFile == NULL)
        {
            printf("pWavePlaydata open file fail\n");
            pWavePlaydata->ThreadExit = true;
            pthread_exit(NULL); // thread exit
            return NULL;
        }

        // read wave header
        fread((void*)&pWavePlaydata->mWaveHeader,WAVE_HEADER_SIZE,1,pWavePlaydata->pFile);
        ALOGD("BitsPerSample = %d",pWavePlaydata->mWaveHeader.BitsPerSample);
        ALOGD("NumChannels = %d",pWavePlaydata->mWaveHeader.NumChannels);
        ALOGD("SampleRate = %d",pWavePlaydata->mWaveHeader.SampleRate);
        // setting audiohardware

        mAudioBuffer = new char[WAVE_BUFFER_SIZE];
        int actualbuffersize =WAVE_BUFFER_SIZE;

        if(pWavePlaydata->mWaveHeader.BitsPerSample == 8)
        {
            actualbuffersize = actualbuffersize <<1;
            mAudio8to16Buffer = new short[actualbuffersize<<1];
        }
        if(pWavePlaydata->mWaveHeader.NumChannels == 1)
        {
            actualbuffersize = actualbuffersize <<1;
        }
        printf("pWavePlaydata open file %s \n",pWavePlaydata->FileName);
        printf("actualbuffersize = %d \n",actualbuffersize);
        mAudioFinalBuffer = new short[actualbuffersize<<1];

        Audio_Wave_Afe_Set_Stereo();

        bool mFlag_Aud_DL1_SlaveOn = ::ioctl(mFd, GET_DL1_SLAVE_MODE,0);
        if (mFlag_Aud_DL1_SlaveOn)
        {
            SizeByte = 9984;
        }
        else
        {
            SizeByte = 12288;
        }
        ::ioctl(mFd, AUD_SET_CLOCK, 1);
        ::ioctl(mFd, INIT_DL1_STREAM, SizeByte);    // init DL1
        ::ioctl(mFd, START_DL1_STREAM, 0);
        Channels = 0x1| 0x2; //CHANNEL_OUT_STEREO = (CHANNEL_OUT_FRONT_LEFT | CHANNEL_OUT_FRONT_RIGHT),
        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();
        printf("ApplyGain \n");
        mVolumeController->ApplyGain(0xffff,OUT_SPEAKER);  //apply volume

        mAsmReg->Afe_Set_Stream_Attribute(1, Channels , pWavePlaydata->mWaveHeader.SampleRate);  //PCM_16_BIT , CHANNEL_OUT_STEREO , 8k Hz
        mAsmReg->Afe_Set_Stream_Gain(0xffff);
        pWavePlaydata->mWaveHeader.SampleRate>>6;
        mAsmReg->Afe_Set_Timer(IRQ1_MCU,pWavePlaydata->mWaveHeader.SampleRate>>6);
        mAudFtm->FTM_AnaLpk_on();
        mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
        mAsmReg->Afe_DL_Start(AFE_MODE_DAC);
        if (pWavePlaydata->i4Output == Output_HS)
        {
            mAnaReg->SetAnaReg(AUDIO_CON3,0x01e0,0xffff);
            mAnaReg->SetAnaReg(AUDIO_CON5,0x4400,0xffff); //Enable HS + HPL buffer
            mAnaReg->SetAnaReg(AUDIO_CON1,0x0404,0xffff); //adjust HS volume
            Audio_Set_Speaker_Off(Channel_Stereo);
            Audio_Set_Earpiece_On();
        }
        else if (pWavePlaydata->i4Output == Output_HP)
        {
            Audio_Set_Speaker_Off(Channel_Stereo);
            Audio_Set_HeadPhone_On(Channel_Stereo);
        }
        else if (pWavePlaydata->i4Output == Output_LPK)
        {
            Audio_Set_Speaker_On(Channel_Stereo);
        }
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();

        // continue render to hardware
        while(!feof(pWavePlaydata->pFile) && pWavePlaydata->ThreadExit == false)
        {
            int readdata = 0, writedata =0;
            readdata = fread(mAudioBuffer,WAVE_BUFFER_SIZE,1,pWavePlaydata->pFile);
            if(pWavePlaydata->mWaveHeader.BitsPerSample == 8 &&pWavePlaydata->mWaveHeader.NumChannels == 2)
            {
                int count = WAVE_BUFFER_SIZE;
                short *dst =mAudio8to16Buffer ;
                char  *src = mAudioBuffer;
                while(count --)
                {
                    *dst++ = (short)( *src++^0x80 ) << 8;
                }
                writedata = ::write(mFd, mAudio8to16Buffer, WAVE_BUFFER_SIZE*2); // write to asm hardware
                ALOGV("8 2 Audio_Wave_Playabck_routine mAudio8to16Buffer write to hardware... read = %d writedata = %d",readdata,writedata);
            }
            else if(pWavePlaydata->mWaveHeader.BitsPerSample == 16 &&pWavePlaydata->mWaveHeader.NumChannels == 1)
            {
                int count = WAVE_BUFFER_SIZE;
                short  *src = (short*)mAudioBuffer;
                short *dst =mAudioFinalBuffer ;
                while(count--)
                {
                    *dst++ = *src ;
                    *dst++ = *src++ ;
                }
                writedata = ::write(mFd, mAudioFinalBuffer, WAVE_BUFFER_SIZE*2); // write to asm hardware
                ALOGV("16 1 Audio_Wave_Playabck_routine mAudio8to16Buffer write to hardware... read = %d writedata = %d",readdata,writedata);
            }
            else if(pWavePlaydata->mWaveHeader.BitsPerSample == 8 &&pWavePlaydata->mWaveHeader.NumChannels == 1)
            {
                int count = WAVE_BUFFER_SIZE;
                short *dst =mAudio8to16Buffer ;
                char  *src = mAudioBuffer;
                while(count --)
                {
                    *dst++ = (short)( *src++^0x80 ) << 8;
                }
                count = WAVE_BUFFER_SIZE<<1;
                short *src1 = mAudio8to16Buffer;
                short *dst1 = mAudioFinalBuffer ;
                while(count--)
                {
                    *dst1++ = *src1;
                    *dst1++ = *src1;
                    src1++;
                }
                writedata = ::write(mFd, mAudioFinalBuffer, WAVE_BUFFER_SIZE*4); // write to asm hardware
                ALOGV("8 1 Audio_Wave_Playabck_routine mAudio8to16Buffer write to hardware... read = %d writedata = %d",readdata,writedata);
            }
            else if(pWavePlaydata->mWaveHeader.BitsPerSample == 16 &&pWavePlaydata->mWaveHeader.NumChannels == 2)
            {
                writedata = ::write(mFd, mAudioBuffer, WAVE_BUFFER_SIZE); // write to asm hardware
                ALOGV("16 2 Audio_Wave_Playabck_routine mAudioBuffer write to hardware... read = %d writedata = %d",readdata,writedata);
            }
        }
        //Request Analog clock before access analog hw
        mAnaReg->AnalogAFE_Request_ANA_CLK();
        mAsmReg->Afe_DL_Mute(AFE_MODE_DAC);
        Audio_Set_Speaker_Off(Channel_Stereo);
        Audio_Set_HeadPhone_Off(Channel_Stereo);
        Audio_Set_Earpiece_Off();
        mAsmReg->Afe_DL_Stop(AFE_MODE_DAC);
        mAudFtm->FTM_AnaLpk_off();
        ::ioctl(mFd, STANDBY_DL1_STREAM, 0);
        ::ioctl(mFd, AUD_SET_CLOCK, 0);
        //Release Analog clock after access analog hw
        mAnaReg->AnalogAFE_Release_ANA_CLK();

        if(mAudioBuffer)
        {
            delete[] mAudioBuffer;
            mAudioBuffer = NULL;
        }
        if(mAudio8to16Buffer)
        {
            delete[] mAudio8to16Buffer;
            mAudio8to16Buffer = NULL;
        }
        if(mAudioFinalBuffer)
        {
            delete[] mAudioFinalBuffer;
            mAudioFinalBuffer = NULL;
        }

        if(pWavePlaydata->pFile)
        {
            fclose(pWavePlaydata->pFile);
            pWavePlaydata->pFile = NULL;
        }

        // thread exit;
        pWavePlaydata->ThreadExit = true;
        pWavePlaydata->ThreadStart = false;

        printf("Audio_Wave_Playabck_routine Exit \n");
        pthread_exit(NULL); // thread exit
        return NULL;
    }


    int Audio_Wave_playback(void* arg)
    {
        printf("Audio_Wave_playback with arg = %x \n",arg);
        WavePlayData *pWavePlaydata = (WavePlayData*)arg;
        if(pWavePlaydata == NULL || arg == NULL)
        {
            ALOGD("Audio_Wave_Playabck_routine Exit \n");
            pthread_exit(NULL); // thread exit
            return NULL;
        }

        if(pWavePlaydata->WavePlayThread == NULL)
        {
            // create playback thread
            printf("pthread_create WavePlayThread\n");
            pthread_create(&pWavePlaydata->WavePlayThread, NULL, Audio_Wave_Playabck_routine,arg);
            //pthread_join(pWavePlaydata->WavePlayThread, NULL);
        }
        printf("Audio_Wave_playback return \n");
        return true;
    }

    int Audio_READ_SPK_OC_STA(void)
    {
        int s4RegValue = 100;
        if (mFd>=0)
        {
            s4RegValue = ::ioctl(mFd,AUDDRV_AMP_OC_READ,0);// Set OC CFG
            printf("Audio_READ_SPK_OC_STA get status(%d) \n",s4RegValue);
        }
        else
        {
            printf("Audio_READ_SPK_OC_STA unable get mFd \n");
            s4RegValue = -1;
        }
        return s4RegValue;
    }
//Louder Speaker Over Current Test
    int LouderSPKOCTest(char left_channel, char right_channel)
    {
        int Speaker_Channel =0;
        unsigned int u4RegValue = 0;
        if( left_channel == 0 && right_channel == 0)
        {
            mAudFtm->Afe_Enable_SineWave(false);
            mAudFtm->FTM_AnaLpk_off();
            u4RegValue = 0;
            //disable speaker and OC
            if (mFd>=0)
            {
                ::ioctl(mFd,AUDDRV_AMP_OC_CFG,u4RegValue);// Set OC CFG
                ::ioctl(mFd,AUDDRV_HQA_AMP_AMPEN,0);
            }
            else
            {
                printf("LouderSPKOCTest unable get mFd \n");
            }
        }
        else
        {
            mAudFtm->FTM_AnaLpk_on();
            mAsmReg->Afe_DL_Unmute(AFE_MODE_DAC);
            mAudFtm->Afe_Enable_SineWave(true);
            u4RegValue = ((unsigned int)0xC0 << 16 )+(unsigned int)0xC0;
            //enable speaker and OC
            if (mFd>=0)
            {
                ::ioctl(mFd,AUDDRV_HQA_AMP_MODESEL,1);//class-AB
                ::ioctl(mFd,AUDDRV_HQA_AMP_RECEIVER,0);//spk mode
                ::ioctl(mFd,AUDDRV_HQA_AMP_AMPEN,1);
                ::ioctl(mFd,AUDDRV_AMP_OC_CFG,u4RegValue);// Set OC CFG
            }
            else
            {
                printf("LouderSPKOCTest unable get mFd \n");
            }
        }
        return (true);
    }
#ifdef __cplusplus
};
#endif

#elif defined(MT6589)

#include <include/AudioMTKHardware.h>
#include <include/AudioDigitalControlFactory.h>
#include <include/AudioAnalogControlFactory.h>
#include <include/AudioDigitalControlFactory.h>
#include <include/AudioMTKStreamOut.h>
#include <include/AudioMTKStreamIn.h>
#include <include/AudioMTKStreamInManager.h>
#include <include/AudioFtm.h>

#include <include/LoopbackManager.h>

// golbal hardware pointer
static    android::AudioMTKHardware *gAudioHardware = NULL;
static    android::AudioMTKStreamOut *gAudioStreamOut = NULL;
static    android_audio_legacy::AudioStreamIn *gAudioStreamIn = NULL;
static    android::AudioFtm *gAudioFtm;

#ifdef __cplusplus
extern "C" {
#endif

int Common_Audio_init(void)
{
    // if hardware is not create , create haredware , else return.
    if(gAudioHardware == NULL)
    {
        gAudioHardware = new android::AudioMTKHardware(false);
    }
    // get ftm module
    if(gAudioFtm == NULL)
    {
        gAudioFtm = android::AudioFtm::getInstance();
    }
    return true;
}

int Common_Audio_deinit(void)
{
    if(gAudioStreamIn != NULL )
    {
        gAudioStreamIn->standby();
        //gAudioHardware->closeInputStream(gAudioStreamIn);
        //gAudioStreamIn = NULL;
    }
    /*if(gAudioHardware != NULL)
    {
        delete gAudioHardware;
        gAudioHardware = NULL;;
    }*/
    return true;
}
void Audio_Set_Speaker_Vol(int level)
{
    gAudioFtm->Audio_Set_Speaker_Vol(level);
}

void Audio_Set_Speaker_On(int Channel)
{
    gAudioFtm->Audio_Set_Speaker_On(Channel);
}

void Audio_Set_Speaker_Off(int Channel)
{
    gAudioFtm->Audio_Set_Speaker_Off(Channel);
}

void Audio_Set_HeadPhone_On(int Channel)
{
    gAudioFtm->Audio_Set_HeadPhone_On(Channel);
}

void Audio_Set_HeadPhone_Off(int Channel)
{
    gAudioFtm->Audio_Set_HeadPhone_Off(Channel);
}

void Audio_Set_Earpiece_On()
{
    gAudioFtm->Audio_Set_Earpiece_On();
}

void Audio_Set_Earpiece_Off()
{
    gAudioFtm->Audio_Set_Earpiece_Off();
}


int PhoneMic_Receiver_Loopback(char echoflag)
{
    if (echoflag == MIC1_ON) {
        android::LoopbackManager::GetInstance()->SetLoopbackOn(android::AP_MAIN_MIC_AFE_LOOPBACK, android::LOOPBACK_OUTPUT_RECEIVER);
    }
    else if (echoflag == MIC2_ON) {
        android::LoopbackManager::GetInstance()->SetLoopbackOn(android::AP_REF_MIC_AFE_LOOPBACK, android::LOOPBACK_OUTPUT_RECEIVER);
    }
    else {
        android::LoopbackManager::GetInstance()->SetLoopbackOff();
    }
    return true;
}

int PhoneMic_EarphoneLR_Loopback(char echoflag)
{
    if (echoflag == MIC1_ON) {
        android::LoopbackManager::GetInstance()->SetLoopbackOn(android::AP_MAIN_MIC_AFE_LOOPBACK, android::LOOPBACK_OUTPUT_EARPHONE);
    }
    else if (echoflag == MIC2_ON) {
        android::LoopbackManager::GetInstance()->SetLoopbackOn(android::AP_REF_MIC_AFE_LOOPBACK, android::LOOPBACK_OUTPUT_EARPHONE);
    }
    else {
        android::LoopbackManager::GetInstance()->SetLoopbackOff();
    }
    return true;
}

int PhoneMic_SpkLR_Loopback(char echoflag)
{
    if (echoflag == MIC1_ON) {
        android::LoopbackManager::GetInstance()->SetLoopbackOn(android::AP_MAIN_MIC_AFE_LOOPBACK, android::LOOPBACK_OUTPUT_SPEAKER);
    }
    else if (echoflag == MIC2_ON) {
        android::LoopbackManager::GetInstance()->SetLoopbackOn(android::AP_REF_MIC_AFE_LOOPBACK, android::LOOPBACK_OUTPUT_SPEAKER);
    }
    else {
        android::LoopbackManager::GetInstance()->SetLoopbackOff();
    }
    return true;
}


int HeadsetMic_Receiver_Loopback(char bEnable, char bHeadsetMic)
{
    if (bEnable) {
        android::LoopbackManager::GetInstance()->SetLoopbackOn(android::AP_HEADSET_MIC_AFE_LOOPBACK, android::LOOPBACK_OUTPUT_RECEIVER);
    }
    else {
        android::LoopbackManager::GetInstance()->SetLoopbackOff();
    }
    return true;
}

int HeadsetMic_EarphoneLR_Loopback(char bEnable, char bHeadsetMic)
{
    if (bEnable) {
        android::LoopbackManager::GetInstance()->SetLoopbackOn(android::AP_HEADSET_MIC_AFE_LOOPBACK, android::LOOPBACK_OUTPUT_EARPHONE);
    }
    else {
        android::LoopbackManager::GetInstance()->SetLoopbackOff();
    }
    return true;
}

int HeadsetMic_SpkLR_Loopback(char echoflag)
{
    if (echoflag) {
        android::LoopbackManager::GetInstance()->SetLoopbackOn(android::AP_HEADSET_MIC_AFE_LOOPBACK, android::LOOPBACK_OUTPUT_SPEAKER);
    }
    else {
        android::LoopbackManager::GetInstance()->SetLoopbackOff();
    }
    return true;
}


#ifdef FEATURE_FTM_ACSLB
int PhoneMic_Receiver_Acoustic_Loopback(int Acoustic_Type, int *Acoustic_Status_Flag, int bHeadset_Output)
{
    ALOGD("PhoneMic_Receiver_Acoustic_Loopback Acoustic_Type=%d, headset_available=%d",Acoustic_Type, bHeadset_Output);
    /*  Acoustic loopback
    *   0: Dual mic (w/o DMNR)acoustic loopback off
    *   1: Dual mic (w/o DMNR)acoustic loopback
    *   2: Dual mic (w/  DMNR)acoustic loopback off
    *   3: Dual mic (w/  DMNR)acoustic loopback
    */
        

    static android::LoopbackManager *pLoopbackManager = android::LoopbackManager::GetInstance();
    android::loopback_output_device_t loopback_output_device;
    if (bHeadset_Output == true) {
        loopback_output_device = android::LOOPBACK_OUTPUT_EARPHONE;
    }
    else {
        loopback_output_device = android::LOOPBACK_OUTPUT_RECEIVER; // default use receiver here
    }



    bool retval = true;
    static int acoustic_status = 0;
    switch(Acoustic_Type) {
        case ACOUSTIC_STATUS:
            *Acoustic_Status_Flag = acoustic_status;
            break;
        case DUAL_MIC_WITHOUT_DMNR_ACS_OFF:
            // close single mic acoustic loopback
            pLoopbackManager->SetLoopbackOff();
            acoustic_status = DUAL_MIC_WITHOUT_DMNR_ACS_OFF;
            break;
        case DUAL_MIC_WITHOUT_DMNR_ACS_ON:
            // open dual mic acoustic loopback (w/o DMNR)
            pLoopbackManager->SetLoopbackOn(android::MD_DUAL_MIC_ACOUSTIC_LOOPBACK_WITHOUT_DMNR, loopback_output_device);
            acoustic_status = DUAL_MIC_WITHOUT_DMNR_ACS_ON;
            break;
        case DUAL_MIC_WITH_DMNR_ACS_OFF:
            // close dual mic acoustic loopback
            pLoopbackManager->SetLoopbackOff();
            acoustic_status = DUAL_MIC_WITH_DMNR_ACS_OFF;
            break;
        case DUAL_MIC_WITH_DMNR_ACS_ON:
            // open dual mic acoustic loopback (w/ DMNR)
            pLoopbackManager->SetLoopbackOn(android::MD_DUAL_MIC_ACOUSTIC_LOOPBACK_WITH_DMNR, loopback_output_device);
            acoustic_status = DUAL_MIC_WITH_DMNR_ACS_ON;
            break;
        default:
            break;
    }

    ALOGD("PhoneMic_Receiver_Acoustic_Loopback out -");
    return retval;
}
#endif


int RecieverTest(char receiver_test)
{
    return gAudioFtm->RecieverTest(receiver_test);
}

int LouderSPKTest(char left_channel, char right_channel)
{
    return gAudioFtm->LouderSPKTest(left_channel,right_channel);
}

bool recordInit(int device_in)
{
    if(gAudioStreamIn == NULL)
    {
        android::AudioParameter paramVoiceMode = android::AudioParameter();
        paramVoiceMode.addInt(android::String8("HDREC_SET_VOICE_MODE"),0);
        gAudioHardware->setParameters(paramVoiceMode.toString());
        
        uint32_t device = AUDIO_DEVICE_IN_BUILTIN_MIC;
        int format = AUDIO_FORMAT_PCM_16_BIT;
        uint32_t channel = AUDIO_CHANNEL_IN_STEREO;
        uint32_t sampleRate = 48000;
        status_t status = 0;
        android::AudioParameter param = android::AudioParameter();

        if(device_in == MATV_I2S)
        {
            sampleRate = 32000;
            device = AUDIO_DEVICE_IN_AUX_DIGITAL2 ;
            gAudioStreamIn = gAudioHardware->openInputStream(device,&format,&channel,&sampleRate,&status,(android_audio_legacy::AudioSystem::audio_in_acoustics)0);
            param.addInt(android::String8(android::AudioParameter::keyInputSource), android_audio_legacy::AUDIO_SOURCE_MATV);
        }
        else if(device_in == MATV_ANALOG)
        {
            sampleRate = 48000;
            device = AUDIO_DEVICE_IN_FM;
            gAudioStreamIn = gAudioHardware->openInputStream(device,&format,&channel,&sampleRate,&status,(android_audio_legacy::AudioSystem::audio_in_acoustics)0);
            param.addInt(android::String8(android::AudioParameter::keyInputSource), android_audio_legacy::AUDIO_SOURCE_MATV);
        }
        else
        {
            gAudioStreamIn = gAudioHardware->openInputStream(device,&format,&channel,&sampleRate,&status,(android_audio_legacy::AudioSystem::audio_in_acoustics)0);
            android::AudioParameter param = android::AudioParameter();
            param.addInt(android::String8(android::AudioParameter::keyInputSource), android_audio_legacy::AUDIO_SOURCE_MIC);
        }
        gAudioStreamIn->setParameters(param.toString());
    }
    android::AudioParameter param = android::AudioParameter();
    if(device_in == BUILTIN_MIC)
        param.addInt(android::String8(android::AudioParameter::keyRouting), AUDIO_DEVICE_IN_BUILTIN_MIC);
    else if(device_in == WIRED_HEADSET)
        param.addInt(android::String8(android::AudioParameter::keyRouting), AUDIO_DEVICE_IN_WIRED_HEADSET);
    else if(device_in == MATV_ANALOG)
        param.addInt(android::String8(android::AudioParameter::keyRouting), AUDIO_DEVICE_IN_FM);    
    else if(device_in == MATV_I2S)
        param.addInt(android::String8(android::AudioParameter::keyRouting), AUDIO_DEVICE_IN_AUX_DIGITAL2);

    gAudioStreamIn->setParameters(param.toString());

    return true;
}
int readRecordData(void * pbuffer,int bytes)
{
    int nBytes = 0;
    
    nBytes = gAudioStreamIn->read(pbuffer,bytes);
    return nBytes;
}

int EarphoneTest(char bEnable)
{
    return gAudioFtm->EarphoneTest(bEnable);
}

int FMLoopbackTest(char bEnable)
{
    return gAudioFtm->FMLoopbackTest(bEnable);
}

int Audio_I2S_Play(int enable_flag)
{
    return gAudioFtm->Audio_FM_I2S_Play(enable_flag);
}

int Audio_FMTX_Play(bool Enable, unsigned int  Freq)
{
   // mt6583 not suupport FM TX
    return true;
}

unsigned int ATV_AudioWrite(void* buffer, unsigned int bytes)
{
    return 0;
}

int ATV_AudAnalogPath(char bEnable)
{
    ALOGD("ATV_AudAnalogPath bEnable=%d",bEnable);
    FMLoopbackTest(bEnable);
    return true;
}

int Audio_HDMI_Play(bool Enable, unsigned int Freq)
{
    return true;
}

int PCM_decode_data(WaveHdr *wavHdr,  char *in_buf, int block_size, char *out_buf, int *out_size)
{
    int i, j;
    uint16_t *ptr_d;
    uint8_t  *ptr_s;
    int readlen = 0;
    int writelen = 0;

    uint16_t channels = wavHdr->NumChannels;
    uint16_t bits_per_sample = wavHdr->BitsPerSample;

    ptr_s = (uint8_t*)in_buf;
    ptr_d = (uint16_t*)out_buf;
    readlen = block_size;
    *out_size = 0;

    switch (bits_per_sample)
    {
        case 8:
            if (channels == 2)
            {
                for (i=0;i<readlen;i++)
                {
                    *(ptr_d+j) = (uint16_t)(*(ptr_s+i) -128) << 8;
                    j++;
                }
            }
            else
            {
                for (i=0;i<readlen;i++)
                {
                    *(ptr_d+j) = (uint16_t)(*(ptr_s+i) -128) << 8;
                    *(ptr_d+j+1) =  *(ptr_d+j);
                    j+=2;
                }
            }
            writelen = (j<<1);
            break;
        case 16:
            if (channels == 2)
            {
                for (i=0;i<readlen;i+=2)
                {
                    *(ptr_d+j) = *(ptr_s+i) + ((uint16_t)(*(ptr_s+i+1)) <<8);
                    j++;
                }
            }
            else
            {
                for(i=0;i<readlen;i+=2)
                {
                    *(ptr_d+j) = *(ptr_s+i) + ((uint16_t)(*(ptr_s+i+1)) <<8);
                    *(ptr_d+j+1) = *(ptr_d+j);
                    j+=2;
                }
            }
            writelen = (j<<1);
            break;
        default:
            ptr_d = (uint16_t*)(out_buf);
            break;
    }
    *out_size = writelen;
    return true;
}

static void *Audio_Wave_Playabck_routine(void *arg)
{
    char *inBuffer = NULL;
    char *outBuffer = NULL;
    int format;
    uint32_t channels;
    uint32_t sampleRate;
    status_t status;
    uint32_t ReadBlockLen;
    uint32_t hwBufferSize;
    int out_size;

    WavePlayData *pWavePlaydata = (WavePlayData*)arg;
    if(pWavePlaydata == NULL || arg == NULL)
    {
        ALOGD("Audio_Wave_Playabck_routine Exit \n");
        pthread_exit(NULL); // thread exit_sockets
        return NULL;
    }

    printf("pWavePlaydata open file %s \n",pWavePlaydata->FileName);
    pWavePlaydata->pFile =fopen(pWavePlaydata->FileName,"rb");
    //pWavePlaydata->pFile =fopen("/sdcard/testpattern1.wav","rb");
    if(pWavePlaydata->pFile == NULL)
    {
        printf("pWavePlaydata open file fail\n");
        pWavePlaydata->ThreadExit = true;
        pthread_exit(NULL); // thread exit
        return NULL;
    }

    // read wave header
    fread((void*)&pWavePlaydata->mWaveHeader,WAVE_HEADER_SIZE,1,pWavePlaydata->pFile);
    ALOGD("BitsPerSample = %d",pWavePlaydata->mWaveHeader.BitsPerSample);
    ALOGD("NumChannels = %d",pWavePlaydata->mWaveHeader.NumChannels);
    ALOGD("SampleRate = %d",pWavePlaydata->mWaveHeader.SampleRate);

    gAudioFtm->FTM_AnaLpk_on();

    //config output format channel= 2 , bits_per_sample=16
    sampleRate = pWavePlaydata->mWaveHeader.SampleRate;
    channels = android_audio_legacy::AudioSystem::CHANNEL_OUT_STEREO;

    if (pWavePlaydata->mWaveHeader.BitsPerSample == 8 || pWavePlaydata->mWaveHeader.BitsPerSample == 16)
        format = android_audio_legacy::AudioSystem::PCM_16_BIT;
    else
	    format = android_audio_legacy::AudioSystem::PCM_16_BIT;

    //create output stream
    android_audio_legacy::AudioStreamOut *streamOutput = gAudioHardware->openOutputStream(0, &format, &channels, &sampleRate, &status);

    hwBufferSize = streamOutput->bufferSize(); //16k bytes

    if (pWavePlaydata->mWaveHeader.NumChannels == 1) {
        switch (pWavePlaydata->mWaveHeader.BitsPerSample) {
            case 8:
                ReadBlockLen = hwBufferSize >> 2;
                break;
            case 16:
                ReadBlockLen = hwBufferSize >> 1;
                break;
            default:
                ReadBlockLen = 0;
                break;
        }
    }
    else {
        switch (pWavePlaydata->mWaveHeader.BitsPerSample) {
            case 8:
                ReadBlockLen = hwBufferSize >> 1;
                break;
            case 16:
                ReadBlockLen = hwBufferSize;
                break;
            default:
                ReadBlockLen = 0;
                break;
        }
    }

    inBuffer = new char[ReadBlockLen];
    outBuffer = new char[hwBufferSize];

    printf("ReadBlockLen = %d, hwBufferSize \n",ReadBlockLen, hwBufferSize);

    //Select audio output device
    if (pWavePlaydata->i4Output == Output_HS)
    {
        android::AudioResourceManager::getInstance()->setDlOutputDevice(AUDIO_DEVICE_OUT_EARPIECE);
    }
    else if (pWavePlaydata->i4Output == Output_HP)
    {
        android::AudioResourceManager::getInstance()->setDlOutputDevice(AUDIO_DEVICE_OUT_WIRED_HEADPHONE);
    }
    else if (pWavePlaydata->i4Output == Output_LPK)
    {
        android::AudioResourceManager::getInstance()->setDlOutputDevice(AUDIO_DEVICE_OUT_SPEAKER);
    }

    // continue render to hardware
    while(!feof(pWavePlaydata->pFile) && pWavePlaydata->ThreadExit == false)
    {
        int readdata = 0, writedata =0;
        readdata = fread(inBuffer, ReadBlockLen, 1, pWavePlaydata->pFile);
        PCM_decode_data(&pWavePlaydata->mWaveHeader, inBuffer, ReadBlockLen, outBuffer, &out_size);

        writedata = streamOutput->write(outBuffer, out_size);
        ALOGV("Audio_Wave_Playabck_routine write to hardware... read = %d writedata = %d", readdata, out_size);
    }

    gAudioFtm->FTM_AnaLpk_off();
    streamOutput->standby();
    gAudioHardware->closeOutputStream(streamOutput);

    if(inBuffer)
    {
        delete[] inBuffer;
        inBuffer = NULL;
    }
    if(outBuffer)
    {
        delete[] outBuffer;
        outBuffer = NULL;
    }

    if(pWavePlaydata->pFile)
    {
        fclose(pWavePlaydata->pFile);
        pWavePlaydata->pFile = NULL;
    }

    // thread exit;
    pWavePlaydata->ThreadExit = true;
    pWavePlaydata->ThreadStart = false;

    printf("Audio_Wave_Playabck_routine Exit \n");
    pthread_exit(NULL); // thread exit_sockets
    return NULL;
}

//-------------------------------------------------------------------
//FUNCTION:
//		Audio_Wave_playback
//DESCRIPTION:
//		Audio_Wave_playback
//
//PARAMETERS:
//
//
//RETURN VALUE:
//
//DEPENDENCY:
//		Common_Audio_init must be called before
//
//GLOBALS AFFECTED
//		None
//-------------------------------------------------------------------
int Audio_Wave_playback(void* arg)
{
    printf("Audio_Wave_playback with arg = %x \n",arg);
    WavePlayData *pWavePlaydata = (WavePlayData*)arg;
    if(pWavePlaydata == NULL || arg == NULL)
    {
        ALOGD("Audio_Wave_Playabck_routine Exit \n");
        pthread_exit(NULL); // thread exit
        return NULL;
    }

    if(pWavePlaydata->WavePlayThread == NULL)
    {
        // create playback thread
        printf("pthread_create WavePlayThread\n");
        pthread_create(&pWavePlaydata->WavePlayThread, NULL, Audio_Wave_Playabck_routine,arg);
    }
    printf("Audio_Wave_playback return \n");
    return true;
}

int Audio_READ_SPK_OC_STA(void)
{
    return gAudioFtm->Audio_READ_SPK_OC_STA();
}

int LouderSPKOCTest(char left_channel, char right_channel)
{
    return gAudioFtm->LouderSPKOCTest(left_channel,right_channel);
}

#ifdef __cplusplus
};
#endif

#endif

#else  // dummy audio function   -->   #ifndef FEATURE_DUMMY_AUDIO

#ifdef __cplusplus
extern "C" {
#endif

    int Common_Audio_init(void)
    {
        return true;
    }

    int Common_Audio_deinit(void)
    {
        return true;
    }

    int PhoneMic_Receiver_Loopback(char echoflag)
    {
        return true;
    }

    int PhoneMic_EarphoneLR_Loopback(char echoflag)
    {
        return true;
    }

    int HeadsetMic_SpkLR_Loopback(char echoflag)
    {
        return true;
    }
    int PhoneMic_SpkLR_Loopback(char echoflag)
    {
        return true;
    }

    int RecieverTest(char receiver_test)
    {
        return true;
    }

    int LouderSPKTest(char left_channel, char right_channel)
    {
        return true;
    }
    bool recordInit(int device_in)
    {
    	return true;
	  }
    int readRecordData(void * pbuffer,int bytes)
    {
        return 0;
    }

    int HeadsetMic_Receiver_Loopback(char bEnable, char bHeadsetMic)
    {
        return true;
    }

    int EarphoneTest(char bEnable)
    {
        return true;
    }

    int FMLoopbackTest(char bEnable)
    {
        return true;
    }

    int HeadsetMic_EarphoneLR_Loopback(char bEnable, char bHeadsetMic)
    {
        return true;
    }

    /*
    int EarphoneMicbiasEnable(int bMicEnable)
    {
        return true;
    }
    */
    int Audio_I2S_Play(int enable_flag)
    {
        return true;
    }

    int Audio_Wave_playback(void* arg)
    {
        return true;
    }

    int Audio_READ_SPK_OC_STA(void)
    {
        return true;
    }

    int LouderSPKOCTest(char left_channel, char right_channel)
    {
        return true;
    }

     int Audio_FMTX_Play(bool Enable, unsigned int  Freq)
    {
        return true;
    }
#ifdef __cplusplus
};
#endif

#endif   // #ifndef FEATURE_DUMMY_AUDIO

