/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

extern "C" {
#include "common.h"
#include "miniui.h"
#include "ftm.h"
}

#include "cct_feature.h"
#include "AcdkCommon.h"
#include "AcdkIF.h"
///#include "AcdkCCTFeature.h"

#include <stdlib.h>

#define FOR_FACOTRY_MODE

#include <stdio.h>
extern "C" {
#include "jpeglib.h"
#include <setjmp.h>
}
#define TAG                  "[MATV_PREV] "
#define atv_abs(a) (((a) < 0) ? -(a) : (a)) 

static int bSendDataToACDK(ACDK_CCT_FEATURE_ENUM    FeatureID,
                           UINT8                   *pInAddr,
                           UINT32                   nInBufferSize,
                           UINT8                   *pOutAddr,
                           UINT32                   nOutBufferSize,
                           UINT32                  *pRealOutByeCnt)
{
    ACDK_FEATURE_INFO_STRUCT rAcdkFeatureInfo; 

    rAcdkFeatureInfo.puParaIn = pInAddr;
    rAcdkFeatureInfo.u4ParaInLen = nInBufferSize;
    rAcdkFeatureInfo.puParaOut = pOutAddr;
    rAcdkFeatureInfo.u4ParaOutLen = nOutBufferSize;
    rAcdkFeatureInfo.pu4RealParaOutLen = pRealOutByeCnt;


    return (MDK_IOControl(FeatureID, &rAcdkFeatureInfo));
}

#ifdef __cplusplus
extern "C" {
#endif
static volatile int atvcapture_done = FALSE;
static char szCapFileName[256];
static char szRawFileName[256];

static unsigned int g_ImgCnt = 1;

struct my_error_mgr
{
    struct jpeg_error_mgr pub;    /* "public" fields */

    jmp_buf setjmp_buffer;    /* for return to caller */
};

typedef struct my_error_mgr *my_error_ptr;


METHODDEF(void)
my_error_exit(j_common_ptr cinfo)
{
    my_error_ptr myerr = (my_error_ptr) cinfo->err;

    (*cinfo->err->output_message)(cinfo);

    longjmp(myerr->setjmp_buffer, 1);
}


int parse_JPEG_file(char *jpgfilename, char *rawfilename)
{
    /* This struct contains the JPEG decompression parameters and pointers to
    * working space (which is allocated as needed by the JPEG library).
    */
    struct jpeg_decompress_struct cinfo;
    struct my_error_mgr jerr;
    FILE *infile;
    JSAMPARRAY buffer;
    int row_stride;

    if ((infile = fopen(jpgfilename, "rb")) == NULL)
    {
        LOGD("can't open %s", jpgfilename);
        return 0;
    }

    cinfo.err = jpeg_std_error(&jerr.pub);
    jerr.pub.error_exit = my_error_exit;

    if (setjmp(jerr.setjmp_buffer))
    {
        jpeg_destroy_decompress(&cinfo);
        fclose(infile);
        return 0;
    }

    FILE *pFp = fopen(rawfilename, "wb");

    if (NULL == pFp)
    {
        LOGD("Can't open file to save raw Image");
        return 0;
    }
    int i4WriteCnt = 0;

    /* Now we can initialize the JPEG decompression object. */
    jpeg_create_decompress(&cinfo);
    jpeg_stdio_src(&cinfo, infile);


    (void) jpeg_read_header(&cinfo, TRUE);
    cinfo.out_color_space = JCS_RGB;

    (void) jpeg_start_decompress(&cinfo);
    row_stride = cinfo.output_width * cinfo.output_components;
    /* Make a one-row-high sample array that will go away when done with image */
    buffer = (*cinfo.mem->alloc_sarray)
             ((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);


    while (cinfo.output_scanline < cinfo.output_height)
    {
        (void) jpeg_read_scanlines(&cinfo, buffer, 1);

        i4WriteCnt = fwrite(buffer[0], 1, row_stride , pFp);
        ///LOGD("saving file to save raw Image w%d wd%d ", row_stride, i4WriteCnt);
        ///put_scanline_someplace(buffer[0], row_stride);
    }


    (void) jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);

    fclose(infile);
    fclose(pFp);

    return 1;
}


int matv_preview_init()
{
    g_ImgCnt = 1;
    LOGD(TAG"Open ACDK ");

    if (MDK_Open() == 0)
    {
        LOGE(TAG"MDK_Open() Fail ");
        return -1;
    }

    //set ATV mode
    LOGD(TAG"Set ATV Mode ");
    ACDK_FEATURE_INFO_STRUCT rAcdkFeatureInfo;
    INT32 srcDev = 4; //ATV
    rAcdkFeatureInfo.puParaIn = (UINT8 *)&srcDev;
    rAcdkFeatureInfo.u4ParaInLen = sizeof(INT32);
    rAcdkFeatureInfo.puParaOut = NULL;
    rAcdkFeatureInfo.u4ParaOutLen = 0;
    rAcdkFeatureInfo.pu4RealParaOutLen = NULL;
    MDK_IOControl(ACDK_CCT_FEATURE_SET_SRC_DEV, &rAcdkFeatureInfo);

    LOGD(TAG"Init ACDK ");

    if (MDK_Init() == 0)
    {
        LOGE(TAG"MDK_Init() Fail ");
        return -1;
    }

    return 0;
}

int matv_preview_deinit()
{
    MDK_DeInit();
    MDK_Close();
    return 0;
}

int matv_preview_start()
{
    LOGD(TAG"ACDK_CCT_OP_PREVIEW_LCD_START");

    ACDK_CCT_CAMERA_PREVIEW_STRUCT rCCTPreviewConfig;
    rCCTPreviewConfig.fpPrvCB = NULL;
    rCCTPreviewConfig.u2PreviewWidth = 320;
    rCCTPreviewConfig.u2PreviewHeight = 240;

    unsigned int u4RetLen = 0;


    bool bRet = bSendDataToACDK(ACDK_CCT_OP_PREVIEW_LCD_START, (unsigned char *)&rCCTPreviewConfig,
                                sizeof(ACDK_CCT_CAMERA_PREVIEW_STRUCT),
                                NULL,
                                0,
                                &u4RetLen);

    if (!bRet)
    {
        LOGE(TAG"ACDK_CCT_OP_PREVIEW_LCD_START Fail");
    }

    //camera_state = CAMERA_STATE_PREVIEW;

    return 0;
}

int matv_preview_stop()
{
    LOGD(TAG"ACDK_CCT_OP_PREVIEW_LCD_STOP ");

    unsigned int u4RetLen = 0;
    bool bRet = bSendDataToACDK(ACDK_CCT_OP_PREVIEW_LCD_STOP, NULL, 0, NULL, 0, &u4RetLen);

    if (!bRet)
    {
        return 1;
    }

    //camera_state = CAMERA_STATE_IDLE;
    return 0;
}

int matv_preview_reset_layer_buffer(void)
{
    unsigned int u4RetLen = 0;
    bool bRet = 0;
    ACDK_FEATURE_INFO_STRUCT rAcdkFeatureInfo;

    rAcdkFeatureInfo.puParaIn = NULL;
    rAcdkFeatureInfo.u4ParaInLen = 0;
    rAcdkFeatureInfo.puParaOut = NULL;
    rAcdkFeatureInfo.u4ParaOutLen = 0;
    rAcdkFeatureInfo.pu4RealParaOutLen = &u4RetLen;

    bRet = MDK_IOControl(ACDK_CCT_FEATURE_RESET_LAYER_BUFFER, &rAcdkFeatureInfo);

    if (!bRet)
    {
        return 1;
    }

    return 0;
}

bool mATVSaveJPEGImg(char *a_pBuf,  unsigned int a_u4Size)
{
    sprintf(szCapFileName, "/data/matv_%04d.jpg" , g_ImgCnt);
    LOGD("Save image file name:%s", szCapFileName);

    FILE *pFp = fopen(szCapFileName, "wb");

    if (NULL == pFp)
    {
        LOGD("Can't open file to save Image");
        return FALSE;
    }

    int i4WriteCnt = fwrite(a_pBuf, 1, a_u4Size , pFp);   
    fclose(pFp);
    
    sprintf(szRawFileName, "/data/matv_%04d.raw" , g_ImgCnt);

    return TRUE;
}

void vATVCapCb(void *a_pParam)
{
    LOGD("matv Capture Callback ");

    ImageBufInfo *pImgBufInfo = (ImageBufInfo *)a_pParam;

    if (pImgBufInfo->eImgType == JPEG_TYPE)
    {
        LOGD("Size:%d", pImgBufInfo->imgBufInfo.imgSize);
        LOGD("Width:%d", pImgBufInfo->imgBufInfo.imgWidth);
        LOGD("Height:%d", pImgBufInfo->imgBufInfo.imgHeight);

        mATVSaveJPEGImg((char *)pImgBufInfo->imgBufInfo.bufAddr,
                        pImgBufInfo->imgBufInfo.imgSize);
    }
    else
    {
        LOGD("UnKnow Format ");
        atvcapture_done = FALSE;
        return;
    }

    atvcapture_done = TRUE;
}


/////////////////////////////////////////////////////////////////////////
//
//   camera_capture_test () -
//!
//!  brief for camera capture test
//
/////////////////////////////////////////////////////////////////////////
bool matv_capture_check()
{
    
    bool return_value = true; 
    char *srcraw = NULL;
    char *dstraw = NULL;
    FILE *psrcfp = NULL;
    FILE *pdstfp = NULL;
    int err_cnt = 0;
    int srcbytes=0;
    int dstbytes=0;
    
    LOGD("matv_capture-");

    LOGD("matv Get Sensor Resolution Info");
    ACDK_CCT_SENSOR_RESOLUTION_STRUCT  SensorResolution;
    memset(&SensorResolution, 0, sizeof(ACDK_CCT_SENSOR_RESOLUTION_STRUCT));

    unsigned int u4RetLen = 0;
    MINT32 sensorType = -1;
    bool bRet = false;

    //====== Get Sensor Resolution Info ======
#if 0
    bRet = bSendDataToACDK(ACDK_CCT_V2_OP_GET_SENSOR_RESOLUTION,
                                NULL,
                                0,
                                (UINT8 *)&SensorResolution,
                                sizeof(ACDK_CCT_SENSOR_RESOLUTION_STRUCT),
                                &u4RetLen);


    if (!bRet)
    {
        LOGD("[matvcapture] Get Sensor Resolution Fail");
        return_value = false;
        goto capture_exit;
    }
    else
    {
        LOGD("[matvcapture] SensorPreviewWidth  = %u", SensorResolution.SensorPreviewWidth);
        LOGD("[matvcapture] SensorPreviewHeight = %u", SensorResolution.SensorPreviewHeight);
        LOGD("[matvcapture] SensorFullWidht     = %u", SensorResolution.SensorFullWidth);
        LOGD("[matvcapture] SensorFullHeight    = %u", SensorResolution.SensorFullHeight);
    }

    //====== Get Sensor Type Info ======
    bRet = bSendDataToACDK(ACDK_CCT_OP_GET_SENSOR_TYPE,
                           NULL,
                           0,
                           (UINT8 *)&sensorType,
                           sizeof(MINT32),
                           &u4RetLen);

    if (!bRet)
    {
        LOGD("[matvcapture] Get Sensor Resolution Fail");
        return_value = false;
        goto capture_exit;
    }
    else
    {
        LOGD("[matvcapture] sensorType = %d", sensorType);
    }
#endif

    ACDK_CCT_STILL_CAPTURE_STRUCT rCCTStillCapConfig;    

#if 0    
    rCCTStillCapConfig.eCameraMode = FACTORY_MODE;
    rCCTStillCapConfig.bFactoryAuto = FALSE; //cotta-- added for factory mode auto-testing
    {
        LOGD("[matvcapture]normal");

        rCCTStillCapConfig.eOutputFormat = OUTPUT_JPEG;

        //align to 16x
        rCCTStillCapConfig.u2JPEGEncWidth =  SensorResolution.SensorFullWidht & (~0xF);
        rCCTStillCapConfig.u2JPEGEncHeight =  SensorResolution.SensorFullHeight & (~0xF);
        rCCTStillCapConfig.fpCapCB = vATVCapCb;
    }
#else
        rCCTStillCapConfig.eCameraMode   = CAPTURE_MODE;
        rCCTStillCapConfig.eOperaMode    = ACDK_OPT_FACTORY_MODE;
        rCCTStillCapConfig.eOutputFormat = OUTPUT_JPEG;
        rCCTStillCapConfig.u2JPEGEncWidth =  312 & (~0xF);     //will be alignd to 16x
        rCCTStillCapConfig.u2JPEGEncHeight =  238 & (~0xF);    //will be alignd to 16x
        rCCTStillCapConfig.fpCapCB = vATVCapCb;
        rCCTStillCapConfig.i4IsSave = 1;    // 0-no save, 1-save   
    
#endif

    atvcapture_done = FALSE;
    bRet = bSendDataToACDK(ACDK_CCT_OP_SINGLE_SHOT_CAPTURE_EX,
                           (unsigned char *)&rCCTStillCapConfig,
                           sizeof(ACDK_CCT_STILL_CAPTURE_STRUCT),
                           NULL,
                           0,
                           &u4RetLen);



    //wait JPEG Done;
    while (!atvcapture_done)
    {
        usleep(5000);
    }
    
    g_ImgCnt++;
    
    LOGD("[matvcapture] - done");

    if (!bRet)
    {
        return_value = false;
        goto capture_exit;
    }

    //check pattern.
    parse_JPEG_file(szCapFileName, szRawFileName);
    if(fopen("/data/matv_pattern.raw", "r") == NULL)
    {
        LOGD("[matvcapture] parser matv_patter to raw");
        parse_JPEG_file("/system/res/matv/matv_pattern.jpg", "/data/matv_pattern.raw");
    }

    srcraw = new char[3* 304 *224];
    dstraw = new char[3* 304 *224];

    if((srcraw == NULL) || (dstraw ==NULL))
    {
        LOGD("[matvcapture] allocate memory fail");
        return_value = false;
        goto capture_exit;
    }
    psrcfp = fopen(szRawFileName,"r");
    pdstfp = fopen("/data/matv_pattern.raw","r");  ///"/res/images/matv_pattern.raw"

    if((psrcfp == NULL) || (pdstfp == NULL))
    {
        LOGD("[matvcapture] open compare file fail");
        return_value = false;
        goto capture_exit;
    }

    srcbytes=fread(srcraw, 1, 3* 304 *224, psrcfp);
    dstbytes=fread(dstraw, 1, 3* 304 *224, pdstfp);
    
    for(int i=0; i< 3* 304 *224;i++)
    {
        if(atv_abs(srcraw[i] - dstraw[i]) > 10)
            err_cnt++;
    }

    LOGD("[matvcapture] srcbytes %d, dstbytes %d, err_cnt is %d", srcbytes, dstbytes, err_cnt);
        
    if(err_cnt > 304 *224)
        return_value = false;

capture_exit:
    
    if(srcraw != NULL) 
        delete [] srcraw;
    if(dstraw != NULL) 
        delete [] dstraw;

    if((psrcfp != NULL))
        fclose(psrcfp);
    if((pdstfp != NULL))
        fclose(pdstfp); 

    //remove temp file
    char buf[256];
    sprintf(buf, "rm %s",szCapFileName);    
    system(buf);
    memset(buf, 0, 256);
    sprintf(buf, "rm %s",szRawFileName);    
    system(buf);

    return return_value;
}


#ifdef __cplusplus
};
#endif

