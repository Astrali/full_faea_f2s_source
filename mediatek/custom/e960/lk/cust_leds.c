//#include <platform/cust_leds.h>
#include <cust_leds.h>
//#include <asm/arch/mt6577_pwm.h>
	#include <platform/mt_gpio.h>
	 #include <platform/mt_pwm.h>

//extern int DISP_SetBacklight(int level);

extern int disp_bls_set_backlight(unsigned int level);

#if 1 //defined(BL_IC_KTD259) 
#define KTD259_BRIGHTNESS_TOTAL_LEVEL   (32)
#define KTD259_BRIGHTNESS_STEP_LEVEL    (8)
int  ktd259_pre_gpio_dev_level = -1;

static int  Cust_KTD259_SetBacklight(int level, int div)
{
	struct pwm_spec_config pwm_setting;
	static int clock_enable = 0;
	unsigned int con;
	int phylevel;
	int pulse_count;


    #ifdef GPIO_MAINLCD_EN_PIN
    mt_set_gpio_mode(GPIO_MAINLCD_EN_PIN, GPIO_MAINLCD_EN_PIN_M_GPIO);
    mt_set_gpio_dir(GPIO_MAINLCD_EN_PIN, GPIO_DIR_OUT);
    if( level > 0 )
    {
        mdelay(20);
        mt_set_gpio_out(GPIO_MAINLCD_EN_PIN, GPIO_OUT_ONE);
    }
    else
    {
        mt_set_gpio_out(GPIO_MAINLCD_EN_PIN, GPIO_OUT_ZERO);
    }
    #endif

    return 0;


}
#else
 	#error "ERROR : e928 use ktd259 as backlight ic only !!!"
#endif


static struct cust_mt65xx_led cust_led_list[MT65XX_LED_TYPE_TOTAL] = {
	{"red",               MT65XX_LED_MODE_NONE, -1,{0}},
	{"green",             MT65XX_LED_MODE_NONE, -1,{0}},
	{"blue",              MT65XX_LED_MODE_NONE, -1,{0}},
	{"jogball-backlight", MT65XX_LED_MODE_NONE, -1,{0}},
	{"keyboard-backlight",MT65XX_LED_MODE_NONE, -1,{0}},
	{"button-backlight",  MT65XX_LED_MODE_PMIC, MT65XX_LED_PMIC_BUTTON,{0}},
	//{"lcd-backlight",     MT65XX_LED_MODE_CUST_BLS_PWM, (int)disp_bls_set_backlight,{0}},
	{"lcd-backlight",     MT65XX_LED_MODE_CUST_LCM, (int)Cust_KTD259_SetBacklight,{0}},
};

struct cust_mt65xx_led *get_cust_led_list(void)
{
	return cust_led_list;
}

