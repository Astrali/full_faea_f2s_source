/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2001
*
*******************************************************************************/
/****
 * Revision 1.2 2013/05/08 dinghongzhen
 * modem update
 ********/
/*******************************************************************************
 *
 * Filename:
 * ---------
 *   l1d_custom_rf.h
 *
 * Project:
 * --------
 *   MT6167
 *
 * Description:
 * ------------
 *   MT6167 2G RF constance definition
 *
 * Author:
 * -------
 *
 *
 *==============================================================================
 *******************************************************************************/

#ifndef  _L1D_CUSTOM_RF_H_
#define  _L1D_CUSTOM_RF_H_
/* --------------------------------------------------------------------------- */

#if !defined(MT6167_2G_RF)
   #error "rf files mismatch with compile option!"
#endif

/*MT6167*/ /*--------------------------------------------------------*/
/*MT6167*/ /*   PA option                                            */
/*MT6167*/ /*--------------------------------------------------------*/
/*MT6167*/ #define  SKY77590
/*MT6167*/ //#define  RF3236
/*MT6167*/
/*--------------------------------------------------------*/
/*   Event Timing Define                                  */
/*--------------------------------------------------------*/
/*MT6167*/
/*MT6167*/ #define  QB_SR0               220
/*MT6167*/ #define  QB_SR1               194  /* SR1 should be smaller than (QB_SR1-25QB)           */
/*MT6167*/ #define  QB_SR2               58   /* SR2 should be larger  than (QB_RX_FENA_2_FSYNC+8QB)*/
/*MT6167*/ #define  QB_SR2M              22
/*MT6167*/ #define  QB_SR3               5    /* SR3 should be larger than (QB_RX_FSYNC_2_FENA+2QB) */
/*MT6167*/ #define  QB_PR1               218  /* QB_PR1>QB_SR1 to prevent RF conflict among 2/3G */
/*MT6167*/ #define  QB_PR2               50
/*MT6167*/ #define  QB_PR3               5               
/*MT6167*/ #define  QB_ST0               280
/*MT6167*/ #define  QB_ST1               269  /* ST1  should be smaller than (QB_ST1-10QB)           */
/*MT6167*/ #define  QB_ST2B              33   /* ST2B should be larger  than (QB_TX_FENA_2_FSYNC+8QB)*/
/*MT6167*/ #define  QB_ST3               38   /* ST3  should be larger  than (QB_TX_FSYNC_2_FENA+7QB)*/
/*MT6167*/ #define  QB_PT1               282  /* QB_PT1>QB_ST1 to prevent RF conflict among 2/3G     */
/*MT6167*/ #define  QB_PT2               56
/*MT6167*/ #define  QB_PT2B              6
/*MT6167*/ #define  QB_PT3               38   //ori: 30  
/*MT6167*/ #define  QB_ST2M_G8           10
/*MT6167*/ #define  QB_ST2M_8G           6
/*MT6167*/ #define  QB_PT2M1_G8          -1
/*MT6167*/ #define  QB_PT2M2_G8          -3
/*MT6167*/ #define  QB_PT2M1_8G          10
/*MT6167*/ #define  QB_PT2M2_8G          4
/*MT6167*/
/*MT6167*/
/*MT6167*/ #define  QB_APCON             14 //OH:11
/*MT6167*/ #define  QB_APCMID            20 //OH:18
/*MT6167*/ #define  QB_APCOFF            6  //56: 6
/*MT6167*/ #define  QB_APCDACON          18 //0
/*MT6167*/ #define  TX_PROPAGATION_DELAY 48 //56:47 / OH:46
/*MT6167*/
/*MT6167*/
/*MT6167*/ /*--------------------------------------------------*/
/*MT6167*/ /*   define  BPI data for MT6167                    */
/*MT6167*/ /*--------------------------------------------------*/
/*MT6167*/ /*  PRCB : bit   pin                                */
/*MT6167*/ /*          0    ASM_VCTRL_A        (RF3236_BS2)    */
/*MT6167*/ /*          1    ASM_VCTRL_B        (RF3236_BS1)    */
/*MT6167*/ /*          2    ASM_VCTRL_C        (RF3236_MODE)   */
/*MT6167*/ /*          3    ASM_VCTRL_D        (B4PA_EN)       */
/*MT6167*/ /*          4    GGE_PA_ENABLE/MIPI (RF3236_TXEN)   */
/*MT6167*/ /*         17    GSM_ERR_DET_ID     (Pin: 14)       */
/*MT6167*/ /*--------------------------------------------------*/
/*MT6167*/
/*MT6167*/ /*------------------------------------------------------*/
/*MT6167*/ /*  GSM_ERR_DET_ID(Pin:14) has no dedicate output pin,  */
/*MT6167*/ /*  and it is mapped to bit "17" for SW control.        */
/*MT6167*/ /*  For accurate RF conflict detection, this value must */
/*MT6167*/ /*  set "17" and is unchangable.                        */
/*MT6167*/ /*------------------------------------------------------*/
/*MT6167*/ #define  GSM_ERR_DET_ID         (               17) /* For accurate RF conflict detection, this value must set "17" */
/*MT6167*/                                                     /* and is unchangable.                                          */           
/*MT6167*/ #define  PDATA_GSM_ERR_DET      (1<<GSM_ERR_DET_ID)  
/*MT6167*/
/*MT6167*/ #define  PDATA_GMSK              0x00000
/*MT6167*/ #define  PDATA_8PSK              0x00000
/*MT6167*/ 
/*MT6167*/ /*SKY77590*/ #ifdef   SKY77590
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PR1       (0x00           |PDATA_GSM_ERR_DET) /* Co-banding with 3G B5 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PR2       (0x01           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PR3       (0x00                             )
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PR1          (0x00           |PDATA_GSM_ERR_DET) /* Co-banding with 3G B8 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PR2          (0x04           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PR3          (0x00                             )
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PR1          (0x00           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PR2          (0x02           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PR3          (0x00                             )
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PR1          (0x00           |PDATA_GSM_ERR_DET) /* Co-banding with 3G B2 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PR2          (0x02           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PR3          (0x00                             )
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PT1       (0x00           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x82 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PT2       (0x02           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x82 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PT2B      (0x12           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PT3       (0x00                             )
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PT2M1_G8  (0x12|PDATA_8PSK|PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PT2M2_G8  (0x12|PDATA_8PSK|PDATA_GSM_ERR_DET) 
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PT2M1_8G  (0x02           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM850_PT2M2_8G  (0x12           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PT1          (0x00           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x82 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PT2          (0x02           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x82 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PT2B         (0x12           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PT3          (0x00                             )
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PT2M1_G8     (0x12|PDATA_8PSK|PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PT2M2_G8     (0x12|PDATA_8PSK|PDATA_GSM_ERR_DET) 
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PT2M1_8G     (0x02           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_GSM_PT2M2_8G     (0x12           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PT1          (0x00           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x81 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PT2          (0x03           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x81 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PT2B         (0x13           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PT3          (0x00                             )
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PT2M1_G8     (0x13|PDATA_8PSK|PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PT2M2_G8     (0x13|PDATA_8PSK|PDATA_GSM_ERR_DET) 
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PT2M1_8G     (0x03           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_DCS_PT2M2_8G     (0x13           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PT1          (0x00           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x81 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PT2          (0x03           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x81 */
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PT2B         (0x13           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PT3          (0x00                             )
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PT2M1_G8     (0x13|PDATA_8PSK|PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PT2M2_G8     (0x13|PDATA_8PSK|PDATA_GSM_ERR_DET) 
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PT2M1_8G     (0x03           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_PCS_PT2M2_8G     (0x13           |PDATA_GSM_ERR_DET)
/*MT6167*/ /*SKY77590*/ #define  PDATA_INIT             (0x00                             )
/*MT6167*/ /*SKY77590*/ #define  PDATA_IDLE             (0x00                             )
/*MT6167*/
/*MT6167*/ #else
/*MT6167*/ #define  PDATA_GSM850_PR1       (0x00           |PDATA_GSM_ERR_DET) /* Co-banding with 3G B5 */
/*MT6167*/ #define  PDATA_GSM850_PR2       (0x06           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_GSM850_PR3       (0x00                             )
/*MT6167*/ #define  PDATA_GSM_PR1          (0x00           |PDATA_GSM_ERR_DET) /* Co-banding with 3G B8 */
/*MT6167*/ #define  PDATA_GSM_PR2          (0x02           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_GSM_PR3          (0x00                             )
/*MT6167*/ #define  PDATA_DCS_PR1          (0x00           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_DCS_PR2          (0x05           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_DCS_PR3          (0x00                             )
/*MT6167*/ #define  PDATA_PCS_PR1          (0x00           |PDATA_GSM_ERR_DET) /* Co-banding with 3G B2 */
/*MT6167*/ #define  PDATA_PCS_PR2          (0x04           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_PCS_PR3          (0x00                             )
/*MT6167*/ #define  PDATA_GSM850_PT1       (0x00           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x82 */
/*MT6167*/ #define  PDATA_GSM850_PT2       (0x03           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x82 */
/*MT6167*/ #define  PDATA_GSM850_PT2B      (0x12           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_GSM850_PT3       (0x00                             )
/*MT6167*/ #define  PDATA_GSM850_PT2M1_G8  (0x12|PDATA_8PSK|PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_GSM850_PT2M2_G8  (0x12|PDATA_8PSK|PDATA_GSM_ERR_DET) 
/*MT6167*/ #define  PDATA_GSM850_PT2M1_8G  (0x02           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_GSM850_PT2M2_8G  (0x12           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_GSM_PT1          (0x00           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x82 */
/*MT6167*/ #define  PDATA_GSM_PT2          (0x03           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x82 */
/*MT6167*/ #define  PDATA_GSM_PT2B         (0x12           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_GSM_PT3          (0x00                             )
/*MT6167*/ #define  PDATA_GSM_PT2M1_G8     (0x12|PDATA_8PSK|PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_GSM_PT2M2_G8     (0x12|PDATA_8PSK|PDATA_GSM_ERR_DET) 
/*MT6167*/ #define  PDATA_GSM_PT2M1_8G     (0x02           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_GSM_PT2M2_8G     (0x12           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_DCS_PT1          (0x00           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x81 */
/*MT6167*/ #define  PDATA_DCS_PT2          (0x03           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x81 */
/*MT6167*/ #define  PDATA_DCS_PT2B         (0x11           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_DCS_PT3          (0x00                             )
/*MT6167*/ #define  PDATA_DCS_PT2M1_G8     (0x11|PDATA_8PSK|PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_DCS_PT2M2_G8     (0x11|PDATA_8PSK|PDATA_GSM_ERR_DET) 
/*MT6167*/ #define  PDATA_DCS_PT2M1_8G     (0x01           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_DCS_PT2M2_8G     (0x11           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_PCS_PT1          (0x00           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x81 */
/*MT6167*/ #define  PDATA_PCS_PT2          (0x03           |PDATA_GSM_ERR_DET) /* 0x83 for good isolation, ori:0x81 */
/*MT6167*/ #define  PDATA_PCS_PT2B         (0x11           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_PCS_PT3          (0x00                             )
/*MT6167*/ #define  PDATA_PCS_PT2M1_G8     (0x11|PDATA_8PSK|PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_PCS_PT2M2_G8     (0x11|PDATA_8PSK|PDATA_GSM_ERR_DET) 
/*MT6167*/ #define  PDATA_PCS_PT2M1_8G     (0x01           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_PCS_PT2M2_8G     (0x11           |PDATA_GSM_ERR_DET)
/*MT6167*/ #define  PDATA_INIT             (0x00                             )
/*MT6167*/ #define  PDATA_IDLE             (0x00                             )
/*MT6167*/ #endif
/*MT6167*/
/*MT6167*//*----------------------------------------------*/
/*MT6167*//*   APC Compensate Thresholds                  */
/*MT6167*//*----------------------------------------------*/
/*MT6167*/
/*MT6167*/ #define    SECONDS2FRAME(n)                     ((int)((n)*1000/4.615))
/*MT6167*/ #define    VOLT2UVOLT(n)                        ((int)((n)*1000000))
/*MT6167*/ #define    TEMP2MTEMP(n)                        ((int)((n)*1000))
/*MT6167*/
/*MT6167*/ #define    BAT_VOLTAGE_SAMPLE_PERIOD            SECONDS2FRAME(180)
/*MT6167*/ #define    BAT_VOLTAGE_AVERAGE_COUNT            1
/*MT6167*/ #define    BAT_LOW_VOLTAGE                      VOLT2UVOLT(3.5)
/*MT6167*/ #define    BAT_HIGH_VOLTAGE                     VOLT2UVOLT(4.0)
/*MT6167*/
/*MT6167*/ #define    BAT_TEMPERATURE_SAMPLE_PERIOD        SECONDS2FRAME(180)
/*MT6167*/ #define    BAT_TEMPERATURE_AVERAGE_COUNT        1
/*MT6167*/ #define    BAT_LOW_TEMPERATURE                  TEMP2MTEMP(0)
/*MT6167*/ #define    BAT_HIGH_TEMPERATURE                 TEMP2MTEMP(50)
/*MT6167*/
/*MT6167*/ #define    RF_TEMPERATURE_SAMPLE_PERIOD         SECONDS2FRAME(1)
/*MT6167*/ #define    RF_TEMPERATURE_AVERAGE_COUNT         1
/*MT6167*/
/*MT6167*//*----------------------------------------------*/
/*MT6167*//*   Crystal parameter                          */
/*MT6167*//*----------------------------------------------*/
/*MT6167*/ #if  IS_AFC_VCXO_SUPPORT
/*MT6167*/ #define Custom_RF_XO_CapID   150 /* RF SOP, Range:0~255 */
/*MT6167*/ #else
/*MT6167*/ #define Custom_RF_XO_CapID   0   /* For MT6167 with VCTCXO */
/*MT6167*/ #endif
/*MT6167*/
/*MT6167*/ /**************************************/
/*MT6167*/ /* Define your band mode selection on */
/*MT6167*/ /* High Band and Low Band receivers   */
/*MT6167*/ /*        LNA_1 : High Band           */
/*MT6167*/ /*        LNA_2 : Low  Band           */
/*MT6167*/ /*        LNA_3 : High Band           */
/*MT6167*/ /*        LNA_4 : Low  Band           */
/*MT6167*/ /*        LNA_5 : High Band           */
/*MT6167*/ /**************************************/
/*MT6167*/
/*MT6167*/ #define GSM850_PATH_SEL LNA_2 /* co-band with 3G band5 */
/*MT6167*/ #define GSM_PATH_SEL    LNA_4 /* co-band with 3G band8 */
/*MT6167*/ #define DCS_PATH_SEL    LNA_5
/*MT6167*/ #define PCS_PATH_SEL    LNA_1 /* co-band with 3G band2 */
/*MT6167*/
/*MT6167*//*======================================================================================== */
/*MT6167*/
/*MT6167*/ /*----------------------------------------------*/
/*MT6167*/ /*   TX Power Control (TXPC) Support            */
/*MT6167*/ /*----------------------------------------------*/
/*MT6167*/
/*MT6167*/ #define IS_BSI_CLOSED_LOOP_TXPC_ON      0
/*MT6167*/
/*MT6167*/ #define TXPC_EPSK_TP_SLOPE_LB          ((25<<8)+29) /* Unit: degree/dB. Temperature increment that causes 1-dB EPSK TX power drop */
/*MT6167*/ #define TXPC_EPSK_TP_SLOPE_HB          ((30<<8)+27) /* Two slope method : [( temp<20:slpoe1)<<8 + (temp>=20:slpoe2)], slope must < 256 */
/*MT6167*/
/*MT6167*/ /*----------------------------------------------*/
/*MT6167*/ /*   DCXO LPM parameter                         */
/*MT6167*/ /*----------------------------------------------*/
/*MT6167*/ #define CUSTOM_CLOAD_FREQ_OFFSET   88940 /*in unit of Hz*/
/*MT6167*/
/*MT6167*/ /*----------------------------------------------------*/
/*MT6167*/ /*   Enable or disable the clock1, 2, 3, and 4 output */
/*MT6167*/ /*   1 : Enable                                       */
/*MT6167*/ /*   0 : Disable                                      */
/*MT6167*/ /*----------------------------------------------------*/
/*MT6167*/ #define CLK1_EN                         1 /* CLK1 is enabled for BB */
/*MT6167*/ #define CLK2_EN                         0
/*MT6167*/ #define CLK3_EN                         0
/*MT6167*/ #define CLK4_EN                         1
/*MT6167*/
/*MT6167*/ /*----------------------------------------------*/
/*MT6167*/ /*   TX power rollback parameter                */
/*MT6167*/ /*----------------------------------------------*/
/*MT6167*/ /*Unit: 1/8 dB*/
/*MT6167*/ /*GSM850 GMSK*/
/*MT6167*/ #define GSM850_TX_ROLLBACK_2T_GMSK      8
/*MT6167*/ #define GSM850_TX_ROLLBACK_3T_GMSK     24
/*MT6167*/ #define GSM850_TX_ROLLBACK_4T_GMSK     32
/*MT6167*/ #define GSM850_TX_ROLLBACK_5T_GMSK     40
/*MT6167*/ 
/*MT6167*/ /*GSM GMSK*/
/*MT6167*/ #define GSM_TX_ROLLBACK_2T_GMSK         8
/*MT6167*/ #define GSM_TX_ROLLBACK_3T_GMSK        24
/*MT6167*/ #define GSM_TX_ROLLBACK_4T_GMSK        32
/*MT6167*/ #define GSM_TX_ROLLBACK_5T_GMSK        40
/*MT6167*/ 
/*MT6167*/ /*DCS GMSK*/
/*MT6167*/ #define DCS_TX_ROLLBACK_2T_GMSK         8
/*MT6167*/ #define DCS_TX_ROLLBACK_3T_GMSK        24
/*MT6167*/ #define DCS_TX_ROLLBACK_4T_GMSK        32
/*MT6167*/ #define DCS_TX_ROLLBACK_5T_GMSK        40
/*MT6167*/ 
/*MT6167*/ /*PCS GMSK*/
/*MT6167*/ #define PCS_TX_ROLLBACK_2T_GMSK         8
/*MT6167*/ #define PCS_TX_ROLLBACK_3T_GMSK        24
/*MT6167*/ #define PCS_TX_ROLLBACK_4T_GMSK        32
/*MT6167*/ #define PCS_TX_ROLLBACK_5T_GMSK        40
/*MT6167*/ 
/*MT6167*/ /*GSM850 EPSK*/
/*MT6167*/ #define GSM850_TX_ROLLBACK_2T_EPSK      8
/*MT6167*/ #define GSM850_TX_ROLLBACK_3T_EPSK     24
/*MT6167*/ #define GSM850_TX_ROLLBACK_4T_EPSK     32
/*MT6167*/ #define GSM850_TX_ROLLBACK_5T_EPSK     40
/*MT6167*/ 
/*MT6167*/ /*GSM EPSK*/
/*MT6167*/ #define GSM_TX_ROLLBACK_2T_EPSK         8
/*MT6167*/ #define GSM_TX_ROLLBACK_3T_EPSK        24
/*MT6167*/ #define GSM_TX_ROLLBACK_4T_EPSK        32
/*MT6167*/ #define GSM_TX_ROLLBACK_5T_EPSK        40
/*MT6167*/ 
/*MT6167*/ /*DCS EPSK*/
/*MT6167*/ #define DCS_TX_ROLLBACK_2T_EPSK         8
/*MT6167*/ #define DCS_TX_ROLLBACK_3T_EPSK        24
/*MT6167*/ #define DCS_TX_ROLLBACK_4T_EPSK        32
/*MT6167*/ #define DCS_TX_ROLLBACK_5T_EPSK        40
/*MT6167*/ 
/*MT6167*/ /*PCS EPSK*/
/*MT6167*/ #define PCS_TX_ROLLBACK_2T_EPSK         8
/*MT6167*/ #define PCS_TX_ROLLBACK_3T_EPSK        24
/*MT6167*/ #define PCS_TX_ROLLBACK_4T_EPSK        32
/*MT6167*/ #define PCS_TX_ROLLBACK_5T_EPSK        40
/*MT6167*/ /*============================================================================== */
#endif

