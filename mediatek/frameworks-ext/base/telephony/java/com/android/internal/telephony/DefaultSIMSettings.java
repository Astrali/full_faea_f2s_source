/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*
 *
 */

package com.android.internal.telephony;

import static android.Manifest.permission.READ_PHONE_STATE;
import android.app.ActivityManagerNative;
import android.util.Log;
import android.content.Context;
import android.content.Intent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.provider.Settings;
import android.provider.Telephony.SIMInfo;
import android.provider.Telephony.SimInfo;
import android.net.ConnectivityManager;
import android.net.sip.SipManager;
import android.os.SystemProperties;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import java.util.List;
import android.os.UserHandle;

import com.mediatek.common.featureoption.FeatureOption;
import com.mediatek.common.MediatekClassFactory;
import com.mediatek.common.telephony.ITelephonyExt;
import com.android.internal.telephony.gemini.GeminiPhone;

public class DefaultSIMSettings {
    private static final String LOG_TAG = "PHONE";

    /* SIM inserted status constants */
    private static final int STATUS_NO_SIM_INSERTED = 0x00;
    private static final int STATUS_SIM1_INSERTED = 0x01;    
    private static final int STATUS_SIM2_INSERTED = 0x02;        
    private static final int STATUS_DUAL_SIM_INSERTED = STATUS_SIM1_INSERTED | STATUS_SIM2_INSERTED;

    public static final String ACTION_ON_SIM_DETECTED = "ACTION_ON_SIM_DETECTED";

    public static final String INTENT_KEY_DETECT_STATUS = "simDetectStatus";
    public static final String INTENT_KEY_SIM_COUNT = "simCount";
    public static final String INTENT_KEY_NEW_SIM_SLOT = "newSIMSlot";
    public static final String INTENT_KEY_NEW_SIM_STATUS = "newSIMStatus";
    public static final String EXTRA_VALUE_NEW_SIM = "NEW";
    public static final String EXTRA_VALUE_REMOVE_SIM = "REMOVE";
    public static final String EXTRA_VALUE_SWAP_SIM = "SWAP";

    synchronized public static void onAllIccidQueryComplete(Context context, Phone phone,
        String iccid1, String iccid2, boolean is3GSwitched)
    {
        logd("onAllIccidQueryComplete start");
        ContentResolver contentResolver = context.getContentResolver();
        String oldIccIdInSlot1 = null;
        String oldIccIdInSlot2 = null;
        iccid1 = iccid1 == null ? "" : iccid1;
        iccid2 = iccid2 == null ? "" : iccid2;
        boolean isSIM1Inserted = !"".equals(iccid1);
        boolean isSIM2Inserted = !"".equals(iccid2);

        ITelephonyExt telephonyExt = null;
        try {
            telephonyExt = MediatekClassFactory.createInstance(ITelephonyExt.class);
        } catch (Exception e){
            e.printStackTrace();
        }

        SIMInfo oldSimInfo1 = SIMInfo.getSIMInfoBySlot(context, 0);
        if (oldSimInfo1 != null) {
            oldIccIdInSlot1 = oldSimInfo1.mICCId;
            logd("onAllIccidQueryComplete old IccId In Slot0 is " + oldIccIdInSlot1);
            if (!isSIM1Inserted || !iccid1.equals(oldIccIdInSlot1)) {
                ContentValues value = new ContentValues(1);
                value.put(SimInfo.SLOT, -1);
                contentResolver.update(ContentUris.withAppendedId(SimInfo.CONTENT_URI, oldSimInfo1.mSimId), 
                        value, null, null);
                logd("onAllIccidQueryComplete reset Slot0 to -1, iccid1 = " + iccid1 + " ");
            }
        } else {
            logd("onAllIccidQueryComplete No sim in slot0 for last time " );
        }

        SIMInfo oldSimInfo2 = SIMInfo.getSIMInfoBySlot(context, 1);
        if (oldSimInfo2 != null) {
            oldIccIdInSlot2 = oldSimInfo2.mICCId;
            logd("onAllIccidQueryComplete old IccId In Slot1 is " + oldIccIdInSlot2);
            if(!isSIM2Inserted || !iccid2.equals(oldIccIdInSlot2)) {
                ContentValues value = new ContentValues(1);
                value.put(SimInfo.SLOT, -1);
                contentResolver.update(ContentUris.withAppendedId(SimInfo.CONTENT_URI, oldSimInfo2.mSimId), 
                        value, null, null); 
                logd("onAllIccidQueryComplete reset Slot1 to -1, iccid2 = " + iccid2 + " ");
            }
        } else {
            logd("onAllIccidQueryComplete No sim in slot1 for last time " );

        }

        oldIccIdInSlot1 = oldIccIdInSlot1 == null ? "" : oldIccIdInSlot1;
        oldIccIdInSlot2 = oldIccIdInSlot2 == null ? "" : oldIccIdInSlot2;

        //check if the Inserted sim is new
        int nNewCardCount = 0;
        int nNewSIMStatus = 0;
        if (iccid1 == null || iccid1.equals("")) {
            logd("onAllIccidQueryComplete No SIM inserted in Slot 0, set the slot for Removed SIM to NONE " );
        } else {
            if (iccid1.startsWith("ff") || iccid1.equals(iccid2)) {
                //some special SIM cards have no invalid ICCID. so we add a suffix to prove that we can show two SIM cards 
                //even if two SIMs of that kind are inserted at the same time.  So this kind of SIM will be always treated as a new SIM.
                SIMInfo.insertICCId(context, iccid1+"1", 0); 
                logd("onAllIccidQueryComplete special SIM with invalid ICCID is inserted in slot1" );
            } else if (!iccid1.equals(oldIccIdInSlot1)) {
                SIMInfo.insertICCId(context, iccid1, 0); 
            }
            if (!iccid1.equals(oldIccIdInSlot1)) {
                if (!FeatureOption.MTK_GEMINI_SUPPORT || (FeatureOption.MTK_GEMINI_SUPPORT && !iccid1.equals(oldIccIdInSlot2))) {
                    //one new card inserted into slot1
                    nNewCardCount++;                      
                    nNewSIMStatus |= STATUS_SIM1_INSERTED;
                }
            }       
        }

        if (iccid2 == null || iccid2.equals("")) {
            logd("onAllIccidQueryComplete No SIM inserted in Slot 1, set the slot for Removed SIM to NONE " );
        } else {
            if (iccid2.startsWith("ff") || iccid2.equals(iccid1)) {
                SIMInfo.insertICCId(context, iccid2+ "2", 1);
                logd("onAllIccidQueryComplete special SIM with invalid ICCID is inserted in slot2" );
            } else if (!iccid2.equals(oldIccIdInSlot2)) {
                SIMInfo.insertICCId(context, iccid2, 1);
            }
            if (!iccid2.equals(oldIccIdInSlot2)) {
                if (!FeatureOption.MTK_GEMINI_SUPPORT || (FeatureOption.MTK_GEMINI_SUPPORT && !iccid2.equals(oldIccIdInSlot1))){
                    //one new card inserted into slot1
                    nNewCardCount++;                     
                    nNewSIMStatus |= STATUS_SIM2_INSERTED;
                }
            }
        }

        long simIdForSlot1 = -3;
        long simIdForSlot2 = -3; 
        List<SIMInfo> simInfos = SIMInfo.getInsertedSIMList(context);
        int nSIMCount = (simInfos == null ? 0 : simInfos.size());
        for (int i=0; i<nSIMCount; i++) {
            SIMInfo temp = simInfos.get(i);
            if (temp.mSlot == PhoneConstants.GEMINI_SIM_1){
                simIdForSlot1 = temp.mSimId;
            } else if (temp.mSlot == PhoneConstants.GEMINI_SIM_2) {
                simIdForSlot2 = temp.mSimId;
            }           
        }
        logd("onAllIccidQueryComplete simIdForSlot [" + simIdForSlot1 + ", " + simIdForSlot2 + "]");

        if (nNewCardCount > 0 ) {   
            logd("onAllIccidQueryComplete New SIM detected. " );
            setColorForNewSIM(simInfos, context);
            int airplaneMode = Settings.Global.getInt(contentResolver, Settings.Global.AIRPLANE_MODE_ON, 0);
            if (airplaneMode > 0) {
                setDefaultNameForAllNewSIM(simInfos, context);
            } else {
                setDefaultNameIfImsiReadyOrLocked(simInfos, context);
            }
        }

        //if (FeatureOption.MTK_GEMINI_ENHANCEMENT) {
        if (!FeatureOption.MTK_BSP_PACKAGE) {
            //get all default SIM setting
            long oldVTDefaultSIM = Settings.System.getLong(contentResolver, Settings.System.VIDEO_CALL_SIM_SETTING, Settings.System.DEFAULT_SIM_NOT_SET);
            long oldVoiceCallDefaultSIM =  Settings.System.getLong(contentResolver, 
                    Settings.System.VOICE_CALL_SIM_SETTING, Settings.System.DEFAULT_SIM_NOT_SET);
            long oldSmsDefaultSIM = Settings.System.getLong(contentResolver, Settings.System.SMS_SIM_SETTING, Settings.System.DEFAULT_SIM_NOT_SET); 
            long oldGprsDefaultSIM = Settings.System.getLong(contentResolver, Settings.System.GPRS_CONNECTION_SIM_SETTING, Settings.System.DEFAULT_SIM_NOT_SET); 

            if (FeatureOption.MTK_VT3G324M_SUPPORT) {            
                long nVTDefSIM = Settings.System.DEFAULT_SIM_NOT_SET;
                int n3gSIMSlot = phone.get3GCapabilitySIM();
                if (FeatureOption.MTK_GEMINI_3G_SWITCH) {
                    if (n3gSIMSlot == PhoneConstants.GEMINI_SIM_2 && isSIM2Inserted) {
                        nVTDefSIM = simIdForSlot2;
                    } else if (n3gSIMSlot == PhoneConstants.GEMINI_SIM_1 && isSIM1Inserted) {
                        nVTDefSIM = simIdForSlot1;
                    }
                } else {
                    if(isSIM1Inserted) {
                        nVTDefSIM = simIdForSlot1;
                    }
                }

                Settings.System.putLong(contentResolver, Settings.System.VIDEO_CALL_SIM_SETTING, nVTDefSIM);
            }

            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
            if (nSIMCount > 1) {
                if (oldVoiceCallDefaultSIM == Settings.System.DEFAULT_SIM_NOT_SET) {
                    Settings.System.putLong(contentResolver, Settings.System.VOICE_CALL_SIM_SETTING, 
                                               Settings.System.DEFAULT_SIM_SETTING_ALWAYS_ASK);
                }

                if (oldSmsDefaultSIM == Settings.System.DEFAULT_SIM_NOT_SET) {
                    Settings.System.putLong(contentResolver, Settings.System.SMS_SIM_SETTING, 
                                                Settings.System.DEFAULT_SIM_SETTING_ALWAYS_ASK);
                }
                
                if (oldGprsDefaultSIM == Settings.System.DEFAULT_SIM_NOT_SET) {
                    if (telephonyExt.isDefaultDataOn()) {
                        if (is3GSwitched)
                            connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_2);
                        else
                            connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_1);
                    } else {
                        connectivityManager.setMobileDataEnabled(false);
                    }
                }
            } else if (nSIMCount == 1) {
                long simId = simInfos.get(0).mSimId;
                int isVoipEnabled = Settings.System.getInt(contentResolver, Settings.System.ENABLE_INTERNET_CALL, 0);
                if (!(SipManager.isVoipSupported(context) && isVoipEnabled != 0) ||
                    (oldVoiceCallDefaultSIM == Settings.System.DEFAULT_SIM_NOT_SET))
                {
                    Settings.System.putLong(contentResolver, Settings.System.VOICE_CALL_SIM_SETTING, simId);
                }

                Settings.System.putLong(contentResolver, Settings.System.SMS_SIM_SETTING, simId);

                // [mtk02772]
                // TODO: Data connection has different behavior between Gemini and non-Gemini,
                //       This part should be extracted to itself from SIM framework
                if (FeatureOption.MTK_GEMINI_SUPPORT == true) {   
                    if (oldGprsDefaultSIM == Settings.System.DEFAULT_SIM_NOT_SET) {
                        if (telephonyExt.isDefaultDataOn()) {
                            connectivityManager.setMobileDataEnabledGemini(simInfos.get(0).mSlot);
                        } else {
                            connectivityManager.setMobileDataEnabled(false);
                        }
                    }
                } else {
                    if(oldGprsDefaultSIM == Settings.System.DEFAULT_SIM_NOT_SET){
                        Settings.System.putLong(contentResolver, Settings.System.GPRS_CONNECTION_SIM_SETTING, simId);
                    }   
                }
            }

            boolean hasSIMRemoved = (iccid1.equals("") && !oldIccIdInSlot1.equals("")) || (iccid2.equals("") && !oldIccIdInSlot2.equals(""));
            logd("onAllIccidQueryComplete. handling SIM detect dialog [" + iccid1 + ", " + iccid2 + ", " + oldIccIdInSlot1 + ", " + oldIccIdInSlot2
                + ", " + nNewCardCount + ", " + hasSIMRemoved + ", " + nSIMCount + "]");

            long defSIM = Settings.System.DEFAULT_SIM_NOT_SET;
            if (nSIMCount > 1) {
                defSIM = Settings.System.DEFAULT_SIM_SETTING_ALWAYS_ASK;  
            } else if(nSIMCount == 1) {
                defSIM = simInfos.get(0).mSimId;
            }

            if (isSIMRemoved(oldVoiceCallDefaultSIM,simIdForSlot1,simIdForSlot2)) {
                Settings.System.putLong(contentResolver, Settings.System.VOICE_CALL_SIM_SETTING, defSIM);
            }

            if (isSIMRemoved(oldSmsDefaultSIM,simIdForSlot1,simIdForSlot2)) {
                Settings.System.putLong(contentResolver, Settings.System.SMS_SIM_SETTING, defSIM);
            }

            // [mtk02772]
            // TODO: Data connection has different behavior between Gemini and non-Gemini,
            //       This part should be extracted to itself from SIM framework
            if (FeatureOption.MTK_GEMINI_SUPPORT == true) { 
                if (isSIMRemoved(oldGprsDefaultSIM,simIdForSlot1,simIdForSlot2)) {
                    if (telephonyExt.isDefaultDataOn()) {
                        if (nSIMCount > 1) {
                            if (is3GSwitched)
                                connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_2);
                            else
                                connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_1);
                        } else {
                            if (nSIMCount > 0)
                                connectivityManager.setMobileDataEnabledGemini(simInfos.get(0).mSlot);
                        }
                    } else {
                        connectivityManager.setMobileDataEnabled(false);
                    }
                } else if (telephonyExt.isDefaultEnable3GSIMDataWhenNewSIMInserted()) {
                    if (oldGprsDefaultSIM > 0) {
                        if (nNewCardCount > 0 || (!iccid1.equals("") && iccid1.equals(oldIccIdInSlot2)) || (!iccid2.equals("") && iccid2.equals(oldIccIdInSlot1))) {
                            logd("onAllIccidQueryComplete. SIM swapped and data on, default switch to 3G SIM");
                            if (nSIMCount > 1) {
                                if (is3GSwitched)
                                    connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_2);
                                else
                                    connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_1);
                            } else {
                                if (nSIMCount > 0)
                                    connectivityManager.setMobileDataEnabledGemini(simInfos.get(0).mSlot);
                            }
                        }
                    } else {
                        if (nNewCardCount > 0 && nNewCardCount == nSIMCount) {
                            logd("onAllIccidQueryComplete. All SIM new, data off and default switch data to 3G SIM");
                            if (nSIMCount > 1) {
                                if (is3GSwitched)
                                    connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_2);
                                else
                                    connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_1);
                            } else {
                                if (nSIMCount > 0)
                                    connectivityManager.setMobileDataEnabledGemini(simInfos.get(0).mSlot);
                            }
                        }
                    }
                }
            } else { // non-Gemini 
                if (isSIMRemoved(oldGprsDefaultSIM,simIdForSlot1,simIdForSlot2)) {
                    Settings.System.putLong(contentResolver, Settings.System.GPRS_CONNECTION_SIM_SETTING, 
                                                              Settings.System.GPRS_CONNECTION_SIM_SETTING_NEVER);  
                }
            }

            if (nNewCardCount == 0) {
                if (!hasSIMRemoved && iccid1.equals(oldIccIdInSlot1) && iccid2.equals(oldIccIdInSlot2)) {
                    logd("onAllIccidQueryComplete. all SIM inserted into the same slot");
                } else {
                    if ((!iccid1.equals("") && iccid1.equals(oldIccIdInSlot2)) || (!iccid2.equals("") && iccid2.equals(oldIccIdInSlot1))) {
                        logd("onAllIccidQueryComplete. SIM swapped");
                        onSIMDetected(context, EXTRA_VALUE_SWAP_SIM, nSIMCount, nNewSIMStatus);
                    } else if (nSIMCount > 0) {
                        logd("onAllIccidQueryComplete No new SIM detected and Default SIM for some service has been removed[A]" );
                        onSIMDetected(context, EXTRA_VALUE_REMOVE_SIM, nSIMCount, nNewSIMStatus);
                    }
                }
            } else {
                logd("getAllIccIdsDone. New SIM detected.");
                onSIMDetected(context, EXTRA_VALUE_NEW_SIM, nSIMCount, nNewSIMStatus);
            }

            long gprsDefaultSIM = Settings.System.getLong(contentResolver, Settings.System.GPRS_CONNECTION_SIM_SETTING, Settings.System.DEFAULT_SIM_NOT_SET);
            if (gprsDefaultSIM != Settings.System.DEFAULT_SIM_NOT_SET && gprsDefaultSIM != Settings.System.GPRS_CONNECTION_SIM_SETTING_NEVER) {
                int slot = SIMInfo.getSlotById(context, gprsDefaultSIM);            
                if (slot != SimInfo.SLOT_NONE) {
                    if (FeatureOption.MTK_GEMINI_SUPPORT)
                        ((GeminiPhone)phone).setGprsConnType(1, slot);
                    connectivityManager.setMobileDataEnabledGemini(slot);
                } else {
                    logd("onAllIccidQueryComplete: gprsDefaultSIM does not exist in slot then skip.");
                }
            }
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
            int gprsDefaultSlot = Settings.System.getInt(contentResolver, Settings.System.GPRS_CONNECTION_SETTING, Settings.System.GPRS_CONNECTION_SETTING_DEFAULT) - 1;
            logd("onAllIccidQueryComplete original data settings: " + gprsDefaultSlot);
            if (gprsDefaultSlot == Settings.System.DEFAULT_SIM_NOT_SET_INT) {
                if (telephonyExt.isDefaultDataOn()) {
                    if (is3GSwitched) {
                        if (isSIM2Inserted)
                            connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_2);
                        else if (isSIM1Inserted)
                            connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_1);
                    } else {
                        if (isSIM1Inserted)
                            connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_1);
                        else if (isSIM2Inserted)
                            connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_2);
                    }
                } else {
                    connectivityManager.setMobileDataEnabled(false);
                }
            } else if (gprsDefaultSlot == PhoneConstants.GEMINI_SIM_1 || gprsDefaultSlot == PhoneConstants.GEMINI_SIM_2) {
                if ((isSIM1Inserted && gprsDefaultSlot == PhoneConstants.GEMINI_SIM_1) || (isSIM2Inserted && gprsDefaultSlot == PhoneConstants.GEMINI_SIM_2)) {
                    //data SIM slot has SIM inserted
                    connectivityManager.setMobileDataEnabledGemini(gprsDefaultSlot);
                } else {
                    if (telephonyExt.isDefaultDataOn()) { //data SIM slot has no SIM and default data on
                        if (isSIM1Inserted)
                            connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_1);
                        else if (isSIM2Inserted)
                            connectivityManager.setMobileDataEnabledGemini(PhoneConstants.GEMINI_SIM_2);
                    } else { //data SIM slot has no SIM and default data off
                        connectivityManager.setMobileDataEnabled(false);
                    }
                }
            } else {
                connectivityManager.setMobileDataEnabled(false);
            }

            gprsDefaultSlot = Settings.System.getInt(contentResolver, Settings.System.GPRS_CONNECTION_SETTING, Settings.System.GPRS_CONNECTION_SETTING_DEFAULT) - 1;
            logd("onAllIccidQueryComplete final data settings: " + gprsDefaultSlot);
            if (FeatureOption.MTK_GEMINI_SUPPORT && gprsDefaultSlot == PhoneConstants.GEMINI_SIM_1 || gprsDefaultSlot == PhoneConstants.GEMINI_SIM_2)
                ((GeminiPhone)phone).setGprsConnType(1, gprsDefaultSlot);
        }

        SystemProperties.set(TelephonyProperties.PROPERTY_SIM_INFO_READY, "true");
        logd("onAllIccidQueryComplete PROPERTY_SIM_INFO_READY after set is " + SystemProperties.get(TelephonyProperties.PROPERTY_SIM_INFO_READY, null) );

        Intent intent = new Intent(TelephonyIntents.ACTION_SIM_INFO_UPDATE);
        logd("broadCast intent ACTION_SIM_INFO_UPDATE");
        ActivityManagerNative.broadcastStickyIntent(intent, READ_PHONE_STATE, UserHandle.USER_ALL);
    }

    private static void setDefaultNameForAllNewSIM(List<SIMInfo> simInfos, Context context){
        int nSIMCount = (simInfos == null ? 0 : simInfos.size());
        logd("setDefaultNameForAll nSIMCount  is " + nSIMCount);
        for (int i=0; i<nSIMCount; i++) {
            SIMInfo temp = simInfos.get(i);
            if (temp.mDisplayName == null){
                logd("setDefaultNameForAll set default name for slot" + temp.mSlot);
                SIMInfo.setDefaultName(context,temp.mSimId, null);
            }       
        }       
    }

    private static void setDefaultNameIfImsiReadyOrLocked(List<SIMInfo> simInfos, Context context){
        int nSIMCount = (simInfos == null ? 0 : simInfos.size());
        logd("setDefaultNameIfImsiReadyOrLocked nSIMCount  is " + nSIMCount);
        String operatorName = null;
        for (int i=0; i<nSIMCount; i++) {
            SIMInfo temp = simInfos.get(i);
            if (temp.mDisplayName == null){
                logd("setDefaultNameIfImsiReadyOrLocked the " + i + "th mDisplayName is null ");
                if (PhoneConstants.GEMINI_SIM_1 == temp.mSlot) {
                    operatorName = SystemProperties.get(TelephonyProperties.PROPERTY_ICC_OPERATOR_DEFAULT_NAME);
                } else if (PhoneConstants.GEMINI_SIM_2 == temp.mSlot){
                    operatorName = SystemProperties.get(TelephonyProperties.PROPERTY_ICC_OPERATOR_DEFAULT_NAME_2);
                }
                logd("setDefaultNameIfImsiReadyOrLocked operatorName  is " + operatorName);
                if( operatorName != null && !operatorName.equals("")) {                          
                    SIMInfo.setDefaultName(context,temp.mSimId, operatorName);
                }
            }       
        }       
    }
    
    private static void setColorForNewSIM(List<SIMInfo> simInfos, Context context) {
        int simToSet = 0;
        SIMInfo simInfo1 = SIMInfo.getSIMInfoBySlot(context, 0);
        int sim1Color = -1;
        int sim2Color = -1;
        if (simInfo1!= null) {          
            sim1Color = simInfo1.mColor;
            if(!(0 <= sim1Color && sim1Color <= 3)) {
                simToSet = simToSet | STATUS_SIM1_INSERTED;
            }
            logd("setColorForNewSIM SimInfo sim1Color is " + sim1Color);
        }
        SIMInfo simInfo2 = SIMInfo.getSIMInfoBySlot(context, 1);
        if (simInfo2 != null) {
            sim2Color = simInfo2.mColor;
            if(!(0 <= sim2Color && sim2Color <= 3)) {
                simToSet = simToSet | STATUS_SIM2_INSERTED;
            }
            logd("setColorForNewSIM SimInfo sim2Color is " + sim2Color);          
        }
        logd("simToSet is" + simToSet);
        switch (simToSet){
            case 3:
                ContentValues value1 = new ContentValues(1);
                sim1Color = (int)(simInfo1.mSimId-1) %(PhoneConstants.TOTAL_SIM_COLOR_COUNT);
                value1.put(SimInfo.COLOR, sim1Color);
                context.getContentResolver().update(ContentUris.withAppendedId(SimInfo.CONTENT_URI, simInfo1.mSimId), 
                        value1, null, null);
                logd("case3: setColorForNewSIM SimInfo set color SIM in slot0 to "+ sim1Color);   
                ContentValues value2 = new ContentValues(1);
                sim2Color = (int)(simInfo2.mSimId-1) %(PhoneConstants.TOTAL_SIM_COLOR_COUNT);
                value2.put(SimInfo.COLOR, sim2Color);
                context.getContentResolver().update(ContentUris.withAppendedId(SimInfo.CONTENT_URI, simInfo2.mSimId), 
                        value2, null, null);    
                logd("case3: setColorForNewSIM SimInfo set color SIM in slot1 to " + sim2Color);
                break;
            case 2:
                ContentValues valueColor = new ContentValues(1);
                sim2Color = (int)(simInfo2.mSimId-1) %(PhoneConstants.TOTAL_SIM_COLOR_COUNT);
                if (sim2Color == sim1Color) {
                    sim2Color = (sim1Color + 1)%(PhoneConstants.TOTAL_SIM_COLOR_COUNT);
                }
                valueColor.put(SimInfo.COLOR, sim2Color);
                context.getContentResolver().update(ContentUris.withAppendedId(SimInfo.CONTENT_URI, simInfo2.mSimId), 
                        valueColor, null, null);    
                logd("case2: setColorForNewSIM SimInfo set color SIM in slot1 to " + sim2Color); 
                break;
            case 1:
                ContentValues valueColor1 = new ContentValues(1);
                sim1Color = (int)(simInfo1.mSimId-1) %(PhoneConstants.TOTAL_SIM_COLOR_COUNT);
                if (sim1Color == sim2Color) {
                    sim1Color = (sim2Color + 1)%(PhoneConstants.TOTAL_SIM_COLOR_COUNT);
                }
                valueColor1.put(SimInfo.COLOR, sim1Color);
                context.getContentResolver().update(ContentUris.withAppendedId(SimInfo.CONTENT_URI, simInfo1.mSimId), 
                        valueColor1, null, null);   
                logd("case1:setColorForNewSIM SimInfo set color SIM in slot0 to " + sim1Color); 
                break;
            default:
                logd("No need to set color");
                break;              
        }
    }


    private static boolean isSIMRemoved(long defSIMId,long curSIM1, long curSIM2){     
        if (defSIMId <= 0)
            return false;
        else if ((defSIMId != curSIM1) && (defSIMId != curSIM2))
            return true;
        else
            return false;
    }

    private static void onSIMDetected(Context context, String detectStatus, int nSIMCount, int nNewSIMStatus) {
        Intent intent = new Intent(ACTION_ON_SIM_DETECTED);
        intent.putExtra(INTENT_KEY_SIM_COUNT, nSIMCount);
        intent.putExtra(INTENT_KEY_NEW_SIM_STATUS, nNewSIMStatus);
        intent.putExtra(INTENT_KEY_DETECT_STATUS, detectStatus);
        context.sendBroadcast(intent);
    }

    public static void broadCastNewSIMDetected(int nSIMCount, int nNewSIMSlot) {
        Intent intent = new Intent(TelephonyIntents.ACTION_SIM_DETECTED);
        intent.putExtra(INTENT_KEY_DETECT_STATUS, EXTRA_VALUE_NEW_SIM);
        intent.putExtra(INTENT_KEY_SIM_COUNT, nSIMCount);
        intent.putExtra(INTENT_KEY_NEW_SIM_SLOT, nNewSIMSlot);
        logd("broadCast intent ACTION_SIM_DETECTED [" + EXTRA_VALUE_NEW_SIM + ", " +  nSIMCount + ", " + nNewSIMSlot + "]");
        ActivityManagerNative.broadcastStickyIntent(intent, READ_PHONE_STATE, UserHandle.USER_ALL);
    }

    public static void broadCastDefaultSIMRemoved(int nSIMCount) {
        Intent intent = new Intent(TelephonyIntents.ACTION_SIM_DETECTED);
        intent.putExtra(INTENT_KEY_DETECT_STATUS, EXTRA_VALUE_REMOVE_SIM);
        intent.putExtra(INTENT_KEY_SIM_COUNT, nSIMCount);
        logd("broadCast intent ACTION_SIM_DETECTED [" + EXTRA_VALUE_REMOVE_SIM + ", " +  nSIMCount + "]");
        ActivityManagerNative.broadcastStickyIntent(intent, READ_PHONE_STATE, UserHandle.USER_ALL);
    }
    
    public static void broadCastSIMSwapped(int nSIMCount) {
        Intent intent = new Intent(TelephonyIntents.ACTION_SIM_DETECTED);
        intent.putExtra(INTENT_KEY_DETECT_STATUS, EXTRA_VALUE_SWAP_SIM);
        intent.putExtra(INTENT_KEY_SIM_COUNT, nSIMCount);
        logd("broadCast intent ACTION_SIM_DETECTED [" + EXTRA_VALUE_SWAP_SIM + ", " +  nSIMCount + "]");
        ActivityManagerNative.broadcastStickyIntent(intent, READ_PHONE_STATE, UserHandle.USER_ALL);
    }

    public static void broadCastSIMInsertedStatus(int nSIMInsertStatus) {
        Intent intent = new Intent(TelephonyIntents.ACTION_SIM_INSERTED_STATUS);
        intent.putExtra(INTENT_KEY_SIM_COUNT, nSIMInsertStatus);
        logd("broadCast intent ACTION_SIM_INSERTED_STATUS " +  nSIMInsertStatus);
        ActivityManagerNative.broadcastStickyIntent(intent, READ_PHONE_STATE, UserHandle.USER_ALL);
    }

    private static void logd(String message) {
        Log.d(LOG_TAG, "[DefaultSIMSettings] " + message);
    }
}
