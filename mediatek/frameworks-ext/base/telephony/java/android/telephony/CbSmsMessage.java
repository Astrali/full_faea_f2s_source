/* Copyright Statement: 
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package android.telephony;

import static android.telephony.TelephonyManager.PHONE_TYPE_CDMA;

import android.util.Log;

import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.gsm.GsmCbSmsMessage;

/**
 * {@hide}
 */
public class CbSmsMessage {
    private static final String LOG_TAG = "SMS";
    public GsmCbSmsMessage mWrappedMessage;

    public CbSmsMessage() {
        int activePhone = TelephonyManager.getDefault().getPhoneType();

        if (PHONE_TYPE_CDMA == activePhone) {
            Log.e(LOG_TAG, "We didn't support CB for CDMA phone");
        } else {
            mWrappedMessage = new com.android.internal.telephony.gsm.GsmCbSmsMessage();
        }
    }

    private CbSmsMessage(GsmCbSmsMessage cbSms) {
        mWrappedMessage = cbSms;
    }

    public static boolean isDuplicateMessage(
            int oldSn, int oldMsgId, int oldLac, int oldCid,
            int newSn, int newMsgId, int newLac, int newCid) {
        return GsmCbSmsMessage.isDuplicateMessage(
                oldSn, oldMsgId, oldLac, oldCid, newSn, newMsgId, newLac, newCid);
    }

    /**
     * Create an CbSmsMessage from a raw PDU.
     */
    public static CbSmsMessage createFromPdu(byte[] pdu) {
        GsmCbSmsMessage wrappedMessage = null;
        int activePhone = TelephonyManager.getDefault().getPhoneType();

        if (PHONE_TYPE_CDMA == activePhone) {
            Log.e(LOG_TAG, "We didn't support CB for CDMA phone");
        } else {
            wrappedMessage = com.android.internal.telephony.gsm.GsmCbSmsMessage.createFromPdu(pdu);
        }

        return new CbSmsMessage(wrappedMessage);
    }

    /**
     * @return serial number of message
     */
    public int getSerialNumber() {
        return mWrappedMessage.getSerialNumber();
    }

    /**
     * @return message id
     */
    public int getMessageID() {
        return mWrappedMessage.getMessageID();
    }

    /**
     * @return the text of message
     */
    public String getMessageBody() {
        return mWrappedMessage.getMessageBody();
    }

    /**
     * @return display mode
     */
    public int getDisplayMode() {
        return mWrappedMessage.getDisplayMode();
    }

    /**
     * @return raw pdu
     */
    public byte[] getPdu() {
        return mWrappedMessage.getPdu();
    }

    /**
     * Return the user data header (UDH).
     * 
     * @hide
     */
    public SmsHeader getUserDataHeader() {
        return mWrappedMessage.getUserDataHeader();
    }

    public String toString() {
        return mWrappedMessage.toString();
    }

    /**
     * @return SIM ID, but now it doesn't return a valid value
     */
    public int getMessageSimId() {
        return -1;
    }

}
