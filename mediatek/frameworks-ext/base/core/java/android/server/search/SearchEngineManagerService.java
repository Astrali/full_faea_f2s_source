/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.server.search;

import com.android.internal.content.PackageMonitor;

import android.app.ISearchEngineManager;
import android.app.SearchEngineInfo;
import android.app.SearchEngineManager;
import android.app.SearchableInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.os.Process;
import android.provider.Settings;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * The search engine manager service handles the search UI, and maintains a registry of search engines.
 */
public class SearchEngineManagerService extends ISearchEngineManager.Stub {

    // general debugging support
    private static final String TAG = "SearchEngineManagerService";

    // Context that the service is running in.
    private final Context mContext;

    // This list saved all the search engines supported by search framework.
    private List<SearchEngineInfo> mSearchEngineInfos;

    private SearchEngineInfo mDefaultSearchEngine;

    /**
     * Initializes the Search Manager service in the provided system context.
     * Only one instance of this object should be created!
     *
     * @param context to use for accessing DB, window manager, etc.
     */
    public SearchEngineManagerService(Context context)  {
        mContext = context;
        mContext.registerReceiver(new BootCompletedReceiver(),
                new IntentFilter(Intent.ACTION_BOOT_COMPLETED));
    }
    /**
     * Creates the initial searchables list after boot.
     */
    private final class BootCompletedReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            new Thread() {
                @Override
                public void run() {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                    mContext.unregisterReceiver(BootCompletedReceiver.this);
                    initSearchEngineInfos();
                    mContext.registerReceiver(new LocaleChangeReceiver(),
                            new IntentFilter(Intent.ACTION_LOCALE_CHANGED));
                }
            }.start();
        }
    }
    
    /**
     * Gets the search engines for web search 
     * @hide
     */
    public synchronized List<SearchEngineInfo> getAvailableSearchEngines() {
        Log.i(TAG, "get avilable search engines");
    	if (mSearchEngineInfos == null) {
    		initSearchEngineInfos();
    	}
    	return mSearchEngineInfos;
    }
    
    private void initSearchEngineInfos() throws IllegalArgumentException{
    	mSearchEngineInfos = new ArrayList<SearchEngineInfo>();
        Resources res = mContext.getResources();
        String[] searchEngines = res.getStringArray(com.mediatek.internal.R.array.new_search_engines);
        if (null == searchEngines || 1 >= searchEngines.length) {
            // todo: throws an exception in this case.
            throw new IllegalArgumentException("No data found for ");
        }
        String sp = searchEngines[0];
        for (int i = 1; i < searchEngines.length; i++) {
            String configInfo = searchEngines[i];
            SearchEngineInfo info = SearchEngineInfo.parseFrom(configInfo, sp);
            mSearchEngineInfos.add(info);
        }

        // keep old setting.
        if (mDefaultSearchEngine != null) {
            mDefaultSearchEngine = getBestMatchSearchEngine(mDefaultSearchEngine.getName(), mDefaultSearchEngine.faviconUri());
        }

        if (mDefaultSearchEngine == null) {
            mDefaultSearchEngine = mSearchEngineInfos.get(0);
        }
        // tell search widget that search engine changed.
        broadcastSearchEngineChangedInternal(mContext);
    }
    
    /**
     * Creates the initial searchables list after boot.
     */
    private final class LocaleChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
        	initSearchEngineInfos();
        }
    }

    /**
     * Sent a broadcast without extral data.
     */
    private void broadcastSearchEngineChangedInternal(Context context) {
        Intent intent = new Intent(SearchEngineManager.ACTION_SEARCH_ENGINE_CHANGED);
        context.sendBroadcast(intent);
    }

    /**
     * Find a search engine which comes from same provider.
     */
    public SearchEngineInfo getBestMatchSearchEngine(String name, String favicon) {
        SearchEngineInfo engine = getSearchEngineByName(name);
        return (engine != null) ? engine : getSearchEngineByFavicon(favicon);
    }

    /**
     * Get search engine by favicon uri.
     */
    private SearchEngineInfo getSearchEngineByFavicon(String favicon) {
        List<SearchEngineInfo> engines = getAvailableSearchEngines();
        for (SearchEngineInfo engine : engines) {
            if (favicon.equals(engine.faviconUri())) {
                return engine;
            }
        }
        return null;
    }

    /**
     * Get search engine by name.
     */
    private SearchEngineInfo getSearchEngineByName(String name) {
        List<SearchEngineInfo> engines = getAvailableSearchEngines();
        for (SearchEngineInfo engine : engines) {
            if (name.equals(engine.getName())) {
                return engine;
            }
        }
        return null;
    }

    /**
     * Return search engine by specified field.
     */
    public SearchEngineInfo getSearchEngineBy(int field, String value) {
        switch (field) {
            case SearchEngineInfo.NAME:
                return getSearchEngineByName(value);
            case SearchEngineInfo.FAVICON:
                return getSearchEngineByFavicon(value);
            default:
                return null;
        }
    }

    /**
     * Return the first item in config file as system default search engine.
     */
    public SearchEngineInfo getDefaultSearchEngine() {
        return mDefaultSearchEngine;
    }

    /**
     * Set default search engine.
     * the engine must in available engine list.
     */
    public boolean setDefaultSearchEngine(SearchEngineInfo engine) {
        List<SearchEngineInfo> engines = getAvailableSearchEngines();
        for (SearchEngineInfo eng : engines) {
            if (eng.getName().equals(engine.getName())) {
                mDefaultSearchEngine = engine;
                return true;
            }
        }
        return false;
    }
}
