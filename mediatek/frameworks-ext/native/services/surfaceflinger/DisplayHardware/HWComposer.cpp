/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifdef MTK_HWC_SUPPORT_V0
#define HWC_REMOVE_DEPRECATED_VERSIONS 0
#else
#define HWC_REMOVE_DEPRECATED_VERSIONS 1
#endif

#include <utils/RefBase.h>
#include <utils/String8.h>

#include <hardware/hardware.h>
#include <hardware/hwcomposer.h>

#include <cutils/xlog.h>

#include <cutils/properties.h>

#include "Layer.h"
#include "LayerBase.h"
#include "DisplayHardware/HWComposer.h"
#include "SurfaceFlinger.h"
#include "LayerScreenshot.h"

#ifndef NELEM
#define NELEM(x) ((int) (sizeof(x) / sizeof((x)[0])))
#endif

namespace android {

#if !HWC_REMOVE_DEPRECATED_VERSIONS

#define MIN_HWC_HEADER_VERSION 0

static uint32_t hwcApiVersion(const hwc_composer_device* hwc) {
    uint32_t hwcVersion = hwc->common.version;
    if (MIN_HWC_HEADER_VERSION == 0 &&
            (hwcVersion & HARDWARE_API_VERSION_2_MAJ_MIN_MASK) == 0) {
        // legacy version encoding
        hwcVersion <<= 16;
    }
    return hwcVersion & HARDWARE_API_VERSION_2_MAJ_MIN_MASK;
}

static uint32_t hwcHeaderVersion(const hwc_composer_device* hwc) {
    uint32_t hwcVersion = hwc->common.version;
    if (MIN_HWC_HEADER_VERSION == 0 &&
            (hwcVersion & HARDWARE_API_VERSION_2_MAJ_MIN_MASK) == 0) {
        // legacy version encoding
        hwcVersion <<= 16;
    }
    return hwcVersion & HARDWARE_API_VERSION_2_HEADER_MASK;
}

static bool hwcHasApiVersion(const hwc_composer_device* hwc,
        uint32_t version) {
    return hwcApiVersion(hwc) >= (version & HARDWARE_API_VERSION_2_MAJ_MIN_MASK);
}

bool HWComposer::initHWC_0() {
    mHwc_0 = NULL;
    mList = NULL;

    bool needVSyncThread = true;

    // Note: some devices may insist that the FB HAL be opened before HWC
    loadFbHalModule();
    loadHwcModule_0();

    // If HWC does not exist, FB dev is mandatory.
    if (!mHwc_0 && !mFbDev) {
        XLOGE("[0] ERROR: failed to open framebuffer, aborting");
        abort();
    }

    if (mHwc_0) {
        XLOGI("[0] Using %s version %u.%u", HWC_HARDWARE_COMPOSER,
              (hwcApiVersion(mHwc_0) >> 24) & 0xff,
              (hwcApiVersion(mHwc_0) >> 16) & 0xff);

        // do not need vsync thread
        // if API is greater than HWC_DEVICE_API_VERSION_0_3
        needVSyncThread = false;
        if (mHwc_0->common.version >= HWC_DEVICE_API_VERSION_0_3) {
            if (mDebugForceFakeVSync) {
                // make sure to turn h/w vsync off in "fake vsync" mode
                mHwc_0->methods->eventControl(mHwc_0, HWC_EVENT_VSYNC, 0);
            }
        } else {
            needVSyncThread = true;
        }

        // only support primary display
        mAllocatedDisplayIDs.markBit(HWC_DISPLAY_PRIMARY);
        mNumDisplays = 1;
    }

    return needVSyncThread;
}

void HWComposer::deinitHWC_0() {
    eventControl_0(HWC_DISPLAY_PRIMARY, EVENT_VSYNC, 0);

    // 20120815: free member to avoid memory leak
    freeWorkList();

    if (mVSyncThread != NULL) {
        mVSyncThread->requestExitAndWait();
    }
    if (mHwc_0) {
        hwc_close(mHwc_0);
    }
}

void HWComposer::loadHwcModule_0() {
    hw_module_t const* module;

    if (hw_get_module(HWC_HARDWARE_MODULE_ID, &module) != 0) {
        XLOGE("[0] %s module not found", HWC_HARDWARE_MODULE_ID);
        return;
    }

    int err = hwc_open(module, &mHwc_0);
    if (err) {
        XLOGE("[0] %s device failed to initialize (%s)",
              HWC_HARDWARE_COMPOSER, strerror(-err));
        return;
    }

    if (hwcHasApiVersion(mHwc, HWC_DEVICE_API_VERSION_1_0)) {
        XLOGE("%s device version %#x unsupported, will not be used",
              HWC_HARDWARE_COMPOSER, mHwc->common.version);
        hwc_close(mHwc_0);
        mHwc_0 = NULL;
        return;
    }
}

void HWComposer::eventControl_0(int disp, int event, int enabled) {
    if (disp != HWC_DISPLAY_PRIMARY)
    {
        XLOGE("[0] HWComposer only support primary display");
        return;
    }
    status_t err = NO_ERROR;
    if (mHwc_0 && mHwc_0->common.version >= HWC_DEVICE_API_VERSION_0_3) {
        if (!mDebugForceFakeVSync) {
            err = mHwc_0->methods->eventControl(mHwc_0, event, enabled);
            // error here should not happen -- not sure what we should
            // do if it does.
            ALOGE_IF(err, "[0] eventControl(%d, %d) failed %s",
                    event, enabled, strerror(-err));
        }
    }

    if (err == NO_ERROR && mVSyncThread != NULL) {
        mVSyncThread->setEnabled(enabled);
    }
}

status_t HWComposer::createWorkList_0(int32_t id, size_t numLayers) {
    if (id != HWC_DISPLAY_PRIMARY) {
        XLOGE("[0] HWComposer only support primary display");
        return BAD_INDEX;
    }

    if (mHwc_0) {
        DisplayData& disp(mDisplayData[0]);
        if (!mList || disp.capacity < numLayers) {
            freeWorkList();
            size_t size = sizeof(hwc_layer_list) + numLayers*sizeof(hwc_layer_t);
            mList = (hwc_layer_list_t*)calloc(1, size);
            disp.capacity = numLayers;
        }
        mList->flags = HWC_GEOMETRY_CHANGED;
        mList->numHwLayers = numLayers;
    }
    return NO_ERROR;
}

status_t HWComposer::prepare_0() {
    if (mList) {
        int count = LayerScreenshot::getCount();
        bool frozen = LayerScreenshot::isFrozen();
        if (count > 0) {
            mList->mtkListData.mflags |= HWC_LAYERSCREENSHOT_EXIST;
            if (frozen) mList->mtkListData.mflags |= HWC_SCREEN_FROZEN;
        }

        // External Display related
        sp<const DisplayDevice> hw(mFlinger->getDefaultDisplayDevice());
        mList->mtkListData.extOrientation = hw->getOrientation();
    }

    int err = mHwc_0->prepare(mHwc_0, mList);
    if (err == NO_ERROR) {
        DisplayData& disp(mDisplayData[0]);
        disp.hasFbComp = false;
        disp.hasOvComp = false;
        size_t count = mList->numHwLayers;
        for (size_t i = 0 ; i < count ; i++) {
            hwc_layer& l(mList->hwLayers[i]);

            // XLOGD("[0] prepare: %d, type=%d, handle=%p",
            //         i, l.compositionType, l.handle);

            if (l.flags & HWC_SKIP_LAYER) {
                l.compositionType = HWC_FRAMEBUFFER;
            }
            if (l.compositionType == HWC_FRAMEBUFFER) {
                disp.hasFbComp = true;
            }
            if (l.compositionType == HWC_OVERLAY) {
                disp.hasOvComp = true;
            }
        }
    }
    return (status_t)err;
}

status_t HWComposer::commit_0() {
    // get layer swap information and raise flag if necessary
    if (mList && mFlinger->getAndClearLayersSwapRequired(0)) {
        mList->mtkListData.mflags |= HWC_SWAP_REQUIRED;
    }
    EGLDisplay display = eglGetCurrentDisplay();
    EGLSurface surface = eglGetCurrentSurface(EGL_DRAW);
    int err = mHwc_0->set(mHwc_0, display, surface, mList);
    if (mList) {
        mList->flags &= ~HWC_GEOMETRY_CHANGED;
        mList->mtkListData.mflags &=
            ~(HWC_SWAP_REQUIRED | HWC_LAYERSCREENSHOT_EXIST | HWC_SCREEN_FROZEN);
    }
    return (status_t)err;
}

status_t HWComposer::release_0(int disp) const {
    if (disp != HWC_DISPLAY_PRIMARY)
    {
        XLOGE("[0] HWComposer only support primary display");
        return BAD_INDEX;
    }
    if (mHwc_0) {
        if (mHwc_0->common.version >= HWC_DEVICE_API_VERSION_0_3) {
            mHwc_0->methods->eventControl(mHwc_0, HWC_EVENT_VSYNC, 0);
        }
        int err = mHwc_0->set(mHwc_0, NULL, NULL, NULL);
        return (status_t)err;
    }
    return NO_ERROR;
}

HWComposer::LayerListIterator HWComposer::end_0(int32_t id) {
    size_t numLayers = 0;
    if (id == HWC_DISPLAY_PRIMARY) {
        if (mHwc_0 && mList) {
            numLayers = mList->numHwLayers;
        }
    }
    return getLayerIterator_0(id, numLayers);
}
    
void HWComposer::dump_0(String8& result, char* buffer, size_t SIZE) const {
    if (mHwc_0 && mList) {
        result.appendFormat("Hardware Composer state (version %8x):\n", hwcApiVersion(mHwc_0));
        result.appendFormat("  mDebugForceFakeVSync=%d\n", mDebugForceFakeVSync);

        const DisplayData& disp(mDisplayData[HWC_DISPLAY_PRIMARY]);

        result.appendFormat(
                "  Display : %ux%u, xdpi=%f, ydpi=%f, refresh=%lld\n",
                disp.width, disp.height, disp.xdpi, disp.ydpi, disp.refresh);

        const Vector< sp<LayerBase> >& visibleLayersSortedByZ =
                mFlinger->getLayerSortedByZForHwcDisplay(HWC_DISPLAY_PRIMARY);

        result.appendFormat("  numHwLayers=%u, flags=%08x\n",
                mList->numHwLayers, mList->flags);
        result.append(
                "   type   | ovl  |  handle  |   hints  |   flags  | tr | blend |  format  |       source crop         |           frame           | name \n"
                "----------+------+----------+----------+----------+----+-------+----------+---------------------------+---------------------------+------\n");
        //      " ________ | ____ | ________ | ________ | ________ | __ | _____ | ________ | [_____,_____,_____,_____] | [_____,_____,_____,_____] | ____
        for (size_t i = 0; i < mList->numHwLayers; i++) {
            const hwc_layer_t& l(mList->hwLayers[i]);
            int32_t format = -1;
            String8 name("unknown");
            if (i < visibleLayersSortedByZ.size()) {
                const sp<LayerBase> layer(visibleLayersSortedByZ[i]);
                if (layer->getLayer() != NULL) {
                    const sp<GraphicBuffer>& buffer(
                        layer->getLayer()->getActiveBuffer());
                    if (buffer != NULL) {
                        format = buffer->getPixelFormat();
                    }
                }
                name = layer->getName();
            }

            int type = l.compositionType;
            if (type == HWC_FRAMEBUFFER_TARGET) {
                name = "HWC_FRAMEBUFFER_TARGET";
                format = disp.format;
            }

            static char const* compositionTypeName[] = {
                    "GLES",
                    "HWC",
                    "BACKGROUND",
                    "FB TARGET",
                    "UNKNOWN"};
            if (type >= NELEM(compositionTypeName))
                type = NELEM(compositionTypeName) - 1;

            result.appendFormat(
                    "%10s| %4s | %08x | %08x | %08x | %02x | %05x | %08x | [%5d,%5d,%5d,%5d] | [%5d,%5d,%5d,%5d] | %s\n",
                    compositionTypeName[type],
                    l.mtkData.overlayType,
                    intptr_t(l.handle), l.hints, l.flags, l.transform, l.blending, format,
                    l.sourceCrop.left, l.sourceCrop.top, l.sourceCrop.right, l.sourceCrop.bottom,
                    l.displayFrame.left, l.displayFrame.top, l.displayFrame.right, l.displayFrame.bottom,
                    name.string());
        }
    }

    if (mHwc_0 && mHwc_0->dump &&
        mHwc_0->common.version >= HWC_DEVICE_API_VERSION_0_1) {
        mHwc_0->dump(mHwc_0, buffer, SIZE);
        result.append(buffer);
    }
}

/*
 * temporarily copy Iterable here for HWCLayerVersion0
 */
template<typename CONCRETE, typename HWCTYPE>
class Iterable0 : public HWComposer::HWCLayer {
protected:
    HWCTYPE* const mLayerList;
    HWCTYPE* mCurrentLayer;
    Iterable0(HWCTYPE* layer) : mLayerList(layer), mCurrentLayer(layer) { }
    inline HWCTYPE const * getLayer() const { return mCurrentLayer; }
    inline HWCTYPE* getLayer() { return mCurrentLayer; }
    virtual ~Iterable0() { }
private:
    // returns a copy of ourselves
    virtual HWComposer::HWCLayer* dup() {
        return new CONCRETE( static_cast<const CONCRETE&>(*this) );
    }
    virtual status_t setLayer(size_t index) {
        mCurrentLayer = &mLayerList[index];
        return NO_ERROR;
    }
};

/*
 * Concrete implementation of HWCLayer for HWC_DEVICE_API_VERSION_0_1.
 * This implements the HWCLayer side of HWCIterableLayer.
 */
class HWCLayerVersion0 : public Iterable0<HWCLayerVersion0, hwc_layer_t> {
public:
    HWCLayerVersion0(hwc_layer_t* layer)
        : Iterable0<HWCLayerVersion0, hwc_layer_t>(layer) { }

    virtual int32_t getCompositionType() const {
        return getLayer()->compositionType;
    }
    virtual uint32_t getHints() const {
        return getLayer()->hints;
    }
    virtual int getAndResetReleaseFenceFd() {
        return BAD_INDEX;
    }
    virtual void setAcquireFenceFd(int fenceFd) {
    }
    virtual void setPerFrameDefaultState() {
    }
    virtual void setDefaultState() {
        getLayer()->compositionType = HWC_FRAMEBUFFER;
        getLayer()->hints = 0;
        getLayer()->flags = HWC_SKIP_LAYER;
        getLayer()->handle = 0;
        getLayer()->transform = 0;
        getLayer()->blending = HWC_BLENDING_NONE;
        getLayer()->visibleRegionScreen.numRects = 0;
        getLayer()->visibleRegionScreen.rects = NULL;

        getLayer()->mtkData.connectApi = -1;
        getLayer()->mtkData.identity = -1;
    }
    virtual void setSkip(bool skip) {
        if (skip) {
            getLayer()->flags |= HWC_SKIP_LAYER;
        } else {
            getLayer()->flags &= ~HWC_SKIP_LAYER;
        }
    }
    virtual void setBlending(uint32_t blending) {
        getLayer()->blending = blending;
    }
    virtual void setTransform(uint32_t transform) {
        getLayer()->transform = transform;
    }
    virtual void setFrame(const Rect& frame) {
        reinterpret_cast<Rect&>(getLayer()->displayFrame) = frame;
    }
    virtual void setCrop(const Rect& crop) {
        reinterpret_cast<Rect&>(getLayer()->sourceCrop) = crop;
    }
    virtual void setVisibleRegionScreen(const Region& reg) {
        hwc_region_t& visibleRegion = getLayer()->visibleRegionScreen;
        SharedBuffer const* sb = reg.getSharedBuffer(&visibleRegion.numRects);
        visibleRegion.rects = reinterpret_cast<hwc_rect_t const *>(sb->data());
    }
    virtual void setBuffer(const sp<GraphicBuffer>& buffer) {
        if (buffer == 0 || buffer->handle == 0) {
            getLayer()->compositionType = HWC_FRAMEBUFFER;
            getLayer()->flags |= HWC_SKIP_LAYER;
            getLayer()->handle = 0;
        } else {
            getLayer()->handle = buffer->handle;
        }
        // mtkData
        getLayer()->mtkData.buffer = buffer;
    }
    virtual void onDisplayed() {
        hwc_region_t& visibleRegion = getLayer()->visibleRegionScreen;
        SharedBuffer const* sb = SharedBuffer::bufferFromData(visibleRegion.rects);
        if (sb) {
            sb->release();
            // not technically needed but safer
            visibleRegion.numRects = 0;
            visibleRegion.rects = NULL;
        }
    }
    // mtkData
    virtual int getMva() {
        return 0;
    }
    virtual void setLayerType(uint32_t type) {
    }
    virtual void setDirty(bool dirty) {
        if (dirty)
            getLayer()->mtkData.mflags |= HWC_DIRTY_LAYER;
        else
            getLayer()->mtkData.mflags &= ~HWC_DIRTY_LAYER;
    }
    virtual void setConnectedApi(int32_t api) {
        getLayer()->mtkData.connectApi = api;
    }
    virtual void setIdentity(int32_t id) {
        getLayer()->mtkData.identity = id;
    }
    virtual void setFillColor(struct hwc_color color) {
        getLayer()->mtkData.fillColor = color;
    }
    virtual void setMatrix(const Transform& tr) {
        float *m = getLayer()->mtkData.transformMatrix;
        for (int i = 0, j = 0; i < 9; i += 3, j++) {
            m[i + 0] = tr[0][j];
            m[i + 1] = tr[1][j];
            m[i + 2] = tr[2][j];
        }
    }
    virtual void setStereosFlags(uint32_t flag) {
    }
};

HWComposer::LayerListIterator HWComposer::getLayerIterator_0(
    int32_t id, size_t index) {
    if (id != HWC_DISPLAY_PRIMARY) {
        return LayerListIterator();
    }
    if (!mHwc_0 || !mList || index > mList->numHwLayers) {
        return LayerListIterator();
    }
    return LayerListIterator(new HWCLayerVersion0(mList->hwLayers), index);
}

// 20120815: free member to avoid memory leak
void HWComposer::freeWorkList() {
    if (mList != NULL) {
        DisplayData& disp(mDisplayData[0]);
        for (uint32_t i = 0; i < disp.capacity; i++) {
            mList->hwLayers[i].mtkData.buffer = NULL;
        }
        free(mList);
    }
}

#endif // HWC_REMOVE_DEPRECATED_VERSIONS

nsecs_t HWComposer::getRefreshPeriod() const {
    nsecs_t refresh = 0;
#if !HWC_REMOVE_DEPRECATED_VERSIONS
    if (mHwc_0) {
        mHwc_0->query(mHwc_0, HWC_VSYNC_PERIOD, (int *)&refresh);
    }
#else
    if (mHwc) {
        mHwc->query(mHwc, HWC_VSYNC_PERIOD, (int *)&refresh);
    }
#endif
    return refresh;
}

// 20120814: add property function for debug purpose
void HWComposer::VSyncThread::setProperty() {
    char value[PROPERTY_VALUE_MAX];
    property_get("debug.sf.sw_vsync_fps", value, "0");
    int fps = atoi(value);
    if (fps > 0) {
        mRefreshPeriod = nsecs_t(1e9 / fps);
        XLOGD("[VSYNC] set sw vsync fps(%d), period(%lld)", fps, mRefreshPeriod);
    }
}

// ---------------------------------------------------------------------------
}; // namespace android
