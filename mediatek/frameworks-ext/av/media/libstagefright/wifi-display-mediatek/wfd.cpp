/*
 * Copyright 2012, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "wfd"
#include <utils/Log.h>

#include "sink/WifiDisplaySink.h"
#include "source/WifiDisplaySource.h"
#include "ParsedMessage.h"

#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>
#include <media/IMediaPlayerService.h>
#include <media/stagefright/DataSource.h>
#include <media/stagefright/foundation/ADebug.h>

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "wfdUibcTest.h"
namespace android {

static void setStreamMode(const char *buffer){
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("media.player"));

    sp<IMediaPlayerService> service =
        interface_cast<IMediaPlayerService>(binder);

    CHECK(service.get() != NULL);
    CHECK(buffer != NULL);

    if(strlen(buffer) != 1){
        ALOGE("Bad arguments:%s", buffer);
        return;
    }
    
    int mode = buffer[0] - '0';    
    service->setBitrateControl(mode);
}

static void enableDisableRemoteDisplay(const char *iface) {
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("media.player"));

    sp<IMediaPlayerService> service =
        interface_cast<IMediaPlayerService>(binder);

    CHECK(service.get() != NULL);

    service->enableRemoteDisplay(iface, WifiDisplaySource::kTestModeFlag);
}

static void enableFastRemoteDisplay(const char *iface) {
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("media.player"));

    sp<IMediaPlayerService> service =
        interface_cast<IMediaPlayerService>(binder);

    CHECK(service.get() != NULL);

    service->enableRemoteDisplay(iface, WifiDisplaySource::kFastSetupFlag | WifiDisplaySource::kTestModeFlag);
}

static void enableFastRtpRemoteDisplay(const char *iface) {
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("media.player"));

    sp<IMediaPlayerService> service =
        interface_cast<IMediaPlayerService>(binder);

    CHECK(service.get() != NULL);

    service->enableRemoteDisplay(iface, WifiDisplaySource::kFastRtpFlag | WifiDisplaySource::kTestModeFlag);
}


}  // namespace android

static void usage(const char *me) {
    fprintf(stderr,
            "usage:\n"
            "           %s -c host[:port]\tconnect to wifi source\n"
            "           -u uri        \tconnect to an rtsp uri\n"
            "           -e ip[:port]       \tenable remote display\n"
            "           -d            \tdisable remote display\n",
            "           -f            \tenable fast remote display\n",
            "           -b host[:port]          \tsend UIBC test command\n",
            me);
}
int connectUibc(const char* remoteHost, int32_t port)
{
    struct sockaddr_in stSockAddr;
    int Res;
    int SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
 
    if (-1 == SocketFD)
    {
      ALOGE("cannot create socket");
      exit(1);
    }
 
    memset(&stSockAddr, 0, sizeof(stSockAddr));
 
    stSockAddr.sin_family = AF_INET;
    stSockAddr.sin_port = htons(WFD_UIBC_SERVER_PORT);
    Res = inet_pton(AF_INET, remoteHost, &stSockAddr.sin_addr);
 
    if (0 > Res)
    {
      ALOGE("error: first parameter is not a valid address family");
      close(SocketFD);
      exit(1);
    }
    else if (0 == Res)
    {
      ALOGE("char string (second parameter does not contain valid ipaddress)");
      close(SocketFD);
      exit(1);
    }
 
    if (-1 == connect(SocketFD, (struct sockaddr *)&stSockAddr, sizeof(stSockAddr)))
    {
      ALOGE("connect failed:%d", errno);
      close(SocketFD);
      exit(1);
 
    }
 
    /* perform read write operations ... */    
    int i=PEER_START_INDEX;
    while(i<=PEER_END_INDEX){
        ALOGE("send UIBC message %d",i);        
            if(write(SocketFD,  peerList[i] , peerListLength[i]) == -1){
            ALOGE("wangfj write failed:%d", errno);
            close(SocketFD);
            exit(1);        
        }
        ALOGE("send UIBC message %d done ",i);
        sleep(1);
        i++;
    }
    shutdown(SocketFD, SHUT_RDWR);
 
    close(SocketFD);
    return EXIT_SUCCESS;
}

static void testUibcCmd(const char *testCmd){
    ALOGI("testUibcCmd:%s", testCmd);
    //connectUibc();
}



int main(int argc, char **argv) {
    using namespace android;

    ProcessState::self()->startThreadPool();

    DataSource::RegisterDefaultSniffers();

    AString connectToHost;
    int32_t connectToPort = -1;
    AString uri;
    int     isFastSetup = 0;

    int res;
    while ((res = getopt(argc, argv, "hpic:b:l:t:f:r:u:e:s:d")) >= 0) {
        switch (res) {
            case 'b':
            {
                const char *colonPos = strrchr(optarg, ':');

                if (colonPos == NULL) {
                    connectToHost = optarg;
                    connectToPort = WFD_UIBC_SERVER_PORT;
                } else {
                    connectToHost.setTo(optarg, colonPos - optarg);

                    char *end;
                    connectToPort = strtol(colonPos + 1, &end, 10);

                    if (*end != '\0' || end == colonPos + 1
                            || connectToPort < 1 || connectToPort > 65535) {
                        fprintf(stderr, "Illegal port specified.\n");
                        exit(1);
                    }
                }
                connectUibc(connectToHost.c_str(), connectToPort);
                exit(1);
                break;
            }
            case 't':
            {
                isFastSetup = 1;
            }
            case 'c':
            {
                const char *colonPos = strrchr(optarg, ':');

                if (colonPos == NULL) {
                    connectToHost = optarg;
                    connectToPort = WifiDisplaySource::kWifiDisplayDefaultPort;
                } else {
                    connectToHost.setTo(optarg, colonPos - optarg);

                    char *end;
                    connectToPort = strtol(colonPos + 1, &end, 10);

                    if (*end != '\0' || end == colonPos + 1
                            || connectToPort < 1 || connectToPort > 65535) {
                        fprintf(stderr, "Illegal port specified.\n");
                        exit(1);
                    }
                }
                break;
            }
            case 'u':
            {
                uri = optarg;
                break;
            }

            case 'f':
            {
                enableFastRemoteDisplay(optarg);
                exit(0);
                break;
            }

            case 'r':
            {
                enableFastRtpRemoteDisplay(optarg);
                exit(0);
                break;
            }

            case 'e':
            {
                enableDisableRemoteDisplay(optarg);
                exit(0);
                break;
            }

            case 'd':
            {
                enableDisableRemoteDisplay(NULL);
                exit(0);
                break;
            }

            case 's':
            {
                setStreamMode(optarg);
                exit(0);
                break;
            }
    

            case '?':
            case 'h':
            default:
                testUibcCmd(argv[1]);
                usage(argv[0]);
                exit(1);
        }
    }

    if (connectToPort < 0 && uri.empty()) {
        fprintf(stderr,
                "You need to select either source host or uri.\n");

        exit(1);
    }

    if (connectToPort >= 0 && !uri.empty()) {
        fprintf(stderr,
                "You need to either connect to a wfd host or an rtsp url, "
                "not both.\n");
        exit(1);
    }

    sp<ANetworkSession> session = new ANetworkSession;
    session->start();

    sp<ALooper> looper = new ALooper;
    
    sp<WifiDisplaySink> sink = NULL;
    
    if(!isFastSetup){
        sink = new WifiDisplaySink(session);
    }else{
        sink = new WifiDisplaySink(session, NULL, true);
    }
    
    looper->registerHandler(sink);

    if (connectToPort >= 0) {
        sink->start(connectToHost.c_str(), connectToPort);
    } else {
        sink->start(uri.c_str());
    }

    looper->start(true /* runOnCallingThread */);

    return 0;
}
