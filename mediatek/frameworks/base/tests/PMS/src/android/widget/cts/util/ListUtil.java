/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.widget.cts.util;

import android.app.Instrumentation;
import android.view.KeyEvent;
import android.widget.ListView;

/**
 * Various useful stuff for instrumentation testing listview.
 */
public class ListUtil {

    private final ListView mListView;
    private final Instrumentation mInstrumentation;

    /**
     * @param listView The listview to act on
     * @param instrumentation The instrumentation to use.
     */
    public ListUtil(ListView listView, Instrumentation instrumentation) {
        mListView = listView;
        mInstrumentation = instrumentation;
    }

    /**
     * Set the selected position of the list view.
     * @param pos The desired position.
     */
    public final void setSelectedPosition(final int pos) {
        mListView.post(new Runnable() {
            public void run() {
                mListView.setSelection(pos);
            }
        });
        mInstrumentation.waitForIdleSync();
    }

    /**
     * Get the top of the list.
     */
    public final int getListTop() {
        return mListView.getListPaddingTop();
    }

    /**
     * Get the bottom of the list.
     */
    public final int getListBottom() {
        return mListView.getHeight() - mListView.getListPaddingBottom();
    }

    /**
     * Arrow (up or down as appropriate) to the desired position in the list.
     * @param desiredPos The desired position
     * @throws IllegalStateException if the position can't be reached within 20 presses.
     */
    public final void arrowScrollToSelectedPosition(int desiredPos) {
        if (desiredPos > mListView.getSelectedItemPosition()) {
            arrowDownToSelectedPosition(desiredPos);
        } else {
            arrowUpToSelectedPosition(desiredPos);
        }
    }

    private void arrowDownToSelectedPosition(int position) {
        int maxDowns = 20;
        while(mListView.getSelectedItemPosition() < position && --maxDowns > 0) {
            mInstrumentation.sendCharacterSync(KeyEvent.KEYCODE_DPAD_DOWN);
        }
        if (position != mListView.getSelectedItemPosition()) {
            throw new IllegalStateException("couldn't get to item after 20 downs");
        }

    }

    private void arrowUpToSelectedPosition(int position) {
        int maxUps = 20;
        while(mListView.getSelectedItemPosition() > position && --maxUps > 0) {
            mInstrumentation.sendCharacterSync(KeyEvent.KEYCODE_DPAD_UP);
        }
        if (position != mListView.getSelectedItemPosition()) {
            throw new IllegalStateException("couldn't get to item after 20 ups");
        }
    }

}
