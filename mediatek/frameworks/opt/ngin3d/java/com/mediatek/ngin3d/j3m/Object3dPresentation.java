/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Object3d Presentation for J3M
 */
package com.mediatek.ngin3d.j3m;

import com.mediatek.j3m.Animation;
import com.mediatek.j3m.Model;
import com.mediatek.j3m.SceneNode;
import com.mediatek.ngin3d.presentation.IObject3d;
import com.mediatek.ngin3d.presentation.ObjectSource;
import com.mediatek.ngin3d.Rotation;

import android.util.Log;

/**
 * A presentation object which represents an instance of a Glo object.
 * @hide
 */

public class Object3dPresentation extends ActorPresentation<SceneNode>
    implements IObject3d {

    private static final String TAG = "Object3dPresentation";

    private Animation mAnimation;

    /**
     * Initializes this object with J3M presentation engine
     * @param engine
     */
    public Object3dPresentation(J3mPresentationEngine engine) {
        super(engine);
    }

    /**
     * Called by the presentation engine to initialize the object
     */
    @Override
    public void onInitialize() {
        super.onInitialize();
    }

    /**
     * Called by the presentation engine to unintitialize the object
     */
    @Override
    public void onUninitialize() {
        mAnimation = null;
        super.onUninitialize();
    }

    /**
     * Initializes the Glo presentation from a generic source object
     *
     * @param src object source
     */
    public void setObjectSource(ObjectSource src) {
        super.onInitialize();

        // It isn't clear what the difference between loading from file,
        // and loaded from an asset file is, so they are the same here.
        if (src.srcType == ObjectSource.FILE
                || src.srcType == ObjectSource.ASSET) {
            getSceneNode().setParent(null);
            Log.v(TAG, "Loading file asset: " + (String) src.srcInfo);
            Model model = getEngine().getAssetPool().loadModel(
                    (String) src.srcInfo, getEngine().getRootNode());

            SceneNode node = model.getSceneNode();

            if (node == null) {
                node = getEngine().getJ3m().createSceneNode();
            }

            node.setParent(getAnchorSceneNode());
            setSceneNode(node);

            mAnimation = model.getAnimation();

        } // else if (src.srcType == ObjectSource.RES_ID) {
            // \todo implement
            // Loading from a Android resource (a file specified using an
            // ID from the generated R.java files) will most likely be done
            // by loading the resource in Java and passing the data to J3M.
        //}
    }

    public void update(float progress) {
        if (mAnimation != null) {
            mAnimation.update(progress);
        }
    }

    public void start(float time) {
        if (mAnimation != null) {
            mAnimation.start(time);
        }
    }

    public void stop() {
        if (mAnimation != null) {
            // reset GLO animation to initial status
            mAnimation.start(0f);
            mAnimation.update(0f);
            mAnimation.stop();
        }
    }

    public int getLength() {
        if (mAnimation == null) {
            Log.w(TAG, "Glo object has no animation, so its length is 0");
            return 0;
        } else {
            // ngin3d takes milliseconds whereas J3M takes seconds as length.
            // Hence, time length supplied by native J3M code is
            // multiplied by 1000.
            // i.e. seconds to milliseconds conversion
            return ((int) (mAnimation.getLength() * 1000.0f));
        }
    }

    public void setLoop(boolean loop) {
        if (mAnimation != null) {
            mAnimation.setLooping(loop);
        }
    }

    public boolean getLoop() {
        if (mAnimation != null) {
            return mAnimation.getLooping();
        } else {
            return false;
        }
    }

    private SceneNode getSceneNode(String name) {
        return getSceneNode().find(name);
    }

    public void setRotation(String sceneNodeName, Rotation rotation) {
        SceneNode node = getSceneNode(sceneNodeName);
        node.setRotation(
                rotation.getQuaternion().getQ0(),
                rotation.getQuaternion().getQ1(),
                rotation.getQuaternion().getQ2(),
                rotation.getQuaternion().getQ3());
    }
}
