/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.widgetdemos.clock3d;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.widget.Button;
import android.widget.RemoteViews.RemoteView;

// A widget that allows you to change a floating point value by swiping left and
// right.
@RemoteView
public class FloatTweaker extends Button {
    private String mName = "";
    private float mValue;
    private VelocityTracker mVelocityTracker;
    private OnValueChangeCallback mOnValueChangeCallback;

    public FloatTweaker(Context context) {
        super(context);
        initialise();
    }

    public FloatTweaker(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialise();
    }

    public void initialise() {
        mVelocityTracker = VelocityTracker.obtain();
    }

    @Override
    public void onFinishInflate() {
        updateText();
    }

    // Override the onTouch callback to allow the value to be changed
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.v("FloatTweaker", "onTouchEvent");
        int action = event.getAction();

        if (action == MotionEvent.ACTION_MOVE) {
            mVelocityTracker.addMovement(event);
            mVelocityTracker.computeCurrentVelocity(10); 

            float velocityX = mVelocityTracker.getXVelocity();
            mValue += velocityX;

            // Called when the value changes
            if (mOnValueChangeCallback != null) {
                mValue = mOnValueChangeCallback.onValueChange(mValue);
            }
            updateText();
        }

        return true;
    }

    public void updateText() {
        if (mName.equals("")) {
            setText("");
        } else {
            setText(mName + ": " + Float.toString(mValue));
        }
    }

    public void setValue(float value) {
        mValue = value;
        updateText();
    }

    public float getValue() {
        return mValue;
    }

    public void setName(String name) {
        mName = name;
        updateText();
    }

    public String getName() {
        return mName;
    }

    public interface OnValueChangeCallback {
        float onValueChange(float value);
    }

    public void setOnValueChangeCallback(OnValueChangeCallback callback) {
        mOnValueChangeCallback = callback;
    }

}
