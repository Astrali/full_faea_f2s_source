/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ngin3d.demo;

import android.content.res.Resources;
import android.os.Bundle;
import com.mediatek.ngin3d.Actor;
import com.mediatek.ngin3d.Image;
import com.mediatek.ngin3d.Point;
import com.mediatek.ngin3d.Scale;
import com.mediatek.ngin3d.android.StageActivity;
import com.mediatek.ngin3d.animation.BasicAnimation;
import com.mediatek.ngin3d.animation.Mode;
import com.mediatek.ngin3d.animation.PropertyAnimation;
import com.mediatek.ngin3d.animation.SpriteAnimation;
import com.mediatek.ngin3d.demo.R;

public class SpriteAnimationDemo extends StageActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Resources resources = getResources();
        Image target = Image.createFromResource(resources, R.drawable.angel1);
        mStage.add(target);

        SpriteAnimation sprite = new SpriteAnimation(target, 200);
        sprite.addSprite(resources, R.drawable.angel1);
        sprite.addSprite(resources, R.drawable.angel2);
        sprite.addSprite(resources, R.drawable.angel1);
        sprite.addSprite(resources, R.drawable.angel3);

        BasicAnimation move = new PropertyAnimation(target, Actor.PROP_POSITION, new Point(0, 0), new Point(800, 480))
            .setMode(Mode.EASE_IN_OUT_CUBIC)
            .setLoop(true)
            .setAutoReverse(true);
        BasicAnimation scale = new PropertyAnimation(target, Actor.PROP_SCALE, new Scale(0.5f, 0.5f, 0.5f), new Scale(2, 2, 2))
            .setMode(Mode.LINEAR)
            .setLoop(true)
            .setAutoReverse(true);

        Image target2 = Image.createFromResource(resources, R.drawable.hamtro_walk);
        target2.setPosition(new Point(200, 50));
        mStage.add(target2);
        SpriteAnimation walk = new SpriteAnimation(
            resources,
            target2,
            R.drawable.hamtro_walk,
            5000,
            resources.getDimensionPixelSize(R.dimen.hamtaro_sprite_width),
            resources.getDimensionPixelSize(R.dimen.hamtaro_sprite_height));

        BasicAnimation walkMove = new PropertyAnimation(target2, Actor.PROP_POSITION, new Point(200, 50), new Point(200, 400))
            .setMode(Mode.LINEAR)
            .setLoop(true)
            .setAutoReverse(true)
            .setDuration(2500);

        sprite.setLoop(true);
        sprite.start();
        move.start();
        scale.start();
        walk.setLoop(true);
        walk.start();
        walkMove.start();
    }
}
