/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.notebook;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;

import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mediatek.notebook.NotePad.Notes;

import java.util.ArrayList;
import java.util.List;

public class NoteAdapter extends BaseAdapter {

     private Context mContext;
     public Cursor cur;
     private int mWhich;
     public ViewHolder holder = new ViewHolder();;
     public List<NoteItem> list = new ArrayList<NoteItem>(); 
     public List<NoteItem> mylist = new ArrayList<NoteItem>(); 
     private Resources resource; 
     private ColorStateList colorWork; 
     private ColorStateList colorPersonal; 
     private ColorStateList colorFamily; 
     private ColorStateList colorStudy; 
     private String groupWork; 
     private String groupPersonal;
     private String groupFamily;
     private String groupStudy;
    
     public class ViewHolder {
         TextView mGroup;
         TextView mCreatetime;
         TextView mNote;
         CheckBox mCheckbox;
     }
     public class NoteItem {
         public int id;
         public String note;
         public String create_time;
         public boolean isselect;
         public String notegroup;
         public String modify_time;
     }
     public NoteAdapter(Context context, Cursor cursor, int token) {
            mContext = context;
            cur = cursor;
            mWhich = token;
            setDataOfCurrentActivity();
     }
     private void setDataOfCurrentActivity(){
    	 resource = (Resources) mContext.getResources(); 
         colorWork = (ColorStateList) resource.getColorStateList(R.color.work); 
         colorPersonal = (ColorStateList) resource.getColorStateList(R.color.personal); 
         colorFamily = (ColorStateList) resource.getColorStateList(R.color.family); 
         colorStudy = (ColorStateList) resource.getColorStateList(R.color.study); 
         groupWork = (String) resource.getString(R.string.menu_work); 
         groupPersonal = (String) resource.getString(R.string.menu_personal);
         groupFamily = (String) resource.getString(R.string.menu_family);
         groupStudy = (String) resource.getString(R.string.menu_study);
     }
     public int getCount() {
         return cur.getCount();
     }
     public Object getItem(int position) {
         return list.get(position);
     }
     public long getItemId(int position) {
         return position;
     }
     public static class ViewClass{
    	 TextView title;
    	 TextView createTime;
    	 TextView groupColor;
    	 TextView notegroup;
     }
     public View getView(final int position, View convertView, ViewGroup parent) {
         ViewClass view;
    	 if (convertView == null){
         convertView = LayoutInflater.from(mContext).inflate(R.layout.noteslist_item_context, null);   
             view = new ViewClass();
        	 view.title = (TextView)convertView.findViewById(R.id.title); 
        	 view.createTime = (TextView)convertView.findViewById(R.id.create_time); 
        	 view.groupColor = (TextView)convertView.findViewById(R.id.groupcolor); 
        	 view.notegroup = (TextView)convertView.findViewById(R.id.group);
        	 convertView.setTag(view);
         } else {
        	 view = (ViewClass)convertView.getTag();
         }
         
         NoteItem item = list.get(position); 
         view.title.setText(item.note);
         view.createTime.setText(item.create_time);
         if (item.notegroup.equals(groupPersonal)) {
        	 view.notegroup.setTextColor(colorPersonal);
        	 view.groupColor.setBackgroundResource(R.color.personal);
         } else if (item.notegroup.equals(groupWork)) {
        	 view.notegroup.setTextColor(colorWork);
        	 view.groupColor.setBackgroundResource(R.color.work);
         } else if (item.notegroup.equals(groupFamily)) {
        	 view.notegroup.setTextColor(colorFamily);
        	 view.groupColor.setBackgroundResource(R.color.family);
         } else if (item.notegroup.equals(groupStudy)) {
        	 view.notegroup.setTextColor(colorStudy);
        	 view.groupColor.setBackgroundResource(R.color.study);
         } else {
        	 view.groupColor.setBackgroundResource(R.color.none);
         }
         view.notegroup.setText(item.notegroup);
         if (mWhich == 2) {
                CheckBox cb = (CheckBox)convertView.findViewById(R.id.isdelete);
                cb.setVisibility(0);
                if (item.isselect) {
                    cb.setChecked(true);
                } else {
                    cb.setChecked(false);
                }
         holder.mCheckbox = (CheckBox) convertView.findViewById(R.id.isdelete);
         holder.mCheckbox.setOnClickListener(new CheckBox.OnClickListener() {
             public void onClick(View v) {
                 checkboxClickAction(position);
             }
         });
         }
         else if (mWhich == 0) {
             TextView tv = (TextView)convertView.findViewById(R.id.groupcolor);
             tv.setVisibility(0);
         }
         
         return convertView;
     }
     public void checkboxClickAction(int position) {
         Resources resource = (Resources) mContext.getResources(); 
         String selected = (String) resource.getString(R.string.title_bar_selected); 
         NoteItem item = list.get(position);
         if (item.isselect) {
             item.isselect = false;
         } else {
             item.isselect = true;
         }
         int count = selectedNumber();
         NotePad.Notes.sDeleteNum = count;
         NotePad.Notes.sNoteDelete.invalidateOptionsMenu();
         NotePad.Notes.sActionbar.setTitle(String.valueOf(count) + "  " + selected);
     }
     public void addList(NoteItem item) { 
         list.add(item);   
     }
     public void deleteSelectedNote() {
         for (int i = 0; i < list.size(); i ++) {
             if (list.get(i).isselect) {
                 int noteId = list.get(i).id;
                 Uri muri = Uri.parse(Notes.CONTENT_URI + "/" + noteId);
                 String where =
                         NotePad.Notes._ID +                              
                         " = " +                                          
                         muri.getPathSegments().                           
                             get(NotePad.Notes.NOTE_ID_PATH_POSITION);
                 mContext.getContentResolver().delete(muri, where, null);
             }
         }
     }
     public void selectAllOrNoCheckbox(boolean userSelect) {
         Resources resource = (Resources) mContext.getResources(); 
         String selected = (String) resource.getString(R.string.title_bar_selected); 
         for (int i = 0; i < cur.getCount(); i ++) {
             list.get(i).isselect = userSelect;
             int count = selectedNumber();
             NotePad.Notes.sDeleteNum = count;
             NotePad.Notes.sActionbar.setTitle(String.valueOf(count) + "  " + selected);
         }
     }
     public int selectedNumber() {
         int count = 0;
         for (int i = 0; i < cur.getCount(); i ++) {
             if (list.get(i).isselect) {
                 count ++;
             }
         }
         NotePad.Notes.sDeleteNum = count;
             return count;
     }
}
