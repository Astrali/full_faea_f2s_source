package com.mediatek.smsreg.test;

import android.test.AndroidTestCase;

import com.mediatek.smsreg.SmsRegConst;
import com.mediatek.xlog.Xlog;

public class SmsRegConstTest extends AndroidTestCase {
    public static final String TAG = "test/SmsRegConst";

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Xlog.e(TAG, "SmsRegConst:setUp");
    }

    @Override
    protected void tearDown() throws Exception {
        Xlog.e(TAG, "SmsRegConst:tearDown");
        super.tearDown();
    }

    public void testcase01Const() {
        // init a new object
        new SmsRegConst();

        // compare variable to constant value
        assertEquals(SmsRegConst.CMCCID, 1);
        assertEquals(SmsRegConst.CUCOMID, 2);
        assertEquals(SmsRegConst.GEMINI_SIM_1, 0);
        assertEquals(SmsRegConst.GEMINI_SIM_2, 1);
        assertEquals(SmsRegConst.GEMSIM[0], 0);
        assertEquals(SmsRegConst.GEMSIM[1], 1);
        assertEquals(SmsRegConst.GEMINI_SIM_ID_KEY, "simId");
        assertEquals(SmsRegConst.CONFIG_PATH,
                "/system/etc/dm/smsSelfRegConfig.xml");
        assertEquals(SmsRegConst.SMS_REGIST_ENABLED_PATH,
                "/data/data/com.mediatek.engineermode/sharefile/cta_cmcc");
        assertEquals(SmsRegConst.SIM_STATE_UNKNOWN, 0);
        assertEquals(SmsRegConst.SIM_STATE_ABSENT, 1);
        assertEquals(SmsRegConst.SIM_STATE_PIN_REQUIRED, 2);
        assertEquals(SmsRegConst.SIM_STATE_PUK_REQUIRED, 3);
        assertEquals(SmsRegConst.SIM_STATE_NETWORK_LOCKED, 4);
        assertEquals(SmsRegConst.SIM_STATE_READY, 5);
        assertEquals(SmsRegConst.ACTION_BOOTCOMPLETED,
                "android.intent.action.BOOT_COMPLETED");
        assertEquals(SmsRegConst.DM_REGISTER_SMS_RECEIVED_ACTION,
                "android.intent.action.DM_REGISTER_SMS_RECEIVED");
        assertEquals(SmsRegConst.ACTION_SIM_STATE_CHANGED,
                "android.intent.action.SIM_STATE_CHANGED");
    }
}