/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.smsreg.test;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.test.ServiceTestCase;

import com.android.internal.telephony.IccCardConstants;
import com.mediatek.common.dm.DMAgent;
import com.mediatek.smsreg.InfoPersistentor;
import com.mediatek.smsreg.SmsRegService;
import com.mediatek.telephony.TelephonyManagerEx;
import com.mediatek.xlog.Xlog;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.TimerTask;

/*
 * SmsSelfRegService Auto test
 */
public class SmsRegServiceTest extends ServiceTestCase<SmsRegService> {
    public static final String TAG = "test/SmsRegServiceTest";
    // mSmsRegService is used to save the instance of the service(SmsRegService)
    private SmsRegService mSmsRegService = null;

    public SmsRegServiceTest() {
        super(SmsRegService.class);
        Xlog.e(TAG, "SmsRegServiceTest");
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Xlog.e(TAG, "SmsRegServiceTest setUp");
    }

    @Override
    protected void tearDown() throws Exception {
        Xlog.e(TAG, "SmsRegServiceTest tearDown");
        super.tearDown();
    }

    /*
     * Start SmsReg Service with intent action "null"
     */
    public void testcase01StartNullAction() {
        Xlog.e(TAG, "testcase01StartNullAction");

        // Start SmsRegService under test, Action and Extras are null
        startSmsRegService(null, null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        TelephonyManagerEx mTelephonyManager = (TelephonyManagerEx)
                smsRegServiceGetDeclaredField("mTelephonyManager");
        assertNotNull(mTelephonyManager);
        Xlog.e(TAG, "mTelephonyManager is not null");
    }

    /*
     * Start SmsReg Service with intent action "BOOTCOMPLETED"
     */
    public void testcase02StartBootAction() {
        Xlog.e(TAG, "testcase02StartBootAction");

        // Start SmsRegService under test, while intentAction is
        // "BOOTCOMPLETED" and Extras is null
        startSmsRegService("BOOTCOMPLETED", null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        Object objectSimStateReceiver =
                smsRegServiceGetDeclaredField("mSimStateReceiver");
        assertNotNull(objectSimStateReceiver);
        Xlog.e(TAG, "objectSimStateReceiver is not null");

        Object objectSmsReceivedReceiver =
                smsRegServiceGetDeclaredField("mSmsReceivedReceiver");
        assertNotNull(objectSmsReceivedReceiver);
        Xlog.e(TAG, "objectSmsReceivedReceiver is not null");
    }

    /*
     * Start SmsRegService with intent action "SIM_STATE_CHANGED" not loaded
     */
    public void testcase03StartSimStateChangeNotLoaded() {
        Xlog.e(TAG, "testcase03StartSimStateChangeNotLoaded");
        // Start SmsRegService under test, while intentAction is
        // "SIM_STATE_CHANGED" and Extras is null
        startSmsRegService("SIM_STATE_CHANGED", null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        // "isSendMsg" should be false
        Boolean isSendMsg = (Boolean)
                smsRegServiceGetDeclaredField("mIsSendMsg");
        assertNotNull(isSendMsg);
        assertFalse(isSendMsg.booleanValue());
        Xlog.e(TAG, "Var isSendMsg: " + isSendMsg.booleanValue());
    }

    /*
     * Start SmsRegService with intent action "SIM_STATE_CHANGED"
     */
    public void testcase04StartSimStateChangeLoaded() {
        Xlog.e(TAG, "testcase04StartSimStateChangeLoaded");

        // Start SmsRegService under test, while intentAction is
        // "SIM_STATE_CHANGED" and Extras is bundle
        Bundle bundle = new Bundle();
        bundle.putString(IccCardConstants.INTENT_KEY_ICC_STATE,
                IccCardConstants.INTENT_VALUE_ICC_LOADED);
        startSmsRegService("SIM_STATE_CHANGED", bundle);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        TelephonyManagerEx mTelephonyManager = (TelephonyManagerEx)
                smsRegServiceGetDeclaredField("mTelephonyManager");
        assertNotNull(mTelephonyManager);
        Xlog.e(TAG, "mTelephonyManager is not null");

        // Try to send Register Message
        int simState = mTelephonyManager.getSimState(0);
        if (TelephonyManager.SIM_STATE_READY == simState) {
            String currentSimOperator = mTelephonyManager
                    .getSimOperator(0);
            Xlog.i(TAG, "A sim card ready, operator is "
                    + currentSimOperator);
            if (currentSimOperator == null
                    || currentSimOperator.trim().equals("")) {
                String simIMSI = (String)
                        smsRegServiceGetDeclaredField("simIMSI");
                assertNotNull(simIMSI);
                Xlog.e(TAG, "simIMSI : " + simIMSI);
            }
        }
    }

    /*
     * Start SmsReg Service with intent action "REGISTER_SMS_RECEIVED"
     */
    public void testcase05StartRegSimReceived() {
        Xlog.e(TAG, "testcase05StartRegSimReceived");

        IBinder binder = ServiceManager.getService("DMAgent");
        DMAgent agent = DMAgent.Stub.asInterface(binder);
        // The last IMSI
        String savedIMSI = null;
        String resetIMSI = null;
        String expIMSI = "460000912121001";

        try {
            if (agent.readIMSI() != null) {
                savedIMSI = new String(agent.readIMSI());
                Xlog.e(TAG, "testcase05_ IMSI" + savedIMSI);
            }
        } catch (RemoteException e) {
            Xlog.e(TAG, "get IMSI failed, readIMSI failed!");
            e.printStackTrace();
        }

        // Start SmsRegService under test, while intentAction
        // is "REGISTER_SMS_RECEIVED",Extras is bundle
        Bundle bundle = new Bundle();
        bundle.putString("mIMSI", expIMSI);
        startSmsRegService("REGISTER_SMS_RECEIVED", bundle);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        // Wait for SmsRegService write the IMSI
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            Xlog.e(TAG, "InterruptedException Exception:" + e.getMessage());
            e.printStackTrace();
        }

        // read the saved IMSI
        try {
            if (agent.readIMSI() != null) {
                resetIMSI = new String(agent.readIMSI());
            }
            Xlog.e(TAG, "testcase05 :resetIMSI" + resetIMSI);
        } catch (RemoteException e) {
            Xlog.e(TAG, "get resetIMSI failed!Exception:" + e.getMessage());
            e.printStackTrace();
        }
        assertEquals(expIMSI, resetIMSI);

        boolean isWriteIMSI = false;
        try {
            if (savedIMSI != null) {
                isWriteIMSI = agent.writeIMSI(savedIMSI.getBytes());
            } else {
                isWriteIMSI = agent.writeIMSI(null);
            }
            Xlog.e(TAG, "testcase05: isWriteIMSI" + isWriteIMSI);
        } catch (RemoteException e) {
            Xlog.e(TAG, "WriteIMSI failed!Exception:" + e.getMessage());
            e.printStackTrace();
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Xlog.e(TAG, "InterruptedException Exception:" + e.getMessage());
            e.printStackTrace();
        }

        Object objectSimStateReceiver =
                smsRegServiceGetDeclaredField("mSimStateReceiver");
        assertNull(objectSimStateReceiver);
        Xlog.e(TAG, "simStateReceiver is null");

        Object objectSmsReceivedReceiver =
                smsRegServiceGetDeclaredField("mSmsReceivedReceiver");
        assertNull(objectSmsReceivedReceiver);
        Xlog.e(TAG, "objectSmsReceivedReceiver is null");
    }

    /*
     * Start SmsReg Service with action "BOOTCOMPLETED", test stopService
     */
    public void testcase06StopService() {
        Xlog.e(TAG, "testcase06StopService");

        // Start SmsRegService under test, while intentAction is
        // "BOOTCOMPLETED" and Extras is null
        startSmsRegService("BOOTCOMPLETED", null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Xlog.e(TAG, "testcase06_getDeclaredMethod: stopService");
            Method stopService = mSmsRegService.getClass()
                    .getDeclaredMethod("stopService", null);
            stopService.setAccessible(true);
            stopService.invoke(mSmsRegService, null);

            Object objectSimStateReceiver = smsRegServiceGetDeclaredField(
                    "mSimStateReceiver");
            assertNull(objectSimStateReceiver);
            Xlog.e(TAG, "simStateReceiver is null");

            Object objectSmsReceivedReceiver =
                    smsRegServiceGetDeclaredField("mSmsReceivedReceiver");
            assertNull(objectSmsReceivedReceiver);
            Xlog.e(TAG, "objectSmsReceivedReceiver is not null");
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (SecurityException e) {
            Xlog.e(TAG, "* SecurityException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalArgumentException e) {
            Xlog.e(TAG, "* IllegalArgumentException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Test of Bind SmsRegService
     */
    public void testcase07Bindable() {
        Xlog.e(TAG, "testcase07_Bindable");
        Intent intent = new Intent();
        intent.setClass(getContext(), SmsRegService.class);
        IBinder service = bindService(intent);

        assertNull(service);
        Xlog.e(TAG, "testcase07_Bindable:service is null");
    }

    /*
     * Get Declared Method "isRegister" of mSmsRegService
     */
    public void testcase08MethodisNeedRegister() {
        Xlog.e(TAG, "testcase08MethodisNeedRegister");

        // Start SmsRegService under test, Action and Extras are null
        startSmsRegService(null, null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Method isNeedRegister = mSmsRegService.getClass().
                    getDeclaredMethod("isNeedRegister", null);
            isNeedRegister.setAccessible(true);
            isNeedRegister.invoke(mSmsRegService);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (SecurityException e) {
            Xlog.e(TAG, "* SecurityException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalArgumentException e) {
            Xlog.e(TAG, "* IllegalArgumentException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared Method "isRegisterGemini" of mSmsRegService
     */
    public void testcase09MethodIsNeedRegisterGemini() {
        Xlog.e(TAG, "testcase09MethodIsNeedRegisterGemini");

        // Start SmsRegService under test, Action and Extras are null
        startSmsRegService(null, null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Method isNeedRegisterGemini = mSmsRegService.getClass()
                    .getDeclaredMethod("isNeedRegisterGemini", null);
            isNeedRegisterGemini.setAccessible(true);
            isNeedRegisterGemini.invoke(mSmsRegService);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (SecurityException e) {
            Xlog.e(TAG, "* SecurityException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalArgumentException e) {
            Xlog.e(TAG, "* IllegalArgumentException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared Method "getSendPendingIntent" of mSmsRegService
     */
    public void testcase10GetPendingIntent() {
        Xlog.e(TAG, "testcase10GetPendingIntent");

        // Start SmsRegService under test, while intentAction is
        // "SIM_STATE_CHANGED" and Extras is bundle
        Bundle bundle = new Bundle();
        bundle.putString(IccCardConstants.INTENT_KEY_ICC_STATE,
                IccCardConstants.INTENT_VALUE_ICC_LOADED);
        startSmsRegService("SIM_STATE_CHANGED", bundle);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Method fGetPend = mSmsRegService.getClass().getDeclaredMethod(
                    "getSendPendingIntent", int.class);
            fGetPend.setAccessible(true);
            fGetPend.invoke(mSmsRegService, 0);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared Method "onStop" & "onDestroy" of mSmsRegService
     */
    public void testcase11MethodStopDestroy() {
        Xlog.e(TAG, "testcase11MethodStop");

        // Start SmsRegService under test, while intentAction is
        // "SIM_STATE_CHANGED" and Extras is bundle
        Bundle bundle = new Bundle();
        bundle.putString(IccCardConstants.INTENT_KEY_ICC_STATE,
                IccCardConstants.INTENT_VALUE_ICC_LOADED);
        startSmsRegService("SIM_STATE_CHANGED", bundle);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Method fOnStop = mSmsRegService.getClass().getDeclaredMethod(
                    "onStop", null);
            fOnStop.setAccessible(true);
            fOnStop.invoke(mSmsRegService, null);

            Method fOnDestroy = mSmsRegService.getClass()
                    .getDeclaredMethod("onDestory", null);
            fOnDestroy.setAccessible(true);
            fOnDestroy.invoke(mSmsRegService, null);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared Method "sendRegisterMessage[Gemini]" of mSmsRegService
     */
    public void testcase12MethodSendRegMsg() {
        Xlog.e(TAG, "testcase12MethodSendRegMsg");

        // Start SmsRegService under test, while intentAction is
        // "SIM_STATE_CHANGED" and Extras is bundle
        Bundle bundle = new Bundle();
        bundle.putString(IccCardConstants.INTENT_KEY_ICC_STATE,
                IccCardConstants.INTENT_VALUE_ICC_LOADED);
        startSmsRegService("SIM_STATE_CHANGED", bundle);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Method fSendRegMsg = mSmsRegService.getClass()
                    .getDeclaredMethod("sendRegisterMessage", null);
            fSendRegMsg.setAccessible(true);
            fSendRegMsg.invoke(mSmsRegService, null);

            Method fSendRegMsgGemini = mSmsRegService.getClass()
                    .getDeclaredMethod("sendRegisterMessageGemini",
                            int.class);
            fSendRegMsgGemini.setAccessible(true);
            fSendRegMsgGemini.invoke(mSmsRegService, 0);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared Method "getSimCardMatchCustomized[Gemini]" of mSmsRegService
     */
    public void testcase13MethodGetSimCus() {
        Xlog.e(TAG, "testcase13MethodGetSimCus");

        // Start SmsRegService under test, while intentAction is
        // "SIM_STATE_CHANGED" and Extras is bundle
        Bundle bundle = new Bundle();
        bundle.putString(IccCardConstants.INTENT_KEY_ICC_STATE,
                IccCardConstants.INTENT_VALUE_ICC_LOADED);
        startSmsRegService("SIM_STATE_CHANGED", bundle);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Method fGetSimCus = mSmsRegService.getClass().
                    getDeclaredMethod("getSimCardMatchCustomized", null);
            fGetSimCus.setAccessible(true);
            fGetSimCus.invoke(mSmsRegService, null);

            Method fGetSimCusGemini = mSmsRegService.getClass().
                    getDeclaredMethod("getSimCardMatchCustomizedGemini",
                            null);
            fGetSimCusGemini.setAccessible(true);
            fGetSimCusGemini.invoke(mSmsRegService, null);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared Method "onServiceStateChanged" of PhoneStateListener[Gemini]
     * in mSmsRegService
     */
    public void testcase14FieldListener() {
        Xlog.e(TAG, "testcase14FieldListener");

        // Start the SmsRegService under test,intentAction is
        // "BOOTCOMPLETED",Extras is bundle
        startSmsRegService("SIM_STATE_CHANGED", null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            ServiceState serviceState = new ServiceState();
            serviceState.setState(ServiceState.STATE_IN_SERVICE);
            Xlog.e(TAG, "testcase14 -- fake serviceState "
                            + (serviceState.toString()));

            PhoneStateListener mPhoneStateListener = (PhoneStateListener)
                    smsRegServiceGetDeclaredField("mPhoneStateListener");
            assertNotNull(mPhoneStateListener);
            Method fOnServState = mPhoneStateListener.getClass()
                    .getDeclaredMethod("onServiceStateChanged",
                            ServiceState.class);
            assertNotNull(fOnServState);
            fOnServState.setAccessible(true);
            fOnServState.invoke(mPhoneStateListener, serviceState);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared Method "onServiceStateChanged" of PhoneStateListener
     * in mSmsRegService
     */
    public void testcase15GeminiFieldListener() {
        Xlog.e(TAG, "testcase15GeminiFieldListener");

        // Start the SmsRegService under test,intentAction is
        // "BOOTCOMPLETED",Extras is bundle
        startSmsRegService("SIM_STATE_CHANGED", null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            ServiceState serviceState = new ServiceState();
            serviceState.setState(ServiceState.STATE_IN_SERVICE);
            Xlog.e(TAG, "testcase15 -- fake serviceState "
                            + (serviceState.toString()));

            PhoneStateListener mPhoneStateListenerGemini = (PhoneStateListener)
                    smsRegServiceGetDeclaredField("mPhoneStateListenerGemini");
            assertNotNull(mPhoneStateListenerGemini);
            Method fOnServStateGemini = mPhoneStateListenerGemini
                    .getClass().getDeclaredMethod("onServiceStateChanged",
                            ServiceState.class);
            assertNotNull(fOnServStateGemini);
            fOnServStateGemini.setAccessible(true);
            fOnServStateGemini.invoke(mPhoneStateListenerGemini,
                    serviceState);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get fileds "TimerAndHandler" from mSmsRegService
     */
    public void testcase16FieldTimerAndHandler() {
        Xlog.e(TAG, "testcase16FieldInnerTimerAndHandler");

        // Start the SmsRegService under test,intentAction is
        // "BOOTCOMPLETED",Extras is bundle
        startSmsRegService("BOOTCOMPLETED", null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            TimerTask mTimerTask = (TimerTask)
                    smsRegServiceGetDeclaredField("mTimerTask");
            Method fTimerRun = mTimerTask.getClass().getDeclaredMethod(
                    "run", null);
            fTimerRun.setAccessible(true);
            fTimerRun.invoke(mTimerTask, null);

            Handler mhandler = (Handler)
                    smsRegServiceGetDeclaredField("mHandler");
            Method fHandler = mhandler.getClass().getDeclaredMethod(
                    "handleMessage", Message.class);
            fHandler.setAccessible(true);
            fHandler.invoke(mhandler, new Message());
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get fileds "mSmsRecReceiver" from mSmsRegService
     */
    public void testcase17FieldSimReceiver() {
        Xlog.e(TAG, "testcase17FieldSimReceiver");

        // Start the SmsRegService under test,intentAction is
        // "BOOTCOMPLETED",Extras is bundle
        startSmsRegService("BOOTCOMPLETED", null);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Object mSmsRecReceiver = smsRegServiceGetDeclaredField(
                    "mSmsReceivedReceiver");
            Class mClassSmsRecReceiver = Class.forName("com.mediatek."
                    + "smsreg.SmsRegService$SmsReceivedReceiver");
            assertNotNull(mClassSmsRecReceiver);

            Method fOnReceive = mClassSmsRecReceiver.getDeclaredMethod(
                    "onReceive", Context.class, Intent.class);
            fOnReceive.setAccessible(true);
            fOnReceive.invoke(mSmsRecReceiver, getContext(), new Intent(
                    "android.intent.action.DM_REGISTER_SMS_RECEIVED"));
        } catch (ClassNotFoundException e) {
            Xlog.e(TAG, "* ClassNotFoundException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (UnsupportedOperationException e) {
            Xlog.e(TAG, "* UnsupportedOperationException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared member "mXmlG" of mSmsRegService
     */
    public void testcase18MemberXMLgenerator() {
        Xlog.e(TAG, "testcase18MemberXMLgenerator");

        Bundle bundle = new Bundle();
        bundle.putString(IccCardConstants.INTENT_KEY_ICC_STATE,
                IccCardConstants.INTENT_VALUE_ICC_LOADED);
        // Start the SmsRegService under test,intentAction is
        // "SIM_STATE_CHANGED",Extras is bundle
        startSmsRegService("SIM_STATE_CHANGED", bundle);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Object mXmlG = smsRegServiceGetDeclaredField("mXmlG");
            Class mClassXmlG = Class.forName("com.mediatek."
                    + "smsreg.XMLGenerator");

            Method fGetOemName = mClassXmlG.getDeclaredMethod("getOemName",
                    null);
            fGetOemName.setAccessible(true);
            fGetOemName.invoke(mXmlG, null);

            Method fGetSrcPort = mClassXmlG.getDeclaredMethod("getSrcPort",
                    null);
            fGetSrcPort.setAccessible(true);
            fGetSrcPort.invoke(mXmlG, null);

            Method fGetSmsInfoList = mClassXmlG.getDeclaredMethod(
                    "getSmsInfoList", null);
            fGetSmsInfoList.setAccessible(true);
            List<?> mSmsInfoList = (List<?>) fGetSmsInfoList
                    .invoke(mXmlG, null);

            Class mClassSmsInfoList = Class
                    .forName("com.mediatek.smsreg.SmsInfoUnit");
            if (mSmsInfoList.size() > 0) {
                Method fGetPrefix = mClassSmsInfoList.getDeclaredMethod(
                        "getPrefix", null);
                fGetPrefix.setAccessible(true);
                fGetPrefix.invoke(mSmsInfoList.get(0), null);

                Method fGetContent = mClassSmsInfoList.getDeclaredMethod(
                        "getContent", null);
                fGetContent.setAccessible(true);
                fGetContent.invoke(mSmsInfoList.get(0), null);

                Method fGetPostfix = mClassSmsInfoList.getDeclaredMethod(
                        "getPostfix", null);
                fGetPostfix.setAccessible(true);
                fGetPostfix.invoke(mSmsInfoList.get(0), null);
            }
        } catch (ClassNotFoundException e) {
            Xlog.e(TAG, "* ClassNotFoundException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared member "mInfoPersistentor" of mSmsRegService
     */
    public void testcase19MemberInfoPersistentor() {
        Xlog.e(TAG, "testcase19MemberInfoPersistentor");

        Bundle bundle = new Bundle();
        bundle.putString(IccCardConstants.INTENT_KEY_ICC_STATE,
                IccCardConstants.INTENT_VALUE_ICC_LOADED);
        // Start the SmsRegService under test,intentAction is
        // "SIM_STATE_CHANGED",Extras is bundle
        startSmsRegService("SIM_STATE_CHANGED", bundle);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            InfoPersistentor mInfoPersistentor;
            mInfoPersistentor = (InfoPersistentor) smsRegServiceGetDeclaredField("mInfoPersistentor");
            assertNotNull(mInfoPersistentor);
            if (mInfoPersistentor != null) {
                Xlog.e(TAG, "mInfo is not null");
                mInfoPersistentor.getSavedIMSI();

                Method fSetSavedCTA = mInfoPersistentor.getClass()
                        .getDeclaredMethod("setSavedCTA", String.class);
                fSetSavedCTA.setAccessible(true);

                String savedCTA = "1";
                fSetSavedCTA.invoke(mInfoPersistentor, savedCTA);
                assertEquals(mInfoPersistentor.getSavedCTA(),
                        Integer.parseInt(savedCTA));
            }
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Get Declared member "mSmsBuilder" of mSmsRegService
     */
    public void testcase20MemberSmsBuilder() {
        Xlog.e(TAG, "testcase20MemberSmsBuilder");

        Bundle bundle = new Bundle();
        bundle.putString(IccCardConstants.INTENT_KEY_ICC_STATE,
                IccCardConstants.INTENT_VALUE_ICC_LOADED);
        // Start the SmsRegService under test,intentAction is
        // "SIM_STATE_CHANGED",Extras is bundle
        startSmsRegService("SIM_STATE_CHANGED", bundle);
        mSmsRegService = getService();
        assertNotNull(mSmsRegService);

        try {
            Object mSmsBuilder = smsRegServiceGetDeclaredField("mSmsBuilder");
            Class mClassSmsBuilder = Class
                    .forName("com.mediatek.smsreg.SmsBuilder");
            assertNotNull(mClassSmsBuilder);

            Object mXmlG = smsRegServiceGetDeclaredField("mXmlG");
            Class mClassConfig = Class.forName("com.mediatek."
                    + "smsreg.ConfigInfoGenerator");
            assertNotNull(mXmlG);

            Method fGetContentInfo = mClassSmsBuilder.getDeclaredMethod(
                    "getContentInfo", mClassConfig, String.class);
            fGetContentInfo.setAccessible(true);
            fGetContentInfo.invoke(mSmsBuilder, mXmlG, "getimsi");
            fGetContentInfo.invoke(mSmsBuilder, mXmlG, "getimei");
            fGetContentInfo.invoke(mSmsBuilder, mXmlG, "getversion");
            fGetContentInfo.invoke(mSmsBuilder, mXmlG, "getproduct");
            fGetContentInfo.invoke(mSmsBuilder, mXmlG, "getvendor");
            fGetContentInfo.invoke(mSmsBuilder, mXmlG, "getOem");
            fGetContentInfo.invoke(mSmsBuilder, mXmlG, "invalid_cmd");

            Method fGetSmsContent = mClassSmsBuilder.getDeclaredMethod(
                    "getSmsContent", mClassConfig, int.class);
            fGetSmsContent.setAccessible(true);
            fGetSmsContent.invoke(mSmsBuilder, mXmlG, 0);
            fGetSmsContent.invoke(mSmsBuilder, mXmlG, 1);
            assertNull(fGetSmsContent.invoke(mSmsBuilder, mXmlG, -1));
        } catch (ClassNotFoundException e) {
            Xlog.e(TAG, "* ClassNotFoundException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (NoSuchMethodException e) {
            Xlog.e(TAG, "* NoSuchMethodException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (InvocationTargetException e) {
            Xlog.e(TAG, "* InvocationTargetException:" + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
    }

    /*
     * Start SmsRegService
     */
    private void startSmsRegService(String intentAction, Bundle bundle) {
        Xlog.e(TAG, "startSmsRegService intentAction = " + intentAction);

        Intent intent = new Intent();

        if (intentAction != null) {
            intent.setAction(intentAction);
        }
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setClass(getContext(), SmsRegService.class);
        startService(intent);
    }

    /*
     * Get the declared field of SmsRegService
     */
    private Object smsRegServiceGetDeclaredField(String fieldName) {
        Xlog.e(TAG, "smsRegServiceGetDeclaredField, fieldName= " + fieldName);
        Object object = null;
        Field field = null;

        try {
            field = mSmsRegService.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            object = field.get(mSmsRegService);
        } catch (NoSuchFieldException e) {
            Xlog.e(TAG, "* NoSuchFieldException: " + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        } catch (IllegalAccessException e) {
            Xlog.e(TAG, "* IllegalAccessException: " + e.getMessage());
            e.printStackTrace();
            assertNotNull(null);
        }
        return object;
    }
}
