/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.smsreg.test;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.ProviderTestCase2;

import com.mediatek.smsreg.SmsRegContentProvider;
import com.mediatek.xlog.Xlog;

import java.io.IOException;

public class SmsRegContentProviderTest extends ProviderTestCase2<SmsRegContentProvider> {
    private static final String TAG = "test/SmsRegContentProviderTest";
    private static final String AUTHORITY = "com.mediatek.providers.smsreg";
    private static final String SMSREG_PROVIDER_URI =
            "content://com.mediatek.providers.smsreg";
    private String[] mSmsRegColumn = {"enable", "imei", "op", "smsNumber",
            "smsPort", "manufacturer", "product", "version"};
    private ContentResolver mResolver;

    public SmsRegContentProviderTest() {
        super(SmsRegContentProvider.class,AUTHORITY);
        Xlog.e(TAG, "SmsRegContentProviderTest");
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        //we can not insert a event by MyCalendarProvider,
        // so we use real context contentResolver here.
        mResolver = getContext().getContentResolver();
        Xlog.e(TAG, "SmsRegContentProviderTest:setUp");
    }

    @Override
    protected void tearDown() throws Exception {
        mResolver = null;
        Xlog.e(TAG, "SmsRegContentProviderTest:tearDown");
        super.tearDown();
    }

    public void testcase01providerQuery() throws IOException {
        Cursor cursor = mResolver.query(Uri.parse(SMSREG_PROVIDER_URI),
                mSmsRegColumn, null, null, null);
        assertNotNull(cursor);
        if (cursor == null) {
            Xlog.e(TAG, "case01Query:cursor == null");
        } else {
            try {
                cursor.moveToFirst();
                String enable = cursor.getString(cursor.
                        getColumnIndex("enable"));
                Xlog.e(TAG, "case01Query:enable = " + enable);

                String imei = cursor.getString(cursor.getColumnIndex("imei"));
                Xlog.e(TAG, "case01Query:imei = " + imei);

                String op = cursor.getString(cursor.getColumnIndex("op"));
                Xlog.e(TAG, "case01Query:op = " + op);

                String smsNumber = cursor.getString(cursor.
                        getColumnIndex("smsNumber"));
                Xlog.e(TAG, "case01Query:smsNumber = " + smsNumber);

                String smsPort = cursor.getString(cursor.
                        getColumnIndex("smsPort"));
                Xlog.e(TAG, "case01Query:smsPort = " + smsPort);

                String manufacturer = cursor.getString(cursor.
                        getColumnIndex("manufacturer"));
                Xlog.e(TAG, "case01Query:manufacturer = " + manufacturer);

                String product = cursor.getString(cursor.
                        getColumnIndex("product"));
                Xlog.e(TAG, "case01Query:product = " + product);

                String version = cursor.getString(cursor.
                        getColumnIndex("version"));
                Xlog.e(TAG, "case01Query:version = " + version);
            } finally {
                cursor.close();
            }
        }
    }

    public void testcase02providerGetType() throws IOException {
        String uriType = mResolver.getType(Uri.parse(SMSREG_PROVIDER_URI));
        assertNull(uriType);
    }

    public void testcase03providerInsert() throws IOException {
        ContentValues conValues = new ContentValues();
        conValues.put("product", "MTKMDMP1");
        Uri uri = mResolver.insert(Uri.parse(SMSREG_PROVIDER_URI), conValues);
        assertNull(uri);
    }

    public void testcase04providerUpdate() throws IOException {
        ContentValues conValues = new ContentValues();
        conValues.put("product", "MTKMDMP1");
        int valueUpdate = mResolver.update(Uri.parse(SMSREG_PROVIDER_URI),
                conValues, null, null);
        assertEquals(valueUpdate, 0);
    }

    public void testcase05providerDelete() throws IOException {
        int valDel = mResolver.delete(Uri.parse(SMSREG_PROVIDER_URI),
                null, null);
        assertEquals(valDel, 0);
    }
}