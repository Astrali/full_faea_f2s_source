/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.smsreg.test;

import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.test.AndroidTestCase;
import android.test.mock.MockContext;

import com.mediatek.common.dm.DMAgent;
import com.mediatek.smsreg.SmsRegReceiver;
import com.mediatek.xlog.Xlog;

import java.util.ArrayList;
import java.util.List;

public class SmsRegReceiverTest extends AndroidTestCase {
    public static final String TAG = "test/SmsRegReceiverTest";
    private SmsRegReceiver mReceiver;
    private TestContext mContext;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mReceiver = new SmsRegReceiver();
        mContext = new TestContext();
        Xlog.e(TAG, "SmsRegReceiverTest:setUp");
    }

    @Override
    protected void tearDown() throws Exception {
        Xlog.e(TAG, "SmsRegReceiverTest:tearDown");
        super.tearDown();
    }

    public void testcase01StartSmsRegReceiver() {
        Xlog.e(TAG, "testcase01StartSmsRegReceiver");
        IBinder binder = ServiceManager.getService("DMAgent");
        String savedCAT = null;

        if (binder == null) {
            Xlog.e(TAG, "get DMAgent fail! binder is null!");
            return;
        }
        DMAgent agent = DMAgent.Stub.asInterface(binder);
        boolean isWriteCTA = false;

        try {
            if (agent.readCTA() != null) {
                savedCAT = new String(agent.readCTA());
            }
            Xlog.e(TAG, "testcase01_readCTA:savedCAT" + savedCAT);
        } catch (RemoteException e) {
            Xlog.e(TAG, "readCTA failed!Exception:" + e.getMessage());
            e.printStackTrace();
        }

        try {
            String valCTA = "1";
            isWriteCTA = agent.writeCTA(valCTA.getBytes());
            Xlog.e(TAG, "testcase01_setCTA:isWriteCTA" + isWriteCTA);
        } catch (RemoteException e) {
            Xlog.e(TAG, "writeCTA failed!Exception:" + e.getMessage());
            e.printStackTrace();
        }

        Intent intent = new Intent();
        intent.setAction("android.intent.action.BOOT_COMPLETED");
        Xlog.e(TAG, "testcase01StartSmsRegReceiver:setAction");
        //Add this intent to receiverintent list
        mContext.addIntent(intent);

        try {
            Xlog.e(TAG, "testcase01mReceiver.onReceive");
            mReceiver.onReceive(mContext, intent);
        } catch (UnsupportedOperationException e) {
            Xlog.e(TAG, "UnsupportedOperationException: " + e.getMessage());
            e.printStackTrace();
        }
        assertTrue(mContext.getmStateService());

        Intent receivedIntent = mContext.getReceiverIntents().get(0);
        assertNotNull(receivedIntent.getAction());
        assertEquals("android.intent.action.BOOT_COMPLETED",
                receivedIntent.getAction());

        if (null == savedCAT) {
            try {
                String cTA = "0";
                isWriteCTA = agent.writeCTA(cTA.getBytes());
                Xlog.e(TAG, "setCTA = 0:isWriteCTA" + isWriteCTA);
            } catch (RemoteException e) {
                Xlog.e(TAG, "setCTA = 0failed!Exception:" + e.getMessage());
                e.printStackTrace();
            }
        } else {
            try {
                isWriteCTA = agent.writeCTA(savedCAT.getBytes());
                Xlog.e(TAG, "resetCTA:isWriteCTA" + isWriteCTA);
            } catch (RemoteException e) {
                Xlog.e(TAG, "resetCTA failed!Exception:" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void testcase02notStartSmsRegReceiver() {
        Xlog.e(TAG, "testcase02notStartSmsRegReceiver");
        Intent intent = new Intent();
        intent.setAction("notBOOT_COMPLETED");
        mContext.addIntent(intent);
        Xlog.e(TAG, "testcase02notStartSmsRegReceiver:setAction");

        try {
            Xlog.e(TAG, "testcase02notStartSmsRegReceiver:onReceive");
            mReceiver.onReceive(mContext, intent);
        } catch (UnsupportedOperationException e) {
            Xlog.e(TAG, "UnsupportedOperationException: " + e.getMessage());
            e.printStackTrace();
        }
        assertFalse(mContext.getmStateService());

        Intent receivedIntent = mContext.getReceiverIntents().get(0);
        Xlog.e(TAG, "testcase02notStartSmsRegReceiver:getReceiverIntents");
        assertNotNull(receivedIntent.getAction());
        Xlog.e(TAG, "testcase02notStartSmsRegReceiver:getAction");
        assertEquals("notBOOT_COMPLETED", receivedIntent.getAction());
    }
}

class TestContext extends MockContext {
    private List<Intent> mReceiverIntents = new ArrayList<Intent>();
    private boolean mStateService = false;

    @Override
    public String getPackageName() {
        return "com.mediatek.smsreg.test";
    }

    @Override
    public ComponentName startService(Intent xIntent) {
        setmStateService(true);
        return new ComponentName("com.mediatek.smsreg", "SmsRegService");
    }

    public boolean getmStateService() {
        return mStateService;
    }

    public void setmStateService(boolean valState) {
        mStateService = valState;
    }

    public void addIntent(Intent xIntent) {
        mReceiverIntents.add(xIntent);
    }

    public List<Intent> getReceiverIntents() {
        return mReceiverIntents;
    }
}
