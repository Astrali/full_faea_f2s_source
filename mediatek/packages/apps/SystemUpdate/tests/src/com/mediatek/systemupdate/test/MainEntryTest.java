package com.mediatek.systemupdate.test;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;
import android.view.Menu;
import android.widget.ListView;
import android.widget.TextView;


import com.mediatek.systemupdate.R;
import com.mediatek.common.featureoption.FeatureOption;
import com.mediatek.systemupdate.MainEntry;
import com.mediatek.xlog.Xlog;

public class MainEntryTest extends
        ActivityInstrumentationTestCase2<MainEntry> {

    private final String TAG = "SystemUpdate/MainEntryTest";

    private static final String OTA_PREFERENCE = "googleota";
    private static final String OTA_PRE_STATUS = "downlaodStatus";
    private static final String OTA_UNZ_STATUS = "isunzip";
    private static final String OTA_REN_STATUS = "isrename";
    private static final String OTA_PRE_DOWNLOAND_PERCENT = "downloadpercent";
    private static final String EXTERNAL_USB_STORAGE = "usbotg";

    private static final int STATE_QUERYNEWVERSION = 0;
    private static final int STATE_NEWVERSION_READY = 1;

    /*
     * The testing activity
     */
    private MainEntry mActivity;

    private SharedPreferences mPreference = null;

    private Context mContext;

    /*
     * The intsrumenation
     */
    Instrumentation mInst;

    /*
     * Constructor
     */
    public MainEntryTest() {
        super("com.mediatek.systemupdate", MainEntry.class);

    }

    /*
     * Sets up the test environment before each test.
     */
    @Override
    protected void setUp() throws Exception {

        super.setUp();

        setActivityInitialTouchMode(false);

        mInst = getInstrumentation();

        mContext = mInst.getTargetContext();

        mPreference = mContext.getSharedPreferences(OTA_PREFERENCE,
                Context.MODE_WORLD_READABLE);

    }

    protected void tearDown() throws Exception {

        if (mActivity != null) {
            mActivity.finish();
            Xlog.i(TAG, "activity finish");

        }
        super.tearDown();
    }

    private void resetDownloadDesctiptor() {

        mPreference.edit().putInt(OTA_PRE_DOWNLOAND_PERCENT, -1).commit();
        mPreference.edit().putBoolean(OTA_UNZ_STATUS, false).commit();
        mPreference.edit().putBoolean(OTA_REN_STATUS, false).commit();
        mPreference.edit().putInt(OTA_PRE_STATUS, STATE_QUERYNEWVERSION)
                .commit();

    }

/*    private boolean isNetWorkAvailable() {

        boolean ret = false;

        try {
            ConnectivityManager connetManager = (ConnectivityManager) mContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connetManager == null) {
                Xlog.e(TAG, "isNetWorkAvailable connetManager = null");
                return ret;
            }
            NetworkInfo[] infos = connetManager.getAllNetworkInfo();
            if (infos == null) {
                return ret;
            }

            for (int i = 0; i < infos.length && infos[i] != null; i++) {
                if (infos[i].isConnected() && infos[i].isAvailable()) {
                    ret = true;
                    break;
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
        Xlog.i(TAG, "isNetWorkAvailable result is : " + ret);
        return ret;
    }*/

    /*
     * Test the pre-condition
     */
    public void testcase01_Preconditions() {

        assertTrue(mPreference != null);
        resetDownloadDesctiptor();
        assertTrue(mPreference.getInt(OTA_PRE_STATUS, -1) == STATE_QUERYNEWVERSION);

    }



	public void testcase02_QueryResult() {
		
        if (!Util.isNetWorkAvailable(mContext, null)) {
    		resetDownloadDesctiptor();
    		mActivity = getActivity();

    		assert (mActivity != null);

    		for (int i = 0; i <= 5; i++) {

    			if (mPreference.getInt(OTA_PRE_STATUS, -1) == STATE_QUERYNEWVERSION) {
    				try {
    					Thread.sleep(3000);
    				} catch (InterruptedException e) {
    					fail("Thread.sleep(3000)");
    				}
    			} else {
    				break;
    			}
    		}
            Xlog.i(TAG, "query finish");
    		assertTrue((mPreference.getInt(OTA_PRE_STATUS, -1) == (STATE_QUERYNEWVERSION))
    				|| (mPreference.getInt(OTA_PRE_STATUS, -1) == (STATE_NEWVERSION_READY)));
            Xlog.i(TAG, "assert finish");
        }



	}



}
