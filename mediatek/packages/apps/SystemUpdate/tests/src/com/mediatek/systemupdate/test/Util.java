/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.systemupdate.test;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.mediatek.common.featureoption.FeatureOption;
import com.mediatek.xlog.Xlog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


final class Util {
    public static final String TAG = "SystemUpdate/Util";



    private static String sAvailablePath;



    private static final String EXTERNAL_USB_STORAGE = "usbotg";

    private static String sExternalSdCardPath = null;

    private Util() {
    }

    static class PathName {
        public static final String OTA_PKG_FOLDER = "/googleota";
        public static final String PACKAGE_NAME = "/update.zip";
    }

    static boolean isSdcardAvailable(Context context) {

        if (FeatureOption.MTK_2SDCARD_SWAP || FeatureOption.MTK_SHARED_SDCARD) {

            sExternalSdCardPath = getExternalSDCardPath(context);

            Xlog.i(TAG, "sExternalSdCardPath = " + sExternalSdCardPath);

            if (sExternalSdCardPath != null) {
                StorageManager storManager = (StorageManager) context
                        .getSystemService(Context.STORAGE_SERVICE);

                if (storManager == null) {
                    return false;
                }

                return Environment.MEDIA_MOUNTED.equals(storManager
                        .getVolumeState(sExternalSdCardPath));

            } else {

                return false;

            }

        }

        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());

    }

    static String getExternalSDCardPath(Context context) {

        StorageManager storManager = (StorageManager) context
                .getSystemService(Context.STORAGE_SERVICE);

        if (storManager == null) {
            return null;
        }

        StorageVolume[] volumes = storManager.getVolumeList();

        if (volumes == null) {
            return null;
        }

        for (int i = 0; i < volumes.length; i++) {
            if ((volumes[i] != null) && (volumes[i].isRemovable())) {

                String path = volumes[i].getPath();
                if ((path != null) && (!path.contains(EXTERNAL_USB_STORAGE))) {
                    return path;
                }

            }
        }

        return null;

    }




    static String getAvailablePath(Context context) {
        if (sAvailablePath == null) {
            if (FeatureOption.MTK_2SDCARD_SWAP || FeatureOption.MTK_SHARED_SDCARD) {
                if (sExternalSdCardPath == null) {
                    getExternalSDCardPath(context);
                }
                sAvailablePath = sExternalSdCardPath;
            } else {
                File sdcardSystem = Environment.getExternalStorageDirectory();
                sAvailablePath = sdcardSystem.getPath();
            }
        }
        return sAvailablePath;
    }




    static boolean isNetWorkAvailable(Context context, String typeName) {

        Xlog.i(TAG, "isNetWorkAvailable: context = " + context + "typeName = " + typeName);

        boolean ret = false;

        ConnectivityManager connetManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connetManager == null) {
            Xlog.e(TAG, "isNetWorkAvailable connetManager = null");
            return ret;
        }
        NetworkInfo[] infos = connetManager.getAllNetworkInfo();
        if (infos == null) {
            return ret;
        }
        if ((typeName == null) || (typeName.length() <= 0)) {
            for (int i = 0; i < infos.length && infos[i] != null; i++) {
                if (infos[i].isConnected() && infos[i].isAvailable()) {
                    ret = true;
                    break;
                }
            }
        } else {
            for (int i = 0; i < infos.length && infos[i] != null; i++) {
                if (infos[i].getTypeName().equalsIgnoreCase(typeName) && infos[i].isConnected()
                        && infos[i].isAvailable()) {
                    Xlog.i(TAG, "isNetWorkAvailable name is : " + infos[i].getTypeName());
                    ret = true;
                    break;
                }
            }
        }

        Xlog.i(TAG, "isNetWorkAvailable result is : " + ret);
        return ret;
    }



    static String getPackagePathName(Context context) {

        String packagePath = getAvailablePath(context) + PathName.OTA_PKG_FOLDER;
        File dir = new File(packagePath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
//        if (FeatureOption.MTK_2SDCARD_SWAP || FeatureOption.MTK_SHARED_SDCARD) {
//            if (sExternalSdCardPath == null) {
//                getExternalSDCardPath(context);
//
//            }
//            return sExternalSdCardPath + DirAndFiles.OTA_PKG_FOLDER;
//
//        } else {
//            File sdcardSystem = Environment.getExternalStorageDirectory();
//
//            if (sdcardSystem != null) {
//                return sdcardSystem.getPath() + DirAndFiles.OTA_PKG_FOLDER;
//            } else {
//                return null;
//            }
//
//        }
        return packagePath;
    }



    public static String getPackageFileName(Context context) {
        String path = getPackagePathName(context) + PathName.PACKAGE_NAME;
        Xlog.i(TAG, "getPackageFileName = " + path);
        return path;
    }



}
