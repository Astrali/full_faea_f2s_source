package com.mediatek.systemupdate.test;

import android.test.InstrumentationTestRunner;
import junit.framework.TestSuite;

public class TestRunner extends InstrumentationTestRunner {
    @Override
    public TestSuite getAllTests() {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(MainEntryTest.class);
        suite.addTestSuite(OtaPkgManagerActivityTest.class);
/*        suite.addTestSuite(ContextBaseTests.class);
        suite.addTestSuite(DmAgentTests.class);
        suite.addTestSuite(WapConnectionTest.class);

        suite.addTestSuite(TestSmsRegXMLGenerator.class);
        suite.addTestSuite(TestSmsRegInterface.class);

        suite.addTestSuite(TestAGPSParamsProvider.class);
        suite.addTestSuite(TestConnParamsProvider.class);
        suite.addTestSuite(TestMMSParamsProvider.class);
        suite.addTestSuite(TestStreamingParamsProvider.class);
        suite.addTestSuite(TestWapParamsProvider.class);

        suite.addTestSuite(ScomoInstallTest.class);

        suite.addTestSuite(LawmoHandlerTest.class);

        suite.addTestSuite(UnpackTests.class);
        suite.addTestSuite(UpdateTests.class);*/

        return suite;
    }

}
