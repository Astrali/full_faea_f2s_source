package com.mediatek.videoorbplugin;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;

import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;
import com.mediatek.videoorbplugin.transcode.ProgressDialogFragment;
import com.mediatek.transcode.VideoTranscode;
import com.mediatek.videoorbplugin.transcode.PromptDialogFragment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class TranscoderActivity extends Activity
        implements ProgressDialogFragment.DialogActionListener {

    public static final String TAG = "vo.transcoder";
    public static final int UPDATE_INTERVAL = 200;
    public static final String TRANSCODE_PATH_BASE = Environment.getExternalStorageDirectory().toString();
    public static final String TRANSCODE_PATH = TRANSCODE_PATH_BASE + "/videoorb/";

    public static final int MSG_UPDATE_PROGRESS = 1;
    public static final int MSG_CANCEL = 2;
    public static final int MSG_SUCCESS = 3;
    public static final int MSG_FAILURE = 4;

    private TranscoderTask mTranscoderTask;
    private DialogInterface.OnClickListener mClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int whichButton) {
            switch (whichButton) {
                case DialogInterface.BUTTON_POSITIVE:
                    mProgressDlg = ProgressDialogFragment.newInstance(
                            R.string.transcoding_prompt);
                    mProgressDlg.show(getFragmentManager(), "progressDialog");
                    final int[] measurement = getResources().getIntArray(R.array.video_measure);
                    Log.v(TAG, "Transcode target width : " + measurement[0] + ", height : " + measurement[1]);
                    mTranscoderTask = new TranscoderTask(measurement[0], measurement[1]);
                    mTranscoderTask.execute(getNeedTranscodeVideoUris());
                break;
                case DialogInterface.BUTTON_NEGATIVE:
                    Log.d(TAG, "Transcoding is cancelled");
                    setResult(RESULT_CANCELED);
                    finish();
                break;
            }
        }
    };

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PromptDialogFragment prompt = PromptDialogFragment.newInstance(
                R.string.transcoding_prompt,
                R.string.transcoding_prompt_description,
                mClickListener);
        prompt.show(getFragmentManager(), "alertDialog");
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SUCCESS:
                    if (mProgressDlg != null) {
                        mHandler.removeMessages(MSG_UPDATE_PROGRESS);
                        mProgressDlg.setProgress(100);
                        mProgressDlg.dismiss();
                        mProgressDlg = null;
                        Log.v(TAG, "Transcoder succeed");
                        setResult(RESULT_OK);
                        finish();
                    }
                    break;
                case MSG_UPDATE_PROGRESS:
                    if (mProgressDlg != null) {
                        int progress = mTranscoderTask.getProgress();
                        mProgressDlg.setProgress(mTranscoderTask.getProgress());
                        if (progress < 100) {
                            mHandler.sendEmptyMessageDelayed(MSG_UPDATE_PROGRESS, UPDATE_INTERVAL);
                        }
                    }
                    break;
                case MSG_CANCEL:
                case MSG_FAILURE:
                    if (mProgressDlg != null) {
                        mHandler.removeMessages(MSG_UPDATE_PROGRESS);
                        mProgressDlg.dismiss();
                        mProgressDlg = null;
                        Log.d(TAG, "Transcoder was cancelled or failed : " + msg.what);
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    public ProgressDialogFragment mProgressDlg;
    private void cancelTranscode() {
        if (mTranscoderTask != null) {
            mTranscoderTask.cancelTranscode();
        }
        mHandler.removeMessages(MSG_UPDATE_PROGRESS);
        mHandler.sendEmptyMessageDelayed(MSG_CANCEL, 100);
    }

    /**
     * Implementation of ProgressDialogFragment.DialogActionListener
     * Called from ProgressDialogFragment.
     */
    public void onCancel() {
        cancelTranscode();
    }

    class TranscoderTask extends AsyncTask<ArrayList<Uri>, Integer, ArrayList<String>> {
        private static final int RESULT_OK = 0;
        private static final int RESULT_RESOULTION_TOO_HIGH = -1;
        private static final int RESULT_INVALID_VIDEO = -2;
        private long mTranscodeHandle;
        private int mProgress;
        private int mResult;
        private ArrayList<Uri> mSrc;
        private volatile int current;
        private volatile boolean mIsCancelled;
        private int mTargetWidth;
        private int mTargetHeight;
//        private ArrayList<Uri> mNotTranscoded;

        public TranscoderTask(int targetWidth, int targetHeight) {
            mTargetWidth = targetWidth;
            mTargetHeight = targetHeight;
        }

        protected void onPreExecute() {
            mTranscodeHandle = VideoTranscode.init();
            Log.v(TAG, "Before transcoding handle : " + mTranscodeHandle);
            mHandler.sendEmptyMessageDelayed(MSG_UPDATE_PROGRESS, UPDATE_INTERVAL);
            mResult = RESULT_OK;
        }

        protected ArrayList<String> doInBackground(ArrayList<Uri>... uris) {
            ArrayList<Uri> uriArray = uris[0];
            mSrc = uris[0];
            ArrayList<String> output = new ArrayList<String>();
            int count = mSrc.size();
            for(int i = 0; i < count; ++i) {
                current = i;
                Uri uri = mSrc.get(i);
                boolean needTranscode = isNeedTranscode(uri);
                Log.v(TAG, "Processing source : " + uri + ", need transcode : " + needTranscode);
//                if (!needTranscode) {
//                    if (mNotTranscoded == null) {
//                        mNotTranscoded = new ArrayList<Uri>();
//                    }
//                    mNotTranscoded.add(uri);
//                    continue;
//                }

                String source = getPathFromUri(uri);
                String destination = generateOutputPath(source);
                Log.v(TAG, "Transcoding source : " + source + ", destination : " + destination);
                int result = VideoTranscode.transcode(
                        mTranscodeHandle, source, destination, mTargetWidth, mTargetHeight, 0l, 10l * 1000);
                if (result != VideoTranscode.NO_ERROR) {
                    Log.d(TAG, "Transcoding Failed : " + result);
                    mResult = (result == VideoTranscode.ERROR_UNSUPPORTED_VIDEO) ?
                            RESULT_RESOULTION_TOO_HIGH : RESULT_INVALID_VIDEO;
                } else {
                    output.add(destination);
                    mResult = RESULT_OK;
                    Log.d(TAG, "Transcoding succeed.");
                }

                if (mIsCancelled) {
                    mResult = RESULT_CANCELED;
                    break;
                }
            }
            return output;
        }

        protected void onPostExecute(ArrayList<String> destination) {
            for (int i = 0 ; i < destination.size() && (!mIsCancelled); ++i) {
                ContentValues v = new ContentValues(6);
                long dateTaken = System.currentTimeMillis();
                long fileLength = new File(destination.get(i)).length();
                if (fileLength == 0) {
                    //mHandler.sendEmptyMessage(MSG_TRANSCODE_INVALID_SUICIDE); // TODO
                    return;
                }
                v.put(MediaStore.Video.Media.DATE_TAKEN, dateTaken);
                v.put(MediaStore.Video.Media.ARTIST, "videoorbplugin");
                v.put(MediaStore.Video.Media.SIZE, fileLength);
                v.put(MediaStore.Video.Media.DATA, destination.get(i));
                v.put(MediaStore.Video.Media.DISPLAY_NAME, destination.get(i));
                v.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");

                Uri videoTable = Uri.parse("content://media/external/video/media");
                Uri inserted = getContentResolver().insert(videoTable, v);
                if (inserted == null) {
                    Log.e(TAG, "Media data insert failed : " + videoTable);
                } else {
                    Log.v(TAG, "Media data insertion successfully : " + videoTable);
                }
            }

//            if (mNotTranscoded != null && mNotTranscoded.size() > 0) {
//                for (Uri uri : mNotTranscoded) {
//                    ContentValues v = new ContentValues(1);
//                    v.put(MediaStore.Video.Media.ARTIST, "videoorbplugin");
//                    Uri inserted = getContentResolver().insert(uri, v);
//                    if (inserted == null) {
//                        Log.e(TAG, "Media data update failed : " + uri);
//                    } else {
//                        Log.e(TAG, "Media data update successfully : " + uri);
//                    }
//                }
//
//                mNotTranscoded.clear();
//                mNotTranscoded = null;
//            }

            VideoTranscode.deinit(mTranscodeHandle);
            Log.v(TAG, "onPostExecute : " + mResult);
            mHandler.sendEmptyMessageDelayed(MSG_SUCCESS, 200);
//            if (mResult == RESULT_OK) {
//                mHandler.sendEmptyMessageDelayed(MSG_SUCCESS, 200);
//            } else {
//                mHandler.sendEmptyMessageDelayed(MSG_FAILURE, 200);
//            }
        }

        public void cancelTranscode() {
            mIsCancelled = true;
            if (mTranscodeHandle != 0) {
                VideoTranscode.cancel(mTranscodeHandle);
            }
        }

        public int getProgress() {
            if (mTranscodeHandle == 0 || mSrc == null)
                return 0;

            int total = mSrc.size();
            int currentProgress = VideoTranscode.getProgress(mTranscodeHandle);
            if (currentProgress == 100) {
                currentProgress = 0;
            }
            int progress = (int)(((float)current * 100 / total) + ((float)currentProgress / total));
            if (mProgress < progress)
                mProgress = progress;
            return mProgress;
        }
    }

    static final private int CRITERIA_WIDTH = 320;
    static final private int CRITERIA_HEIGHT = 240;
    public boolean isNeedTranscode(Uri src) {
        String strWidth = null;
        String strHeight = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(this, src);
            String hasVideo = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_VIDEO);
            if (hasVideo == null) {
                Log.e(TAG, "getSourceVideoRect, no videoTrack");
                return false;
            }
            strWidth = retriever.extractMetadata(
                    MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
            strHeight = retriever.extractMetadata(
                    MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        } catch (IllegalArgumentException ex) {
            // Assume this is a corrupt video file
        } finally {
            retriever.release();
        }

        if (strWidth == null || strHeight == null) {
            Log.e(TAG, "invalid video width/height");
            return false;
        }

        int width = Integer.decode(strWidth).intValue();
        int height = Integer.decode(strHeight).intValue();
        if (width == 0 || height == 0) {
            Log.e(TAG, "video width/height is 0");
            return false;
        }
        return (width > CRITERIA_WIDTH || height > CRITERIA_HEIGHT);
    }

    ArrayList<Uri> getNeedTranscodeVideoUris() {
        ArrayList<Uri> src = new ArrayList<Uri>();
        IMediaSource source = MediaSourceFactory.getTranscodeSource(getContentResolver());
        int counts = source.getMediaCount();
        for (int i = 0; i < counts; ++i) {
            Uri videoUri = source.getMediaUri(this, i);
            Log.v(TAG, "Traverse : " + videoUri);
            if (isNeedTranscode(videoUri)) {
                src.add(videoUri);
                Log.v(TAG, "Added Traverse : " + videoUri);
            }
        }
        return src;
    }

    /**
     * File system operations
     */
    private String getPathFromUri(Uri uri) {
        String [] proj = {MediaStore.Video.Media.DATA};
        Cursor c = getContentResolver().query(uri, proj, null, null, null);
        int colIdx = c.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        c.moveToFirst();
        String realPath = c.getString(colIdx);
        c.close();
        return realPath;
    }

    private String createName(long dateTaken) {
        Date date = new Date(dateTaken);
        SimpleDateFormat dateFormat = new SimpleDateFormat("_yyyyMMdd_HHmmss", Locale.US);
        return dateFormat.format(date);
    }

    private void prepareFolder(String path) {
        File f = new File(path);
        if (f.exists()) {
            return;
        }

        if (!f.mkdirs()) {
            Log.e(TAG, "folder creation failed!");
        }
    }

    private String generateOutputPath(String source) {
        long dateTaken = System.currentTimeMillis();
        String postfix = createName(dateTaken);
        File inputFile = new File(source);

        prepareFolder(TRANSCODE_PATH);
        StringBuilder sb = new StringBuilder(TRANSCODE_PATH);
        sb.append(inputFile.getName());
        int i = sb.lastIndexOf(".");
        if (i == -1) {
            sb.append(postfix);
        } else {
            sb.insert(i, postfix);
        }
        return sb.toString();
    }
}