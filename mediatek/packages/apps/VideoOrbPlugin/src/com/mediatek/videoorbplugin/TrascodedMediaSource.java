package com.mediatek.videoorbplugin;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import com.mediatek.ngin3d.Video;

public class TrascodedMediaSource implements IMediaSource {
    private final static String TAG = "vo.trascoded";
    private static final String [] PROJECTION =
            new String [] { MediaStore.Video.Media._ID };
    public static final int MAX_VIDEO_CONTENT = 8;

    private final ContentResolver mCr;
    private Cursor mCursor;
    private int mCounts;

    public TrascodedMediaSource(ContentResolver cr) {
        mCr = cr;
    }

    private Cursor query() {
        if (mCursor == null) {
            mCursor = mCr.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    PROJECTION, selection(), null, sortOrder());
            Log.v(TAG, "query() : " + mCursor);
        }
        return mCursor;
    }

    public int getMediaCount() {
        query();
        if (mCursor != null) {
            mCounts = mCursor.getCount();
            Log.v(TAG, "media count : " + mCounts);
            return mCounts > MAX_VIDEO_CONTENT ?
                    (mCounts = MAX_VIDEO_CONTENT) : mCounts;
        }
        return 0;
    }

    public Video getMedia(Context cts, int index, int width, int height) {
        if (index >= mCounts) {
            return null;
        }
        query();
        if (mCursor == null) {
            return null;
        }

        if (mCursor.moveToPosition(index)) {
            Uri uri = ContentUris.withAppendedId(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, mCursor.getLong(0)); //_ID
            Video clip = Video.createFromVideo(cts, uri, width, height);
            clip.setDoubleSided(true);
            return clip;
        }
        return null;
    }

    public void close() {
        if (mCursor != null) {
            mCursor.close();
        }
    }

    private static final String TRANSCODED_ARTIST_ID = "videoorbplugin";
    private String selection() {
        String query = MediaStore.Video.Media.ARTIST + " = '" + TRANSCODED_ARTIST_ID + "'";
        Log.v(TAG, "selection() : " + query + ", artist = " + TRANSCODED_ARTIST_ID);
        return query;
    }

    private String sortOrder() {
        return MediaStore.Video.Media.DATE_TAKEN + " DESC LIMIT 8";
    }

    public Uri getMediaUri(Context ctx, int index) {
        if (index >= mCounts) {
            return null;
        }
        query();
        if (mCursor == null || !mCursor.moveToPosition(index)) {
            return null;
        }

        return ContentUris.withAppendedId(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, mCursor.getLong(0));
    }
}
