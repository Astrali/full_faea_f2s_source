/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.dm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.internal.telephony.TelephonyIntents;
import com.mediatek.dm.DmConst.TAG;
import com.mediatek.dm.data.IDmPersistentValues;
import com.mediatek.dm.data.PersistentContext;
import com.mediatek.dm.ext.MTKPhone;
import com.mediatek.dm.fumo.DmEntry;
import com.mediatek.dm.util.DmThreadPool;

import java.io.File;
import java.util.concurrent.ExecutorService;

/**
 * Receive WAP_PUSH to start DM Sessions. 
 * Receive SIM_INDICATOR_STATE_CHANGED to check FOTA result
 * and check unprocessed NIA; Receive DM_SWUPDATE to start DM Entry for FOTA
 * 
 * @author MTK80800
 * 
 */
public class DmReceiver extends BroadcastReceiver {

    public static final int NORMAL_REBOOT = 0;
    public static final int UPDATE_REBOOT = 1;
    private static boolean sIsBooting = true;
    private static ExecutorService sExecService;

    public void onReceive(Context context, Intent intent) {
        Log.i(TAG.RECEIVER, "Receiver intent: " + intent);
        if (intent == null || intent.getAction() == null || context == null) {
            return;
        }
        String intentAction = intent.getAction();

        if (TelephonyIntents.ACTION_SIM_INDICATOR_STATE_CHANGED.equalsIgnoreCase(intentAction)) {
            if (!sIsBooting) {
                return;
            }
            int simState = intent.getIntExtra(TelephonyIntents.INTENT_KEY_ICC_STATE, -1);
            Log.d(TAG.RECEIVER, "[phone-state]->" + simState);
            if (simState == MTKPhone.SIM_INDICATOR_NORMAL || simState == MTKPhone.SIM_INDICATOR_ROAMING) {
                Log.i(TAG.RECEIVER, "[phone-state]->SIM_STATE_READY, continue.");
            } else {
                Log.d(TAG.RECEIVER, "[phone-state]->not SIM_STATE_READY, ignore.");
                return;
            }

            Log.d(TAG.RECEIVER, "------- checking status when sim ready --------");

            sIsBooting = false;

            Intent newIntent = new Intent();
            newIntent.setAction(DmConst.IntentAction.ACTION_REBOOT_CHECK);
            CheckReboot checker = new CheckReboot(newIntent, context);

            if (sExecService == null) {
                sExecService = DmThreadPool.getInstance();
            }
            if (sExecService != null && checker != null) {
                sExecService.execute(checker);
            }
            return;
        } else if (DmConst.IntentAction.DM_SWUPDATE.equals(intentAction)) {
            Log.i(TAG.RECEIVER, "Receive software update intent");
            Intent activityIntent = new Intent(context, DmEntry.class);
            activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(activityIntent);
        } else {
            Log.w(TAG.RECEIVER, "Normal intent.");
        }

        Log.i(TAG.RECEIVER, "Start dm service");
        Intent dmIntent = new Intent(intent);
        if (dmIntent == null || dmIntent.getAction() == null) {
            return;
        }
        dmIntent.setClass(context, DmService.class);
        dmIntent.setAction(intentAction);
        context.startService(dmIntent);
    }

    private int checkUpdateResult(Context context) {
        Log.i(TAG.RECEIVER, "checkUpdateResult enter");
        int ret = NORMAL_REBOOT;

        if (IDmPersistentValues.STATE_UPDATE_RUNNING == PersistentContext.getInstance(context)
                .getDLSessionStatus()) {
            Log.d(TAG.RECEIVER, "+++FOTA flag found, delete delta package+++");
            PersistentContext.getInstance(context).deleteDeltaPackage();
            ret = UPDATE_REBOOT;
        }

        return ret;
    }

    private class CheckReboot implements Runnable {
        private Intent mDmIntent;
        private Context mContext;

        public CheckReboot(Intent dmIntent, Context context) {
            mDmIntent = dmIntent;
            mContext = context;
        }

        public void run() {
            boolean isStart = false;
            if (mDmIntent == null || mDmIntent.getAction() == null) {
                Log.w(TAG.RECEIVER, "mDmIntent is null");
                return;
            }
            mDmIntent.setAction(mDmIntent.getAction());
            mDmIntent.setClass(mContext, DmService.class);
            Bundle bundle = new Bundle();

            int res = checkUpdateResult(mContext);
            if (res == UPDATE_REBOOT) {
                Log.i(TAG.RECEIVER, "CheckReboot Start dm service, this is update reboot");
                bundle.putBoolean(DmService.INTENT_EXTRA_UPDATE, true);
                isStart = true;
            }
            Log.i(TAG.RECEIVER, "CheckReboot check nia message");
            boolean niaret = niaExist();
            if (niaret) {
                Log.i(TAG.RECEIVER, "CheckReboot Start dm service really, this is nia exist");
                bundle.putBoolean(DmService.INTENT_EXTRA_NIA, true);
                isStart = true;
            }

            if (isStart) {
                Log.d(TAG.RECEIVER, "+++ starting service...");
                mDmIntent.putExtras(bundle);
                mContext.startService(mDmIntent);
            } else {
                Log.d(TAG.RECEIVER, "--- no need to start service.");
            }
        }

    }

    public boolean niaExist() {
        Log.i(TAG.RECEIVER, "niaexist enter");
        boolean ret = false;
        String niaFolder = DmConst.PathName.NIA_FILE;
        File folder = new File(niaFolder);
        if (!folder.exists()) {
            Log.w(TAG.RECEIVER, "CheckNia the nia dir is noet exist");
            return ret;
        }

        String[] fileExist = folder.list();
        if (fileExist == null || fileExist.length <= 0) {
            Log.w(TAG.RECEIVER, "CheckNia there is no unproceed message");
            return ret;
        }
        ret = true;

        return ret;
    }
}
