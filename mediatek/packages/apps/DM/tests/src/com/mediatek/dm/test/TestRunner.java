/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.dm.test;

import com.mediatek.dm.DmPLDeltaFile;
import com.zutubi.android.junitreport.JUnitReportTestRunner;

import junit.framework.TestSuite;

public class TestRunner extends JUnitReportTestRunner {
    @Override
    public TestSuite getAllTests() {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(Test01StandAloneTests.class);
        suite.addTestSuite(Test02ContextBaseTests.class);
        suite.addTestSuite(Test03DmAgentTests.class);
        suite.addTestSuite(Test04WapConnectionTest.class);

        suite.addTestSuite(Test05TestSmsRegXMLGenerator.class);
        suite.addTestSuite(Test06TestSmsRegInterface.class);

        suite.addTestSuite(Test07TestAGPSParamsProvider.class);
        suite.addTestSuite(Test08TestConnParamsProvider.class);
        suite.addTestSuite(Test09TestMMSParamsProvider.class);
        suite.addTestSuite(Test10TestStreamingParamsProvider.class);
        suite.addTestSuite(Test11TestWapParamsProvider.class);

        // something bad will happen when running following test cases
        suite.addTestSuite(Test13FumoUnpackTests.class);
        suite.addTestSuite(Test14FumoFlowTests.class);
        suite.addTestSuite(Test15FumoEntryTests.class);

        suite.addTestSuite(Test17MTKMediaContainerTest.class);
        suite.addTestSuite(Test18ScomoInstallTest.class);
        suite.addTestSuite(Test19DmControllerTest.class);
        suite.addTestSuite(Test20DmPLDLPkgTest.class);
        suite.addTestSuite(Test21DmReceiverTest.class);
        suite.addTestSuite(Test22DmCpObeserverTest.class);
        suite.addTestSuite(Test23OmaCpReceiverTest.class);

        suite.addTestSuite(Test24ScomoDcHandlerTest.class);
        suite.addTestSuite(Test25ScomoDpHandlerTest.class);
        suite.addTestSuite(Test26ScomoHandlerTest.class);
        suite.addTestSuite(Test27ScomoStateTest.class);
        suite.addTestSuite(Test28ScomoActivityTest.class);
        suite.addTestSuite(Test29ScomoConfirmActivityTest.class);
        suite.addTestSuite(Test30DmServiceTest.class);
        suite.addTestSuite(Test31DmDownloadNotificationTest.class);
        suite.addTestSuite(Test32DmPlDeltaFile.class);
        suite.addTestSuite(Test33DLProgressNotifierTest.class);
        suite.addTestSuite(Test34DmMmiProgressTest.class);
        suite.addTestSuite(Test35DmPLInventoryTest.class);
        suite.addTestSuite(Test36DmScomoNotificationTest.class);
        suite.addTestSuite(Test37ScomoDownloadDetailActivityTest.class);

        suite.addTestSuite(Test38DmMmiFactoryTest.class);
        suite.addTestSuite(Test39DmPlLoggerTest.class);
        suite.addTestSuite(Test40DmXmlWriterTest.class);

        // com.mediatek.dm.util test
        suite.addTestSuite(Test41NiaDecoderTest.class);
        suite.addTestSuite(Test42ScheduleTaskQueueTest.class);
        suite.addTestSuite(Test43FileLoggerTest.class);
        suite.addTestSuite(Test44AutoRegisterNodeIoHandlerTest.class);

        return suite;
    }

}
