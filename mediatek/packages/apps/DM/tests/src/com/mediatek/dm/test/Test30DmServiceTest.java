/**
 * 
 */
package com.mediatek.dm.test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.os.Handler;
import android.os.Message;
import android.test.AndroidTestCase;
import android.util.Log;

import com.mediatek.dm.DmService;
import com.mediatek.dm.DmConst;
import com.mediatek.dm.data.IDmPersistentValues;
import com.mediatek.dm.scomo.DmScomoHandler;
import com.mediatek.dm.util.NiaDecoder;
import com.redbend.vdm.DownloadDescriptor;
import com.redbend.vdm.scomo.VdmScomo;
import com.redbend.vdm.scomo.VdmScomoDp;

import android.content.Intent;
import android.content.Context;

/**
 * @author MTK80987
 *
 */
public class Test30DmServiceTest extends AndroidTestCase {
	
	private static final String TAG = "[DmServiceTest]";
	private static final String INTENT_EXTRA_UPDATE = "update";
	private static final String SCOMO_ROOT = "./Scomo"; 
	private static final byte[] TEST_MSG_1 = {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'};
	private static final byte[] TEST_MSG_0 = {'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'};
	private static final byte[] TEST_MSG_2 = {'2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2'};
	private static final String FILE_NAME = "/data/data/com.mediatek.dm/files/nia/test";
	private Intent intent;
	private Context context;
	
    static final String[] FIELD = {
        "8057371",// DD0_SIZE
        "http://218.206.176.97:7001/dlserver/get?at=f&pkgId=17066&d=bin&msisdn=15110246357&imei=IMEI:864855010012278&taskId=18249299&taskSource=1&sessionId=60837&cid=1",// DD1_OBJECT_URI
        "application/octet-stream",// DD2_TYPE
        "update.zip",// DD3_NAME
        "1.0",// DD4_VERSION
        "CMCC",// DD5_VENDOR
        "ALPS.JB.FPB.p12 back to ALPS.JB.FPB.p9",// DD6_DESCRIPTION
        "http://218.206.176.97:7001/dlserver/get?at=s&msisdn=15110246357&imei=IMEI:864855010012278&taskId=18249299&taskSource=1&sessionId=60837&cid=1",// DD7_INSTALL_NOTIFY_URI
        "http://dm.monternet.com:7001/dlserver/",// DD8_NEXT_URL
        "http://dm.monternet.com:7001/dlserver/",// DD9_INFO_URL
        "http://dm.monternet.com:7001/dlserver/", // DD10_ICON_URI
        "estimatedDownloadTimeInSecs=300;estimatedInstallTimeInSecs=300"// DD11_INSTALL_PARAM
};
	
	
	protected void setUp() throws Exception {
		super.setUp();
		
		intent = new Intent();
		context = getContext();
	}
	
	public void testStartDmServiceWithDifferentActions() {
		Log.d(TAG, "start dmService");
		
		try {
			Log.d(TAG, "send dm_nia intent");
			intent.setAction(DmConst.IntentAction.DM_WAP_PUSH);
			intent.setType(DmConst.IntentType.DM_NIA);
			intent.putExtra("data", TEST_MSG_1);
			intent.setClass(context,DmService.class);
		
			context.startService(intent);
			Thread.sleep(1000);
				
			Log.d(TAG, "send dm_dl_forground");
			intent.setAction(DmConst.IntentAction.DM_DL_FOREGROUND);
			context.startService(intent);
			Thread.sleep(1000);
				
			Log.d(TAG, "send action_reboot_check");
			intent.setAction(DmConst.IntentAction.ACTION_REBOOT_CHECK);
			intent.putExtra(INTENT_EXTRA_UPDATE, true);
			context.startService(intent);
			Thread.sleep(1000);
				
			Log.d(TAG, "send dm_nia_start");
			intent.setAction(DmConst.IntentAction.DM_NIA_START);
			context.startService(intent);
			Thread.sleep(1000);
				
			Log.d(TAG, "action_fumo_ci");
			intent.setAction(DmConst.IntentAction.ACTION_FUMO_CI);
			context.startService(intent);
			Thread.sleep(1000);
		}
		catch (Exception e) {
			Log.d(TAG, "start dmService fail");
			e.printStackTrace();
		} 
	}
	
	public void testHandleMsg() {
		Log.d(TAG, "test handle messages");
		
		try {
			
			if (DmService.getInstance() != null) {
			
				Message msg = new Message();
			
				DownloadDescriptor dd = new DownloadDescriptor();
			
				msg.obj = dd;
				msg.what = IDmPersistentValues.MSG_NEWVERSIONDETECTED;
				Handler handle = DmService.getInstance().mHandler;
				handle.sendMessage(msg);
				
				/*msg.what = IDmPersistentValues.MSG_DLPKGSTARTED;
				handle.sendMessage(msg);
				Thread.sleep(1000);
				
				msg.what = IDmPersistentValues.MSG_DLPKGUPGRADE;
				handle.sendMessage(msg);
				Thread.sleep(1000);
				
				msg.what = IDmPersistentValues.MSG_DM_SESSION_ABORTED;
				handle.sendMessage(msg);
				Thread.sleep(1000);
				
				msg.what = IDmPersistentValues.MSG_NIACONFIRMED;
				handle.sendMessage(msg);
				Thread.sleep(1000);
				
				msg.what = IDmPersistentValues.MSG_DMSESSIONCOMPLETED;
				handle.sendMessage(msg);
				Thread.sleep(1000);
				
				msg.what = IDmPersistentValues.MSG_WAP_CONNECTION_SUCCESS;
				handle.sendMessage(msg);
				Thread.sleep(1000);
				
				msg.what = IDmPersistentValues.MSG_DLPKGCOMPLETE;
				handle.sendMessage(msg);
				Thread.sleep(1000);*/
			
			}
		}
		catch (Exception e) {
			Log.d(TAG, "test handle message fail");	
			e.printStackTrace();
		}
	}
	
	/*public void testOnScomoError() throws InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
		Log.d(TAG, "test onConfrimDownload begin");

		
		Object obj = Class.forName(DmService.class.getName()).newInstance();
		Class<?> cls = obj.getClass();
		Method m = cls.getDeclaredMethod("onScomoError", int.class);
		m.setAccessible(true);
		m.invoke(obj, 1);
		
	}*/
	
	public void testOnScomoInstallFailed() {
		Log.d(TAG, "test onScomoConfirmInstall begin");
		
		try {
			Object obj = Class.forName(DmService.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			Method m = cls.getDeclaredMethod("onScomoInstallFailed", VdmScomoDp.class);
			m.setAccessible(true);
			
			VdmScomoDp dp = VdmScomo.getInstance(SCOMO_ROOT, DmScomoHandler.getInstance()).getDps().get(0);
			
			m.invoke(obj, dp);
		}
		catch (Exception e) {
			Log.d(TAG, "test onScomoConfirmInstall fail");
			e.printStackTrace();
		}
		
	}
	
	public void testOnScomoConfirmDownload() {
		Log.d(TAG, "test onScomoConfirmDownload begin");
		try {
			Object obj = Class.forName(DmService.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			Method m = cls.getDeclaredMethod("onScomoConfirmDownload", VdmScomoDp.class, DownloadDescriptor.class);
			m.setAccessible(true);
			
			
			VdmScomoDp dp = VdmScomo.getInstance(SCOMO_ROOT, DmScomoHandler.getInstance()).getDps().get(0);
			DownloadDescriptor dd = new DownloadDescriptor();
	        dd.field = FIELD;
	        dd.size = 1024;
	        
	        m.invoke(obj, dp, dd);
		}
		catch (Exception e) {
			Log.d(TAG, "test onScomoConfirmDownload fail");
			e.printStackTrace();
		} 
	}
	
	public void testWriteNiaMessage() {
		Log.d(TAG, "test msgInfoWriter begin");
		
		try {
			Intent intent = new Intent();
			intent.setAction(DmConst.IntentAction.DM_WAP_PUSH);
			
			DmService.getInstance().writeNiaMessage(intent);
			
			Object obj = Class.forName(DmService.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			
		}
		catch (Exception e) {
			Log.d(TAG, "test msgInfoWriter fail");
			e.printStackTrace();
		}
	}
	
	public void testProcessNiaMessage() {
		Log.d(TAG, "test processNiaMessage begin");
		
		try {
			DmService.getInstance().processNiaMessage(TEST_MSG_0, FILE_NAME );
			Thread.sleep(500);
			DmService.getInstance().processNiaMessage(TEST_MSG_1, FILE_NAME);
			Thread.sleep(500);
			DmService.getInstance().processNiaMessage(TEST_MSG_2, FILE_NAME);
			Thread.sleep(500);
		}
		catch (Exception e) {
			Log.d(TAG, "test processNiaMessage begin");
			e.printStackTrace();
		}
	}
	
	public void testDeleteNia() {
		Log.d(TAG, "test deleteNia begin");
		
		try{
			DmService.getInstance().deleteNia();
		}
		catch (Exception e) {
			Log.d(TAG, "test deleteNia fail");
			e.printStackTrace();
		}
	}
	
	
	public void testSetNiaAlarm() {
		Log.d(TAG, "test setNiaAlarm begin");
		
		try {
			Object obj = Class.forName(DmService.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			Method m = cls.getDeclaredMethod("setNiaAlarm", long.class, boolean.class, boolean.class);
			m.setAccessible(true);
			
			m.invoke(obj, 1000, true, true);
		}
		catch (Exception e) {
			Log.d(TAG, "test setNiaAlarm begin");
			e.printStackTrace();
		}
	}
	
	public void testCancelNiaAlarm() {
		Log.d(TAG, "test cancelNiaAlarm begin");
		
		try {
			DmService.getInstance().cancelAlarm();
		}
		catch (Exception e) {
			Log.d(TAG, "test cancelNiaAlarm fail");
			e.printStackTrace();
		}
	}
	
	public void testGetReminderAndTiming() {
		Log.d(TAG, "test getReminderAndTiming begin");
		try {
			Object obj = Class.forName(DmService.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			Method m = cls.getDeclaredMethod("getReminderAndTiming", null);
			m.setAccessible(true);
			m.invoke(obj, null);
		}
		catch (Exception e) {
			Log.d(TAG, "test getReminderAndTiming fail");
			e.printStackTrace();
		}
	}
	
	public void tesCancelDlScomoPkg() {
		Log.d(TAG, "test cancelDlScomoPkg begin");
		
		DmService.getInstance().cancelDlPkg();
		DmService.getInstance().cancelDlScomoPkg();
		DmService.getInstance().cancleDmSession();
	}
	
	public void testPauseScomoDlPkg() {
		Log.d(TAG, "test pauseDlScomoPkg begin");
		
		DmService.getInstance().pauseDlScomoPkg();
		//DmService.getInstance().pauseDlPkg();
	}
	
	/*public void testStartDlScomoPkg() {
		Log.d(TAG, "test startDlScomoPkg begin");
		
		DmService.getInstance().startDlScomoPkg();
	}*/
	
	/*public void testShowNiaNotification() {
		Log.d(TAG, "test showNiaNotification begin");
		DmService.getInstance().showNiaNotification(19);
	}*/
	
	/*public void testSetUpdateType() {
		Log.d(TAG, "test setUpdateType begin");
		try {
			Object obj = Class.forName(DmService.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			Field f = cls.getDeclaredField("mTimingArray");
			f.setAccessible(true);
			int[] arr = {1, 2, 3};
			f.set(obj, arr);
			
			DmService.getInstance().setUpdateType(0);
		}
		catch (Exception e) {
			Log.d(TAG, "test setUpdateType fail");
			e.printStackTrace();
		}
	}*/
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
