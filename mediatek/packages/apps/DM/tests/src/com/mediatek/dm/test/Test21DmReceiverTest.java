/**
 * 
 */
package com.mediatek.dm.test;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import android.util.Log;
import android.content.Context;
import android.content.Intent;

import com.mediatek.dm.DmReceiver;
import com.mediatek.dm.DmConst;

/**
 * @author MTK80987
 *
 */
public class Test21DmReceiverTest extends AndroidTestCase {
	private static final String TAG = "[DmReceiverTest]";
	private static final String SWUPDATE_ACTION = "com.mediatek.DMSWUPDATE";
	private Context mContext;
	
	protected void setUp() throws Exception {
		super.setUp();
		mContext = getContext();
	}
	
	public void testNiaExist() {
		Log.d(TAG, "test niaExist begin");
		DmReceiver receiver = new DmReceiver();
		
		boolean flag = receiver.niaExist();
	}
	
	public void testOnReceive() {
		Log.d(TAG, "test onReceive begin");
		
		Intent intent = new Intent();
		intent.setAction(DmConst.IntentAction.DM_SWUPDATE);
		mContext.sendBroadcast(intent);
	}	
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	

}
