/**
 * 
 */
package com.mediatek.dm.test;

import com.mediatek.dm.DmService;
import com.mediatek.dm.scomo.DmScomoDownloadDetailActivity;
import com.mediatek.dm.scomo.DmScomoState;
import com.mediatek.dm.util.NiaDecoder;

import android.test.ActivityInstrumentationTestCase2;
//import android.test.ActivityUnitTestCase;
import android.app.Instrumentation;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author MTK80987
 *
 */
public class Test37ScomoDownloadDetailActivityTest extends 
		ActivityInstrumentationTestCase2<DmScomoDownloadDetailActivity> {
	
	private static final String TAG = "[DmScomoDownloadDetailActivityTest]";
	private Instrumentation instrument;
	public Test37ScomoDownloadDetailActivityTest() throws InterruptedException {
		super(DmScomoDownloadDetailActivity.class);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		instrument = this.getInstrumentation();
		
		Intent startIntent = new Intent();
		startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		/*Object obj = Class.forName(DmScomoDownloadDetailActivity.class.getName()).newInstance();
		Class<?> cls = obj.getClass();
		Field f = cls.getDeclaredField("mNetworkDetectDialog");
		f.setAccessible(true);
		ProgressDialog dlg = new ProgressDialog(this.getInstrumentation().getContext());
		f.set(obj, dlg);*/
		
		//this.getActivity();
	}
	
	public void testOnScomoUpdate() {
		Log.d(TAG, "test onScomoUpdate begin");
		try {
			DmScomoDownloadDetailActivity activity = new DmScomoDownloadDetailActivity();
		
			setDmScomoState(DmScomoState.DOWNLOADING_STARTED);
			activity.onScomoUpdated();
			
			setDmScomoState(DmScomoState.RESUMED);
			activity.onScomoUpdated();
			
			setDmScomoState(DmScomoState.PAUSED);
			activity.onScomoUpdated();
		}
		catch (Exception e) {
			Log.d(TAG, "test onScomoUpdate fail");
			e.printStackTrace();
		}
	}
	
	private void setDmScomoState(int state) throws InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchFieldException {
		Log.d(TAG, "set scomo state though reflect");
		Object obj = Class.forName(DmService.class.getName()).newInstance();
		Class<?> cls = obj.getClass();
		Field field = cls.getDeclaredField("sScomoState");
		field.setAccessible(true);
		DmScomoState mockState = new DmScomoState();
		mockState.mState = state;
		mockState.mVerbose = true;
		field.set(obj, mockState);
	}
	
	
	public void testSetNetWorkTimeoutAlarm() {
		Log.d(TAG, "test setNetWorkTimeoutAlarm begin");
		try
		{
			Object obj = Class.forName(DmScomoDownloadDetailActivity.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			Method m = cls.getDeclaredMethod("setNetworkTimeoutAlarm", null);
			m.setAccessible(true);
			
			m.invoke(obj, null);
		}
		catch (Exception e) {
			Log.d(TAG, "test setNetworkTimeoutAlarm fail");
			e.printStackTrace();
		}
	}
	
	public void testCancleNetworkTimeoutAlarm() {
		Log.d(TAG, "test cancleNetworkTimeoutAlarm begin");
		
		try {
			Object obj = Class.forName(DmScomoDownloadDetailActivity.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			Method m = cls.getDeclaredMethod("cancleNetworkTimeoutAlarm", null);
			m.setAccessible(true);
			
			m.invoke(obj, null);
		}
		catch (Exception e) {
			Log.d(TAG, "test cancleNetworkTimeoutAlarm fail");
			e.printStackTrace();
		}
		
	}
	
	public void testOnNetWorkError() {
		Log.d(TAG, "test onnetorkerror begin");
		try {
			Object obj = Class.forName(DmScomoDownloadDetailActivity.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			Method m = cls.getDeclaredMethod("onNetworkError", null);
			m.setAccessible(true);
			
			m.invoke(obj, null);
		}
		catch (Exception e) {
			Log.d(TAG, "test onNetworkError fail");
			e.printStackTrace();
		}
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
