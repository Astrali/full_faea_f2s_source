package com.mediatek.dm.test;

//import com.jayway.android.robotium.solo.Solo;

import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.content.Context;
import android.content.Intent;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.util.Log;

import com.mediatek.dm.DmConst;
import com.mediatek.dm.data.PersistentContext;
import com.mediatek.dm.fumo.DmEntry;
import com.redbend.vdm.DownloadDescriptor;

import junit.framework.Assert;

public class Test15FumoEntryTests extends ActivityInstrumentationTestCase2<DmEntry> {

    private static final String TAG = "Fumo02EntryTests";
    private static final String VERSION_PREFERENCE = "current_version";
    private static final String UPDATE_PREFERENCE = "system_update";
    private static final String INSTALL_PREFERENCE = "update_install";

    /**
     * ALPS.JB.FPB.p9-p7
     */
    DmEntry mActivity;
    Context mContext;
    Instrumentation mInstrumentation;
    PersistentContext mPersistentContext;
    PreferenceScreen mParentPreference;

    // private Solo mSolo;

    public Test15FumoEntryTests() {
        super(DmEntry.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        mInstrumentation = this.getInstrumentation();
        mContext = mInstrumentation.getTargetContext();
        mActivity = this.getActivity();
        mInstrumentation.waitForIdleSync();
        // mSolo = new Solo(mInstrumentation, mActivity);
        mParentPreference = mActivity.getPreferenceScreen();
        mPersistentContext = PersistentContext.getInstance(mContext);
        setActivityInitialTouchMode(false);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void test01Start() throws Exception {
        Log.v(TAG, "test01Start");
        // ActivityMonitor monitor = mInstrumentation.addMonitor("com.mediatek.dm.fumo.DmEntry",
        // null,
        // false);
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.deleteDeltaPackage();
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();

        Intent intent = new Intent();
        intent.setAction(DmConst.IntentAction.DM_SWUPDATE);
        mContext.sendBroadcast(intent);

        Thread.sleep(300);
        mInstrumentation.waitForIdleSync();
        // Assert.assertTrue(mInstrumentation.checkMonitorHit(monitor, 1));
        // mActivity.finish();
    }

    public void test02DmSessionStart() throws Exception {
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.setDMSessionStatus(PersistentContext.STATE_DM_NIA_START);
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();
        Log.d(TAG, "++++++++++++ setDMSessionStatus STATE_DM_NIA_START++++++++++++"
                + PersistentContext.STATE_DM_NIA_START);
    }

    public void test03DmSessionComplete() throws Exception {
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.setDMSessionStatus(PersistentContext.STATE_DM_NIA_COMPLETE);
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();
        Log.d(TAG, "++++++++++++ setDMSessionStatus STATE_DM_NIA_COMPLETE++++++++++++"
                + PersistentContext.STATE_DM_NIA_COMPLETE);

        // assertEquals(PersistentContext.STATE_DM_NIA_COMPLETE,
        // mPersistentContext.getDMSessionStatus());
    }

    public void test04NewVersionDetected() throws Exception {
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.setDLSessionStatus(PersistentContext.STATE_NEW_VERSION_DETECTED);
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();

    }

    public void test05ResumeDownload() throws Exception {
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.setDLSessionStatus(PersistentContext.STATE_RESUME_DOWNLOAD);
                DownloadDescriptor dd = new DownloadDescriptor();
                dd.field = Test14FumoFlowTests.FIELD;
                dd.size = Test14FumoFlowTests.TOTAL;
                mPersistentContext.setDownloadDescriptor(dd);
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();

        // mSolo = new Solo(mInstrumentation, mActivity);
        // mSolo.clickOnButton(mActivity.getString(R.string.download));

        // assertEquals(PersistentContext.STATE_RESUME_DOWNLOAD,
        // mPersistentContext.getDLSessionStatus());
    }

    public void test06PauseDownload() throws Exception {
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.setDLSessionStatus(PersistentContext.STATE_PAUSE_DOWNLOAD);
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();

        // mSolo = new Solo(mInstrumentation, mActivity);
        // mSolo.clickOnButton(mActivity.getString(R.string.suspend));

        // assertEquals(PersistentContext.STATE_PAUSE_DOWNLOAD,
        // mPersistentContext.getDLSessionStatus());
    }

    public void test07DownloadComplete() {
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.setDLSessionStatus(PersistentContext.STATE_DL_PKG_COMPLETE);
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();
    }

    public void test08DeletePkgWhenPause() {
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.setDLSessionStatus(PersistentContext.STATE_PAUSE_DOWNLOAD);
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();
    }

    public void test09DeletePkgWhenComplete() {
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.setDLSessionStatus(PersistentContext.STATE_DL_PKG_COMPLETE);
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();
    }

    public void test10ClickUpdatePrefWhenPaused() {
        final Runnable statusChange = new Runnable() {
            @UiThreadTest
            public void run() {
                mPersistentContext.setDLSessionStatus(PersistentContext.STATE_PAUSE_DOWNLOAD);
            }
        };
        statusChange.run();

        mInstrumentation.waitForIdleSync();

        final Runnable mUpdateResults = new Runnable() {
            @UiThreadTest
            public void run() {
                Preference p = mActivity.findPreference(UPDATE_PREFERENCE);
                mActivity.onPreferenceTreeClick(null, p);
            }
        };
        mUpdateResults.run();

        mInstrumentation.waitForIdleSync();
    }

    public void test11ClickUpdatePrefWhenCompleted() {
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_DL_PKG_COMPLETE);
        mInstrumentation.waitForIdleSync();

        final Runnable mUpdateResults = new Runnable() {
            @UiThreadTest
            public void run() {
                Preference p = mActivity.findPreference(UPDATE_PREFERENCE);
                mActivity.onPreferenceTreeClick(null, p);
            }
        };
        mUpdateResults.run();
        mInstrumentation.waitForIdleSync();
    }

    public void test12ClickInstallPref() {
        ActivityMonitor monitor = mInstrumentation.addMonitor("com.mediatek.dm.fumo.DmClient",
                null, false);
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_DL_PKG_COMPLETE);
        mInstrumentation.waitForIdleSync();

        final Runnable mUpdateResults = new Runnable() {
            @UiThreadTest
            public void run() {
                Preference p = mActivity.findPreference(INSTALL_PREFERENCE);
                mActivity.onPreferenceTreeClick(null, p);
            } 
        };
        mUpdateResults.run();

        mInstrumentation.waitForIdleSync();

        mPersistentContext.deleteDeltaPackage();
        mInstrumentation.waitForIdleSync();
        Assert.assertTrue(mInstrumentation.checkMonitorHit(monitor, 1));
    }
}
