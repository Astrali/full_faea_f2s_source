/**
 * 
 */
package com.mediatek.dm.test;

import android.test.AndroidTestCase;
import android.util.Log;

import com.mediatek.dm.DmCpObeserver;

/**
 * @author MTK80987
 *
 */
public class Test22DmCpObeserverTest extends AndroidTestCase {
	private static final String TAG = "[DmCpObeserverTest]";
	private static final String TEST_FILED = "testFiled";
	private static final String TEST_VALUE = "testValue";
	
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	public void testNotify() {
		Log.d(TAG, "tets nofity begin");
		try {
			DmCpObeserver obeserver = new DmCpObeserver();
		
			obeserver.notify(TEST_FILED, TEST_VALUE, false);
		}
		catch (Exception e) {
			Log.d(TAG, "test notify fail");
		}
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
}
