package com.mediatek.dm.test;

import android.test.AndroidTestCase;
import android.util.Log;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.graphics.drawable.Drawable;
import android.content.pm.PackageManager;

import com.mediatek.dm.DmService;
import com.mediatek.dm.ext.MTKMediaContainer;
import com.mediatek.dm.scomo.DmScomoHandler;
//import com.mediatek.dm.scomo.DmScomoPackageManager;
import com.mediatek.dm.scomo.DmScomoNotification;
import com.mediatek.dm.ext.MTKPackageManager;
import com.mediatek.dm.data.IDmPersistentValues;
import com.mediatek.dm.DmMmiProgress;
import com.mediatek.dm.scomo.DmScomoState;

import com.redbend.vdm.scomo.VdmScomo;
import com.redbend.vdm.scomo.VdmScomoDc;
import com.redbend.vdm.VdmException;


import junit.framework.Assert;

public class Test27ScomoStateTest extends AndroidTestCase {
	private static final String TAG = "[ScomoStateTest]";
	
	private static final String SCOMO_PACKAGE_PATH = "/data/data/com.mediatek.dm/files/packageinfo.zip";
	
	private DmService mDmService;
	private Context mContext;
	private PackageManager mPackageManager;
	
    protected void setUp() throws Exception {
        super.setUp();
 
        mContext = getContext();
        mPackageManager = mContext.getPackageManager();
        mDmService = DmService.getInstance();
        
        if( mDmService == null ) {
            startDmService();
            Thread.sleep(1000);	
        }

    }
    
    protected void startDmService() {
		Intent dmIntent = new Intent();
		dmIntent.setAction(Intent.ACTION_DEFAULT);
		dmIntent.setClass(mContext, DmService.class);
		mContext.startService(dmIntent);
    }
    
    public void testStore() {
    	Log.d(TAG, "begin test store");
    	try {
        	DmScomoState state = new DmScomoState();
        	state.mState = DmScomoState.ABORTED;
        	
        	DmScomoState.store(mContext, state);
    	}
    	catch (Exception e)
    	{
    		Log.d(TAG, "test store fail");
    	}
    
    }
    
    public void testLoad() {
    	Log.d(TAG, "begin test load");
    	
    	try {
    		DmScomoState state = DmScomoState.load(mContext);
    		Assert.assertNotNull(state);
    	}
    	catch (Exception e) {
    		Log.d(TAG, "test load fail");
    	}
    }
    
    public void testGetVersion() {
    	Log.d(TAG, "begin test getVersion");
    	
    	DmScomoState state = new DmScomoState();
    	
    	state.setArchivePath(SCOMO_PACKAGE_PATH);
    	
    	String version = state.getVersion();
   	
    	Assert.assertNotNull(version);
 
    }
    
    public void testGetName() {
    	Log.d(TAG, "begin test getName");
    	
    	DmScomoState state = new DmScomoState();
    	
    	state.setArchivePath(SCOMO_PACKAGE_PATH);
    	String name = state.getName();

    	Assert.assertNotNull(name);
    }
    
    public void testGetDescription() {
    	Log.d(TAG, "begin test getDescription");
    	
    	DmScomoState state = new DmScomoState();
    	
    	state.setArchivePath(SCOMO_PACKAGE_PATH);
    	
    	String description = state.getDescription().toString();

    	Assert.assertNotNull(description);
    }
    
    public void testGetPackageName() {
    	Log.d(TAG, "begin test getPackageName");
    	
    	DmScomoState state = new DmScomoState();
    	
    	state.setArchivePath(SCOMO_PACKAGE_PATH);   	
    	String name = state.getName();
 
    	
    	Assert.assertNotNull(name);
    }
    
    public void testGetIcon() {
    	Log.d(TAG, "begin test getIcon");
    	
    	DmScomoState state = new DmScomoState();
    	
    	state.setArchivePath(SCOMO_PACKAGE_PATH);    	
    	Drawable icon = state.getIcon();

    	
    	Assert.assertNotNull(icon);
    }

    protected void teardown() throws Exception {
        super.tearDown();
        
    }
}
