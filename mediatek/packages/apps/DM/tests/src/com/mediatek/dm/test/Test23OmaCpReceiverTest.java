/**
 * 
 */
package com.mediatek.dm.test;

import java.lang.reflect.Method;

import android.test.AndroidTestCase;
import android.util.Log;
import android.content.Context;
import android.content.Intent;

import com.mediatek.dm.OmaCpReceiver;
import com.mediatek.dm.option.Options;

/**
 * @author MTK80987
 *
 */
public class Test23OmaCpReceiverTest extends AndroidTestCase {
	
	private static final String TAG = "[OmaCpReceiverTest]";
	private static final String CP_DM_CAPABILITY_ACTION = "com.mediatek.omacp.capability";
	private static final String CP_DM_SETTING_ACTION = "com.mediatek.omacp.settings";
	
	private Context mContext;
	
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	/*public void testReceiver() {
		Log.d(TAG, "test omaReceiver begin");
		
		mContext = getContext();
		
		try {
			Intent intent = new Intent();
			intent.setAction(CP_DM_SETTING_ACTION);
			mContext.sendBroadcast(intent);
		
			Thread.sleep(1000);
			
			intent.setAction(CP_DM_CAPABILITY_ACTION);
			mContext.sendBroadcast(intent);
			
			Thread.sleep(1000);
			
			intent.setAction(Intent.ACTION_DEFAULT);
			mContext.sendBroadcast(intent);
			
			Thread.sleep(1000);
			
		}
		catch (Exception e)
		{	
			Log.d(TAG, "test omaReceiver fail");
			e.printStackTrace();
		}
		
	}*/
	
	public void testOnReceive() {
		Log.d(TAG, "test handleCpCapabilityMessage begin");
		try {
			OmaCpReceiver receiver = new OmaCpReceiver();
		
			Intent intent = new Intent();
			intent.setAction(CP_DM_CAPABILITY_ACTION);
			receiver.onReceive(this.getContext(), intent);
			Thread.sleep(500);
		
			intent.setAction(CP_DM_SETTING_ACTION);
			receiver.onReceive(this.getContext(), intent);
			Thread.sleep(500);
			
			intent.setAction(Intent.ACTION_DEFAULT);
			receiver.onReceive(this.getContext(), intent);
			Thread.sleep(500);
			
		}
		catch (Exception e) {
			Log.d(TAG, "test handleCpCapabilityMessage fail");
			e.printStackTrace();
		}
	
	}
	
	public void testHandleCpConfigMessage() {
		Log.d(TAG, "test handleCpConfigMessage begin");
		try {
		
			OmaCpReceiver receiver = new OmaCpReceiver();
			
			Object obj = Class.forName(OmaCpReceiver.class.getName()).newInstance();
			Class<?> cls = obj.getClass();
			Method m = cls.getDeclaredMethod("handleCpConfigMessage", int.class, String.class);
			m.setAccessible(true);
			m.invoke(obj, 0, "http://172.27.26.1");
			
		}
		catch (Exception e) {
			Log.d(TAG, "test handleCpConfigMessage fail");
			e.printStackTrace();
		}
		
	}
	
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
}
