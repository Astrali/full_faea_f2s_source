package com.mediatek.engineermode;

import android.hardware.*;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.util.Log;

public class GSensorTest extends Activity {
	private TextView gtvx;
	private TextView gtvy;
	private TextView gtvz;

	
	private SensorManager mSensorManager;
	private Sensor Gsensor;
	private sensorEventListenerImpl lsn = new sensorEventListenerImpl();
	private float x,y,z;
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        setContentView(R.layout.gsensor_test);  
        gtvx=(TextView)findViewById(R.id.gtvx);
        gtvy=(TextView)findViewById(R.id.gtvy);
        gtvz=(TextView)findViewById(R.id.gtvz);
        
        
		mSensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
		Gsensor=mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);/*������Ӧ*/
		mSensorManager.registerListener(lsn,Gsensor,SensorManager.SENSOR_DELAY_GAME);
	}
	
	class  sensorEventListenerImpl implements SensorEventListener {
		public void onSensorChanged(SensorEvent e){
			float x = (float)(Math.round(e.values[0]*100))/100;
			float y = (float)(Math.round(e.values[1]*100))/100;
			float z = (float)(Math.round(e.values[2]*100))/100;
			int type = e.sensor.getType();
			if(type == Sensor.TYPE_ACCELEROMETER){
				Log.d("shaokai","1@");
				gtvx.setText("x = " + x + " m/s2");
				gtvy.setText("y = " + y + " m/s2");
				gtvz.setText("z = " + z + " m/s2");
			}
		}
		
		public void onAccuracyChanged(Sensor s,int accuracy){}
	}
	public void onStop() {
		super.onStop();
		mSensorManager.unregisterListener(lsn);
		finish();
	}
}