package com.mediatek.engineermode;

import android.hardware.*;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.util.Log;

public class PSensorTest extends Activity {

	private TextView dtv;
	private TextView dtv2;
	private TextView dtv3;
	
	private SensorManager mSensorManager;
	private Sensor Psensor;
	private sensorEventListenerImpl lsn = new sensorEventListenerImpl();
	private float x,y,z;
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        setContentView(R.layout.psensor_test);  
        dtv=(TextView)findViewById(R.id.dtv);
		dtv2=(TextView)findViewById(R.id.dtv2);
		dtv3=(TextView)findViewById(R.id.dtv3);
        
		mSensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
		Psensor=mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);/*�����Ӧ*/
		mSensorManager.registerListener(lsn,Psensor,SensorManager.SENSOR_DELAY_GAME);
	}
	
	class  sensorEventListenerImpl implements SensorEventListener {
		public void onSensorChanged(SensorEvent e){
			float x = (float)(Math.round(e.values[0]*100))/100;
			float y = (float)(Math.round(e.values[1]*100))/100;
			float z = (float)(Math.round(e.values[2]*100))/100;
			int type = e.sensor.getType();

			if(type == Sensor.TYPE_PROXIMITY){
				Log.d("shaokai","4@@@@");
				dtv.setText("distance = " + x + " cm");
				dtv2.setText("rawdata  = " + y);
				dtv3.setText("min_raw  = " + z);
			}
		}
		
		public void onAccuracyChanged(Sensor s,int accuracy){}
	}
	public void onStop() {
		super.onStop();
		mSensorManager.unregisterListener(lsn);
		finish();
	}
}