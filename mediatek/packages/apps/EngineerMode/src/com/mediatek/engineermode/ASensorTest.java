package com.mediatek.engineermode;

import android.hardware.*;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.util.Log;

public class ASensorTest extends Activity {

	private TextView ltv;
	private TextView ltv2;
	private TextView ltv3;
	
	private SensorManager mSensorManager;
	private Sensor Asensor;
	private sensorEventListenerImpl lsn = new sensorEventListenerImpl();
	private float x,y,z;
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        setContentView(R.layout.asensor_test);
        
        ltv=(TextView)findViewById(R.id.ltv);
		ltv2=(TextView)findViewById(R.id.ltv2);
		ltv3=(TextView)findViewById(R.id.ltv3);
        
		mSensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
		Asensor=mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);/*���߸�Ӧ*/

		mSensorManager.registerListener(lsn,Asensor,SensorManager.SENSOR_DELAY_GAME);
	}
	
	class  sensorEventListenerImpl implements SensorEventListener {
		public void onSensorChanged(SensorEvent e){
			float x = (float)(Math.round(e.values[0]*100))/100;
			float y = (float)(Math.round(e.values[1]*100))/100;
			float z = (float)(Math.round(e.values[2]*100))/100;
			int type = e.sensor.getType();

			if(type == Sensor.TYPE_LIGHT){
				Log.d("shaokai","3@@@");
				ltv.setText("light intensity = " + x + " luxes");
				ltv2.setText("rawdata  = " + y);
				ltv3.setText("min_data = " + z);
			}
		}
		
		public void onAccuracyChanged(Sensor s,int accuracy){}
	}
	public void onStop() {
		super.onStop();
		mSensorManager.unregisterListener(lsn);
		finish();
	}
}