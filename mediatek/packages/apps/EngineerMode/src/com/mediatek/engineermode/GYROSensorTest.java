package com.mediatek.engineermode;

import android.hardware.*;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.util.Log;

public class GYROSensorTest extends Activity {
	private TextView gtvx;
	private TextView gtvy;
	private TextView gtvz;

	
	private SensorManager mSensorManager;
	private Sensor GYROsensor;
	private sensorEventListenerImpl lsn = new sensorEventListenerImpl();
	private float x,y,z;
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        setContentView(R.layout.gyrosensor_test);  
        gtvx=(TextView)findViewById(R.id.ltv);
        gtvy=(TextView)findViewById(R.id.ltv2);
        gtvz=(TextView)findViewById(R.id.ltv3);
        
        
		mSensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
		GYROsensor=mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);/*Ӧ*/
		mSensorManager.registerListener(lsn,GYROsensor,SensorManager.SENSOR_DELAY_GAME);
	}
	
	class  sensorEventListenerImpl implements SensorEventListener {
		public void onSensorChanged(SensorEvent e){
			float x = (float)(Math.round(e.values[0]*100))/100;
			float y = (float)(Math.round(e.values[1]*100))/100;
			float z = (float)(Math.round(e.values[2]*100))/100;
			int type = e.sensor.getType();
			if(type == Sensor.TYPE_GYROSCOPE){
				Log.d("shaokai","1@");
				gtvx.setText("x = " + x + " rad/s ");
				gtvy.setText("y = " + y + " rad/s ");
				gtvz.setText("z = " + z + " rad/s ");
			}
		}
		
		public void onAccuracyChanged(Sensor s,int accuracy){}
	}
	public void onStop() {
		super.onStop();
		mSensorManager.unregisterListener(lsn);
		finish();
	}
}
