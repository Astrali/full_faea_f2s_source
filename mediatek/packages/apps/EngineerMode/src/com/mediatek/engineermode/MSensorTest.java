package com.mediatek.engineermode;

import android.hardware.*;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.util.Log;

public class MSensorTest extends Activity {

	private TextView mtvx;
	private TextView mtvy;
	private TextView mtvz;
	
	private SensorManager mSensorManager;
	private Sensor Msensor;
	private sensorEventListenerImpl lsn = new sensorEventListenerImpl();
	private float x,y,z;
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        setContentView(R.layout.msensor_test);  
		
        mtvx=(TextView)findViewById(R.id.mtvx);
        mtvy=(TextView)findViewById(R.id.mtvy);
        mtvz=(TextView)findViewById(R.id.mtvz);

		mSensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
		Msensor=mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);/*�شŸ�Ӧ*/
		mSensorManager.registerListener(lsn,Msensor,SensorManager.SENSOR_DELAY_GAME);
	}
	
	class  sensorEventListenerImpl implements SensorEventListener {
		public void onSensorChanged(SensorEvent e){
			float x = (float)(Math.round(e.values[0]*100))/100;
			float y = (float)(Math.round(e.values[1]*100))/100;
			float z = (float)(Math.round(e.values[2]*100))/100;
			int type = e.sensor.getType();
			
			if(type == Sensor.TYPE_MAGNETIC_FIELD){
				Log.d("shaokai","2@@");
				mtvx.setText("x = " + x + " ut");
				mtvy.setText("y = " + y + " ut");
				mtvz.setText("z = " + z + " ut");
			}
		}
		
		public void onAccuracyChanged(Sensor s,int accuracy){}
	}
	public void onStop() {
		super.onStop();
		mSensorManager.unregisterListener(lsn);
		finish();
	}
}