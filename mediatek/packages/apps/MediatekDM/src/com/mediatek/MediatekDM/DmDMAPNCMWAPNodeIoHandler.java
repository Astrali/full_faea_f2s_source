package com.mediatek.MediatekDM;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.mediatek.MediatekDM.mdm.MdmException;
import com.mediatek.MediatekDM.mdm.MdmException.MdmError;
import com.mediatek.MediatekDM.mdm.NodeIoHandler;

public class DmDMAPNCMWAPNodeIoHandler implements NodeIoHandler {
    private static final String PREFS_NAME = "dmapn";
    private static final String PROPERTY_NAME = "com.mediatek.mediatekdm.dmapn";
    
    private static final String DEFAULT_VALUE = "cmwap";
    
    public static final String URI = "./Setting/DM/DMAPN";

    private Context mContext;

    public DmDMAPNCMWAPNodeIoHandler(Context context) {
        mContext = context;
    }

    @Override
    public int read(int offset, byte[] data) throws MdmException {
        SharedPreferences sp = mContext.getSharedPreferences(PREFS_NAME, 0);
        byte[] value = sp.getString(PROPERTY_NAME, DEFAULT_VALUE).getBytes();
        if (data != null) {
            for (int i = 0; i < data.length; ++i) {
                data[i] = value[offset + i];
            }
        }
        return value.length;
    }

    @Override
    public void write(int offset, byte[] data, int totalSize) throws MdmException {
        // no partial writes
        if (offset == 0 && data.length == 0 && totalSize != 0) {
            throw new MdmException(MdmError.TREE_EXT_NOT_PARTIAL);
        }
        String value = new String(data);
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putString(PROPERTY_NAME, value);
        editor.commit();
    }

}
