/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.cmmb.app.tests;

import android.app.Instrumentation;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.ChannelListManager;
import com.mediatek.cmmb.app.ChannelListManager.Channel;
import com.mediatek.cmmb.app.HelpActivity;
import com.mediatek.cmmb.app.MainScreen;
import com.mediatek.cmmb.app.MessageListActivity;
import com.mediatek.cmmb.app.PackageActivity;
import com.mediatek.cmmb.app.PlayerActivity;
import com.mediatek.cmmb.app.R;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class MainMenuFunctionTest extends ActivityInstrumentationTestCase2<MainScreen> {

    private static final String TAG = "CMMB::MainMenuFunctionTest";
    
    private MainScreen mActivity;
    private HelpActivity mHelpActivity;
    private MessageListActivity mEbmActivity;
    private static final int OPTION_PKM = 1;
    private static final int OPTION_REFRESH = 2;
    private static final int OPTION_EMB = 3;
    private static final int OPTION_SETTINGS = 4;
    private static final int OPTION_HELP = 5;
    private static final int OPTION_EXIT = 6;
    private static final int OPTION_MTS_SG_RETRIEVE = 7;
    private static final int OPTION_MTS_GBA = 8;
    private static final int OPTION_MTS_SUB_UPDATE = 9;
    private static final int OPTION_MTS_MSK_RETRIEVE = 10;   
    
    private int mTabIndex;
    private Solo mSolo = null;
    
    private static final int TAB_ALL = 0;
    private static final int TAB_FAVORITE = 1;
    
    private static final int STATE_UPDATING = 1;
    
    private ArrayList<Channel> mList; 
    private int mState;
    
    private Context mContext;
    
    Instrumentation mInstrumentation;
    
    public MainMenuFunctionTest() {
        super("com.mediatek.cmmb.app", MainScreen.class);
    }
    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setup>>");
        super.setUp();
        setActivityInitialTouchMode(false);
        mInstrumentation = getInstrumentation();
        mSolo = new Solo(getInstrumentation(), getActivity());
        assertNotNull(mInstrumentation);        
        mActivity = getActivity();
        mContext = mActivity.getApplicationContext();
        assertNotNull(mActivity);         
        
        mInstrumentation.waitForIdleSync();
        Thread.sleep(500);
        checkSimModeAndSelect();        
        mInstrumentation.waitForIdleSync();        
        Log.d(TAG, "setup <<");        
    }

    public void testCase00() throws Exception {        
        Log.d(TAG, "testCase00 >>");        
        waitAgain();
        Log.d(TAG, "testCase00 <<");
    }
    
    public void testCase01refresh() throws Exception {        
        Log.d(TAG, "testCase01_refresh >>");        
        waitAgain();
        mInstrumentation.waitForIdleSync();
        Log.d(TAG, "testcase01_refresh mState = " + getState());
        invokeOptionMenuByOrder(OPTION_REFRESH); 
        mInstrumentation.waitForIdleSync();  
        Log.d(TAG, "testCase01_refresh test specical refresh start");
        // test special city refresh
        if (CMMBUITestUtils.isInMbbmsMode(mActivity)) { 
            mInstrumentation.waitForIdleSync();
            Log.d(TAG, "testcase01_refresh mState = " + getState());
            mSolo.clickOnText(mActivity.getString(R.string.auto_refresh));
            
            mInstrumentation.waitForIdleSync();
            mSolo.sleep(2000);
           
            Log.d(TAG, "add city");
            EditText cityeditor = (EditText)mActivity.findViewById(com.mediatek.cmmb.app.R.id.city_editor);
            mSolo.enterText(cityeditor, "beijing");
            mInstrumentation.waitForIdleSync();
            mSolo.sleep(1000);
            mSolo.clickOnButton(mContext.getString(android.R.string.ok));
            waitAgain();
            Log.d(TAG, "testCase01_refresh test specical refresh end");
        }  
        // test cancel refresh
        Log.d(TAG, "testCase01_refresh test cancel refresh start");
        if (CMMBUITestUtils.isInMbbmsMode(mActivity)) {
            invokeOptionMenuByOrder(OPTION_REFRESH); 
            mInstrumentation.waitForIdleSync();
            mSolo.sleep(2000);
            mSolo.clickOnButton(mContext.getString(android.R.string.cancel));
            mSolo.sleep(2000);
            Log.d(TAG, "testCase01_refresh test cancel refresh end");
        }
        
        //test auto refresh
        Log.d(TAG, "testCase01_refresh test auto refresh start");
        if (CMMBUITestUtils.isInMbbmsMode(mActivity)) {
            invokeOptionMenuByOrder(OPTION_REFRESH); 
            mInstrumentation.waitForIdleSync();
            mSolo.sleep(2000);
            mSolo.clickOnButton(mContext.getString(android.R.string.ok));
            waitAgain();
            Log.d(TAG, "testCase01_refresh test auto refresh end");
        }      
        mSolo.sleep(2000);
        
        Log.d(TAG, "testCase01refresh <<");
    }             
    
    public void testCase03abortrefresh() throws Exception {
        Log.d(TAG, "testCase03_abortrefresh >>");
        Thread.sleep(3000);
        if (getState() == STATE_UPDATING) {
            invokeOptionMenuByOrder(OPTION_EXIT);
        } else {
            Log.d(TAG, "Current state is not updating");            
        }
        Log.d(TAG, "testCase03_abortrefresh <<");
    }
   
    public void testCase04FavoriteAndSwitchTab() throws Exception {        
        Log.d(TAG, "testCase04FavoriteAndSwitchTab>>");
        waitAgain();
        if (getState() == STATE_UPDATING) {  
            Log.d(TAG, "mSate wrong return");
            return;
        }
        ListView listView = (ListView) mActivity.findViewById(com.mediatek.cmmb.app.R.id.list_all);
        assertTrue((listView != null) && (listView.getCount() > 0));
        ListAdapter listAdapter = listView.getAdapter();
        
        for (int i = 0; i < (listView.getCount() / 2); i++) {
            Log.d(TAG, "add favorite i: " + i);
            View view = listAdapter.getView(i, null, listView);
            TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.channel_name);
            String frequencyStr = textView.getText().toString();
       
            mSolo.clickLongOnText(frequencyStr);
            getChannelListValue();
            ChannelListManager.Channel ch = mList.get(i);
            Log.d(TAG, " list view ch:" + ch);
            Log.d(TAG, "isfavorite():" + ch.isFavorite());
            Thread.sleep(1000);
            if (ch.isFavorite()) {
                Log.d(TAG, " remove favo:");
                mSolo.clickOnText(mContext.getString(com.mediatek.cmmb.app.R.string.remove_from_favorites));
            } else {
                Log.d(TAG, " add favo:");
                mSolo.clickOnText(mContext.getString(com.mediatek.cmmb.app.R.string.add_to_favorites));
            }
                mInstrumentation.waitForIdleSync();
                Thread.sleep(1000);         
        }
        
        Thread.sleep(2000);
  
        for (int i = 0; i < 4; i++) {
            Log.d(TAG, "remove favorite i: " + i);
            View view = listAdapter.getView(i, null, listView);
            TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.channel_name);
            String frequencyStr = textView.getText().toString();
 
            mSolo.clickLongOnText(frequencyStr);
            getChannelListValue();
            Channel ch = mList.get(i);
            Log.d(TAG, " list view ch:" + ch);
            Thread.sleep(500);
            if (ch.isFavorite()) {
                mSolo.clickOnText(mContext.getString(com.mediatek.cmmb.app.R.string.remove_from_favorites));
            } else {
                mSolo.clickOnText(mContext.getString(com.mediatek.cmmb.app.R.string.add_to_favorites));
            }
                mInstrumentation.waitForIdleSync();
                Thread.sleep(1000);         
        }
        Thread.sleep(2000); 
        Log.d(TAG, "testCase04Favorite <<");
        
        Log.d(TAG, "testCase04switchTab >>");
        Thread.sleep(2000);
        switchTab();
        Thread.sleep(2000);
        switchTab();
        Thread.sleep(2000); 
        Log.d(TAG, "testCase04FavoriteAndSwitchTab <<");
        return;
    }  

   
    public void testCase06playFreeinCmmb() throws Exception {
        Log.d(TAG, "testCase06_playFreeinCmmb >>");
        waitAgain();
        ListView listView = (ListView) mActivity.findViewById(com.mediatek.cmmb.app.R.id.list_all);
        assertTrue((listView != null) && (listView.getCount() > 0));
        ListAdapter listAdapter = listView.getAdapter();
        for (int i = 0; i < listView.getCount(); i++) {
            Log.d(TAG, "i: " + i);
            View view = listAdapter.getView(i, null, listView);
            TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.channel_name);
            String channelName = textView.getText().toString();
            getChannelListValue();
            ChannelListManager.Channel ch = mList.get(i);
            Log.d(TAG, "play ch:" + ch);
            if (ch.isFree()) {
                mSolo.clickOnText(channelName);
                Thread.sleep(3000);                
                
                PlayerActivity mPlayerActivity = (PlayerActivity)mSolo.getCurrentActivity();
                if (mPlayerActivity != null) {    
                    mPlayerActivity.finish();
                    mInstrumentation.waitForIdleSync();     
                    Thread.sleep(2000);
                }
                break;
            } 
        }
        Log.d(TAG, "testCase06_playFreeinCmmb <<");
    }
    
    public void testCase07playlockCancel() throws Exception {
        Log.d(TAG, "testCase07playlock >>");
        boolean isfound = false;
        waitAgain();
       ListView listView = (ListView) mActivity.findViewById(com.mediatek.cmmb.app.R.id.list_all);
        assertTrue((listView != null) && (listView.getCount() > 0));
        ListAdapter listAdapter = listView.getAdapter();
        for (int i = 0; i < listView.getCount(); i++) {
            View view = listAdapter.getView(i, null, listView);
            TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.channel_name);
            String channelName = textView.getText().toString();
            getChannelListValue();
            ChannelListManager.Channel ch = mList.get(i);
            Log.d(TAG, "play ch:" + ch);
            if (!ch.isFree() && !ch.isSubscribed()) {
                for (int j = 0; j <= i; j++) {
                    sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
                }
                isfound = true;
                Log.d(TAG, "click play lock channel in mbbms mode");
               mSolo.clickOnText(channelName);
               Thread.sleep(2000);                
               mSolo.clickOnButton(mContext.getString(android.R.string.cancel));
               Thread.sleep(2000);
               mSolo.goBack();
                break;
            }            
        }
        
        if (!isfound && CMMBUITestUtils.isInMbbmsMode(mActivity)) {
            invokeOptionMenuByOrder(OPTION_PKM);  
            Thread.sleep(3000);
            
            PackageActivity mPackage = (PackageActivity)mSolo.getCurrentActivity();          
            
            mSolo.waitForText(mPackage.getString(R.string.query_account_info_success), 1, 10000);
            CMMBUITestUtils.sleepThread(2000);
            
            try {
                // get subscribed items
                ListView mSubscribed = 
                    (ListView)CMMBUITestUtils.getField(PackageActivity.class, "mSubscribedListView").get(mPackage);
                int subCount = mSubscribed.getCount() - 1;
                Log.v(TAG, "Subed num = " + subCount);

                // get unSubscribed items
                assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
                CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
                mSolo.clickOnText(mPackage.getString(R.string.unsubscribed_package));
                CMMBUITestUtils.sleepThread(1000);
                ListView mUnSubscribed = 
                    (ListView)CMMBUITestUtils.getField(PackageActivity.class, "mUnsubscribedListView").get(mPackage);
                int unSubCount = mUnSubscribed.getCount();
                Log.v(TAG, "Un Subed num = " + unSubCount);
            
            
                if (subCount > 0){
                    // entry subscribed tab
                    assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
                    CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
                    mSolo.clickOnText(mPackage.getString(R.string.subscribed_package));
                    CMMBUITestUtils.sleepThread(1000);

                    // unsubscribe
                    assertTrue(mSolo.searchButton(mPackage.getString(R.string.unsubscribe)));
                    mSolo.clickOnButton(mPackage.getString(R.string.unsubscribe));
                    mSolo.waitForText(mPackage.getString(R.string.unsubscribe_tip), 1, 3000);
                    CMMBUITestUtils.sleepThread(2000);

                    // confirm to unsubscribe
                    assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
                    CMMBUITestUtils.expectOneDialog(mSolo, mPackage);
                    mSolo.clickOnButton(mPackage.getString(android.R.string.yes));
                    CMMBUITestUtils.sleepThread(20000);
                
                    CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
                    CMMBUITestUtils.sleepThread(1000);
                
                    Log.v(TAG, "Current Activity is Main " + (mActivity == mSolo.getCurrentActivity()));
                    return;
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            
            //play lock again
            for (int i = 0; i < listView.getCount(); i++) {
                View view = listAdapter.getView(i, null, listView);
                TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.channel_name);
                String channelName = textView.getText().toString();
                getChannelListValue();
                ChannelListManager.Channel ch = mList.get(i);
                Log.d(TAG, "play ch:" + ch);
                if (!ch.isFree() && !ch.isSubscribed()) {
                    for (int j = 0; j <= i; j++) {
                        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
                    }
                    isfound = true;
                    Log.d(TAG, "click play lock channel in mbbms mode");
                    mSolo.clickOnText(channelName);
                    Thread.sleep(2000);
                    mSolo.clickOnButton(mContext.getString(android.R.string.cancel));
                    Thread.sleep(2000);
                    mSolo.goBack();
                    break;
                }            
            }              
        }
        Log.d(TAG, "testCase07playlock <<");
    }

    public void testCase08optionmenu() throws Exception {
        Log.d(TAG, "testCase08optionmenu>>");
        waitAgain();
        
        Log.d(TAG, "testCase08optionmenu Settings>>");
        waitAgain();
        invokeOptionMenuByOrder(OPTION_SETTINGS);  
        Thread.sleep(2000);        
        mSolo.goBack();
        Thread.sleep(2000); 
        Log.d(TAG, "testCase08optionmenuSettings <<");
        
        Log.d(TAG, "testCase08optionmenu  emb start");
        invokeOptionMenuByOrder(OPTION_EMB);
        Thread.sleep(2000);
      
        mEbmActivity = (MessageListActivity)mSolo.getCurrentActivity();
        if (mEbmActivity != null) {    
            mEbmActivity.finish();
            mInstrumentation.waitForIdleSync();     
            Thread.sleep(2000); 
        }
        Log.d(TAG, "testCase08optionmenu  emb end");
        Log.d(TAG, "testCase08optionmenu  help start");
        invokeOptionMenuByOrder(OPTION_HELP);
        Thread.sleep(2000);        
      
        mHelpActivity = (HelpActivity)mSolo.getCurrentActivity();
        if (mHelpActivity != null) {    
            mHelpActivity.finish();
            mInstrumentation.waitForIdleSync();     
            Thread.sleep(2000); 
        }
        Log.d(TAG, "testCase08optionmenu  help end");
        
        Log.d(TAG, "testCase08optionmenu exit start");
        waitAgain();
        invokeOptionMenuByOrder(OPTION_EXIT);
        mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
        mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
        mInstrumentation.waitForIdleSync();
        Log.d(TAG, "testCase08optionmenu exit end");
        
        Log.d(TAG, "testCase08optionmenu <<"); 
    }
  
    public void testCase09MtsGBA() throws Exception {
        Log.d(TAG, "testCase09MtsGBA >>");
        waitAgain();
        if (!getMtsEnable()) {
            Log.d(TAG, "no mts menu");
            return ;
        }
        Log.d(TAG, "testCase09Mts GBA start");
        Thread.sleep(2000);
        invokeOptionMenuByOrder(OPTION_MTS_GBA);
        Thread.sleep(4000);        
        Log.d(TAG, "testCase09MtsGBA <<");
    }

    public void testCase10MtsSUB() throws Exception {
        Log.d(TAG, "testCase10MtsSUB >>");
        waitAgain();
        if (!getMtsEnable()) {
            Log.d(TAG, "no mts menu");
            return ;
        }
        Log.d(TAG, "testCase09Mts sub start");
        Thread.sleep(2000);
        invokeOptionMenuByOrder(OPTION_MTS_SUB_UPDATE);
        Thread.sleep(4000);  
        Log.d(TAG, "testCase10MtsSUB <<");
    }
  
    
    public void testCase11MtsSG() throws Exception {
        Log.d(TAG, "testCase11MtsSG >>");
        waitAgain();
        if (!getMtsEnable()) {
            Log.d(TAG, "no mts menu");
            return ;
        }
        Log.d(TAG, "testCase09Mts sg start");
        Thread.sleep(2000);
        invokeOptionMenuByOrder(OPTION_MTS_SG_RETRIEVE);
        Thread.sleep(4000); 
        Log.d(TAG, "testCase11MtsSG <<");
    }
    
    public void testCase12MtsMSK() throws Exception {
        Log.d(TAG, "testCase12MtsMSK >>");
        waitAgain();
        if (!getMtsEnable()) {
            Log.d(TAG, "no mts menu");
            return ;
        }
        Log.d(TAG, "testCase09Mts msk start");
        Thread.sleep(2000);
        invokeOptionMenuByOrder(OPTION_MTS_MSK_RETRIEVE);
        Thread.sleep(4000);  
        Log.d(TAG, "testCase12MtsMSK <<");
    }
    
    /*below are utility functions*/
    private int getState() throws Exception {
        Field fState = MainScreen.class.getDeclaredField("mState");
        fState.setAccessible(true);
        mState = (Integer)fState.get(mActivity);
        Log.d(TAG, "mState:" + mState);
        return mState;
    }
    
    private boolean getMtsEnable() throws Exception {        
        Field mTSEnabled = MainScreen.class.getDeclaredField("mIsMTSEnabled");
        mTSEnabled.setAccessible(true);
        Boolean isMTSEnabled = (Boolean)mTSEnabled.get(mActivity);
        Log.d(TAG, "isMTSEnabled:" + isMTSEnabled);
        return isMTSEnabled;
    }
    
    private void checkSimModeAndSelect() {
        Log.d(TAG, "CheckSimMode >>");

        try {
            Field fSelectSimDialog = MainScreen.class.getDeclaredField("mSelectSimDlg");
            Field fCmmbDialog = MainScreen.class.getDeclaredField("mCmmbDialog");
       if (fSelectSimDialog != null) {
                Log.d(TAG, "show select sim log");
                mInstrumentation.waitForIdleSync();
                // click OK button of save recording dialog
                mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
                mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
                mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
                mInstrumentation.waitForIdleSync();
                return;
            }           
          if (fCmmbDialog != null) {
                Log.d(TAG, "show CMMB confirm log");
                mInstrumentation.waitForIdleSync();
                // click OK button of save recording dialog
                mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
                mInstrumentation.waitForIdleSync();
                return;
            }

     
        } catch (Exception e) {
            Log.d(TAG, " error happened");
        }
    }
    
    private void waitAgain() throws Exception {
        do {
           Log.d(TAG, "wait again");
        Thread.sleep(5000);
    } while (getState() == STATE_UPDATING);
        
    }
    
    private void switchTab() throws Exception {
        Log.d(TAG, "switchTab");
        mTabIndex = getTabIndex(); 
        Log.d(TAG, "switchTab prepare to switch");
        Log.d(TAG, "Switchtab get tab back");
        if (mTabIndex == TAB_ALL) {
            Log.d(TAG, "switch tab favorite");
            mSolo.clickOnText(mContext.getString(com.mediatek.cmmb.app.R.string.main_favourite_channels));
            Thread.sleep(2000);
            mInstrumentation.waitForIdleSync();
        } else if (mTabIndex == TAB_FAVORITE) {
            Log.d(TAG, "switch tab all");
            mSolo.clickOnText(mContext.getString(com.mediatek.cmmb.app.R.string.main_all_channels)); 
            Thread.sleep(2000);
            mInstrumentation.waitForIdleSync();           
        }
        Log.d(TAG, "switchtab mTabIndex = " + mTabIndex);
    }
    
    private void getChannelListValue() throws Exception {
       
        Field mF  = MainScreen.class.getDeclaredField("mChannelListManager");       
        assertNotNull(mF);
        mF.setAccessible(true);
        if (getTabIndex() == TAB_ALL) {            
            mList = ((ChannelListManager)mF.get(mActivity)).getChannelList(); 
            Log.d(TAG, "getChannelListValue list:" + mList);
        } else {
            Log.d(TAG, "current is tab favorite");
        } 
    }
    
    private int getTabIndex() throws Exception {
        Log.d(TAG, "getTabIndex");
        Field ft = MainScreen.class.getDeclaredField("mTabIndex");
        assertNotNull(ft);  
        ft.setAccessible(true);
        mTabIndex = (Integer)ft.get(mActivity);
        Log.d(TAG, "getTabindex index = " + mTabIndex);
        return mTabIndex;        
    }
    
    private void invokeOptionMenuByOrder(int order) {
        Log.d(TAG, "invokeOptionMenuByOrder order:" + order);
        switch (order) {
            case OPTION_PKM:
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_PKM, 0);
                break;
            case OPTION_REFRESH:
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_REFRESH, 0);
                break;
            case OPTION_EMB:
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_EMB, 0);
                break;
            case OPTION_HELP:
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_HELP, 0);
                break;
            case OPTION_EXIT:
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_EXIT, 0);
                break;
            case OPTION_SETTINGS:
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_SETTINGS, 0);
                break;
 case OPTION_MTS_SG_RETRIEVE:
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_MTS_SG_RETRIEVE, 0);
                break;
 case OPTION_MTS_GBA:
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_MTS_GBA, 0);
                break;
 case OPTION_MTS_SUB_UPDATE:
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_MTS_SUB_UPDATE, 0);
                break;
 case OPTION_MTS_MSK_RETRIEVE :
                mInstrumentation.invokeMenuActionSync(mActivity, OPTION_MTS_MSK_RETRIEVE , 0);
                break;
            default:
                Log.d(TAG,"Strange menu is selected.order = " + order);
                break;
        }
        mInstrumentation.waitForIdleSync();    
    }
    
    
    /*
     * (non-Javadoc)
     * @see android.test.ActivityInstrumentationTestCase2#tearDown()
     */
    @Override
    public void tearDown() throws Exception {
        Log.d(TAG, "tearDown >>");
        if (mActivity != null) {
            mActivity.finish();
            mInstrumentation.waitForIdleSync();     
            Thread.sleep(2000);             
        }
        super.tearDown();
        Log.d(TAG, "tearDown <<");
    }
}
