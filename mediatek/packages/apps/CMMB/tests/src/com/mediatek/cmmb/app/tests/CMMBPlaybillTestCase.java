package com.mediatek.cmmb.app.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.content.Intent;

import android.graphics.drawable.Drawable;
import android.provider.Settings;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.ChannelListManager;
import com.mediatek.cmmb.app.ChannelListManager.Channel;
import com.mediatek.cmmb.app.MainScreen;
import com.mediatek.cmmb.app.ModeSwitchManager;
import com.mediatek.cmmb.app.PlaybillActivity;
import com.mediatek.cmmb.app.R;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CMMBPlaybillTestCase extends ActivityInstrumentationTestCase2<PlaybillActivity> {
    
    private static final String TAG = "CMMBPlaybillTestCase";
    
    private static final long UPDATING_TIME = 120000;    
    private static final long MONITOR_TIMEOUT = 3000;
    private static final long MIN_INIT_TIME = 1000;
    private static final long INIT_TIME = 2000;
    private static final long CHECK_TIME = 500;
    private static final long CHECK_TIMEOUT = 4000;
	private static final long PACKAGE_UPDATING =10000;
    private static final long PACKAGE_TIMEOUT = 180000;   
    //MainScreen's state
    private static final int STATE_IDLE = 0;
    private static final int STATE_UPDATED_SERVICE = 2;    
    private static final int STATE_UPDATED_SERVICE_FAIL = 3;
    
    private Instrumentation mInstrumentation = null;
    private PlaybillActivity mPlaybillActivity = null;
    private Solo mSolo = null;
    private MainScreen mMainScreen = null;
	private ActivityMonitor playbillMonitor = null; 
    
    private ModeSwitchManager mModeSwitchManager = null;
    private ChannelListManager mChannelListManager = null;
    private int mMbbmsMode;
    private ListView mAllChannelListView = null;
    private Channel mCurrentChannel = null;

    //Return info of clickProgramEntrance
    private static final long CHANNEL_LIST_EMPTY = 1;
    private static final long CLICK_CANPLAY_CHANNEL_FINISH = 2; 
    private static final long CLICK_NOTCANPLAY_CHANNEL_FINISH = 3; 
    private static final long NO_NOTCANPLAY_CHANNEL = 4;  
	private static final long NO_CANPLAY_CHANNEL = 5;
    
    public CMMBPlaybillTestCase() {
        super(PlaybillActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Log.i(TAG, ">>>set up");
        
        mInstrumentation = getInstrumentation();
        //to make view being focused to be clicked.
        mInstrumentation.setInTouchMode(false);
        getChannel(); 
        addPlayBillMonitor();
        Log.i(TAG, "<<<set up");
    }

    @Override
    protected void tearDown() throws Exception {
        Log.i(TAG, ">>>tearDown");
        if (mPlaybillActivity != null) {
            mPlaybillActivity.finish();
            sleep(MIN_INIT_TIME);//wait for finish done
            mPlaybillActivity = null;
        }
        
        if (mMainScreen != null) {
            mMainScreen.finish();
            sleep(MIN_INIT_TIME);//wait for finish done
            mMainScreen = null;
        }
		if(true == isAirplaneMode()){
			goToAirplaneMode(0);
			Log.i(TAG, "isAirplaneMode"+isAirplaneMode());
		}

		try {
			if(mSolo != null) {
				mSolo.finalize();
				mSolo = null;
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
        removePlayBillMonitor();     
        super.tearDown();
        
        Log.i(TAG, "<<<tearDown");
    }
    
    public void testCase01Title() {
        Log.i(TAG, ">>>testCase01Title");
        
        long mRet;
        mRet = clickProgramEntrance(true);
        
        if (CHANNEL_LIST_EMPTY == mRet) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            assertTrue(false);
        } else if (NO_CANPLAY_CHANNEL == mRet) {
			assertTrue(true);
		} else {
            mPlaybillActivity = getPlaybillActivity(playbillMonitor);
            mSolo = new Solo(mInstrumentation, mPlaybillActivity);
            
            assertNotNull("mPlaybillActivity is null", mPlaybillActivity);
            //title display the selected channel name
            TextView selectedService = (TextView)mPlaybillActivity.findViewById(R.id.selected_service);
            assertTrue(selectedService.getVisibility() == View.VISIBLE);
            String chanNameInTitle = selectedService.getText().toString();
            String seletedChanName = (String)getPrivateFiled(mCurrentChannel, "name");
            assertEquals(seletedChanName, chanNameInTitle);
            
            //title display the date
            TextView selectedDate = (TextView)mPlaybillActivity.findViewById(R.id.selected_date);
            assertTrue(selectedDate.getVisibility() == TextView.VISIBLE);
            String dateString = selectedDate.getText().toString();
            assertFalse(dateString.isEmpty());

			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
        }
        
        Log.i(TAG, "<<<testCase01Title");
    }
    
    public void testCase02SelectChannel() {
        Log.i(TAG, ">>>testCase02SelectChannel");

        long mRet;
        mRet = clickProgramEntrance(true);
        
        if (CHANNEL_LIST_EMPTY == mRet) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            assertTrue(false);
        } else if (NO_CANPLAY_CHANNEL == mRet) {
			assertTrue(true);
		} else {
            mPlaybillActivity = getPlaybillActivity(playbillMonitor);
            mSolo = new Solo(mInstrumentation, mPlaybillActivity);
  
            assertNotNull("mPlaybillActivity is null", mPlaybillActivity);
            //"Channels" button
            Button selectService = (Button)mPlaybillActivity.findViewById(R.id.select_service);
            assertTrue(selectService.isEnabled());
            assertTrue(selectService.isClickable());
            
            String seletedChanName = (String)getPrivateFiled(mCurrentChannel, "name");
            Log.i(TAG, "seletedChanName: " + seletedChanName);
            
            //show the Channels AlertDialog
            clickViewWithoutFocus(selectService);
            sleep(MIN_INIT_TIME);
            assertTrue(mSolo.searchText(mPlaybillActivity.getString(R.string.channels)));
            assertTrue(mSolo.searchText(seletedChanName));
            assertTrue(mSolo.searchButton(mPlaybillActivity.getString(android.R.string.cancel)));
            
            //get the items display on the Channels AlertDialog
            CharSequence[] serviceItems = null;
            try {
                  Method m = mPlaybillActivity.getClass().getDeclaredMethod("getServiceItems", (Class[])null);
                  m.setAccessible(true);
                  serviceItems = (CharSequence[])m.invoke(mPlaybillActivity);
              } catch (NoSuchMethodException e) {
                  e.printStackTrace();
              } catch (IllegalAccessException e) {
                  e.printStackTrace();
              } catch (InvocationTargetException e) {
                  e.printStackTrace();
              }

            //choose the first item to click
            while (mSolo.scrollUp()) {
                mSolo.scrollUp();
            }
            mInstrumentation.waitForIdleSync();
            sleep(MIN_INIT_TIME);
            mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_ENTER);
            mInstrumentation.waitForIdleSync();
            sleep(MIN_INIT_TIME);
            
            //check in title
            TextView selectedService = (TextView)mPlaybillActivity.findViewById(R.id.selected_service);
            String chanNameInTitle = selectedService.getText().toString();
            assertEquals(serviceItems[0].toString(), chanNameInTitle);
            
            //click cancel to quit
            clickViewWithoutFocus(selectService);
            sleep(MIN_INIT_TIME);
            mSolo.clickOnButton(mPlaybillActivity.getString(android.R.string.cancel));
            mInstrumentation.waitForIdleSync();
            sleep(MIN_INIT_TIME);
            mSolo.assertCurrentActivity("Channels AlertDialog cancel fail", PlaybillActivity.class);

			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
        
        Log.i(TAG, "<<<testCase02SelectChannel");
    }
    
    public void testCase03SelectDate() {
        Log.i(TAG, ">>>testCase03SelectDate");
        
        long mRet;
        mRet = clickProgramEntrance(true);
        
        if (CHANNEL_LIST_EMPTY == mRet) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            assertTrue(false);
        } else if (NO_CANPLAY_CHANNEL == mRet) {
			assertTrue(true);
		} else {
            mPlaybillActivity = getPlaybillActivity(playbillMonitor);
            
            mSolo = new Solo(mInstrumentation, mPlaybillActivity);
            assertNotNull("mPlaybillActivity is null", mPlaybillActivity);
            //"Date" button
            Button selectDate = (Button)mPlaybillActivity.findViewById(R.id.select_date);
            assertTrue(selectDate.isEnabled());
            assertTrue(selectDate.isClickable());
            
            //show the Date AlertDialog
            clickViewWithoutFocus(selectDate);
            sleep(MIN_INIT_TIME);
            
            //get and check the items display on the Date AlertDialog
            CharSequence[] dateItems = null;
            try {
                  Method m = mPlaybillActivity.getClass().getDeclaredMethod("getDateItems", (Class[])null);
                  m.setAccessible(true);
                  dateItems = (CharSequence[])m.invoke(mPlaybillActivity);
              } catch (NoSuchMethodException e) {
                  e.printStackTrace();
              } catch (IllegalAccessException e) {
                  e.printStackTrace();
              } catch (InvocationTargetException e) {
                  e.printStackTrace();
              }
            
            if (dateItems == null || dateItems.length < 1) {
                
                //check if show DIALOG_CONTENT_NO_MORE_DATE AlertDialog
                assertTrue(mSolo.searchText(mPlaybillActivity.getString(R.string.no_more_date)));
                assertTrue(mSolo.searchButton(mPlaybillActivity.getString(android.R.string.ok)));
                mSolo.clickOnButton(mPlaybillActivity.getString(android.R.string.ok));
                mInstrumentation.waitForIdleSync();
                sleep(MIN_INIT_TIME);
                mSolo.assertCurrentActivity("DIALOG_CONTENT_NO_MORE_DATE AlertDialog click ok fail", 
                    PlaybillActivity.class);
            } else {
                
                //show Date AlertDialog: check if all the items are showed
                for (int i = 0; i < dateItems.length; i++) {
                    Log.i(TAG, "dateItems " + i + ": " + dateItems[i].toString());
                    mInstrumentation.waitForIdleSync();
                    assertTrue(mSolo.searchText(dateItems[i].toString(), 1, true));
					mSolo.clickOnMenuItem(dateItems[i].toString());
					sleep(MIN_INIT_TIME);
					//show the Date AlertDialog
					clickViewWithoutFocus(selectDate);
					sleep(MIN_INIT_TIME);
                }
                //click cancel to quit
                sleep(MIN_INIT_TIME);
                mSolo.clickOnButton(mPlaybillActivity.getString(android.R.string.cancel));
                mInstrumentation.waitForIdleSync();
                sleep(MIN_INIT_TIME);
                mSolo.assertCurrentActivity("Date AlertDialog cancel fail", PlaybillActivity.class);
            }

			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
        }
		
        Log.i(TAG, "<<<testCase03SelectDate");
    }
    
    public void testCase04ListAndEmpty1() {
        Log.i(TAG, ">>>testCase04ListAndEmpty1");

        long mRet;
        mRet = clickProgramEntrance(true);

        Log.i(TAG, "mRet" + mRet);
        
        if (CHANNEL_LIST_EMPTY == mRet) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            assertTrue(false);
        } else if (NO_CANPLAY_CHANNEL == mRet) {
			assertTrue(true);
		} else {
            mPlaybillActivity = getPlaybillActivity(playbillMonitor);
            mSolo = new Solo(mInstrumentation, mPlaybillActivity);
            forListAndEmpty();

			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		
        Log.i(TAG, "<<<testCase04ListAndEmpty1");
    }
    
    public void testCase05Favorite() {
        Log.i(TAG, ">>>testCase05Favorite");
        
        long mRet;
        mRet = clickProgramEntrance(true);
        
        if (CHANNEL_LIST_EMPTY == mRet) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            assertTrue(false);
        }else if (NO_CANPLAY_CHANNEL == mRet) {
			assertTrue(true);
		} else {
            mPlaybillActivity = getPlaybillActivity(playbillMonitor);
            mSolo = new Solo(mInstrumentation, mPlaybillActivity);
            assertNotNull("mPlaybillActivity is null", mPlaybillActivity);
            //"Favorite" ImageButton
            ImageButton selectFavorite = (ImageButton)mPlaybillActivity.findViewById(R.id.select_favorite);
            assertTrue(selectFavorite.isEnabled());
            assertTrue(selectFavorite.isClickable());
            
            //check favorite
            boolean isFavorite = (Boolean)getPrivateFiled(mCurrentChannel, "isFavorite");
            Log.i(TAG, "isFavorite: " + isFavorite);
            Drawable favoriteNo = mPlaybillActivity.getResources().getDrawable(R.drawable.ic_favorite_no);
            Drawable favoriteYes = mPlaybillActivity.getResources().getDrawable(R.drawable.ic_favorite_yes);
            if (isFavorite) {
                assertTrue(favoriteYes.isVisible());
            } else {
                assertTrue(favoriteNo.isVisible());
            }
            
            //check the toast
            String favorAddedStr = mPlaybillActivity.getString(R.string.favourite_added);
            String favorRemovedStr = mPlaybillActivity.getString(R.string.favourite_removed);
            String curChanName = (String)getPrivateFiled(mCurrentChannel, "name");
            Log.i(TAG, "curChanName: " + curChanName);
            String toastMsg = null;
            if (isFavorite) {
                //removed toast
                toastMsg = String.format(favorRemovedStr, curChanName);
            } else {
                //added toast
                toastMsg = String.format(favorAddedStr, curChanName);
            }
            clickViewWithoutFocus(selectFavorite);//click the button
            assertTrue(mSolo.waitForText(toastMsg, 1, CHECK_TIMEOUT));
            
            //check if works in MainScreen
            sleep(MIN_INIT_TIME);
            checkFavoriteInMainScrn(!isFavorite);//isFavorite has changed

			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
        }
        Log.i(TAG, "<<<testCase05Favorite");
    }

    public void testCase06PackageManagement() {
        Log.i(TAG, ">>>testCase06PackageManagement");
        
        long mRet;
        mRet = clickProgramEntrance(true);
        
        if (CHANNEL_LIST_EMPTY == mRet) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            assertTrue(false);
        } else if (NO_CANPLAY_CHANNEL == mRet) {
			assertTrue(true);
		} else {
            mPlaybillActivity = getPlaybillActivity(playbillMonitor);
            mSolo = new Solo(mInstrumentation, mPlaybillActivity);

            assertNotNull("mPlaybillActivity is null", mPlaybillActivity);

            Activity a = null;
            ActivityMonitor packageManagementMonitor;    
            String sPackageActivity = new String("com.mediatek.cmmb.app.PackageActivity");
            packageManagementMonitor = addMonitor(sPackageActivity);

            String packgeManagementString = mPlaybillActivity.getString(R.string.package_management);
            Log.i(TAG, "R.string.package_management: " + packgeManagementString);
            mSolo.clickOnMenuItem(packgeManagementString);
            mInstrumentation.waitForIdleSync();

			a = packageManagementMonitor.waitForActivityWithTimeout(MONITOR_TIMEOUT);
			assertNotNull("PackageActivity is null", a);

			if(mSolo.waitForText(mPlaybillActivity.getString(R.string.query_account_info_ongoing), 1, PACKAGE_UPDATING)) {
			   if (!(mSolo.waitForText(mPlaybillActivity.getString(R.string.query_account_info_success), 1, PACKAGE_TIMEOUT))) {
				   Log.i(TAG, "Net work time out.");
				   //assertTrue(false);
			   }
			} 
            //sleep(PACKAGE_UPDATING);
            //Start PackageActivity
            while(a.hasWindowFocus()){ 
				mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
				mInstrumentation.waitForIdleSync();
				sleep(MIN_INIT_TIME);
            }
            removeMonitor(packageManagementMonitor);

			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
        }

        Log.i(TAG, "<<<testCase06PackageManagement");
    }

    public void testCase07ListAndEmpty2() {
        Log.i(TAG, ">>>testCase07ListAndEmpty2");
        
        long mRet;      
        mRet = clickProgramEntrance(false);     

        Log.i(TAG, "mRet" + mRet);
        
        if (CHANNEL_LIST_EMPTY == mRet) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            assertTrue(false);
        } else if ( NO_NOTCANPLAY_CHANNEL == mRet ) {
            assertTrue(true);
        } else {
            mPlaybillActivity = getPlaybillActivity(playbillMonitor);
            mSolo = new Solo(mInstrumentation, mPlaybillActivity);
            forListAndEmpty();
			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
        }

        Log.i(TAG, "<<<testCase07ListAndEmpty2");
    }

    public void testCase08ListAndEmpty3() {
        Log.i(TAG, ">>>testCase08ListAndEmpty3");
        
        long mRet;         
        goToAirplaneMode(1);
		Log.i(TAG, "isAirplaneMode" + isAirplaneMode());
        mRet = clickProgramEntrance(false);     

        Log.i(TAG, "mRet" + mRet);
        
        if (CHANNEL_LIST_EMPTY == mRet) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            assertTrue(false);
        } else if ( NO_NOTCANPLAY_CHANNEL == mRet ) {
            assertTrue(true);
        } else {
            mPlaybillActivity = getPlaybillActivity(playbillMonitor);
            mSolo = new Solo(mInstrumentation, mPlaybillActivity);      
            forListAndEmpty();
			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
        }
        
        Log.i(TAG, "<<<testCase08ListAndEmpty3");
    }

    public void testCase09ListAndEmpty4() {
        Log.i(TAG, ">>>testCase09ListAndEmpty4");
        
        long mRet;      
        goToAirplaneMode(1);
        mRet = clickProgramEntrance(false);     

        Log.i(TAG, "mRet" + mRet);
        
        if (CHANNEL_LIST_EMPTY == mRet) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            assertTrue(false);
        } else if ( NO_NOTCANPLAY_CHANNEL == mRet ) {
            assertTrue(true);
        } else {
            mPlaybillActivity = getPlaybillActivity(playbillMonitor);
            mSolo = new Solo(mInstrumentation, mPlaybillActivity);
            forListAndEmpty();
			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
        }
        
        Log.i(TAG, "<<<testCase09ListAndEmpty4");
    }
    
	public void testCase10Expend() {		
        Log.i(TAG, ">>>testCase10Expend");
		long mRet;
		mRet = clickProgramEntrance(true);

		if (CHANNEL_LIST_EMPTY == mRet) {
			Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
			assertTrue(false);
		} else if (NO_CANPLAY_CHANNEL == mRet) {
			assertTrue(true);
		} else {
			mPlaybillActivity = getPlaybillActivity(playbillMonitor);		
			mSolo = new Solo(mInstrumentation, mPlaybillActivity);

			if(mSolo.searchText(mPlaybillActivity.getString(R.string.no_content), true)){
				assertTrue(true);
				try {
					if(mSolo != null) {
						mSolo.finalize();
						mSolo = null;
					}
				} catch (Throwable e) {
					e.printStackTrace();
				}	
				return;
			}
			//"Expend"
            View selectExpend = (View)mPlaybillActivity.findViewById(R.id.item_switch);
            assertTrue(selectExpend.isEnabled());
            assertTrue(selectExpend.isClickable());
			clickViewWithoutFocus(selectExpend);
            sleep(MIN_INIT_TIME);

			Class[] mClass = mPlaybillActivity.getClass().getDeclaredClasses();
			Object holder = null;
			for (int i = 0; i<mClass.length; i++  ){
				String strClassName = mClass[i].getSimpleName();
				if(strClassName.equals("ViewHolder")) {
					try{
						holder = mClass[i].newInstance();
					} catch(InstantiationException e) {	
						e.printStackTrace();
					}catch(IllegalAccessException e) {
						e.printStackTrace();
					}
					break;
				} 
			}

			String DETAIL_PADDING = "\t\t";
		    holder = selectExpend.getTag();
			String detail = (String)getPrivateFiled(holder,"detail");
			Log.i(TAG, "detail = " + detail);
			if(mSolo.waitForText(DETAIL_PADDING + mPlaybillActivity.getString(R.string.content_no_detail), 1, PACKAGE_TIMEOUT)
				|| mSolo.waitForText(DETAIL_PADDING + detail, 1, PACKAGE_TIMEOUT)) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}
					
			try {
				if(mSolo != null) {
					mSolo.finalize();
					mSolo = null;
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}					

		}
		
        Log.i(TAG, "<<<testCase10Expend");
	}	
    private void checkFavoriteInMainScrn(boolean isFavorite) {
        
        //get current channel name
        String curChanName = (String)getPrivateFiled(mCurrentChannel, "name");
        
        //back to MainScreen
        mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
        mInstrumentation.waitForIdleSync();
        sleep(MIN_INIT_TIME);
        
        //click to FAVORITE CHANNELS
        mSolo.clickOnText(mMainScreen.getString(R.string.main_favourite_channels));
        mInstrumentation.waitForIdleSync();
        sleep(MIN_INIT_TIME);
        if (isFavorite) {
            assertTrue(mSolo.searchText(curChanName, true));
        } else {
            assertFalse(mSolo.searchText(curChanName, true));
        }
    } 
    
    private PlaybillActivity getPlaybillActivity(ActivityMonitor playbillMonitor) {
       
       Activity a = null;
       //get the PlaybillActivity instance
       a = playbillMonitor.waitForActivityWithTimeout(MONITOR_TIMEOUT);
       if (a == null) {
           Log.i(TAG, "mPlaybillActivity is null, can not test PlaybillActivity");
           return null;
       }
       
       //wait PlaybillActivity initialization done
       sleep(INIT_TIME);
       setActivity(a);
          
       return (PlaybillActivity)a;
    }
    
     private void getChannel() {
        //launch MainScreen Activity first
        mMainScreen = launchActivity(mInstrumentation.getTargetContext().getPackageName(), MainScreen.class, null);
        if ( mMainScreen == null ) {
            Log.i(TAG, "mMainScreen is null, can not test PlaybillActivity");
        } else {     
            sleep(MIN_INIT_TIME);
            //(AlertDialog)mCmmbDialog may be showed
            clickCmmbAlertDlg();
            sleep(MIN_INIT_TIME);    
            //wait for channel updating
            Log.i(TAG, "Start to sleep for channel updating");
            waitForChanUpdating();
            Log.i(TAG, "End to sleep for channel updating");
            sleep(MIN_INIT_TIME);
        }
    }

    private long clickProgramEntrance(boolean canPlay) {
        //choose an unlock channel to click its program entrance
        mAllChannelListView = (ListView)mMainScreen.findViewById(R.id.list_all);
        if (mAllChannelListView == null || mAllChannelListView.getCount() <= 0) {
            Log.i(TAG, "All channel list is empty, can not test PlaybillActivity");
            return CHANNEL_LIST_EMPTY;
        }
        
        mChannelListManager = (ChannelListManager)getPrivateFiled(mMainScreen, "mChannelListManager");
        BaseAdapter adapter = (BaseAdapter)mAllChannelListView.getAdapter();
        if (canPlay) { 
            if (mModeSwitchManager.getMode() == mMbbmsMode) {
                Log.i(TAG, "it is MBBMS mode");
                for (int i = 0; i < mAllChannelListView.getCount(); i++) {
                    Channel ch = mChannelListManager.getChannelList().get(i); 
                    Log.i(TAG,"ch.isEncrypted()" + ch.isEncrypted());
                    Log.i(TAG,"ch.isFree()" + ch.isFree());
                    Log.i(TAG,"ch.isSubscribed()" + ch.isSubscribed());
                    if ((ch.isEncrypted() && (ch.isFree() || ch.isSubscribed()))
                            || (!ch.isEncrypted())) {
                        
                        View item = adapter.getView(i, null, mAllChannelListView);
                        View programEntrance = item.findViewById(R.id.program_entrance);
                        assertTrue(programEntrance.isClickable());
                        clickViewWithoutFocus(programEntrance);
                        Log.i(TAG, "click on the channel's programEntrance: "
                        + ((TextView)item.findViewById(R.id.channel_name)).getText().toString());
                        mCurrentChannel = ch;
                        break;
                    }
					
					if( i+1 == mAllChannelListView.getCount()){
						return NO_CANPLAY_CHANNEL;
					} 
                }
            } else {
                Log.i(TAG, "it is  not MBBMS mode");
                for (int i = 0; i < mAllChannelListView.getCount(); i++) {
                    Channel ch = mChannelListManager.getChannelList().get(i); 
                    if (ch.isFree()) {                   
                        View item = adapter.getView(i, null, mAllChannelListView);
                        View programEntrance = item.findViewById(R.id.program_entrance);
                        assertTrue(programEntrance.isClickable());
                        clickViewWithoutFocus(programEntrance);
                        Log.i(TAG, "click on the channel's programEntrance: "
                        + ((TextView)item.findViewById(R.id.channel_name)).getText().toString());
                        mCurrentChannel = ch;
                        break;
                    }

					if( i+1 == mAllChannelListView.getCount()){
						return NO_CANPLAY_CHANNEL;
					} 
                }
            }
            
            return  CLICK_CANPLAY_CHANNEL_FINISH;
        } else {  
            Log.i(TAG, "if it is  MBBMS mode"+(mModeSwitchManager.getMode() == mMbbmsMode));
            for (int i = 0; i < mAllChannelListView.getCount(); i++) {

                Channel ch = mChannelListManager.getChannelList().get(i);
                Log.i(TAG,"mAllChannelListView.getCount()"+mAllChannelListView.getCount());         
                Log.i(TAG,"ch.isEncrypted()" + ch.isEncrypted());
                Log.i(TAG,"ch.isFree()" + ch.isFree());
                Log.i(TAG,"ch.isSubscribed()" + ch.isSubscribed());
                
                if (mModeSwitchManager.getMode() == mMbbmsMode) {
                    if ((ch.isEncrypted() && (ch.isFree() || ch.isSubscribed()))
                                                || (!ch.isEncrypted())) {
                        Log.i(TAG,"iiiii" + i);
						if( i+1 == mAllChannelListView.getCount()){
							return NO_NOTCANPLAY_CHANNEL;
						} else {	
                        	continue; 
						}  
                    }
                } else {
                    if (ch.isFree()) {  
                        Log.i(TAG,"iiiii" + i);
						if( i+1 == mAllChannelListView.getCount()){
							return NO_NOTCANPLAY_CHANNEL;
						} else {	
							continue; 
						}  
                    }
                }
                
                if (!ch.isFree() &&  i != 0) {         
                    View item = adapter.getView(i, null, mAllChannelListView);
                    View programEntrance = item.findViewById(R.id.program_entrance);
                    assertTrue(programEntrance.isClickable());
                    clickViewWithoutFocus(programEntrance);
                    Log.i(TAG, "click on the channel's programEntrance: "
                    + ((TextView)item.findViewById(R.id.channel_name)).getText().toString());
                    mCurrentChannel = ch;
                    break;
                }                               
				if( i+1 == mAllChannelListView.getCount()){
					return NO_NOTCANPLAY_CHANNEL;
				} 
                
            }
            
            return  CLICK_NOTCANPLAY_CHANNEL_FINISH;
        }
    }    
 
    private void waitForChanUpdating() {
        
        long startTime = System.currentTimeMillis();
        int mainScreenState = (Integer)getPrivateFiled(mMainScreen, "mState");
        
        while (mainScreenState != STATE_UPDATED_SERVICE
                && mainScreenState != STATE_UPDATED_SERVICE_FAIL
                && mainScreenState != STATE_IDLE) {
            if (System.currentTimeMillis() - startTime > UPDATING_TIME) {
                Log.i(TAG, "waitForChanUpdating timeout");
                break;
            }
            sleep(CHECK_TIME);
            mainScreenState = (Integer)getPrivateFiled(mMainScreen, "mState");
        }
        Log.i(TAG, "mainScreenState: " + mainScreenState);
    }
    
    private void clickCmmbAlertDlg() {
        //check if has sim <=> check if it is mMbbmsMode
        mModeSwitchManager = (ModeSwitchManager)getPrivateFiled(mMainScreen, "mModeSwitchManager");
        mMbbmsMode = (Integer)getPrivateFiled(mMainScreen, "MBBMS_MODE");
        if (mModeSwitchManager.getMode() != mMbbmsMode) {
            Log.i(TAG, "CmmbAlertDlg Is Show");
            mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_ENTER);
            mInstrumentation.waitForIdleSync();
        } else {
            Log.i(TAG, "CmmbAlertDlg Is Not Show");
        }
    }
    
    //access private filed from of obj
    private Object getPrivateFiled(Object obj, String variable) {
        Field field = null;
        try {
            field = obj.getClass().getDeclaredField(variable);
            field.setAccessible(true);
            return field.get(obj);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private void sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    private void clickViewWithoutFocus(final View view) {
		
		mSolo = new Solo(mInstrumentation, mPlaybillActivity);

		while (!(view.hasWindowFocus())) {
			mSolo.goBack(); 				
			CMMBUITestUtils.sleepThread(2000);
		}
	
        mInstrumentation.runOnMainSync(new Runnable() {
            
            @Override
            public void run() {    
                Log.d(TAG,"clickView view = " + view);      	
				view.performClick();
            }
        });
		
		try {
			mSolo.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
        
        mInstrumentation.waitForIdleSync();
    }
    
    private ActivityMonitor addMonitor(String variable) {
        ActivityMonitor mMonitor = mInstrumentation.addMonitor(
                variable, null, false);
        return mMonitor;
    }
    
    private void removeMonitor(ActivityMonitor mMonitor) {
        mInstrumentation.removeMonitor(mMonitor);
    }
	
	private void addPlayBillMonitor() {   
		String variable = new String("com.mediatek.cmmb.app.PlaybillActivity");
		playbillMonitor = addMonitor(variable);
	}
	
	private void removePlayBillMonitor() {
		if(null != playbillMonitor) {
			removeMonitor(playbillMonitor);
		}
	}
	
    private void goToAirplaneMode(int mValue) {
      Settings.Global.putInt(getInstrumentation().getTargetContext().getContentResolver(),
              Settings.Global.AIRPLANE_MODE_ON, mValue);
      Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
      if(1 == mValue) {
        intent.putExtra("state", true);
      } else {
        intent.putExtra("state", false);
      }
      getInstrumentation().getTargetContext().sendBroadcast(intent);
      Log.i(TAG, "set flight mode");
      sleep(10000);
    }
    	
	private boolean isAirplaneMode() {
		int mValue;
		mValue = Settings.Global.getInt(getInstrumentation().getTargetContext().getContentResolver(),
					  Settings.Global.AIRPLANE_MODE_ON, 0);
		if(1 == mValue ) {
			return true;
		} else {
			return false;
		}
	}
    private void forListAndEmpty() {
        boolean mIsSGMode = false;
        boolean isSubscribed = false;
        assertNotNull("mPlaybillActivity is null", mPlaybillActivity);     
        TextView emptyView = (TextView)mPlaybillActivity.findViewById(android.R.id.empty);
        ListView listView = (ListView)mPlaybillActivity.findViewById(android.R.id.list);

        if (emptyView.getVisibility() == View.VISIBLE) {
               Log.i(TAG, "emptyView.getVisibility() == View.VISIBLE");
               assertTrue(listView.getVisibility() == View.GONE);
               assertTrue(mSolo.searchText(mPlaybillActivity.getString(R.string.no_content), true));
        } else {
               Log.i(TAG, "listView.getVisibility() == View.VISIBLE");
               assertTrue(listView.getVisibility() == View.VISIBLE);
               assertTrue(listView != null && listView.getCount() > 0);     

               Log.i(TAG, " listView.getCount() = " + listView.getCount());
               
               for ( int i = 0; i < listView.getCount(); i++ ) {
                       Log.i(TAG, "i = " + i);
                        //mSolo.clickInList(1, 1);
                       mSolo.clickInList(i + 1,0);
                       try {  
                               Method m = mPlaybillActivity.getClass().getDeclaredMethod("isSubscribed", (Class[])null);
                               m.setAccessible(true);
                               isSubscribed = (Boolean)m.invoke(mPlaybillActivity);
                       } catch (NoSuchMethodException e) {
                               e.printStackTrace();
                       } catch (IllegalAccessException e) {
                               e.printStackTrace();
                       } catch (InvocationTargetException e) {
                               e.printStackTrace();
                       }
                       
                       Log.i(TAG, "isSubscribed = " + isSubscribed);
                       
                       if (isSubscribed) {                      
                                   Activity a = null;
                                   ActivityMonitor playerMonitor;    
                                   String variable = new String("com.mediatek.cmmb.app.PlayerActivity");
                                   playerMonitor = addMonitor(variable);
                                   //Start PlayerActivity
                                   a = playerMonitor.waitForActivityWithTimeout(MONITOR_TIMEOUT);
                                   assertNotNull("PackageActivity is null", a);  
								   mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
								   mInstrumentation.waitForIdleSync();
								   sleep(MIN_INIT_TIME);
                                   removeMonitor(playerMonitor);
                                   break;
                        } else { 
                                   Object mCurrentService = getPrivateFiled(mPlaybillActivity, "mCurrentService");
                                   String serviceName = (String)getPrivateFiled(mCurrentService, "serviceName");
                                   mIsSGMode = (Boolean)getPrivateFiled(mPlaybillActivity, "mIsSGMode");
                                   
                                   Log.i(TAG, "mIsSGMode = " + mIsSGMode);

                                   if (mIsSGMode) {
                                    
                                          Activity a = null;
                                          ActivityMonitor packageManagementMonitor;   
                                          String sPackageActivity = new String("com.mediatek.cmmb.app.PackageActivity");
                                          packageManagementMonitor = addMonitor(sPackageActivity);                                        
                                           assertTrue(mSolo.searchText(String.format(
                                                                                       mPlaybillActivity.getString(R.string.need_subscription_hint), serviceName), true));
                                           mSolo.clickOnButton(mPlaybillActivity.getString(android.R.string.ok));
                                           mInstrumentation.waitForIdleSync();
										//sleep(PACKAGE_UPDATING);

										a = packageManagementMonitor.waitForActivityWithTimeout(MONITOR_TIMEOUT);
										assertNotNull("PackageActivity is null", a);
										//mSolo.sendKey(KeyEvent.KEYCODE_BACK);

										if(mSolo.waitForText(mPlaybillActivity.getString(R.string.query_account_info_ongoing), 1, PACKAGE_UPDATING)) {
											if (!(mSolo.waitForText(mPlaybillActivity.getString(R.string.query_account_info_success), 1, PACKAGE_TIMEOUT))) {
												Log.i(TAG, "Net work time out.");
												//assertTrue(false);
											}
										} 

										while (a.hasWindowFocus()) {
											mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
											mInstrumentation.waitForIdleSync();
											sleep(MIN_INIT_TIME);
										}
                                            removeMonitor(packageManagementMonitor);


                                   } else {
                                           assertTrue(mSolo.searchText(String.format(
                                                                                      mPlaybillActivity.getString(R.string.channel_unavailable_now), serviceName), true));
                                   }   
                                   break;     
                        }                      
           }
        }
    }

}
