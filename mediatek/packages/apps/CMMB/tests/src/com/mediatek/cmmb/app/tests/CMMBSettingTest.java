/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.cmmb.app.tests;

import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.MainScreen;
import com.mediatek.cmmb.app.R;
import com.mediatek.cmmb.app.SettingActivity;

import java.util.ArrayList;

public class CMMBSettingTest extends ActivityInstrumentationTestCase2<SettingActivity> {
    private static final String TAG = "CMMBUITest-SettingActivity";
    
    private Instrumentation mInst = null;
    private MainScreen mMainActivity = null;
    private Solo mSolo = null;
    private boolean mInMbbmsMode = false;
    
    public CMMBSettingTest() {
        super(SettingActivity.class);
    }
    
    @Override
    protected void setUp() throws Exception {
        Log.v(TAG, "setUp <<<<<");
        super.setUp();

        mInst = getInstrumentation();
        assertNotNull(mInst);

        mSolo = new Solo(mInst);
        assertNotNull(mSolo);

        mInst.waitForIdleSync();
        mInst.setInTouchMode(false);
        mMainActivity = launchActivity("com.mediatek.cmmb.app", MainScreen.class, null);
        assertNotNull(mMainActivity);
        setActivity(mMainActivity);
        
        CMMBUITestUtils.sleepThread(2000);
        
        mInMbbmsMode = CMMBUITestUtils.isInMbbmsMode(mMainActivity);
        
        CMMBUITestUtils.checkSimModeAndSelect(mInst);
        CMMBUITestUtils.waitForUpdateDone(mMainActivity);

        Log.v(TAG, "setUp >>>>>");
    }
    
    @Override
    protected void tearDown() throws Exception {
        Log.v(TAG, "tearDown <<<<<");

        CMMBUITestUtils.sleepThread(2000);

        if (mMainActivity != null) {
            mMainActivity.finish();
            mMainActivity = null;
        }

        super.tearDown();
        CMMBUITestUtils.sleepThread(2000);
        Log.v(TAG, "tearDown >>>>>");
    }
    
    public void test01Setting() {
        Log.v(TAG, "test01Setting <<<<<");
        
        if (!mInMbbmsMode) {
            return;
        }
        
        SettingActivity setting = (SettingActivity)entrySetting(mInst, mSolo);
        CMMBUITestUtils.sleepThread(2000);

        // click cancel on restore
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(mMainActivity.getString(R.string.restore));
        assertTrue(mSolo.waitForText(setting.getString(R.string.restore_warning), 1, 3000));
        mSolo.goBack();

        // click ok to  restore
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.restore));
        assertTrue(mSolo.waitForText(setting.getString(R.string.restore_warning), 1, 3000));
        mSolo.clickOnText(setting.getString(android.R.string.ok));
        CMMBUITestUtils.sleepThread(2000);
        
        // back to main screen
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.goBack();
        
        Log.v(TAG, "test01Setting >>>>>");
    }
    
    public void test02Setting() {
        Log.v(TAG, "test02Setting <<<<<");
        
        if (!mInMbbmsMode) {
            return;
        }
        
        SettingActivity setting = (SettingActivity)entrySetting(mInst, mSolo);
        CMMBUITestUtils.sleepThread(2000);

        // click on item sg service host
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.sg_server_host));
        assertTrue(mSolo.waitForText(setting.getString(R.string.sg_server_host), 1, 3000));
        
        // TODO: DB.Setting
        EditText sgAdressText = (EditText)mSolo.getEditText("sg.mbbms.chinamobile.com");
        assertNotNull(sgAdressText);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(1000);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(2000);

        // click on item sg service host
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.sg_server_host));
        CMMBUITestUtils.sleepThread(2000);
        mSolo.clickOnText(setting.getString(android.R.string.ok));
        CMMBUITestUtils.sleepThread(2000);

        // click on item sg service port
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);        
        mSolo.clickOnText(setting.getString(R.string.sg_server_port));
        assertTrue(mSolo.waitForText(setting.getString(R.string.sg_server_port), 1, 3000));
        
        // TODO: DB.Setting
        EditText sgPortText = (EditText)mSolo.getEditText("80");
        assertNotNull(sgPortText);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(1000);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(2000);

        // click on item sg service port
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.sg_server_port));
        CMMBUITestUtils.sleepThread(2000);
        mSolo.clickOnText(setting.getString(android.R.string.ok));
        CMMBUITestUtils.sleepThread(2000);

        // click on item wap host
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.wap_host));
        assertTrue(mSolo.waitForText(setting.getString(R.string.wap_host), 1, 3000));
        
        // TODO: DB.Setting
        EditText wapHostText = (EditText)mSolo.getEditText("10.0.0.172");
        assertNotNull(wapHostText);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(1000);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(2000);

        // click on item wap host
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.wap_host));
        CMMBUITestUtils.sleepThread(2000);
        mSolo.clickOnText(setting.getString(android.R.string.ok));
        CMMBUITestUtils.sleepThread(2000);

        // click on item wap port
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.wap_port));
        assertTrue(mSolo.waitForText(setting.getString(R.string.wap_port), 1, 3000));
        
        // TODO: DB.Setting
        EditText wapPortText = (EditText)mSolo.getEditText("80");
        assertNotNull(wapPortText);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(1000);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(2000);

        // click on item wap port
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.wap_port));
        CMMBUITestUtils.sleepThread(2000);
        mSolo.clickOnText(setting.getString(android.R.string.ok));
        CMMBUITestUtils.sleepThread(2000);

        // back to Main Screen
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(2000);
        
        Log.v(TAG, "test02Setting >>>>>");
    }
    
    @SuppressWarnings("unchecked")
    public void test03Setting() {
        Log.v(TAG, "test03Setting <<<<<");
        
        if (!mInMbbmsMode) {
            return;
        }
        
        SettingActivity setting = (SettingActivity)entrySetting(mInst, mSolo);
        CMMBUITestUtils.sleepThread(2000);

        // click on item apn
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.apn));
        assertTrue(mSolo.waitForText(setting.getString(R.string.apn), 1, 3000));

        // get apn list and check        
        try {
            ArrayList<String> apnList = 
                (ArrayList<String>)(CMMBUITestUtils.getField(SettingActivity.class, "mApnNameList").get(setting));
            assertTrue(apnList.size() > 0);
            
            ArrayList<View> allViews = mSolo.getViews();
            for (String apnStr : apnList) {
                Log.v(TAG, "apnStr = \"" + apnStr + "\"");
                boolean flag = false;
                for (View view : allViews) {
                    if (view instanceof TextView) {
                        Log.v(TAG, "apnStr = \"" + ((TextView)view).getText().toString() + "\"");
                        if (apnStr.equalsIgnoreCase(((TextView)view).getText().toString())) {
                            flag = true;
                        }
                    }
                }
                assertTrue(flag);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        mSolo.goBack();
        CMMBUITestUtils.sleepThread(1000);

        // select on apn
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnText(setting.getString(R.string.apn));
        CMMBUITestUtils.sleepThread(2000);
        mSolo.clickInList(1);
        CMMBUITestUtils.sleepThread(2000);

        // back to Main Screen
        assertTrue(CMMBUITestUtils.isExpectedActivity(setting));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(2000);
        
        Log.v(TAG, "test03Setting >>>>>");
    }
    
    private  SettingActivity entrySetting(Instrumentation inst, Solo solo) {
        gobackToMain(solo);
        CMMBUITestUtils.expectNoDialog(solo, mMainActivity);
        ActivityMonitor am = new ActivityMonitor(SettingActivity.class.getName(), null, false);
        inst.addMonitor(am);
        solo.clickOnMenuItem(mMainActivity.getString(R.string.setting));
        SettingActivity setting = (SettingActivity)inst.waitForMonitorWithTimeout(am, 3000);
        assertNotNull(setting);
        setActivity(setting);

        return setting;
    }

    private void gobackToMain(Solo solo) {
        while(false == CMMBUITestUtils.isExpectedActivity(mMainActivity)) {
            CMMBUITestUtils.expectNoDialog(solo, mMainActivity);
            solo.goBack();
            CMMBUITestUtils.sleepThread(1000);
        }
    }
}
