package com.mediatek.cmmb.app.tests;

import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.os.Message;
import android.test.ServiceTestCase;
import android.test.suitebuilder.annotation.Suppress;
import android.util.Log;

import com.mediatek.mbbms.MBBMSStore;
import com.mediatek.mbbms.ServerStatus;
import com.mediatek.mbbms.protocol.ContentReference;
import com.mediatek.mbbms.protocol.Extension;
import com.mediatek.mbbms.protocol.IMPIResponse;
import com.mediatek.mbbms.protocol.LTKM;
import com.mediatek.mbbms.protocol.LTKMResponseMSKExtension;
import com.mediatek.mbbms.protocol.MSK;
import com.mediatek.mbbms.protocol.MSKLifeTime;
import com.mediatek.mbbms.protocol.MonetaryPrice;
import com.mediatek.mbbms.protocol.PackageItem;
import com.mediatek.mbbms.protocol.PriceInfo;
import com.mediatek.mbbms.protocol.ProtocolUtil;
import com.mediatek.mbbms.protocol.PurchaseDataFragment;
import com.mediatek.mbbms.protocol.PurchaseDataReference;
import com.mediatek.mbbms.protocol.PurchaseItem;
import com.mediatek.mbbms.protocol.PurchaseItemFragment;
import com.mediatek.mbbms.protocol.RegReqExtType;
import com.mediatek.mbbms.protocol.RegisterResponse;
import com.mediatek.mbbms.protocol.RegistrationRequestExtension;
import com.mediatek.mbbms.protocol.RegistrationResponseExtension;
import com.mediatek.mbbms.protocol.RetrieveSKResponse;
import com.mediatek.mbbms.protocol.ServiceReference;
import com.mediatek.mbbms.protocol.SubscriptionUpdateExtension;
import com.mediatek.mbbms.protocol.UserType;
import com.mediatek.mbbms.service.CMMBServiceClient;
import com.mediatek.mbbms.service.CMMBServiceClient.MBBMSMSKID;
import com.mediatek.mbbms.service.MBBMSService;
import com.mediatek.mbbms.service.SettingsUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MBBMSServiceTestCase extends ServiceTestCase<MBBMSService> {
    private static final String TAG = "MBBMSServiceTestCase";
    
    public MBBMSServiceTestCase() {    
        super(MBBMSService.class);
    }
    
    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setUp>>>");
        super.setUp();
        Log.d(TAG, "setUp<<<");
    }
    
    @Override
    protected void tearDown() throws Exception {
        Log.d(TAG, "tearDown>>>");
        super.tearDown();
        Log.d(TAG, "tearDown<<<");
    }
    
    
    @Suppress
    public void test01Bindable() {
        Log.d(TAG, "test01_bindable>>>");
        MBBMSService service = getMBBMSService();
        assertNotNull(service);
        service.stopTranscationThread();
        Log.d(TAG, "test01_bindable<<<");
    }
    
    @Suppress
    public void test02DoSyncSG() {
        Log.d(TAG, "test02_doSyncSG>>>");  
        MBBMSService service = getMBBMSService();
        ServerStatus ret = service.syncServiceGuide(null);
        Log.d(TAG, "doSyncSG: ret code" + ret.code);
        
        try {
            Thread.sleep(120000);
        } catch (InterruptedException e) {
            e.printStackTrace();    
        }
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        service.stopTranscationThread();
        Log.d(TAG, "test02_doSyncSG<<<");
    }
    
    @Suppress
    public void test03DoAccountInqueryBeforeGBA() {
        Log.d(TAG, "test03_doAccountInqueryBeforeGBA>>>");
        MBBMSService service = getMBBMSService();
        ServerStatus ret = service.processAccountInquiry();
        Log.d(TAG, "Account Inquiry: ret code" + ret.code);
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        service.stopTranscationThread();
        Log.d(TAG, "test03_doAccountInqueryBeforeGBA<<<");
    }
    
    @Suppress
    public void test04DoSubscribeBeforeGBA() {
        Log.d(TAG, "test04_doSubscribeBeforeGBA>>>");
        subscribe();       
        Log.d(TAG, "test04_doSubscribeBeforeGBA<<<");
    }
    
    @Suppress    
    public void test05DoGBA() throws InterruptedException {
        Log.d(TAG, "test05_doGBA>>>"); 
        MBBMSService service = getMBBMSService();
        ServerStatus ret = service.syncServiceGuide(null);
        Log.d(TAG, "doSyncSG: ret code" + ret.code);
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        ret = service.processGBAInitialization();
        Log.d(TAG, "GBA: ret code" + ret.code);
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        service.stopTranscationThread();
        Log.d(TAG, "test05_doGBA<<<");
    }
    
    @Suppress
    public void test06DoAccountInquery() {
        Log.d(TAG, "test06_doAccountInquery>>>");
        MBBMSService service = getMBBMSService();
        ServerStatus ret = service.processAccountInquiry();
        Log.d(TAG, "Account Inquiry: ret code" + ret.code);
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        service.stopTranscationThread();
        Log.d(TAG, "test06_doAccountInquery<<<");
    }
    
    @Suppress
    public void test07DoSubscribe() {
        Log.d(TAG, "test05_doSubscribe>>>");
        subscribe();       
        Log.d(TAG, "test07_doSubscribe<<<");
    }
    
    @Suppress
    public void test08DoSubscriptionUpdate() {
        Log.d(TAG, "test08_doSubscriptionUpdate>>>");
        MBBMSService service = getMBBMSService();
        ServerStatus ret = service.processSubscriptionUpdate();
        Log.d(TAG, "processSubscriptionUpdate: ret code" + ret.code);
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        service.stopTranscationThread();
        Log.d(TAG, "test08_doSubscriptionUpdate<<<");
    }
    
    @Suppress
    public void test09DoMSKRetrieve() throws InterruptedException {
        Log.d(TAG, "test09_doMSKRetrieve>>>");
        MBBMSService service = getMBBMSService();
        ServerStatus ret = service.syncServiceGuide(null);
        Log.d(TAG, "doSyncSG: ret code" + ret.code);
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ret = service.processSubscriptionUpdate();
        Log.d(TAG, "processSubscriptionUpdate: ret code" + ret.code);
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        ret = service.processSKRetrieve();
        Log.d(TAG, "MSK: ret code" + ret.code);
        if (!ret.code.equals(MBBMSService.STATUS_SUCCEED)) {
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();    
            }
                
            ret = service.processSKRetrieve();
            Log.d(TAG, "MSK: ret code" + ret.code);
        }        
        service.stopTranscationThread();
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        Log.d(TAG, "test09_doMSKRetrieve<<<");
    }


    @Suppress
    public void test10DoSyncESG() {
    Log.d(TAG, "test10_doSyncESG>>>");
    MBBMSService service = getMBBMSService();
    service.onModeSwitch(0, false);
        ServerStatus ret = service.syncEServiceGuide();
        Log.d(TAG, "doSyncSG: ret code" + ret.code);
        
        try {
            Thread.sleep(120000);
        } catch (InterruptedException e) {
            e.printStackTrace();    
        }
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        service.stopTranscationThread();
    Log.d(TAG, "test10_doSyncESG<<<");
    }
    
    public void test11FragmentClassTest() {
        Log.d(TAG, "test11_FragmentClassTest>>>");
        contentReferenceTest();
        ltkmTest();
        mskltTest();
        packageItemTest();
        registerResponseTest();
        registrationRequestExtensionTest();
        registrationResponseExtensionTest();
        subscriptionUpdateExtensionTest(); 
        ltkmResponseMSKExtensionTest();
        mskTest();
        retrieveSKResponseTest();
        Log.d(TAG, "test11_FragmentClassTest<<<");    
    }

    public void test12FragmentClassTest() {
        Log.d(TAG, "test12_FragmentClassTest>>>");
        purchaseDataReferenceTest();
        impiResponseTest();
        extensionTest();
        priceInfoTest();
        monetaryPriceTest();
        purchaseDataFragmentTest();
        purchaseItemFragmentTest();
        purchaseItemTest();
        impiResponceTest();
        Log.d(TAG, "test12_FragmentClassTest<<<");    
    }
    
    
    public void test13PrivateTest() throws NoSuchMethodException, NoSuchFieldException, InstantiationException,
    IllegalAccessException, InvocationTargetException,FileNotFoundException, IOException {
        Log.d(TAG, "test01_bindable>>>");
        MBBMSService service = getMBBMSService();
        assertNotNull(service);
        ServerStatus ret = service.syncServiceGuide(null);
        Log.d(TAG, "doSyncSG: ret code" + ret.code);
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FlectUtil.invoke(service, "doGBA", null);

        transactionHandlerTest(service);
        
        new SettingsUtils().restore(getSystemContext().getContentResolver());
        new SettingsUtils().getTerminalCapability(); 
            
        FlectUtil.invoke(service, "clearUserInfo", null);
        CMMBServiceClient client = (CMMBServiceClient) FlectUtil.getFiled(service, "mClient");
        clientTest(client);
        service.clientError(client, 300);
        Object[] args1 = new Object[5];
        args1[0] = FlectUtil.getFiled(service, "mPurchaseServer");    
        args1[1] = "8";
        args1[2] = FlectUtil.getFiled(service, "mIMSI");
        args1[3] = FlectUtil.getFiled(service, "mCmmbSn");
        
        String purchaseItemGlobalId = null;
        String purchaseDataGlobalId = null;
        Cursor cursor1 = getSystemContext().getContentResolver().query(MBBMSStore.SG.PurchaseItemContentDetail.CONTENT_URI,
                    new String[]{MBBMSStore.SG.PurchaseItemContentDetail.GLOBAL_CONTENT_ID,
                    MBBMSStore.SG.PurchaseItemContentDetail.PURCHASE_DATA_ID},
                    null, null, null);
        if (cursor1 != null) {
            while (cursor1.moveToNext()) {
            purchaseItemGlobalId = cursor1.getString(0);
            purchaseDataGlobalId = cursor1.getString(1);
            cursor1.close();
          }
        }
        
        List<PurchaseItem> items = new ArrayList<PurchaseItem>();
        PurchaseItem item = new PurchaseItem();
        item.setGlobalRef(purchaseItemGlobalId);
        item.setPurchaseDataReference(purchaseDataGlobalId, null);
        items.add(item);
        args1[4] = items;
        Class[] argsType1 = new Class[]{String.class, String.class, String.class, String.class, List.class};
        FlectUtil.invoke(service, "doServiceRequest", argsType1, args1);
        FlectUtil.invoke(service, "doUnsubscribeRequest", argsType1, args1);
        
        Cursor cursor2 = getSystemContext().getContentResolver().query(MBBMSStore.ESG.ServiceDetail.CONTENT_URI
                ,new String []{MBBMSStore.ESG.ServiceDetail.SERVICE_ID}
                ,null, null, null);
        String serviceId = null;
        if (cursor2 != null) {
            while (cursor2.moveToNext()) {
                serviceId = cursor2.getString(0);
            if (serviceId != null) {
                break;
                }
            }
            cursor2.close();
        }
        Log.d(TAG, "service ID + " + serviceId);
        
        Object[] args2 = new Object[1];
        args2[0] = serviceId;
        FlectUtil.invoke(service, "findProtectionKey", args2);
        FlectUtil.invoke(service, "getAccessId", args2);
        
        FlectUtil.invoke(service, "isKeyExisted", args2);
        
        FlectUtil.invoke(service, "testAccountInfo", null);
        
        FlectUtil.invoke(service, "cmmbTune", new Class[]{int.class, boolean.class}, new Object[]{53000000, false});
        FlectUtil.invoke(service, "handleUAMError", new Class[]{int.class}, new Object[]{0x6985});
        FlectUtil.invoke(service, "handleUAMError", new Class[]{int.class}, new Object[]{0});
        
        service.clientMSK(new MBBMSMSKID());
        service.getProp(0);
        
        ProtocolUtil pu = (ProtocolUtil)FlectUtil.getFiled(service, "mHttpUTIL"); 
        String domain = (String)FlectUtil.getFiled(service, "mSGDomain"); 
        protocolTest(pu, domain);

        new ServiceReference().getIdRef();
        new PackageItem("String", "String", "String", 1.0f, 1, 1, true).isIsLocal();
        new PackageItem().isIsLocal();
        FlectUtil.invoke(service, "processSKRetrieve", new Object[]{"1"});
        FlectUtil.invoke(service, "doStopWaitESG", null);

        FlectUtil.invoke(service, "decode3GResult", 
            new Object[]{"/0LWtjqJtOLUmQnmvo/Dag6BQdNDjQSwlvBCJaemasDAQMGCQMCCAIFAgkHAAAB"});

        ArrayList<String> succeedKeys = new ArrayList<String>();
        succeedKeys.add("AAABBB");
        succeedKeys.add("CCCDDD");
        FlectUtil.invoke(service, "checkSKRetrieveFail", new Class[]{String.class, 
            List.class}, new Object[]{"1", succeedKeys});
        
        service.openChannel(1, 5300000);
        service.stopOpenChannel();
        
        FlectUtil.invoke(service, "doTerminate", null);

        FlectUtil.invoke(service, "UsimVector", new Class[]{MBBMSService.class}, new Object[]{service},
                 "toString", null,  null);
        
        service.stopTranscationThread();
        Log.d(TAG, "test01_bindable<<<");
    }
    
    private MBBMSService getMBBMSService() {
        Intent intent = new Intent(getSystemContext(), MBBMSService.class);
        intent.putExtra(MBBMSService.CONFIGURATION_RESOURCE_ID, com.mediatek.cmmb.app.R.raw.configurations);
        IBinder ibander = bindService(intent);
        if (ibander.isBinderAlive()) {
            Log.d(TAG, "binderAlive");
            return ((MBBMSService.LocalBinder)ibander).getService(); 
        }
        return null;
    }    
    
    private void subscribe() {
        MBBMSService service = getMBBMSService();
        ServerStatus ret = service.processAccountInquiry();
        Log.d(TAG, "Account Inquiry: ret code" + ret.code);
        assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
        Cursor cursor = getSystemContext().getContentResolver().query(MBBMSStore.SG.PurchaseItemContentDetail.CONTENT_URI,
                    new String[]{MBBMSStore.SG.PurchaseItemContentDetail.GLOBAL_CONTENT_ID,
                    MBBMSStore.SG.PurchaseItemContentDetail.PURCHASE_DATA_ID,
                    MBBMSStore.SG.PurchaseItemContentDetail.RESPONSE_CODE},
                    null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
            String purchaseItemGlobalId = cursor.getString(0);
            String purchaseDataGlobalId = cursor.getString(1);
            String responseCode = cursor.getString(2);
            Log.d(TAG, "query responseCode:" + responseCode);
            if (responseCode.equals("0")) {
                ret = service.subscribe(purchaseItemGlobalId, purchaseDataGlobalId);
                    Log.d(TAG, "subscribe ret:" + ret);
                assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
                    try {
                    Thread.sleep(20000);
                    } catch (InterruptedException e) {
                    e.printStackTrace();    
                    }
                    ret = service.unsubscribe(purchaseItemGlobalId, purchaseDataGlobalId);
                    Log.d(TAG, "unsubscribe ret:" + ret);
                    assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
            } else if (responseCode.equals("1")) {
                ret = service.unsubscribe(purchaseItemGlobalId, purchaseDataGlobalId);
                    Log.d(TAG, "unsubscribe ret:" + ret);
                assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
                try {
                    Thread.sleep(20000);
                    } catch (InterruptedException e) {
                    e.printStackTrace();    
                    }
                    ret = service.subscribe(purchaseItemGlobalId, purchaseDataGlobalId);
                    Log.d(TAG, "subscribe ret:" + ret);
                    assertEquals(ret.code, MBBMSService.STATUS_SUCCEED);
            }
            }
            cursor.close();
        }
        service.stopTranscationThread();        
    }

    private void contentReferenceTest() {
        ContentReference cr = new ContentReference();
        cr.setIdRef("testcr");
        String ref = cr.getIdRef();
        assertEquals(ref, "testcr");
    }

    private void ltkmTest() {
        LTKM ltkm = new LTKM();
        ltkm.setValidFrom("testvalidform");
        String validfrom = ltkm.getValidFrom();
        assertEquals(validfrom, "testvalidform");
        ltkm.setValidUntil("testvalidUntil");
        String validuntil = ltkm.getValidUntil();
        assertEquals(validuntil, "testvalidUntil");
        ltkm.setMskID("testMSKID");
        String mskid = ltkm.getMskID();
        assertEquals(mskid, "testMSKID");
        Log.d(TAG, "test LTKM:" + ltkm);
    }

    private void mskltTest() {
        MSKLifeTime msklt = new MSKLifeTime();
        msklt.setValidFrom("2012-10-01");
        String from = msklt.getValidFrom();
        assertEquals(from, "2012-10-01");
        msklt.setValidTo("2112-10-01");
        String to = msklt.getValidTo();
        assertEquals(to, "2112-10-01");
        Log.d(TAG, "test MSKLifeTime:" + msklt);
    }

    private void packageItemTest() {
        PackageItem pi = new PackageItem("globalID", "name", "des", 1.0f, 0, 100, false, null);
        pi.setGlobalId("globalID");
        String id = pi.getGlobalId();
        assertEquals(id, "globalID");
        pi.setName("name");
        String name = pi.getName();
        assertEquals(name, "name");
        pi.setDescription("des");
        String des = pi.getDescription();
        assertEquals(des, "des");
        pi.setPrice(1.0f);
        float price = pi.getPrice();
        assertEquals(price, 1.0f);
        pi.setSubscribeType(0);
        int type = pi.getSubscribeType();
        assertEquals(type, 0);
        pi.setPeriod(100);
        int period = pi.getPeriod();
        assertEquals(period, 100);
        pi.setIsLocal(true);
        pi.setChannels(null);
        List<String> channel = pi.getChannels();
        assertEquals(channel, null);
    }

    private void registerResponseTest() {
        RegisterResponse r = new RegisterResponse();
        r.setDescription("des");
        String des = r.getDescription();
        assertEquals(des, "des");
        r.setResponseCode("000");
        String code = r.getResponseCode();
        assertEquals(code, "000");
        r.setServiceID("001");
        String id = r.getServiceID();
        assertEquals(id, "001");
        r.setsUpdateExtensions(null);
        ArrayList updateextentions = r.getsUpdateExtensions();
        assertEquals(updateextentions, null);
        Log.d(TAG, "test RegisterResponse:" + r);
    }

    private void registrationRequestExtensionTest() {
        RegistrationRequestExtension r = new RegistrationRequestExtension();
        r.setVersion("1");
        String version = r.getVersion();
        assertEquals(version, "1");
        r.setTypelistArrayList(null);
        ArrayList<RegReqExtType> list = r.getTypelistArrayList();
        assertEquals(list, null);
    }

    private void registrationResponseExtensionTest() {
        RegistrationResponseExtension r = new RegistrationResponseExtension();
        r.setVersion("1");
        String version = r.getVersion();
        assertEquals(version, "1");
        r.setlTKMDeliveries(null);
        ArrayList<String> ltkm = r.getlTKMDeliveries();
        assertEquals(ltkm, null);
    }

    private void subscriptionUpdateExtensionTest() {
        SubscriptionUpdateExtension sue = new SubscriptionUpdateExtension();
        LTKM l1 = new LTKM(); 
        ArrayList<LTKM> ls = new ArrayList<LTKM>();
        ls.add(l1);
        sue.setLtkms(ls);
        ArrayList ltkms = sue.getLtkms();
        sue.setVersion("1");
        String version = sue.getVersion();
        assertEquals(version, "1"); 
        Log.d(TAG, "test SubscriptionUpdateExtension:" + sue);  
    }

    private void ltkmResponseMSKExtensionTest() {
        LTKMResponseMSKExtension lrme = new LTKMResponseMSKExtension();
        lrme.setVersion("2.2");
        String version = lrme.getVersion();
        assertEquals(version, "2.2");
        lrme.setlTKMList(null);
        assertEquals(lrme.getlTKMList(), null);
        Log.d(TAG, "test ltkmResponseMSKExtension:" + lrme); 
    }

    private void mskTest() {
        MSK msk = new MSK();
        msk.setKeyDomainID("did");
        assertEquals(msk.getKeyDomainID(), "did");
        msk.setMSKID("id");
        assertEquals(msk.getMSKID(), "id");
        msk.setLifeTime(null);
        assertEquals(msk.getLifeTime(), null);
        Log.d(TAG, "test MSK:" + msk);  
    }

    private void retrieveSKResponseTest() {
        RetrieveSKResponse r = new RetrieveSKResponse();
        r.setDescription("des");
        assertEquals(r.getDescription(), "des");
        r.setExtension(null);
        assertEquals(r.getExtension(), null);
        r.setMsk(null);
        assertEquals(r.getMsk(), null);
        r.setResponseCode("000");
        assertEquals(r.getResponseCode(), "000");
        Log.d(TAG, "test RetrieveSKResponse:" + r); 
    }

    private void purchaseDataReferenceTest() {
        PurchaseDataReference p = new PurchaseDataReference();
        p.setIdRefString("idref");
        p.getIdRefString();
        p.setPrice("1.0");
        p.getPrice();
        Log.d(TAG, "test purchaseDataReferenceTest:" + p);
    }

    private void impiResponseTest() {
        IMPIResponse i = new IMPIResponse();
        i.setUserIDType("type");
        assertEquals("type", i.getUserIDType());
        i.setDescription("des");
        assertEquals("des", i.getDescription());
        Log.d(TAG, "test IMPIResponseTest:" + i);
    }

    private void extensionTest() {
        Extension ex = new Extension();
        ex.setDescriptionList(null);
        assertEquals(null, ex.getDescriptionList());
        ex.setUrls(null);
        assertEquals(null, ex.getUrls());
    }

    private void priceInfoTest() {
        PriceInfo p = new PriceInfo();
        p.setPeriod("3000");
        assertEquals("3000", p.getPeriod());
        p.setSubscription_type("000");
        assertEquals("000", p.getSubscription_type());
    }

    private void monetaryPriceTest() {
        MonetaryPrice m = new MonetaryPrice();
        m.getCurrency();
        m.getPrice();
    }

    private void purchaseDataFragmentTest() {
        PurchaseDataFragment p = new PurchaseDataFragment();
        p.getDescriptionList();
        p.getExtensions();
        p.getId();
        p.getPreviewDataReferences();
        p.getVersion();
        p.setPrivateExt(null);
    }

    private void purchaseItemFragmentTest() {
        PurchaseItemFragment p = new PurchaseItemFragment();
        p.getContentReferences();
        p.getDescriptionList();
        p.getGlobalPurchaseItemId();
        p.getId();
        p.setPrivateExt(null);
        p.getPrivateExt();
        p.getVersion();
    }
    
    private void purchaseItemTest() {
            PurchaseItem pi = new PurchaseItem();
            pi.setGlobalRef("gref");
            pi.getGlobalRef();
            pi.setPurchaseDateReference(null);
            pi.setPurchaseDataReference("ref", "1.00");
            pi.setItemwiseStatusCode("000");
            pi.getPurchaseDateReference();
            pi.getItemwiseStatusCode();
            pi.setDescriptionStrings(null);
            pi.getDescriptionStrings();
            pi.setPurchaseItemFragment(null);
            pi.getPurchaseItemFragment();
            pi.setPurchaseData(null);
            pi.getPurchaseData();
            Log.d(TAG, "test purchaseItemTest:" + pi);
        }
        
        private void impiResponceTest() {
            IMPIResponse ir = new IMPIResponse();
            ir.clear();
            ir.setUserID("id");
            ir.getUserID();
            ir.setUserIDType("type");
            ir.getUserIDType();
            ir.setDescription("des");
            ir.getDescription();
            ir.setBSFAddress("bsf");
            ir.getBSFAddress();
            Log.d(TAG, "test impiResponceTest:" + ir);
        }
            
        private void protocolTest(ProtocolUtil pu, String domain) throws NoSuchMethodException, NoSuchFieldException,
        IllegalAccessException, InvocationTargetException, FileNotFoundException, IOException {
            pu.setProxy("sg.mbbms.chinamobile.com", 80);

            ArrayList<String> verMsg = new ArrayList<String>();
        verMsg.add("aaa");
        try {
                pu.doServiceKeyACK(new ProtocolUtil.IResponseHandler() {
            public void handleResponse(InputStream iStream) {}
                    public Object getParsedResult() {
                 return new Object();
            }
                    public int getStatusCode() {
            return 0;
            }
        }, domain, "useId",  UserType.USER_IMSI, verMsg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
                
        private void clientTest(CMMBServiceClient client) throws NoSuchMethodException, NoSuchFieldException, 
        InstantiationException, IllegalAccessException, InvocationTargetException {
             
                 client.isMSKValid("String", "String");
                client.AsciiToHex("3aF2");

                FlectUtil.invoke(client, "encodeBytesToHexString", new Object[]{new byte[]{25, 13,9}});
                 
             }

        private void transactionHandlerTest(MBBMSService service) throws NoSuchMethodException, NoSuchFieldException, 
            IllegalAccessException, InvocationTargetException, FileNotFoundException, InstantiationException {
            FlectUtil.invoke(service, "TransactionHandler", new Class[]{MBBMSService.class}, new Object[]{service},
                 "checkIsServiceMatch", new Class[]{Integer.class, Integer.class},  new Object[]{5300000,13});

            Integer num1 = new Integer(10);
        Integer num2 = new Integer(20);
            ArrayList<Integer> list = new ArrayList<Integer>();
            list.add(num1);
            list.add(num2);
            HashMap<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();
            map.put(num1, list);
            FlectUtil.invoke(service, "TransactionHandler", new Class[]{MBBMSService.class}, new Object[]{service},
                    "dumpMap", new Class[]{HashMap.class,boolean.class},  new Object[]{map, true});

            FlectUtil.invoke(service, "TransactionHandler", new Class[]{MBBMSService.class}, new Object[]{service},
                    "getInputStream", new Class[]{int.class},  new Object[]{5300000});

            ArrayList<Integer> list2 = new ArrayList<Integer>();
            list2.add(new Integer(20));
            list2.add(new Integer(28));
            list2.add(new Integer(43));
            ArrayList<Integer> list21 = new ArrayList<Integer>();
            list2.add(new Integer(20));
            list2.add(new Integer(28));
            list2.add(new Integer(43));
            ArrayList<Integer> list22 = new ArrayList<Integer>();
            list2.add(new Integer(20));
            list2.add(new Integer(28));
            list2.add(new Integer(43));
            HashMap<Integer, List<Integer>> map2 = new HashMap<Integer, List<Integer>>();
            map2.put(new Integer(28), list21);
            map2.put(new Integer(43), list22);
            FlectUtil.invoke(service, "TransactionHandler", new Class[]{MBBMSService.class}, new Object[]{service},
                "getServiceFrequencyNo", new Class[]{List.class, HashMap.class},  new Object[]{list2, map2});

            ArrayList<Integer> list3 = new ArrayList<Integer>();
            list2.add(new Integer(21));
            list2.add(new Integer(29));
            list2.add(new Integer(41));
            FlectUtil.invoke(service, "TransactionHandler", new Class[]{MBBMSService.class}, new Object[]{service},
            "updateService", new Class[]{int.class, ArrayList.class},  new Object[]{29, list3});

        Message msg = new Message();
        msg.what = 0;
        FlectUtil.invoke(service, "TransactionHandler", new Class[]{MBBMSService.class}, new Object[]{service},
        "handleMessage", new Class[]{Message.class},  new Object[]{msg});
        }

}
