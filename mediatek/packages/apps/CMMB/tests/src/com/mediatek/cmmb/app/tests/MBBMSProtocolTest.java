package com.mediatek.cmmb.app.tests;

import android.test.InstrumentationTestCase;
import android.util.Log;
import android.util.Xml;

import com.mediatek.mbbms.protocol.AccountResponse;
import com.mediatek.mbbms.protocol.LTKM;
import com.mediatek.mbbms.protocol.LTKMResponseMSKExtension;
import com.mediatek.mbbms.protocol.MSK;
import com.mediatek.mbbms.protocol.MSKLifeTime;
import com.mediatek.mbbms.protocol.PurchaseData;
import com.mediatek.mbbms.protocol.RegisterResponse;
import com.mediatek.mbbms.protocol.RequestPayloadTemplateUtil;
import com.mediatek.mbbms.protocol.ServiceResponse;
import com.mediatek.mbbms.protocol.SubscriptionUpdateExtension;
import com.mediatek.mbbms.protocol.UnsubscribeResponse;

import org.xmlpull.v1.XmlSerializer;

import java.io.StringWriter;
import java.util.ArrayList;

public class MBBMSProtocolTest extends InstrumentationTestCase {
    public static final String TAG = "MBBMSProtocolTest";
    
    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setUp>>>");
        super.setUp();
        Log.d(TAG, "setUp<<<");
    }
    
    @Override
    protected void tearDown() throws Exception {
        Log.d(TAG, "tearDown>>>");
        super.tearDown();
        Log.d(TAG, "tearDown<<<");
    }

    public void test01LTKMResponseMSKExtension() {
        Log.d(TAG, "test01LTKMResponseMSKExtension>>>");
        LTKMResponseMSKExtension instance = new LTKMResponseMSKExtension();
        instance.setVersion("version");
        assertEquals("version", instance.getVersion());
        
        String str = "ltkm1";
        ArrayList<String> list = new ArrayList<String>();
        list.add(str);
        instance.setlTKMList(list);
        assertEquals(list, instance.getlTKMList());
        
        Log.d(TAG, "test01LTKMResponseMSKExtension:" + instance);
        Log.d(TAG, "test01LTKMResponseMSKExtension<<<");
    }

    public void test02ServiceResponse() {
        Log.d(TAG, "test02ServiceResponse()>>>");
        
        ServiceResponse instance = new ServiceResponse();        
        instance.clear();
        
        instance.setRequestID("ID");        
        assertEquals("ID", instance.getRequestID());
        
        instance.setGlobalStatusCode("SC");
        assertEquals("SC", instance.getGlobalStatusCode());
        
        instance.setDescription("dec");
        assertEquals("dec", instance.getDescription());
        
        instance.setPurchaseItems(null);
        assertEquals(null, instance.getPurchaseItems());
        
        instance.setlTKMlList(null);
        assertEquals(null, instance.getlTKMlList());
        
        Log.d(TAG, "test02ServiceResponse()<<<");
    }

    public void test03AccountResponse() {
        Log.d(TAG, "test03AccountResponse>>>");
        
        AccountResponse instance = new AccountResponse();
        
        instance.setRequestID("ID");
        assertEquals("ID", instance.getRequestID());
        
        instance.setGlobalStatusCode("CODE");
        assertEquals("CODE", instance.getGlobalStatusCode());
        
        instance.setDescription("des");
        assertEquals("des", instance.getDescription());
        
        instance.setBillingInfoList(null);
        assertEquals(null, instance.getBillingInfoList());
        
        instance.setPurchaseItems(null);
        assertEquals(null, instance.getPurchaseItems());
        
        Log.d(TAG, "test03AccountResponse<<<");
    }

    public void test04RegisterResponse() {
        Log.d(TAG, "test04RegisterResponse>>>");
        RegisterResponse instance = new RegisterResponse();

        instance.setDescription("dec");
        assertEquals("dec", instance.getDescription());

        instance.setServiceID("ID");
        assertEquals("ID", instance.getServiceID());

        instance.setResponseCode("000");
        assertEquals("000", instance.getResponseCode());

        SubscriptionUpdateExtension sub = new SubscriptionUpdateExtension();
        sub.setVersion("1");
        assertEquals("1", sub.getVersion());

        LTKM ltkm = new LTKM();
        ltkm.setValidFrom("form");
        assertEquals("form", ltkm.getValidFrom());

        ltkm.setValidUntil("util");
        assertEquals("util", ltkm.getValidUntil());

        ltkm.setMskID("ID");
        assertEquals("ID", ltkm.getMskID());

        ArrayList<LTKM> ltkmList = new ArrayList<LTKM>();
        ltkmList.add(ltkm);
        sub.setLtkms(ltkmList);
        assertEquals(ltkmList, sub.getLtkms());

        ArrayList<SubscriptionUpdateExtension> subList = new ArrayList<SubscriptionUpdateExtension>();
        subList.add(sub);
        instance.setsUpdateExtensions(subList);
        assertEquals(subList, instance.getsUpdateExtensions());
        Log.d(TAG, "test04RegisterResponse:" + instance);
        Log.d(TAG, "test04RegisterResponse<<<");
    }

    public void test05RequestPayloadTemplateUtil() throws Exception {
        Log.d(TAG, "test05RequestPayloadTemplateUtil>>>");
        new RequestPayloadTemplateUtil();
        StringWriter writer = new StringWriter();
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(writer);
        
        RequestPayloadTemplateUtil.addTerminalCapability(serializer, "terminal");
        RequestPayloadTemplateUtil.addUserAgent(serializer, "userAgent");
        
        MSK msk = new MSK();
        msk.setKeyDomainID("keyDomainID");
        assertEquals("keyDomainID", msk.getKeyDomainID());

        msk.setMSKID("mSKID");
        assertEquals("mSKID", msk.getMSKID());

        MSKLifeTime lifetime = new MSKLifeTime();
        msk.setLifeTime(lifetime);
        assertEquals(lifetime, msk.getLifeTime());
        Log.d(TAG, "MSK:" + msk);    
        
        ArrayList<MSK> mskList = new ArrayList<MSK>();
        mskList.add(msk);
        RequestPayloadTemplateUtil.addSKRetrievePayLoad(serializer, mskList, 
            "imsi", "cmmbsn", "userAgent", "terminalCapability");
        Log.d(TAG, "test05RequestPayloadTemplateUtil<<<");
    }

    public void test06UnsubscribeResponse() {
        Log.d(TAG, "test06UnsubscribeResponse>>>");
        UnsubscribeResponse instance = new UnsubscribeResponse();
        instance.clear();
        instance.setRequestID("requestID");
        assertEquals("requestID", instance.getRequestID());

        instance.setGlobalStatusCode("globalStatusCode");
        assertEquals("globalStatusCode", instance.getGlobalStatusCode());

        instance.setDescription("description");
        assertEquals("description", instance.getDescription());

        instance.setPurchaseItems(null);
        assertEquals(null, instance.getPurchaseItems());

        instance.setlTKMlList(null);
        assertEquals(null, instance.getlTKMlList());
        Log.d(TAG, "test06UnsubscribeResponse<<<");
    }

    public void test07PurchaseData() {
        Log.d(TAG, "test07PurchaseData>>>");
        PurchaseData instance = new PurchaseData();
        instance.setIdRef("idRef");
        assertEquals("idRef", instance.getIdRef());

        instance.setPurchaseDataFragment(null);
        assertEquals(null, instance.getPurchaseDataFragment());
        Log.d(TAG, "test07PurchaseData<<<");
    }
}
