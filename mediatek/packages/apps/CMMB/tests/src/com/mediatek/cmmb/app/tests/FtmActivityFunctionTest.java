package com.mediatek.cmmb.app.tests;

import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.FtmActivity;

public class FtmActivityFunctionTest extends ActivityInstrumentationTestCase2<FtmActivity> {
    private static final String TAG = "CMMB::FtmActivityFuntionTest";
    
    public static final String TESTKEY = "MODE";
    public static final String FREQKEY = "FREQ";
    public static final int INTERNALTESTVALUE = 0;
    public static final int EXTERNALTESTVALUE = 1;    
    private Instrumentation mInstrumentation;
    private FtmActivity mActivity;
   // private CmmbActivity mCmmbActivity;
    private Solo mSolo;
    
    public FtmActivityFunctionTest() {
        super("com.mediatek.cmmb.app", FtmActivity.class);
    }    
    
    public void setUp()  throws Exception {
        Log.d(TAG,"setUp>>");
        super.setUp();
        mInstrumentation = getInstrumentation();
        mInstrumentation.setInTouchMode(false);         
        setActivityInitialTouchMode(false);    
        Log.d(TAG, "setUp <<");
    }
    
    public void tearDown()  throws Exception {
        Log.d(TAG,"tearDown >>");
        if (mActivity != null) {
            mActivity.finish();
            mInstrumentation.waitForIdleSync();
                     
        }
        super.tearDown();
        Log.d(TAG,"tearDown <<");
    }
     
    public void testCase01External() {
        Log.d(TAG, "testCase01External >>");  
        ActivityMonitor playerMonitor = new ActivityMonitor(
                "com.mediatek.cmmb.app.FtmActivity", null, false);
        mInstrumentation.addMonitor(playerMonitor);
        Intent intentE = new Intent();
        intentE.setComponent(new ComponentName(
                "com.mediatek.cmmb.app",
                "com.mediatek.cmmb.app.FtmActivity"));
        intentE.putExtra(TESTKEY, EXTERNALTESTVALUE);

        launchActivityWithIntent(
                mInstrumentation.getTargetContext().getPackageName(), FtmActivity.class, intentE);

        mInstrumentation.waitForIdleSync();
        sleep(2000);
        
        mActivity = (FtmActivity)playerMonitor.waitForActivityWithTimeout(5000);
        if (mActivity == null) {
           Log.d(TAG, "activity failed");
         }

        setActivity(mActivity);
        checkLoading();
        sleep(2000);  
        
        //select one channel to play
        playfromList();
        
        Log.d(TAG, "testCase01External <<");        
    }
    
    
    public void testCase02internal() {
        Log.d(TAG, "testCase02internal >>");  
        ActivityMonitor playerMonitor = new ActivityMonitor(
                "com.mediatek.cmmb.app.FtmActivity", null, false);
        mInstrumentation.addMonitor(playerMonitor);
        Intent intentI = new Intent();
        intentI.setComponent(new ComponentName(
                "com.mediatek.cmmb.app",
                "com.mediatek.cmmb.app.FtmActivity"));
        intentI.putExtra(TESTKEY, INTERNALTESTVALUE);        
        intentI.putExtra(FREQKEY, "530");
 
        launchActivityWithIntent(
                mInstrumentation.getTargetContext().getPackageName(), FtmActivity.class, intentI);

        mInstrumentation.waitForIdleSync();
        sleep(2000);
        
        mActivity = (FtmActivity)playerMonitor.waitForActivityWithTimeout(5000);
        if (mActivity == null) {
           Log.d(TAG, "activity failed");
         }

        setActivity(mActivity);
        sleep(5000);        
                
        Log.d(TAG, "testCase02internal <<");  
    }

    private void playfromList() {
        Log.d(TAG, "playfromList >>");

        ListView listView = (ListView) mActivity.findViewById(com.mediatek.cmmb.app.R.id.channel_list_in_player);
        if ((listView == null) || (listView.getCount() <= 0)) {
            Log.d(TAG, "list view is null or empty ");
            return ;
        }
        mSolo = new Solo(getInstrumentation(), getActivity());
        ListAdapter listAdapter = listView.getAdapter();
        Log.d(TAG, "fist visible view item :" + listView.getFirstVisiblePosition());
        int position = listView.getFirstVisiblePosition();
/*
        try { 
        Field channellist = FtmActivity.class.getDeclaredField("mChannelList");
        channellist.setAccessible(true);
        ArrayList<Channel> chlist = (ArrayList)channellist.get(mActivity);
        } catch (Exception e) {
            Log.d(TAG, "get channel list error");
        }
        int i;
            
        for (i = position; i < listView.getCount(); i++) {
            ChannelListManager.Channel ch = chlist.get(i);            
            if (ch.isFree()) {               
                break;
            }            
            mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
        }
*/
        View view = listAdapter.getView(position, null, listView);
        TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.player_channel_name);
        String channelName = textView.getText().toString();
        Log.d(TAG, "playfromlistname:" + channelName);
        try {  
        Thread.sleep(2000);
        mSolo.clickOnText(channelName);
        Thread.sleep(5000);
        } catch (Exception e) {
            Log.d(TAG, "Exception e:" + e.getMessage());
        }
        
        Log.d(TAG, "playfromList <<");
    }
    
    
    private void sleep(int time) {
        try {
        Thread.sleep(time);
        } catch (InterruptedException e) {
            Log.d(TAG, "Exception sleep");
        }
    }
    
    private void checkLoading() {
        boolean waiting = true;
        Context contxt = mActivity.getApplicationContext();
        do {
            View progressView = (View)mActivity.findViewById(com.mediatek.cmmb.app.R.id.progress_indicator);
        TextView view = (TextView)mActivity.findViewById(com.mediatek.cmmb.app.R.id.notification);
        String updating = (String)contxt.getString(com.mediatek.cmmb.app.R.string._updating);
        String loading = (String)contxt.getString(com.mediatek.cmmb.app.R.string.loading);
        String textviewString = view.getText().toString();
        Log.d(TAG, "loading visible: " + view.getVisibility());
        if (progressView.getVisibility() == View.VISIBLE && (
                textviewString.equals(updating) || 
                textviewString.equals(loading))) {
            waiting = true;
        } else {
            waiting = false;
        }
        
        Log.d(TAG, "checkLoading waiting again waiting:" + waiting);
        sleep(2000);        
        } while(waiting);
    }
}
