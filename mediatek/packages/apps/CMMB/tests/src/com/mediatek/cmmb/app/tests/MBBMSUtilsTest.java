package com.mediatek.cmmb.app.tests;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.test.InstrumentationTestCase;
import android.util.Log;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.EBManager;
import com.mediatek.cmmb.app.Utils;
import com.mediatek.cmmb.app.Utils.SavedLocation;
import com.mediatek.mbbms.ServerStatus;
import com.mediatek.mbbms.Utility;
import com.mediatek.mbbms.service.CMMBServiceClient.CMMBEmergencyMessage;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class MBBMSUtilsTest extends InstrumentationTestCase {
    private static final String TAG = "MBBMSUtilsTest";
    
    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, getName() + " start!");
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        Log.d(TAG, getName() + " end!");
    }

    public void test01Utility() {
        @SuppressWarnings("unused")
        Utility ut = new Utility();     // test constructor
        ContentValues cv = new ContentValues();
        String key = "Key";
        String value = "Value";
        String value2 = "Another-value";

        Utility.putExtension(cv, key, value);
        assertEquals(value + ";", cv.getAsString(key));
        Utility.putExtension(null, key, value);
        Utility.putExtension(cv, null, value);
        assertEquals(value + ";", cv.getAsString(key));
        Utility.putExtension(cv, key, null);
        assertEquals(value + ";|;", cv.getAsString(key));
        Utility.putExtension(cv, key, value2);
        assertEquals(value + ";|;|" + value2 + ";", cv.getAsString(key));
        cv.clear();
        
        Utility.putExtensionValue(cv, key, value);
        assertEquals(value, cv.getAsString(key));
        Utility.putExtensionValue(cv, key, null);
        assertEquals(value + ",", cv.getAsString(key));
        Utility.putExtensionValue(cv, key, value2 + ";");
        assertEquals(value + ",," + value2 + ";", cv.getAsString(key));
        Utility.putExtensionValue(cv, key, value2);     
        assertEquals(value + ",," + value2 + ";" + value2, cv.getAsString(key));
        cv.clear();
        
        Utility.putLanguage(cv, key, value);
        assertEquals(value + ",", cv.getAsString(key));
        Utility.putLanguage(null, key, value);
        Utility.putLanguage(cv, null, value);
        assertEquals(value + ",", cv.getAsString(key));
        Utility.putLanguage(cv, key, null);
        assertEquals(value + ",;zho,", cv.getAsString(key));
        Utility.putLanguage(cv, key, value2);
        assertEquals(value + ",;zho,;" + value2 + ",", cv.getAsString(key));
        cv.clear();

        Utility.putMultiLanguageValue(cv, key, value);
        assertEquals(value, cv.getAsString(key));
        Utility.putMultiLanguageValue(null, key, value);
        Utility.putMultiLanguageValue(cv, null, value);
        assertEquals(value, cv.getAsString(key));
        Utility.putMultiLanguageValue(cv, key, null);
        assertEquals("", cv.getAsString(key));
        Utility.putMultiLanguageValue(cv, key, value2);
        assertEquals(value2, cv.getAsString(key));
        cv.clear();

        Utility.putMultiValue(cv, key, value);
        assertEquals(value, cv.getAsString(key));
        Utility.putMultiValue(null, key, value);
        Utility.putMultiValue(cv, null, value);
        assertEquals(value, cv.getAsString(key));
        Utility.putMultiValue(cv, key, null);
        assertEquals(value, cv.getAsString(key));
        Utility.putMultiValue(cv, key, value2);
        assertEquals(value + "," + value2, cv.getAsString(key));
        cv.clear();
        
        Utility.putTime(cv, key, value);
        Utility.putTime(null, key, value);
        Utility.putTime(cv, null, value);
        Utility.putTime(cv, key, null);
        assertEquals(null, cv.getAsLong(key));
        cv.clear();
        
        // "YYYY-MM-DD HH:mm:ss"
        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT+08:00")); 
        Utility.putTime(cv, key, "2012-10-09 14:49:00");
        Utility.putTime(null, key, "2012-10-09 14:49:00");
        Utility.putTime(cv, null, "2012-10-09 14:49:00");
        cal.setTimeInMillis(cv.getAsLong(key));
        assertEquals(2012, cal.get(Calendar.YEAR));
        assertEquals(Calendar.OCTOBER, cal.get(Calendar.MONTH));
        assertEquals(9, cal.get(Calendar.DATE));
        assertEquals(2, cal.get(Calendar.HOUR));
        assertEquals(49, cal.get(Calendar.MINUTE));
        assertEquals(0, cal.get(Calendar.SECOND));
        cv.clear();
    }

    public void test02Utils() {
        Context ctxTarget = getInstrumentation().getTargetContext();
        Resources resTarget = ctxTarget.getResources();
        
        Utils.getNow();        
        Utils.getSearchCursor();        
        Utils.isSuccess(null);
        Utils.getFormatTime(ctxTarget, System.currentTimeMillis());
        Utils.showToast(ctxTarget, android.R.string.emptyPhoneNumber);
        assertEquals(true, Utils.checkLocalTime());
        assertEquals("zho", Utils.getCurrentLang());

        SavedLocation sl;
        
        String displayName = "ProvinceTest-CityTest-CountyTest";
        assertEquals(displayName, SavedLocation.getDisplayName("ProvinceTest-CityTest-CountyTest"));
        displayName = "SuggestedCity";
        assertEquals(displayName, SavedLocation.getDisplayName("|-SuggestedCity"));
        
        String strResult = "SavedLocation(province=ProvinceTest, city=CityTest, county=CountyTest)";
        sl = Utils.parseSavedLocation("ProvinceTest-CityTest-CountyTest");
        assertEquals(strResult, sl.toString());
        assertEquals(null, Utils.parseSavedLocation(null));
        assertEquals(null, Utils.parseSavedLocation("           "));
               
        assertEquals(com.mediatek.cmmb.app.R.drawable.ebm_level4, Utils.getEBLevelIcon(0));
        assertEquals(com.mediatek.cmmb.app.R.drawable.ebm_level1, Utils.getEBLevelIcon(1));
        assertEquals(com.mediatek.cmmb.app.R.drawable.ebm_level2, Utils.getEBLevelIcon(2));
        assertEquals(com.mediatek.cmmb.app.R.drawable.ebm_level3, Utils.getEBLevelIcon(3));
        assertEquals(com.mediatek.cmmb.app.R.drawable.ebm_level4, Utils.getEBLevelIcon(4));
        
        String ebmLevel = resTarget.getString(com.mediatek.cmmb.app.R.string.ebm_level);
        assertEquals(String.format(ebmLevel, 0), Utils.getEBLevelString(ctxTarget, 0));
        assertEquals(String.format(ebmLevel, 1), Utils.getEBLevelString(ctxTarget, 1));
        assertEquals(String.format(ebmLevel, 2), Utils.getEBLevelString(ctxTarget, 2));
        assertEquals(String.format(ebmLevel, 3), Utils.getEBLevelString(ctxTarget, 3));
        assertEquals(String.format(ebmLevel, 4), Utils.getEBLevelString(ctxTarget, 4));
        
        ServerStatus ss;
        assertEquals("Wrong", Utils.getErrorDescription(resTarget, null, "Wrong"));
        
        ss = new ServerStatus("2204", "This is temp error of 2204.");
        assertEquals("This is temp error of 2204.", Utils.getErrorDescription(resTarget, ss, null));
        
        ss = new ServerStatus("000");
        assertEquals("Success.", Utils.getErrorDescription(resTarget, ss, "Success."));

        ss = new ServerStatus("2204");
        assertNotNull(Utils.getErrorDescription(resTarget, ss, "Default."));
        
        ss = new ServerStatus("xxxxxx");
        assertNotNull("Default.", Utils.getErrorDescription(resTarget, ss, "Default."));
    }
    
    public void test03EBManagerProfile() throws IOException, InterruptedException,
            NoSuchMethodException, ClassNotFoundException, IllegalArgumentException,
            IllegalAccessException, InvocationTargetException {
        Method md = Class.forName(EBManager.class.getDeclaredClasses()[0].getName())
                .getDeclaredMethod("getTime", new Class[] {int.class, int.class});
        md.setAccessible(true);
        md.invoke(null, new Object[] {1000, 1000});
        md.setAccessible(false);
    }
}

/**
 * @author mtk80495 used to simulate the emergency broadcast
 */
class SimulateEbMsg {
    private int mLevel1MsgId = 0;
    private int mLevel2MsgId = 0;
    private int mLevel3MsgId = 0;
    private int mLevel4MsgId = 0;
    private Context mCnt;
    private Solo mSolo;

    public SimulateEbMsg(Context cnt, Solo solo) {
        mCnt = cnt;
        mSolo = solo;
    }

    public void add100Msgs() {
        // Get a non-duplicated next ID
        int nextIds[] = getNextMsgIds();
        int nextId = 0;
        for (int i = 0; i < 4; i++) {
            if (nextId < nextIds[i]) {
                nextId = nextIds[i];
            }
        }
        
        // Start to send EB-message.
        for (int i = nextId; i < nextId + 100; i++) {
            int level = i % 5 + 1;
            boolean nullContent = i % 4 == 0;
            addMsg(i, level, nullContent, null);
            if (level == 1 || level == 2) {
                mSolo.goBack();
                CMMBUITestUtils.sleepThread(200);
            }
        }
    }

    public void add5Msgs() {
        for (int i = 0; i < 5; i++) {
            int level = i % 5 + 1;
            boolean nullContent = i % 4 == 0;
            addMsg(i, level, nullContent, null);
            if (level == 1 || level == 2) {
                CMMBUITestUtils.sleepThread(500);
                mSolo.goBack();
                CMMBUITestUtils.sleepThread(200);
            }
        }
    }

    public void add4DuplicatedMsgs() {
        EBManager ebManager = getEBManager();
        addMsg(mLevel1MsgId, 1, false, ebManager);
        addMsg(mLevel2MsgId, 2, false, ebManager);
        addMsg(mLevel3MsgId, 3, false, ebManager);
        addMsg(mLevel4MsgId, 4, false, ebManager);
    }

    /**
     * @return Returns the new valid message IDs for next message. They will be
     *         listed in an array with 4 elements.
     */
    public int[] getNextMsgIds() {
        return new int[] {
                mLevel1MsgId + 1,
                mLevel2MsgId + 1,
                mLevel3MsgId + 1,
                mLevel4MsgId + 1
        };
    }

    /**
     * Simulate that an emergency broadcast is coming
     * 
     * @param id get from {@link #getNextMsgIds() getNextMsgIds}
     * @param level range from 1 to 4, out of range will not affect anything
     * @param nullContent message content will be null if set this argument to
     *            true
     * @param ebManager an instance of EBManager or null are acceptable
     */
    public void addMsg(int id, int level, boolean nullContent, EBManager ebManager) {
        CMMBEmergencyMessage msg = new CMMBEmergencyMessage();

        switch (level) {
            case 1:
                mLevel1MsgId = id;
                break;
            case 2:
                mLevel2MsgId = id;
                break;
            case 3:
                mLevel3MsgId = id;
                break;
            case 4:
                mLevel4MsgId = id;
                break;
            default:
                break;
        }

        if (nullContent) {
            msg.msg_content = null;
        } else {
            msg.msg_content = "id=" + String.valueOf(id) + ", level=" + String.valueOf(level);
        }
        msg.msg_date = 1;
        msg.msg_id = id;
        msg.msg_len = 255;
        msg.msg_level = level;
        msg.msg_time = 1;
        msg.msg_type = 1;
        msg.net_id = 1;
        msg.net_level = 1;

        if (ebManager == null) {
            ebManager = getEBManager();
        }

        if (ebManager != null) {
            ebManager.onEB(msg);
            CMMBUITestUtils.sleepThread(200);
        }
    }

    private EBManager getEBManager() {
        Constructor<?> con;
        EBManager ebManager = null;
        try {
            con = Class.forName(EBManager.class.getName()).getDeclaredConstructor(
                    new Class[] {
                    Context.class
                });
            con.setAccessible(true);
            ebManager = (EBManager) con.newInstance(mCnt);
            con.setAccessible(false);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return ebManager;
    }
}