/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.cmmb.app.tests;

import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.os.Handler;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.HelpActivity;
import com.mediatek.cmmb.app.MainScreen;
import com.mediatek.cmmb.app.R;

public class CMMBHelpTest extends ActivityInstrumentationTestCase2<HelpActivity> {
    private static final String TAG = "CMMBUITest-HelpActivity";
    
    private Instrumentation mInst = null;
    private MainScreen mMainActivity = null;
    private HelpActivity mHelp = null;
    private boolean mInMbbmsMode = false;
    private Solo mSolo = null;
    
    public CMMBHelpTest() {
        super(HelpActivity.class);
    }
    
    @Override
    protected void setUp() throws Exception {
        Log.v(TAG, "setUp <<<<<");
        super.setUp();
        
        mInst = getInstrumentation();
        assertNotNull(mInst);
        
        mSolo = new Solo(mInst);
        assertNotNull(mSolo);

        mInst.waitForIdleSync();
        mInst.setInTouchMode(false);
        mMainActivity = launchActivity("com.mediatek.cmmb.app", MainScreen.class, null);
        assertNotNull(mMainActivity);
        setActivity(mMainActivity);
        
        CMMBUITestUtils.sleepThread(2000);
        
        CMMBUITestUtils.checkSimModeAndSelect(mInst);
        CMMBUITestUtils.waitForUpdateDone(mMainActivity);
        
        Log.v(TAG, "setUp >>>>>");
    }

    @Override
    protected void tearDown() throws Exception {
        Log.v(TAG, "tearDown <<<<<");
        
        CMMBUITestUtils.sleepThread(2000);

        if (mMainActivity != null) {
            mMainActivity.finish();
            mMainActivity = null;
        }
        
        super.tearDown();
        CMMBUITestUtils.sleepThread(2000);
        Log.v(TAG, "tearDown >>>>>");
    }
    
    public void test01Help() {
        Log.v(TAG, "test01Help <<<<<");
        
        ActivityMonitor am = new ActivityMonitor(HelpActivity.class.getName(), null, false);
        mInst.addMonitor(am);

        // goto help Activity
        assertTrue(CMMBUITestUtils.isExpectedActivity(mMainActivity));
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnMenuItem(mMainActivity.getString(R.string.help));
        
        mHelp = (HelpActivity)mInst.waitForMonitorWithTimeout(am, 3000);
        assertNotNull(mHelp);
        
        TextView textView = (TextView)mHelp.findViewById(R.id.content);
        assertTrue(textView.getText().toString().equalsIgnoreCase(mHelp.getString(R.string.help_info)));
        CMMBUITestUtils.sleepThread(2000);

        // goBack to Main Screen
        assertTrue(CMMBUITestUtils.isExpectedActivity(mHelp));
        CMMBUITestUtils.expectNoDialog(mSolo, mHelp);
        mInst.waitForIdleSync();
        mSolo.goBack();
        
        Log.v(TAG, "test01Help >>>>>");
    }
}
