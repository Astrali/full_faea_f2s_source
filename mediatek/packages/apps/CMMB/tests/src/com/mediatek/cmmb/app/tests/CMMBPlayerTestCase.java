package com.mediatek.cmmb.app.tests;//notice: when copy this file to target build, change "test" to "tests"

import android.app.Activity;
import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.graphics.drawable.Drawable;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.tests.MBBMSUtilsTest;
import com.mediatek.cmmb.app.ChannelListManager;
import com.mediatek.cmmb.app.ChannelListManager.Channel;
import com.mediatek.cmmb.app.MainScreen;
import com.mediatek.cmmb.app.ModeSwitchManager;
import com.mediatek.cmmb.app.PlayerActivity;
import com.mediatek.cmmb.app.R;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CMMBPlayerTestCase extends
        ActivityInstrumentationTestCase2<PlayerActivity> {

    private static final String TAG = "CMMB::CMMBPlayerTestCase";
    private static final long UPDATING_TIME = 120000;
    private static final long MONITOR_TIMEOUT = 3000;
    private static final long MIN_INIT_TIME = 1000;
    private static final long INIT_TIME = 2000;
    private static final long CHECK_TIME = 500;
    private static final long CHECK_TIMEOUT = 5000;

    // PlayerAcivity's state
    private static final int STATE_STARTED = 3;// channel is playing

    // MainScreen's state
    private static final int STATE_IDLE = 0;
    private static final int STATE_UPDATED_SERVICE = 2;
    private static final int STATE_UPDATED_SERVICE_FAIL = 3;

    private int mMbbmsMode;

    private Instrumentation mInstrumentation = null;
    private Solo mPlayerSolo = null;
    private MainScreen mMainScreen = null;
    private PlayerActivity mPlayerActivity = null;

    private ModeSwitchManager mModeSwitchManager = null;
    private ChannelListManager mChannelListManager = null;

    private ListView mAllChannelListView = null;
    private View mControlPanel = null;
    private ImageView mPrevCtrl = null;
    private ImageView mPlayStopButton = null;
    private ImageView mNextCtrl = null;
    private ImageView mChannelListCtrl = null;
    private ImageView mSnapButton = null;
    private ImageView mFavoriteIndicator = null;

    public CMMBPlayerTestCase() {
        super(PlayerActivity.class);    
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Log.i(TAG, ">>>set up");

        mInstrumentation = getInstrumentation();
        // to make view being focused to be clicked.
        mInstrumentation.setInTouchMode(false);
        mPlayerActivity = getPlayerActivity();
        mPlayerSolo = new Solo(mInstrumentation, mPlayerActivity);

        Log.i(TAG, "<<<set up");
    }

    @Override
    protected void tearDown() throws Exception {
        Log.i(TAG, ">>>tearDown");

        if (mPlayerActivity != null) {
            mPlayerActivity.finish();
            sleep(MIN_INIT_TIME);// wait for finish done
            mPlayerActivity = null;
        }

        if (mMainScreen != null) {
            mMainScreen.finish();
            sleep(MIN_INIT_TIME);// wait for finish done
            mMainScreen = null;
        }
        try {
            mPlayerSolo.finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }

        super.tearDown();

        Log.i(TAG, "<<<tearDown");
    }

    public void testCase01FullScreen() {
        Log.i(TAG, ">>>testCase01FullScreen");
        
        assertNotNull("mPlayerActivity is null", mPlayerActivity);
        // choose "Full Screen" item
        mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
        mInstrumentation.waitForIdleSync();
        sendKeys(KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_RIGHT,
                KeyEvent.KEYCODE_DPAD_CENTER);
        mInstrumentation.waitForIdleSync();

        // can switch channel through control panel
        showCtrlPanel();
        canSwitchChannel();

        // exit full screen
        String exitFullScrn = mPlayerActivity
                .getString(R.string.exit_full_screen);
        mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
        mInstrumentation.waitForIdleSync();
        mPlayerSolo.clickOnMenuItem(exitFullScrn);

        Log.i(TAG, "<<<testCase01FullScreen");
    }

    public void testCase02SwitchChanInList() {
        Log.i(TAG, ">>>testCase02SwitchChanInList");

        assertNotNull("mPlayerActivity is null", mPlayerActivity);
        showCtrlPanel();
        mChannelListCtrl = (ImageView) mPlayerActivity
                .findViewById(R.id.channel_list);
        assertTrue(mChannelListCtrl.isEnabled());
        clickViewWithoutFocus(mChannelListCtrl);

        sleep(MIN_INIT_TIME);

        ListView channelListView = (ListView) mPlayerActivity
                .findViewById(R.id.channel_list_in_player);
        assertTrue(channelListView != null && channelListView.isShown()
                && channelListView.getCount() > 0);

        int firstChannel = channelListView.getFirstVisiblePosition();
        Log.i(TAG, "FirstVisiblePosition: " + firstChannel);
        BaseAdapter adapter = (BaseAdapter) channelListView.getAdapter();
        View item = adapter.getView(firstChannel, null, channelListView);
        String channelNameStr = ((TextView) (item
                .findViewById(R.id.player_channel_name))).getText().toString();
        mPlayerSolo.clickOnText(channelNameStr);
        mInstrumentation.waitForIdleSync();

        sleep(MIN_INIT_TIME);

        // display channel number
        TextView channelNameView0 = (TextView) mPlayerActivity
                .findViewById(R.id.channel_name0);
        TextView channelNumberView = (TextView) mPlayerActivity
                .findViewById(R.id.channel_number);
        assertTrue(mPlayerSolo.waitForView(channelNumberView, 4000, false));
        assertEquals(channelNumberView.getText().toString(),
                String.valueOf(firstChannel));

        // display channel name
        assertTrue(channelNameView0.getVisibility() == View.VISIBLE);
        assertEquals(channelNameStr, channelNameView0.getText().toString());

        Log.i(TAG, "<<<testCase02SwitchChanInList");
    }

    public void testCase03PlayerUiBasicFunc() {
        Log.i(TAG, ">>>testCase03PlayerUiBasicFunc");

        assertNotNull("mPlayerActivity is null", mPlayerActivity);

        // snapshot
        sleep(CHECK_TIMEOUT);// wait for control panel disappear
        snapShotFunc();

        // play and stop
        sleep(CHECK_TIMEOUT);// wait for control panel disappear
        playAndStopFunc();

        // emergency broadcast
        sleep(CHECK_TIMEOUT);// wait for control panel disappear
        emergencyBroadcastFunc();

        // add and remove favorite channel
        mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);// back to
                                                                    // PlayerActivity
        sleep(CHECK_TIMEOUT);// wait for control panel disappear
        favoriteChannelFunc();
        
        sleep(CHECK_TIMEOUT);
        favoriteChannelFunc();

      // switch channel
        showCtrlPanel();
        canSwitchChannel();
        clickViewWithoutFocus(mNextCtrl);
        
       // sleep(CHECK_TIMEOUT);        
    /*
        boolean issubscribe = mPlayerSolo.waitForText(
            mPlayerActivity.getString(android.R.string.ok), 1, CHECK_TIMEOUT);*/
        boolean issubscribe = checksubscribeDlg();
        Log.d(TAG, "test switch song issubscribe: " + issubscribe);
        if (issubscribe) {
        	Log.d(TAG, "click to subcribe");
        	mPlayerSolo.clickOnText(mPlayerActivity.getString(android.R.string.ok));
        	sleep(10000);
        	mPlayerSolo.goBack();
        }
        showCtrlPanel();
        canSwitchChannel();
        clickViewWithoutFocus(mPrevCtrl);

        Log.i(TAG, "<<<testCase03PlayerUiBasicFunc");
    }

    public void testCase04EMBlowLevel() { 
        Log.d(TAG,"testCase04EMBlowLevel >> "); 
        SimulateEbMsg sim = new SimulateEbMsg(mInstrumentation.getTargetContext(), mPlayerSolo);
        int[] a = sim.getNextMsgIds();
        sim.addMsg(a[2], 3, false, null);
        mPlayerSolo.sleep(2000);
        Log.d(TAG, "send emb done");
        boolean iswait = false;
        do {        	
        iswait = mPlayerSolo.waitForText(
        		mPlayerActivity.getString(com.mediatek.cmmb.app.R.string.read), 1, 1000);
        Log.d(TAG, "wait for emb comming");
        } while (!iswait);
        
        mPlayerSolo.clickOnText(mPlayerActivity.getString(com.mediatek.cmmb.app.R.string.read));
        mPlayerSolo.sleep(2000);
        mPlayerSolo.goBack();
        mPlayerSolo.sleep(2000);
        Log.d(TAG,"testCase04EMBlowLevel << ");  
    }
    
    private PlayerActivity getPlayerActivity() {

        Activity a = null;
        ActivityMonitor playerMonitor = mInstrumentation.addMonitor(
                "com.mediatek.cmmb.app.PlayerActivity", null, false);

        // launch MainScreen Activity first
        mMainScreen = launchActivity(mInstrumentation.getTargetContext()
                .getPackageName(), MainScreen.class, null);

        if (mMainScreen == null) {
            Log.i(TAG, "mMainScreen is null, can not test PlayerActivity");
            return null;
        }
        sleep(MIN_INIT_TIME);

        // (AlertDialog)mCmmbDialog may be showed
        clickCmmbAlertDlg();
        sleep(MIN_INIT_TIME);

        // wait for channel updating
        Log.i(TAG, "Start to sleep for channel updating");
        waitForChanUpdating();
        Log.i(TAG, "End to sleep for channel updating");

        // click an unlock channel to start PlayerActivity
        sleep(MIN_INIT_TIME);
        clickUnlockChannel();

        // get the PlayerActivity instance
        a = playerMonitor.waitForActivityWithTimeout(MONITOR_TIMEOUT);
        if (a == null) {
            Log.i(TAG, "mPlayerActivity is null, can not test PlayerActivity");
            return null;
        }

        // wait PlayerActivity initialization done
        sleep(INIT_TIME);
        setActivity(a);

        mInstrumentation.removeMonitor(playerMonitor);

        return (PlayerActivity) a;
    }

    private void waitForChanUpdating() {

        long startTime = System.currentTimeMillis();
        int mainScreenState = (Integer) getPrivateFiled(mMainScreen, "mState");

        while (mainScreenState != STATE_UPDATED_SERVICE
                && mainScreenState != STATE_UPDATED_SERVICE_FAIL
                && mainScreenState != STATE_IDLE) {
            if (System.currentTimeMillis() - startTime > UPDATING_TIME) {
                Log.i(TAG, "waitForChanUpdating timeout");
                break;
            }
            sleep(CHECK_TIME);
            mainScreenState = (Integer) getPrivateFiled(mMainScreen, "mState");
        }
        Log.i(TAG, "mainScreenState: " + mainScreenState);
    }

    private void clickCmmbAlertDlg() {
        // check if has sim <=> check if it is mMbbmsMode
        mModeSwitchManager = (ModeSwitchManager) getPrivateFiled(mMainScreen,
                "mModeSwitchManager");
        mMbbmsMode = (Integer) getPrivateFiled(mMainScreen, "MBBMS_MODE");

        if (mModeSwitchManager.getMode() != mMbbmsMode) {
            Log.i(TAG, "CmmbAlertDlg Is Show");
            mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_ENTER);
            mInstrumentation.waitForIdleSync();
        } else {
            try {
                Field fSelectSimDialog = MainScreen.class.getDeclaredField("mSelectSimDlg");
                if (fSelectSimDialog != null) {
                    Log.d(TAG, "show select sim log");
                    mInstrumentation.waitForIdleSync();
                    // click OK button of save recording dialog
                    mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
                    mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
                    mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
                    mInstrumentation.waitForIdleSync();
                }
            } catch (NoSuchFieldException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void clickUnlockChannel() {
        // click an unlock channel to start PlayerActivity
        mAllChannelListView = (ListView) mMainScreen
                .findViewById(R.id.list_all);
        if (mAllChannelListView == null || mAllChannelListView.getCount() <= 0) {
            Log.i(TAG, "All channel list is empty, can not test PlayerActivity");
            return;
        }

        mChannelListManager = (ChannelListManager) getPrivateFiled(mMainScreen,
                "mChannelListManager");
        BaseAdapter adapter = (BaseAdapter) mAllChannelListView.getAdapter();
        boolean hasUnlockChan = false;

        if (mModeSwitchManager.getMode() == mMbbmsMode) {
            Log.i(TAG, "it is MBBMS mode");
            for (int i = 0; i < mAllChannelListView.getCount(); i++) {
                Channel ch = mChannelListManager.getChannelList().get(i);
                if (ch.isFree()) {

                    for (int j = 0; j <= i; j++) {
                        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
                    }
                    sendKeys(KeyEvent.KEYCODE_DPAD_CENTER);
                    mInstrumentation.waitForIdleSync();

                    View item = adapter.getView(i, null, mAllChannelListView);
                    Log.i(TAG,
                            "click on a unlock channel: "
                                    + ((TextView) item
                                            .findViewById(R.id.channel_name))
                                            .getText().toString());
                    hasUnlockChan = true;
                    break;
                }
            }
        } else {
            Log.i(TAG, "it is  not MBBMS mode");
            for (int i = 0; i < mAllChannelListView.getCount(); i++) {
                Channel ch = mChannelListManager.getChannelList().get(i);
                if (ch.isFree()) {

                    for (int j = 0; j <= i; j++) {
                        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
                    }
                    sendKeys(KeyEvent.KEYCODE_DPAD_CENTER);
                    mInstrumentation.waitForIdleSync();

                    View item = adapter.getView(i, null, mAllChannelListView);
                    Log.i(TAG,
                            "click on a unlock channel: "
                                    + ((TextView) item
                                            .findViewById(R.id.channel_name))
                                            .getText().toString());
                    hasUnlockChan = true;
                    break;
                }
            }
        }

        if (!hasUnlockChan) {
            Log.i(TAG,
                    "no unlock or free channel to click, can not test PlayerActivity");
        }
    }

    private boolean checksubscribeDlg() {
        View subDlg = (View) waitForObjectWithTimeout("mDlgNeedSubscrib",
                CHECK_TIMEOUT);
            
        if (subDlg.getVisibility() == View.VISIBLE) {
            Log.d(TAG, "sub dlg is visible");
            return true;
        } else {
Log.d(TAG, "sub dlg is not visible");
        return false;
        } 

        }
    // access private filed from of Activity
    private Object getPrivateFiled(Activity activity, String variable) {
        Field field = null;
        try {
            field = activity.getClass().getDeclaredField(variable);
            field.setAccessible(true);
            return field.get(activity);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // show the control_panel
    private void showCtrlPanel() {
        assertNotNull("mPlayerSolo is null", mPlayerSolo);
        mPlayerSolo.clickOnScreen(50, 50);
        mInstrumentation.waitForIdleSync();

        mControlPanel = (View) waitForObjectWithTimeout("mControlPanel",
                CHECK_TIMEOUT);
        assertNotNull("mControlPanel is null", mControlPanel);
        assertEquals(View.VISIBLE, mControlPanel.getVisibility());
        assertTrue(mControlPanel.isEnabled());
    }

    // can switch channel
    private void canSwitchChannel() {
        mPrevCtrl = (ImageView) mPlayerActivity
                .findViewById(R.id.previous_control);
        mNextCtrl = (ImageView) mPlayerActivity.findViewById(R.id.next_control);
        mChannelListCtrl = (ImageView) mPlayerActivity
                .findViewById(R.id.channel_list);
        assertTrue(mPrevCtrl.isEnabled());
        assertTrue(mNextCtrl.isEnabled());
        assertTrue(mChannelListCtrl.isEnabled());
    }

    private Object waitForObjectWithTimeout(String variable, long timeOut) {
        Log.i(TAG, ">>>waitForObjectWithTimeout");

        long startTime = System.currentTimeMillis();
        Object obj = getPrivateFiled(mPlayerActivity, variable);
        while (obj == null) {
            if (System.currentTimeMillis() - startTime > timeOut) {
                break;
            }
            sleep(CHECK_TIME);
            obj = getPrivateFiled(mPlayerActivity, variable);
        }
        Log.i(TAG, "<<<waitForObjectWithTimeout");

        return obj;
    }

    // private void clickView(final View view) {
    // mInstrumentation.runOnMainSync(new Runnable() {
    //
    // @Override
    // public void run() {
    // boolean focus = view.requestFocus();
    // Log.d(TAG,"clickView  focus = " + focus);
    // if (focus) {
    // Log.d(TAG,"clickView view = " + view);
    // view.performClick();
    // }
    // }
    // });
    //
    // mInstrumentation.waitForIdleSync();
    // }

    private void clickViewWithoutFocus(final View view) {
        mInstrumentation.runOnMainSync(new Runnable() {

            @Override
            public void run() {
                Log.d(TAG, "clickView view = " + view);
                view.performClick();
            }
        });

        mInstrumentation.waitForIdleSync();
    }

    private void snapShotFunc() {

        showCtrlPanel();
        mSnapButton = (ImageView) mPlayerActivity
                .findViewById(R.id.capture_control);
        assertEquals(View.VISIBLE, mSnapButton.getVisibility());
        assertTrue(mSnapButton.isEnabled());
        assertTrue(mSnapButton.isClickable());

        clickViewWithoutFocus(mSnapButton);
        int playerState = (Integer) getPrivateFiled(mPlayerActivity, "mState");
        boolean preparing = (Boolean) getPrivateFiled(mPlayerActivity,
                "mPreparing");
        if (playerState != STATE_STARTED || preparing) {
            String toastMsg = mPlayerActivity
                    .getString(R.string.not_support_snap);
            mPlayerSolo.waitForText(toastMsg, 1, CHECK_TIMEOUT);
        }
    }

    private void playAndStopFunc() {

        showCtrlPanel();
        mPlayStopButton = (ImageView) mPlayerActivity
                .findViewById(R.id.play_stop);
        assertEquals(View.VISIBLE, mPlayStopButton.getVisibility());
        assertTrue(mPlayStopButton.isEnabled());
        assertTrue(mPlayStopButton.isClickable());
        clickViewWithoutFocus(mPlayStopButton);
        sleep(CHECK_TIMEOUT);// wait to play or stop
        // show control panel again to check the play_stop button's drawable
        showCtrlPanel();
        mPlayStopButton = (ImageView) mPlayerActivity
                .findViewById(R.id.play_stop);
        // get the mState variable of mPlayerActivity
        int playerState = (Integer) getPrivateFiled(mPlayerActivity, "mState");
        Drawable playView = mPlayerActivity.getResources().getDrawable(
                R.drawable.btn_ic_play);
        Drawable stopView = mPlayerActivity.getResources().getDrawable(
                R.drawable.btn_ic_stop);
        if (playerState == STATE_STARTED) {
            assertTrue(stopView.isVisible());
        } else {
            assertTrue(playView.isVisible());
        }
    }

    private void favoriteChannelFunc() {

        showCtrlPanel();
        mFavoriteIndicator = (ImageView) mPlayerActivity
                .findViewById(R.id.favorite_control);
        assertEquals(View.VISIBLE, mFavoriteIndicator.getVisibility());
       // assertTrue(mFavoriteIndicator.isEnabled());
       // assertTrue(mFavoriteIndicator.isClickable());
        clickViewWithoutFocus(mFavoriteIndicator);
        sleep(MIN_INIT_TIME);

        // get private method: isCurrentChannelMyFavorite(), then get if current
        // channel is favorite
        boolean isFavorite = false;
        try {
            Method m = mPlayerActivity.getClass().getDeclaredMethod(
                    "isCurrentChannelMyFavorite", (Class[]) null);
            m.setAccessible(true);
            isFavorite = (Boolean) m.invoke(mPlayerActivity);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        Drawable addToFavorite = mPlayerActivity.getResources().getDrawable(
                R.drawable.add_to_favorite);
        Drawable removeFromFavorite = mPlayerActivity.getResources()
                .getDrawable(R.drawable.remove_from_favorite);

        if (isFavorite) {
            assertTrue(removeFromFavorite.isVisible());
        } else {
            assertTrue(addToFavorite.isVisible());
        }

        // check favorite in MainSreen
       // checkFavoriteInMainScrn(isFavorite);
    }

    private void checkFavoriteInMainScrn(boolean isFavorite) {

        // get current channel name
        TextView channelNameView0 = (TextView) mPlayerActivity
                .findViewById(R.id.channel_name0);
        String curChanNameString = channelNameView0.getText().toString();

        // back to MainScreen
        mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
        mInstrumentation.waitForIdleSync();
        sleep(MIN_INIT_TIME);

        // click to FAVORITE CHANNELS
        mPlayerSolo.clickOnText(mMainScreen
                .getString(R.string.main_favourite_channels));
        mInstrumentation.waitForIdleSync();
        sleep(MIN_INIT_TIME);
        if (isFavorite) {
            assertTrue(mPlayerSolo.searchText(curChanNameString, true));
        } else {
            assertFalse(mPlayerSolo.searchText(curChanNameString, true));
        }
    }    

    private void emergencyBroadcastFunc() {

        Activity a = null;
        ActivityMonitor monitor = mInstrumentation.addMonitor(
                "com.mediatek.cmmb.app.MessageListActivity", null, false);

        String emergencyBroadcaString = mPlayerActivity
                .getString(R.string.emergency_broadcast);
        Log.i(TAG, "R.string.emergency_broadcast: " + emergencyBroadcaString);
        mPlayerSolo.clickOnMenuItem(emergencyBroadcaString);
        mInstrumentation.waitForIdleSync();

        // check if the MessageListActivity Activity is started
        a = monitor.waitForActivityWithTimeout(MONITOR_TIMEOUT);
        assertNotNull("MessageListActivity is null", a);
    }

}