/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.cmmb.app.tests;

import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.net.Uri;
import android.os.Handler;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.EBManager;
import com.mediatek.cmmb.app.EBManager.EBListener;
import com.mediatek.cmmb.app.MainScreen;
import com.mediatek.cmmb.app.MessageListActivity;
import com.mediatek.cmmb.app.MessageDetailActivity;
import com.mediatek.cmmb.app.R;

public class CMMBEBTest extends ActivityInstrumentationTestCase2<MessageListActivity> implements EBListener {
    private static final String TAG = "CMMBUITest-MessageListActivity";
    
    private Instrumentation mInst = null;
    private MainScreen mMainActivity = null;
    private Solo mSolo = null;
    private boolean mInMbbmsMode = false;
    
    public CMMBEBTest () {
        super(MessageListActivity.class);
    }
    
    @Override
    protected void setUp() throws Exception {
        Log.v(TAG, "setUp <<<<<");
        super.setUp();
        
        mInst = getInstrumentation();
        assertNotNull(mInst);
        
        mSolo = new Solo(mInst);
        assertNotNull(mSolo);
        
        mInst.waitForIdleSync();
        mInst.setInTouchMode(false);
        mMainActivity = launchActivity("com.mediatek.cmmb.app", MainScreen.class, null);
        assertNotNull(mMainActivity);
        setActivity(mMainActivity);
        
        CMMBUITestUtils.sleepThread(2000);
        
        mInMbbmsMode = CMMBUITestUtils.isInMbbmsMode(mMainActivity);
        
        CMMBUITestUtils.checkSimModeAndSelect(mInst);
        CMMBUITestUtils.waitForUpdateDone(mMainActivity);

        Log.v(TAG, "setUp >>>>>");
    }
    
    @Override
    protected void tearDown() throws Exception {
        Log.v(TAG, "tearDown <<<<<");

        CMMBUITestUtils.sleepThread(2000);
        
        if (mMainActivity != null) {
            mMainActivity.finish();
            mMainActivity = null;
        }
        
        super.tearDown();
        CMMBUITestUtils.sleepThread(2000);
        Log.v(TAG, "tearDown >>>>>");
    }

    public void test01Simulation() {
        Log.v(TAG, "test01Simulation<<<<<");
        
        EBManager.removeEBListener(this);
        
        EBManager.addEBListener(this);
        SimulateEbMsg sim = new SimulateEbMsg(mInst.getTargetContext(), mSolo);
        sim.add100Msgs();
        sim.add4DuplicatedMsgs();
        EBManager.removeEBListener(this);

        Log.v(TAG, "test01Simulation>>>>>");
    }

    public void test02EB() {
        Log.v(TAG, "test02EB <<<<<");

        addEB();
        CMMBUITestUtils.sleepThread(2000);

        // entry EB List Activity
        MessageListActivity mEBList = (MessageListActivity)entryEB(mInst, mSolo);
        CMMBUITestUtils.sleepThread(1000);

        // clear all pre EB Messsages
        CMMBUITestUtils.expectNoDialog(mSolo, mMainActivity);
        mSolo.clickOnMenuItem(mMainActivity.getString(R.string.delete_all));
        assertTrue(mSolo.waitForText(mMainActivity.getString(R.string.ebm_no_list), 1, 3000));

        // add 5 EB Messages
        addEB();
        CMMBUITestUtils.sleepThread(3000);

        // confirm EB List not null
        ListView listView = (ListView)mEBList.getListView();
        assertTrue((listView != null) && (listView.getCount() > 0));

        // click first EB in list
        assertTrue(CMMBUITestUtils.isExpectedActivity(mEBList));
        CMMBUITestUtils.expectNoDialog(mSolo, mEBList);
        mSolo.clickInList(1);

        // entry EB Message Detail Activity
        ActivityMonitor am = new ActivityMonitor(MessageListActivity.class.getName(), null, false);
        mInst.addMonitor(am);
        MessageDetailActivity ebDetail = (MessageDetailActivity)mInst.waitForMonitorWithTimeout(am, 3000);

        // delete this EB Message
        CMMBUITestUtils.expectNoDialog(mSolo, ebDetail);
        mSolo.clickOnMenuItem(mMainActivity.getString(R.string.delete));
        CMMBUITestUtils.sleepThread(2000);

        // Long click on first List item, and select delete menu
        assertTrue(CMMBUITestUtils.isExpectedActivity(mEBList));
        CMMBUITestUtils.expectNoDialog(mSolo, mEBList);
        mSolo.clickLongInList(1);
        assertTrue(mSolo.waitForText(mMainActivity.getString(R.string.delete), 1, 3000));
        mSolo.clickOnText(mMainActivity.getString(R.string.delete));

        // Delete all EBMessages that for test
        CMMBUITestUtils.sleepThread(2000);
        assertTrue(CMMBUITestUtils.isExpectedActivity(mEBList));
        CMMBUITestUtils.expectNoDialog(mSolo, mEBList);
        mSolo.clickOnMenuItem(mMainActivity.getString(R.string.delete_all));
        assertTrue(mSolo.waitForText(mMainActivity.getString(R.string.ebm_no_list), 1, 3000));

        // back to Main Screen
        CMMBUITestUtils.sleepThread(2000);
        assertTrue(CMMBUITestUtils.isExpectedActivity(mEBList));
        CMMBUITestUtils.expectNoDialog(mSolo, mEBList);
        mSolo.goBack();

        Log.v(TAG, "test02EB >>>>>");
    }
    
    private MessageListActivity entryEB(Instrumentation inst, Solo solo) {
        ActivityMonitor am = new ActivityMonitor(MessageListActivity.class.getName(), null, false);
        inst.addMonitor(am);

        gobackToMain(solo);
        CMMBUITestUtils.expectNoDialog(solo, mMainActivity);
        solo.clickOnMenuItem(mMainActivity.getString(R.string.emergency_broadcast));

        MessageListActivity ebList = (MessageListActivity)inst.waitForMonitorWithTimeout(am, 3000);
        assertNotNull(ebList);
        setActivity(ebList);

        return ebList;
    }
    
    private void gobackToMain(Solo solo) {
        while(false == CMMBUITestUtils.isExpectedActivity(mMainActivity)) {
            CMMBUITestUtils.expectNoDialog(solo, mMainActivity);
            solo.goBack();
            CMMBUITestUtils.sleepThread(1000);
        }
    }
    
    @Override
    public void notify(int level, String content, Uri uri) {
        Log.v(TAG, "EB notify (" + String.valueOf(level) + ", " + content + ", " + uri + ").");
    }

    private void addEB() {
        SimulateEbMsg mockEB = new SimulateEbMsg(mInst.getTargetContext(), mSolo);

        mockEB.add5Msgs();
    }
}
