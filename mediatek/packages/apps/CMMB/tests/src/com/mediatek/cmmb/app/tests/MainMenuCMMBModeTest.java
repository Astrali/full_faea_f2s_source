package com.mediatek.cmmb.app.tests;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.ChannelListManager;
import com.mediatek.cmmb.app.ChannelListManager.Channel;
import com.mediatek.cmmb.app.MainScreen;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class MainMenuCMMBModeTest extends ActivityInstrumentationTestCase2<MainScreen> {
     private static final String TAG = "CMMB::MainMenuCMMBModeTest";
     private MainScreen mActivity;
     private Solo mSolo = null;
     Instrumentation mInstrumentation;
     Context mContext;
     
     private static final int STATE_UPDATING = 1;
     
     private static final int TAB_ALL = 0;
        
     private int mState;
     private int mTabIndex;
     private ArrayList<Channel> mList; 
          
     public MainMenuCMMBModeTest() {
          super("com.mediatek.cmmb.app", MainScreen.class);
     }
     
     @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setup>>");
        // View.setKeepScreenOn(true);
        Settings.Global.putInt(getInstrumentation().getTargetContext().getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 1);
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", true);
        getInstrumentation().getTargetContext().sendBroadcast(intent);
        Log.d(TAG, "set flight mode");
        Thread.sleep(10000);

        // ConnectivityManager cm =
        // (ConnectivityManager)getInstrumentation().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        // cm.setMobileDataEnabled(false);
        super.setUp();

        setActivityInitialTouchMode(false);
        mInstrumentation = getInstrumentation();
        mSolo = new Solo(getInstrumentation(), getActivity());
        assertNotNull(mInstrumentation);
        mActivity = getActivity();
        mContext = mActivity.getApplicationContext();
        assertNotNull(mActivity);
        mInstrumentation.waitForIdleSync();
        Thread.sleep(1000);

        mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
        mInstrumentation.waitForIdleSync();

        Log.d(TAG, "setup <<");
    }
    
   public void testCase01waitflightmode() throws Exception {
       Log.d(TAG, "testCase01 >>");
       waitAgain();
       Log.d(TAG, "testCase01 <<");
   }
     
   public void testCase02playlock() throws Exception {
       Log.d(TAG, "testCase02playlock >>");
       waitAgain();
       ListView listView = (ListView) mActivity.findViewById(com.mediatek.cmmb.app.R.id.list_all);
       assertTrue((listView != null) && (listView.getCount() > 0));
       ListAdapter listAdapter = listView.getAdapter();
       for (int i = 0; i < listView.getCount(); i++) {
           Log.d(TAG, "testCase02playlock i: " + i);
           View view = listAdapter.getView(i, null, listView);
           TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.channel_name);
           String channelName = textView.getText().toString();
           getChannelListValue();
           ChannelListManager.Channel ch = mList.get(i);
           Log.d(TAG, "play ch:" + ch);
           if (!ch.isFree()) {
               for (int j = 0; j <= i; j++) {
                   sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
               }
               Log.d(TAG, "click play lock channel in cmmb mode");
               mSolo.clickOnText(channelName);
               Thread.sleep(2000);                
               mSolo.clickOnButton(mContext.getString(android.R.string.ok));
               Thread.sleep(2000);   
               break;
           }            
       }
       Log.d(TAG, "testCase02playlock <<");
   }
   
    public void testCase03ModeChangeok() throws Exception {
        Log.d(TAG, "testCase03ModeChangeok >>"); 
        /*mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
        mInstrumentation.waitForIdleSync(); 
        Thread.sleep(1000);*/
        waitAgain();
        Thread.sleep(1000);
        //flight mode off entry mbbms mode
        Settings.Global.putInt(mContext.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0);
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", false);
        mContext.sendBroadcast(intent);
        Log.d(TAG, "set flight mode off");
        mInstrumentation.waitForIdleSync(); 
        Thread.sleep(10000);
        
        checkMBBMSModeDilog();
        
        mSolo.clickOnButton(mContext.getString(android.R.string.ok));
        mInstrumentation.waitForIdleSync(); 
        Thread.sleep(1000);
        checkSimModeAndSelect();
        waitAgain();
        Log.d(TAG, "testCase03ModeChangeok <<");
    }  

    
    public void testCase05ModeChangeCancel() throws Exception {
        Log.d(TAG, "testCase04ModeChangeCancel >>");
        waitAgain();
        
        mSolo.sleep(2000);
        //flight mode off entry mbbms mode
        Settings.Global.putInt(mContext.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0);
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);     
        intent.putExtra("state", false);
        mContext.sendBroadcast(intent);
        
        mSolo.sleep(10000);
        
        checkMBBMSModeDilog();
        
        mSolo.clickOnButton(mContext.getString(android.R.string.cancel));
        mInstrumentation.waitForIdleSync(); 
        Thread.sleep(1000);
        checkSimModeAndSelect();
        waitAgain();
        Log.d(TAG, "testCase04ModeChangeCancel <<");
    }
   
    @Override
    public void tearDown() throws Exception {
        Log.d(TAG, "tearDown >>");
       // setKeepScreenOn(false);
        
        mSolo.sleep(2000);
        
        if (mActivity != null) {
            mActivity.finish();
            mInstrumentation.waitForIdleSync();     
            Thread.sleep(2000);             
        }
        super.tearDown();       
        Log.d(TAG, "tearDown <<");
    }

    /*below are utility functions*/
    
    private void checkSimModeAndSelect() throws Exception {
        Log.d(TAG, "CheckSimMode >>");
  
        try {
        Field fSelectSimDialog = MainScreen.class.getDeclaredField("mSelectSimDlg");
        Field fCmmbDialog = MainScreen.class.getDeclaredField("mCmmbDialog");
        if (fCmmbDialog != null) {
            Log.d(TAG, "show CMMB confirm log");
            mInstrumentation.waitForIdleSync();            
            // click OK button of save recording dialog
            mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
            mInstrumentation.waitForIdleSync();
            return ;  
        }
      
        if (fSelectSimDialog != null) {
            Log.d(TAG, "show select sim log");
            mInstrumentation.waitForIdleSync();            
            // click OK button of save recording dialog
            mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
            mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
            mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
            mInstrumentation.waitForIdleSync();
            return ;        
        }
        }  catch (Exception e) {
          Log.d(TAG, " error happened");  
        }
    }
    private int getState() throws Exception {
        Field fState = MainScreen.class.getDeclaredField("mState");
        fState.setAccessible(true);
        mState = (Integer)fState.get(mActivity);
        Log.d(TAG, "mState:" + mState);
        return mState;
    }
    
    private void waitAgain() throws Exception {
        int i = 0;
        do {
           Log.d(TAG, "wait again i" + i);
        Thread.sleep(5000);
        i += 1;
    } while (getState() == STATE_UPDATING && i < 20);
        
    }
    
    private void checkMBBMSModeDilog() throws Exception {
        boolean wait = false;
        String recover = mActivity.getString(com.mediatek.cmmb.app.R.string.gsm_signal_recover);
        int i = 0;
        do {
            Log.d(TAG, "checkMBBMSModeDilog i:" + i);            
            i += 1;            
            wait = mSolo.waitForText(recover, 1, 5000);
            
            Log.d(TAG, "wait:" + wait);
        } while (!wait && i < 20);                
    }
    
    private void getChannelListValue() throws Exception {
        
        Field mF  = MainScreen.class.getDeclaredField("mChannelListManager");       
        assertNotNull(mF);
        mF.setAccessible(true);
        if (getTabIndex() == TAB_ALL) {            
            mList = ((ChannelListManager)mF.get(mActivity)).getChannelList(); 
            Log.d(TAG, "getChannelListValue list:" + mList);
        } else {
            Log.d(TAG, "current is tab favorite");
        } 
    }
    
    private int getTabIndex() throws Exception {
        Log.d(TAG, "getTabIndex");
        Field ft = MainScreen.class.getDeclaredField("mTabIndex");
        assertNotNull(ft);  
        ft.setAccessible(true);
        mTabIndex = (Integer)ft.get(mActivity);
        Log.d(TAG, "getTabindex index = " + mTabIndex);
        return mTabIndex;        
    }
}
