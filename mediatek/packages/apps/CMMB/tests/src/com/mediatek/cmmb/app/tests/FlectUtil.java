package com.mediatek.cmmb.app.tests;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class FlectUtil {
    
    public static Object invoke(Object instance, String medthodName, Object[] args) 
    throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Class[] classTypes = null;
        if (args != null) {
            classTypes = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                if (args[i] != null) {
                    classTypes[i] = args[i].getClass();
                }
            }
        }
        Class<?> c = instance.getClass();
        Method m = c.getDeclaredMethod(medthodName, classTypes);
        m.setAccessible(true);
        return m.invoke(instance, args);
    }
    
    public static Object invoke(Object instance, String medthodName, Class[] argsTypes, Object[] args) 
    throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Class<?> c = instance.getClass();
        Method m = c.getDeclaredMethod(medthodName, argsTypes);
        m.setAccessible(true);
        return m.invoke(instance, args);
    }
          
    public static Object getFiled(Object instance, String fieldName) 
    throws NoSuchFieldException, IllegalAccessException {
        Class<?> c = instance.getClass();
        Field f = c.getDeclaredField(fieldName);
        f.setAccessible(true);
        Object o = f.get(instance);
        return o;
    }
              
    public static void setFiled(Object instance, String fieldName,Object value) 
    throws NoSuchFieldException, IllegalAccessException {
        Class<?> c = instance.getClass();
        Field f = c.getDeclaredField(fieldName);
        f.setAccessible(true);
        f.set(instance, value);
    }
    
    public static Object invoke(Object outerInstance, String innerClassName, Class[] constructorTypes, 
        Object[] constructorArgs, String medthodName, Class[] classTypes, Object[] args)
    throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?>[] clz = outerInstance.getClass().getDeclaredClasses();
        for (int i = 0; i < clz.length; i ++) {
            if (clz[i].getName().equals(outerInstance.getClass().getName() + "$" + innerClassName)) {
                Constructor con = clz[i].getDeclaredConstructor(constructorTypes);
                con.setAccessible(true);
                    return invoke(con.newInstance(constructorArgs), medthodName, classTypes, args);
                }
          }
          throw new NoSuchMethodException();
    }
}
