package com.mediatek.cmmb.app.tests;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.test.ProviderTestCase2;
import android.util.Log;

import com.mediatek.mbbms.ESGManager;
import com.mediatek.mbbms.MBBMSStore;
import com.mediatek.mbbms.MBBMSStore.DB;
import com.mediatek.mbbms.MBBMSStore.EB;
import com.mediatek.mbbms.MBBMSStore.ESG;
import com.mediatek.mbbms.MBBMSStore.ESG.ContentMedia;
import com.mediatek.mbbms.MBBMSStore.ESG.ServiceMedia;
import com.mediatek.mbbms.MBBMSStore.SG;
import com.mediatek.mbbms.MBBMSStore.SG.ContentPreviewData;
import com.mediatek.mbbms.MBBMSStore.SG.PurchaseDataPreviewData;
import com.mediatek.mbbms.MBBMSStore.SG.ServicePreviewData;
import com.mediatek.mbbms.SGManager;
import com.mediatek.mbbms.providers.MBBMSProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MBBMSProviderTest extends ProviderTestCase2<MBBMSProvider> {
    private static final String TAG = "MBBMSProviderTest";
    
    public MBBMSProviderTest() {
        super(MBBMSProvider.class, "com.mediatek.providers.mbbms");
    }
    
    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, getName() + " start!");
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        Log.d(TAG, getName() + " end!");
    }

    public void test01Sanity() {
        Log.d(TAG, "test00Sanity");
        ContentValues vals = new ContentValues();
        vals.put(MBBMSStore.DB.Status.DATABASE_NAME, 0);
        vals.put(MBBMSStore.DB.Status.STATUS, MBBMSStore.DB.Status.STATUS_ENUM_SANITY_TEST);
        getMockContentResolver().update(MBBMSStore.DB.Status.CONTENT_URI, vals, null, null); 
        Log.d(TAG, "test00Sanity");
    }
    
    @SuppressWarnings("deprecation")
    public void test02ServiceGetIsSubscribed() {
        MBBMSStore.SG.Service.getIsSubscribed(getMockContentResolver(), 1);     // exists
        MBBMSStore.SG.Service.getIsSubscribed(getMockContentResolver(), "0");   // not exists 
        MBBMSStore.ESG.Service.getIsSubscribed(getMockContentResolver(), 1);    // exists
        MBBMSStore.ESG.Service.getIsSubscribed(getMockContentResolver(), "0");  // not exists 
    }
    
    public void test03ErrorHandle() {
        ContentValues vals = new ContentValues();
        vals.put(MBBMSStore.SG.SGDD.SGDD_ID, 0);
        vals.put(MBBMSStore.SG.SGDD.VERSIOIN, 0);
        getMockContentResolver().update(MBBMSStore.SG.SGDD.CONTENT_URI, vals, null, null);
    }
    
    public void test04Query() {
        wrappedQuery(MBBMSStore.DB.Status.CONTENT_URI);
        
        wrappedQuery(MBBMSStore.DB.Setting.CONTENT_URI);

        wrappedQuery(MBBMSStore.EB.Broadcast.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.EB.Broadcast.CONTENT_URI));

        wrappedQuery(MBBMSStore.ESG.Service.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.ESG.Service.CONTENT_URI));

        wrappedQuery(MBBMSStore.ESG.Content.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.ESG.Content.CONTENT_URI));

        wrappedQuery(MBBMSStore.ESG.Schedule.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.ESG.Schedule.CONTENT_URI));

        wrappedQuery(MBBMSStore.ESG.Media.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.ESG.Media.CONTENT_URI));

        wrappedQuery(MBBMSStore.ESG.ContentSpecial.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.ESG.ContentSpecial.CONTENT_URI));

        wrappedQuery(Uri.parse(appendId(MBBMSStore.ESG.Content.CONTENT_URI) + "/special"));
        wrappedQuery(Uri.parse(appendId(MBBMSStore.ESG.Content.CONTENT_URI) + "/special/1"));

        wrappedQuery(MBBMSStore.ESG.ServiceSpecial.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.ESG.ServiceSpecial.CONTENT_URI));

        wrappedQuery(Uri.parse(appendId(MBBMSStore.ESG.Service.CONTENT_URI) + "/special"));
        wrappedQuery(Uri.parse(appendId(MBBMSStore.ESG.Service.CONTENT_URI) + "/special/1"));

        wrappedQuery(MBBMSStore.SG.Subscription.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.Subscription.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.PreviewData.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.PreviewData.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.Service.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.Service.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.Content.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.Content.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.PurchaseChannel.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.PurchaseChannel.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.PurchaseItem.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.PurchaseData.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.PurchaseData.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.Access.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.Access.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.SGDD.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.SGDD.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.KMS.CONTENT_URI);

        wrappedQuery(MBBMSStore.SG.FragmentAccessMap.CONTENT_URI);

        wrappedQuery(MBBMSStore.ESG.ContentDetail.CONTENT_URI);

        wrappedQuery(MBBMSStore.ESG.ContentMedia.getContentUri(1));
        wrappedQuery(appendId(MBBMSStore.ESG.ContentMedia.getContentUri(1)));

        wrappedQuery(MBBMSStore.ESG.ServiceMedia.getContentUri(1));
        wrappedQuery(appendId(MBBMSStore.ESG.ServiceMedia.getContentUri(1)));

        wrappedQuery(MBBMSStore.SG.ContentPreviewData.getContentUri(1));
        wrappedQuery(appendId(MBBMSStore.SG.ContentPreviewData.getContentUri(1)));

        wrappedQuery(MBBMSStore.SG.ServicePreviewData.getContentUri(1));
        wrappedQuery(appendId(MBBMSStore.SG.ServicePreviewData.getContentUri(1)));

        wrappedQuery(MBBMSStore.SG.PurchaseDataPreviewData.getContentUri(1));
        wrappedQuery(appendId(MBBMSStore.SG.PurchaseDataPreviewData.getContentUri(1)));

        wrappedQuery(MBBMSStore.SG.PurchaseItemDetail.CONTENT_URI);

        wrappedQuery(Uri.parse(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI) + "/content"));
        wrappedQuery(Uri.parse(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI) + "/content/1"));

        wrappedQuery(Uri.parse(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI) + "/service"));
        wrappedQuery(Uri.parse(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI) + "/service/1"));

        wrappedQuery(MBBMSStore.SG.SGFragment.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.SGFragment.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.PurchaseItemService.HISTORY_CONTENT_URI);

        wrappedQuery(MBBMSStore.SG.Schedule.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.Schedule.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.InteractivityData.CONTENT_URI);
        wrappedQuery(appendId(MBBMSStore.SG.InteractivityData.CONTENT_URI));

        wrappedQuery(MBBMSStore.SG.PurchaseItemContent.HISTORY_CONTENT_URI);
        
        wrappedQuery(MBBMSStore.SG.PurchaseItemContentDetail.CONTENT_URI);
        
        wrappedQuery(MBBMSStore.SG.InteractivityDataDetail.CONTENT_URI);

        wrappedQuery(MBBMSStore.SG.PurchaseItemContent.CONTENT_URI);

        wrappedQuery(MBBMSStore.SG.PurchaseItemService.CONTENT_URI);

        wrappedQuery(MBBMSStore.SG.ServiceDetailMore.CONTENT_URI);

        try {
            wrappedQuery(Uri.EMPTY);
            Log.d(TAG, "This is an empty uri!");
        } catch (IllegalStateException e) {
            fail("Here should not get IllegalStateException.");
        }
    }

    public void test05GetType() {
        assertEquals(MBBMSStore.DB.Setting.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.DB.Setting.CONTENT_URI));

        assertEquals(MBBMSStore.EB.Broadcast.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.EB.Broadcast.CONTENT_URI));
        assertEquals(MBBMSStore.EB.Broadcast.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.EB.Broadcast.CONTENT_URI)));

        assertEquals(MBBMSStore.ESG.Service.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.ESG.Service.CONTENT_URI));
        assertEquals(MBBMSStore.ESG.Service.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.ESG.Service.CONTENT_URI)));

        assertEquals(MBBMSStore.ESG.Content.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.ESG.Content.CONTENT_URI));
        assertEquals(MBBMSStore.ESG.Content.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.ESG.Content.CONTENT_URI)));

        assertEquals(MBBMSStore.ESG.Schedule.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.ESG.Schedule.CONTENT_URI));
        assertEquals(MBBMSStore.ESG.Schedule.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.ESG.Schedule.CONTENT_URI)));

        assertEquals(MBBMSStore.ESG.Media.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.ESG.Media.CONTENT_URI));
        assertEquals(MBBMSStore.ESG.Media.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.ESG.Media.CONTENT_URI)));

        assertEquals(MBBMSStore.ESG.ContentSpecial.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.ESG.ContentSpecial.CONTENT_URI));
        assertEquals(MBBMSStore.ESG.ContentSpecial.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.ESG.ContentSpecial.CONTENT_URI)));

        assertEquals(MBBMSStore.ESG.ContentSpecial.CONTENT_TYPE,
                getProvider().getType(Uri.parse(appendId(MBBMSStore.ESG.Content.CONTENT_URI) + "/special")));
        assertEquals(MBBMSStore.ESG.ContentSpecial.CONTENT_TYPE_ITEM,
                getProvider().getType(Uri.parse(appendId(MBBMSStore.ESG.Content.CONTENT_URI) + "/special/1")));

        assertEquals(MBBMSStore.ESG.ServiceSpecial.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.ESG.ServiceSpecial.CONTENT_URI));
        assertEquals(MBBMSStore.ESG.ServiceSpecial.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.ESG.ServiceSpecial.CONTENT_URI)));

        assertEquals(MBBMSStore.ESG.ServiceSpecial.CONTENT_TYPE,
                getProvider().getType(Uri.parse(appendId(MBBMSStore.ESG.Service.CONTENT_URI) + "/special")));
        assertEquals(MBBMSStore.ESG.ServiceSpecial.CONTENT_TYPE_ITEM,
                getProvider().getType(Uri.parse(appendId(MBBMSStore.ESG.Service.CONTENT_URI) + "/special/1")));

        assertEquals(MBBMSStore.SG.Subscription.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.Subscription.CONTENT_URI));
        assertEquals(MBBMSStore.SG.Subscription.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.Subscription.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.PreviewData.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.PreviewData.CONTENT_URI));
        assertEquals(MBBMSStore.SG.PreviewData.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.PreviewData.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.Service.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.Service.CONTENT_URI));
        assertEquals(MBBMSStore.SG.Service.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.Service.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.Content.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.Content.CONTENT_URI));
        assertEquals(MBBMSStore.SG.Content.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.Content.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.PurchaseChannel.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.PurchaseChannel.CONTENT_URI));
        assertEquals(MBBMSStore.SG.PurchaseChannel.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.PurchaseChannel.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.PurchaseItem.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.PurchaseItem.CONTENT_URI));
        assertEquals(MBBMSStore.SG.PurchaseItem.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.PurchaseData.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.PurchaseData.CONTENT_URI));
        assertEquals(MBBMSStore.SG.PurchaseData.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.PurchaseData.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.Access.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.Access.CONTENT_URI));
        assertEquals(MBBMSStore.SG.Access.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.Access.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.SGDD.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.SGDD.CONTENT_URI));
        assertEquals(MBBMSStore.SG.SGDD.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.SGDD.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.KMS.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.KMS.CONTENT_URI));

        assertEquals(MBBMSStore.SG.FragmentAccessMap.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.FragmentAccessMap.CONTENT_URI));

        assertEquals(MBBMSStore.SG.FragmentPreviewDataMap.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.FragmentPreviewDataMap.CONTENT_URI));

        assertEquals(MBBMSStore.SG.FragmentPurchaseItemMap.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.FragmentPurchaseItemMap.CONTENT_URI));

        assertEquals(MBBMSStore.SG.PurchaseDataChannelMap.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.PurchaseDataChannelMap.CONTENT_URI));

        assertEquals(MBBMSStore.ESG.ContentDetail.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.ESG.ContentDetail.CONTENT_URI));
        assertEquals(MBBMSStore.ESG.ContentDetail.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.ESG.ContentDetail.CONTENT_URI)));

        assertEquals(MBBMSStore.ESG.ContentMedia.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.ESG.ContentMedia.getContentUri(1)));
        assertEquals(MBBMSStore.ESG.ContentMedia.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.ESG.ContentMedia.getContentUri(1))));

        assertEquals(MBBMSStore.ESG.ServiceMedia.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.ESG.ServiceMedia.getContentUri(1)));
        assertEquals(MBBMSStore.ESG.ServiceMedia.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.ESG.ServiceMedia.getContentUri(1))));

        assertEquals(MBBMSStore.SG.ContentPreviewData.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.ContentPreviewData.getContentUri(1)));
        assertEquals(MBBMSStore.SG.ContentPreviewData.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.ContentPreviewData.getContentUri(1))));

        assertEquals(MBBMSStore.SG.ServicePreviewData.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.ServicePreviewData.getContentUri(1)));
        assertEquals(MBBMSStore.SG.ServicePreviewData.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.ServicePreviewData.getContentUri(1))));

        assertEquals(MBBMSStore.SG.PurchaseDataPreviewData.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.PurchaseDataPreviewData.getContentUri(1)));
        assertEquals(MBBMSStore.SG.PurchaseDataPreviewData.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.PurchaseDataPreviewData.getContentUri(1))));

        assertEquals(MBBMSStore.SG.PurchaseItemDetail.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.PurchaseItemDetail.CONTENT_URI));
        assertEquals(MBBMSStore.SG.PurchaseItemDetail.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.PurchaseItemDetail.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.PurchaseItemContent.CONTENT_TYPE,
                getProvider().getType(Uri.parse(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI) + "/content")));
        assertEquals(MBBMSStore.SG.PurchaseItemContent.CONTENT_TYPE_ITEM,
                getProvider().getType(Uri.parse(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI) + "/content/1")));

        assertEquals(MBBMSStore.SG.PurchaseItemService.CONTENT_TYPE,
                getProvider().getType(Uri.parse(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI) + "/service")));
        assertEquals(MBBMSStore.SG.PurchaseItemService.CONTENT_TYPE_ITEM,
                getProvider().getType(Uri.parse(appendId(MBBMSStore.SG.PurchaseItem.CONTENT_URI) + "/service/1")));

        assertEquals(MBBMSStore.SG.SGFragment.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.SGFragment.CONTENT_URI));
        assertEquals(MBBMSStore.SG.SGFragment.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.SGFragment.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.PurchaseItemService.HISTORY_CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.PurchaseItemService.HISTORY_CONTENT_URI));

        assertEquals(MBBMSStore.SG.Schedule.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.Schedule.CONTENT_URI));
        assertEquals(MBBMSStore.SG.Schedule.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.Schedule.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.InteractivityData.CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.InteractivityData.CONTENT_URI));
        assertEquals(MBBMSStore.SG.InteractivityData.CONTENT_TYPE_ITEM,
                getProvider().getType(appendId(MBBMSStore.SG.InteractivityData.CONTENT_URI)));

        assertEquals(MBBMSStore.SG.PurchaseItemContent.HISTORY_CONTENT_TYPE,
                getProvider().getType(MBBMSStore.SG.PurchaseItemContent.HISTORY_CONTENT_URI));
        
        assertEquals(MBBMSStore.SG.PurchaseItemContentDetail.CONTENT_TYPE, 
                getProvider().getType(MBBMSStore.SG.PurchaseItemContentDetail.CONTENT_URI));
        
        try {
            getProvider().getType(Uri.EMPTY);
            fail("Here should get IllegalStateException");
        } catch (IllegalStateException e) {
            Log.d(TAG, "This is an empty uri!");
        }
    }
    
    public void test06insert() {
        getMockContentResolver().insert(MBBMSStore.DB.Setting.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.EB.Broadcast.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.ESG.Service.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.ESG.Content.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.ESG.Schedule.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.ESG.Media.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.ESG.ContentSpecial.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.ESG.ServiceSpecial.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.Subscription.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.PreviewData.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.Service.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.Content.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.PurchaseChannel.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.PurchaseItem.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.PurchaseData.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.Access.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.SGDD.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.KMS.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.FragmentAccessMap.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.FragmentPreviewDataMap.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.FragmentPurchaseItemMap.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.PurchaseDataChannelMap.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.SGFragment.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.Schedule.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.InteractivityData.CONTENT_URI, null);
        getMockContentResolver().insert(MBBMSStore.SG.FragmentInteractivityDataMap.CONTENT_URI, null);        
        try {
            getMockContentResolver().insert(MBBMSStore.SG.PurchaseItemContent.HISTORY_CONTENT_URI, null);
            fail("Here should get IllegalStateException");
        } catch (IllegalStateException e) {
            Log.d(TAG, "This is an empty uri!");
        }
    }
    
    public void test07BulkInsert() {
        getMockContentResolver().bulkInsert(MBBMSStore.EB.Broadcast.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.ESG.Service.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.ESG.Content.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.ESG.Schedule.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.ESG.Media.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.ESG.ContentSpecial.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.ESG.ServiceSpecial.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.Subscription.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.PreviewData.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.Service.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.Content.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.PurchaseChannel.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.PurchaseItem.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.PurchaseData.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.Access.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.SGDD.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.KMS.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.FragmentAccessMap.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.FragmentPreviewDataMap.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.FragmentPurchaseItemMap.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.PurchaseDataChannelMap.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.SGFragment.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.Schedule.CONTENT_URI, null);
        getMockContentResolver().bulkInsert(MBBMSStore.SG.InteractivityData.CONTENT_URI, null);        
        try {
            getMockContentResolver().bulkInsert(MBBMSStore.SG.PurchaseItemContent.HISTORY_CONTENT_URI, null);
            fail("Here should get IllegalStateException");
        } catch (IllegalStateException e) {
            Log.d(TAG, "This is an empty uri!");
        }
    }
    
    public void test08Delete() {
        getMockContentResolver().delete(DB.Setting.CONTENT_URI, null, null);

        getMockContentResolver().delete(EB.Broadcast.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(EB.Broadcast.CONTENT_URI), null, null);

        getMockContentResolver().delete(ESG.Service.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(ESG.Service.CONTENT_URI), null, null);

        getMockContentResolver().delete(ESG.Content.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(ESG.Content.CONTENT_URI), null, null);

        getMockContentResolver().delete(ESG.Schedule.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(ESG.Schedule.CONTENT_URI), null, null);

        getMockContentResolver().delete(ESG.Media.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(ESG.Media.CONTENT_URI), null, null);

        getMockContentResolver().delete(ESG.ContentSpecial.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(ESG.ContentSpecial.CONTENT_URI), null, null);

        getMockContentResolver().delete(ESG.ServiceSpecial.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(ESG.ServiceSpecial.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.Subscription.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.Subscription.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.PreviewData.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.PreviewData.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.Service.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.Service.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.Content.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.Content.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.PurchaseChannel.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.PurchaseChannel.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.PurchaseItem.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.PurchaseItem.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.PurchaseData.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.PurchaseData.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.Access.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.Access.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.SGDD.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.SGDD.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.SGFragment.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.SGFragment.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.Schedule.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.Schedule.CONTENT_URI), null, null);

        getMockContentResolver().delete(SG.InteractivityData.CONTENT_URI, null, null);
        getMockContentResolver().delete(appendId(SG.InteractivityData.CONTENT_URI), null, null);
        
        try {
            getMockContentResolver().delete(SG.PurchaseItemContent.HISTORY_CONTENT_URI, null, null);
            fail("Here should get IllegalStateException");
        } catch (IllegalStateException e) {
            Log.d(TAG, "This is an invalid uri!");
        }
        
        getMockContentResolver().delete(MBBMSStore.ESG.CONTENT_URI, null, null);
        getMockContentResolver().delete(MBBMSStore.SG.CONTENT_URI, null, null);
    }
    
    public void test09ReCreate() {
        getMockContentResolver().update(MBBMSStore.CONTENT_URI, null, null, null);
    }
    
    public void test10ESG() throws IOException {
        InputStream is = getTestContext().getAssets().open("ESG.xml");
        ESGManager mgr = ESGManager.getInstance(getMockContext());
        mgr.setFrequency(20);
        mgr.parse(getFileString(is));
        mgr.persistData(true);
        ContentMedia.getMediaBitmap(getMockContentResolver(), 1, 1);
        ContentMedia.getMediaBitmap(getMockContentResolver(), 1, 3);
        ServiceMedia.getMediaBitmap(getMockContentResolver(), 1, 1);
        ServiceMedia.getMediaBitmap(getMockContentResolver(), 1, 3);
        
        ContentMedia.getMediaBitmap(getMockContentResolver(), 1, 1, new BitmapFactory.Options());
        ContentMedia.getMediaBitmap(getMockContentResolver(), 1, 3, new BitmapFactory.Options());
        ServiceMedia.getMediaBitmap(getMockContentResolver(), 1, 1, new BitmapFactory.Options());
        ServiceMedia.getMediaBitmap(getMockContentResolver(), 1, 3, new BitmapFactory.Options());
    }
    
    public void test11SG() throws IOException, InterruptedException {
        InputStream is = getTestContext().getAssets().open("SG.raw");
        SGManager mgr = new SGManager(getMockContext());
        mgr.parse(is);
        is.close();
        ContentPreviewData.getPreviewBitmap(getMockContentResolver(), 1, 3);
        ContentPreviewData.getPreviewBitmap(getMockContentResolver(), 1, 128);
        ServicePreviewData.getPreviewBitmap(getMockContentResolver(), 1, 3);
        ServicePreviewData.getPreviewBitmap(getMockContentResolver(), 1, 128);
        PurchaseDataPreviewData.getPreviewBitmap(getMockContentResolver(), 1, 3);
        PurchaseDataPreviewData.getPreviewBitmap(getMockContentResolver(), 1, 128);
        ContentPreviewData.getPreviewBitmap(getMockContentResolver(), 1, 3, new BitmapFactory.Options());
        ContentPreviewData.getPreviewBitmap(getMockContentResolver(), 1, 128, new BitmapFactory.Options());
        ServicePreviewData.getPreviewBitmap(getMockContentResolver(), 1, 3, new BitmapFactory.Options());
        ServicePreviewData.getPreviewBitmap(getMockContentResolver(), 1, 128, new BitmapFactory.Options());
        PurchaseDataPreviewData.getPreviewBitmap(getMockContentResolver(), 1, 3, new BitmapFactory.Options());
        PurchaseDataPreviewData.getPreviewBitmap(getMockContentResolver(), 1, 128, new BitmapFactory.Options());
    }
    
    public String getFileString(InputStream is) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte buf[] = new byte[1024];
        int len;
        while ((len = is.read(buf)) != -1) {
            os.write(buf, 0, len);
        }
        os.close();
        is.close();
        return os.toString();
    }
    
    private Uri appendId(Uri uri) {
        return ContentUris.withAppendedId(uri, 1);
    }
    
    private void wrappedQuery(Uri uri) {
        Cursor cr = getMockContentResolver().query(uri, null, null, null, null);
        if (cr != null) {
            cr.close();
        }
    }
}
