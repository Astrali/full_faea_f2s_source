/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.cmmb.app.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.MainScreen;
import com.mediatek.cmmb.app.ModeSwitchManager;
import com.mediatek.cmmb.app.R;

import java.lang.reflect.Field;

public class CMMBUITestUtils {
    private static final String TAG = "CMMBUITestUtils";

    private static final int MBBMS_MODE = 2;
    private static ModeSwitchManager sModeSwitchManager = null;

    private static boolean sInMbbmsMode = false;

    public static Field getField(Class<?> cls, String name) {
        Field field = null;
        try {
            field = cls.getDeclaredField(name);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        field.setAccessible(true);
        return field;
    }

    public static boolean isInMbbmsMode(Activity activity) {
        try {
            sModeSwitchManager = (ModeSwitchManager) (getField(MainScreen.class,
                    "mModeSwitchManager").get(activity));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (sModeSwitchManager.getMode() == MBBMS_MODE) {
            sInMbbmsMode = true;
        } else {
            sInMbbmsMode = false;
        }

        return sInMbbmsMode;
    }

    public static void waitForUpdateDone(Object obj) {
        int state = -1;
        do {
            try {
                state = getField(MainScreen.class, "mState").getInt(obj);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            sleepThread(3000);
        } while (state != 2);
    }

    public static void sleepThread(long miliseconds) {
        try {
            Thread.sleep(miliseconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isExpectedActivity(Activity expectActivity) {
        View[] decorViews = getWindowDecorViews();

        // another solution: find current decorView context parent util activity
        for(int i=decorViews.length; i>0; --i) {
            if(decorViews[i-1].getContext() instanceof Activity) {
                return (expectActivity == decorViews[i-1].getContext());
            }
        }

        return false;
    }

    public static void expectNoDialog(Solo solo, Context context) {
        View[] decorViews = getWindowDecorViews();
        int i = decorViews.length;

        while(true) {
            if(decorViews[i-1].getContext() instanceof Activity) {
                break;
            } else {
                kickOneDialog(solo, context);

                decorViews = getWindowDecorViews();
                i = decorViews.length;
            }
        }
    }

    private static void kickOneDialog(Solo solo, Context context) {
        if(solo.searchText(context.getString(R.string.quit_confirm))) {
            // cmmb exit dialog
            solo.clickOnText(context.getString(android.R.string.cancel));
            sleepThread(1000);
        } else {
            solo.goBack();
            sleepThread(1000);
        }
    }

    public static boolean expectOneDialog(Solo solo, Context context) {
        View[] decorViews = getWindowDecorViews();
        int i = decorViews.length;

        while(true) {
            if ((i <= 1) || (decorViews[i-1].getContext() instanceof Activity)) {
                return false;
            } else {
                boolean flagTop = decorViews[i-1].getContext() instanceof Activity;
                boolean flagSecond = decorViews[i-2].getContext() instanceof Activity;

                if ((false == flagTop) && (true == flagSecond)) {
                    return true;
                } else if((false == flagTop) && false == flagSecond) {
                    kickOneDialog(solo, context);

                    decorViews = getWindowDecorViews();
                    i = decorViews.length;
                } else {
                    return false;
                }
            }
        }
    }

    public static boolean expectOneDialog(String includedString, Solo solo, Context context) {
        View[] decorViews = getWindowDecorViews();
        int i = decorViews.length;

        while(true) {
            if ((i <= 1) || (decorViews[i].getContext() instanceof Activity)) {
                return false;
            } else {
                boolean flagTop = decorViews[i-1].getContext() instanceof Activity;
                boolean flagSecond = decorViews[i-2].getContext() instanceof Activity;

                if ((false == flagTop) && (true == flagSecond)) {
                    return solo.searchText(includedString);
                } else if((false == flagTop) && false == flagSecond) {
                    kickOneDialog(solo, context);

                    decorViews = getWindowDecorViews();
                    i = decorViews.length;
                } else {
                    return false;
                }
            }
        }
    }

    public static View[] getWindowDecorViews() {
        Field viewsField;
        Field instanceField;

        Log.v(TAG, "getWindowDecorViews.");

        try {
            viewsField = sWindowManager.getDeclaredField("mViews");

            if (android.os.Build.VERSION.SDK_INT >= 17) {
                instanceField = sWindowManager.getDeclaredField("sDefaultWindowManager");
            } else if (android.os.Build.VERSION.SDK_INT >= 13) {
                instanceField = sWindowManager.getDeclaredField("sWindowManager");
            } else {
                instanceField = sWindowManager.getDeclaredField("mWindowManager");
            }

            viewsField.setAccessible(true);
            instanceField.setAccessible(true);
            Object instance = instanceField.get(null);

            return (View[]) viewsField.get(instance);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
    
    public static void checkSimModeAndSelect(Instrumentation instrumentation) {
        Log.d(TAG, "CheckSimMode >>");

        try {
            Field fSelectSimDialog = MainScreen.class.getDeclaredField("mSelectSimDlg");
            Field fCmmbDialog = MainScreen.class.getDeclaredField("mCmmbDialog");
            if (fCmmbDialog != null) {
                Log.d(TAG, "show CMMB confirm log");
                instrumentation.waitForIdleSync();
                // click OK button of save recording dialog
                instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
                instrumentation.waitForIdleSync();
                return;
            }

            if (fSelectSimDialog != null) {
                Log.d(TAG, "show select sim log");
                instrumentation.waitForIdleSync();
                // click OK button of save recording dialog
                instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
                instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
                instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
                instrumentation.waitForIdleSync();
                return;
            }
        } catch (Exception e) {
            Log.d(TAG, " error happened");
        }
    }
    
    private static Class<?> sWindowManager;
    static {
        try {
            if (android.os.Build.VERSION.SDK_INT >= 17) {
                sWindowManager = Class.forName("android.view.WindowManagerGlobal");
            } else {
                sWindowManager = Class.forName("android.view.WindowManagerImpl");
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
}
