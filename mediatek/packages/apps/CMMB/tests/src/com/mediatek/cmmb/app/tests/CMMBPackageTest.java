/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.cmmb.app.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.ContentSelectActivity;
import com.mediatek.cmmb.app.MainScreen;
import com.mediatek.cmmb.app.PackageActivity;
import com.mediatek.cmmb.app.R;

import com.mediatek.mbbms.MBBMSStore;

import java.lang.reflect.InvocationTargetException;


public class CMMBPackageTest extends ActivityInstrumentationTestCase2<PackageActivity> {
    private static final String TAG = "CMMBUITest-PackageTest";
    
    private Instrumentation mInst = null;
    private MainScreen mMainActivity = null;
    private PackageActivity mPackage = null;
    private boolean mInMbbmsMode = false;
    private Solo mSolo = null;
    
    public CMMBPackageTest() {
        super(PackageActivity.class);
    }
    
    @Override
    protected void setUp() throws Exception {
        Log.v(TAG, "setUp <<<<<");
        super.setUp();
        
        mInst = getInstrumentation();
        assertNotNull(mInst);
        
        mSolo = new Solo(mInst);
        assertNotNull(mSolo);

        mInst.waitForIdleSync();
        mInst.setInTouchMode(false);
        mMainActivity = launchActivity("com.mediatek.cmmb.app", MainScreen.class, null);
        assertNotNull(mMainActivity);
        setActivity(mMainActivity);
        
        CMMBUITestUtils.sleepThread(2000);
        
        mInMbbmsMode = CMMBUITestUtils.isInMbbmsMode(mMainActivity);
        
        CMMBUITestUtils.checkSimModeAndSelect(mInst);
        CMMBUITestUtils.waitForUpdateDone(mMainActivity);

        Log.v(TAG, "setUp >>>>>");
    }

    @Override
    protected void tearDown() throws Exception {
        Log.v(TAG, "tearDown <<<<<");
        
        CMMBUITestUtils.sleepThread(2000);

        if (mPackage != null) {
            mPackage.finish();
            mPackage = null;
        }

        if (mMainActivity != null) {
            mMainActivity.finish();
            mMainActivity = null;
        }
        
        super.tearDown();
        CMMBUITestUtils.sleepThread(2000);
        Log.v(TAG, "tearDown >>>>>");
    }
    
    public void test01Package() {
        Log.v(TAG, "test01Package <<<<<");
        
        if (!mInMbbmsMode) {
            return;
        }

        // goto Package Activity
        mPackage = entryPackage(mInst, mSolo);
        CMMBUITestUtils.sleepThread(1000);

        // wait for sync purchase item done and check
        mSolo.waitForText(mPackage.getString(R.string.query_account_info_success), 1, 10000);
        assertTrue(mSolo.searchText(mMainActivity.getString(R.string.subscribed_package)));
        assertTrue(mSolo.searchText(mMainActivity.getString(R.string.unsubscribed_package)));

        // goBack to Main Screen
        assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        mInst.waitForIdleSync();
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(1000);
        
        Log.v(TAG, "test01Package >>>>>"); 
    }
    
    public void test02Package() {
        Log.v(TAG, "test02Package <<<<<");
        
        if (!mInMbbmsMode) {
            return;
        }
        
        // goto Package Activity
        mPackage = entryPackage(mInst, mSolo);
        CMMBUITestUtils.sleepThread(1000);

        // wait for sync purchase item done
        mSolo.waitForText(mPackage.getString(R.string.query_account_info_success), 1, 10000);
        
        try {
            // get subscribed items
            ListView mSubscribed = 
                (ListView)CMMBUITestUtils.getField(PackageActivity.class, "mSubscribedListView").get(mPackage);
            int subCount = mSubscribed.getCount() - 1;
            Log.v(TAG, "Subed num = " + subCount);

            // get unSubscribed items
            assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
            CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
            mSolo.clickOnText(mMainActivity.getString(R.string.unsubscribed_package));
            CMMBUITestUtils.sleepThread(1000);
            ListView mUnSubscribed = 
                (ListView)CMMBUITestUtils.getField(PackageActivity.class, "mUnsubscribedListView").get(mPackage);
            int unSubCount = mUnSubscribed.getCount();
            Log.v(TAG, "Un Subed num = " + unSubCount);
        
        
            if (subCount > 0){
                // entry subscribed tab
                assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
                CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
                mSolo.clickOnText(mMainActivity.getString(R.string.subscribed_package));
                CMMBUITestUtils.sleepThread(1000);

                // unsubscribe
                assertTrue(mSolo.searchButton(mMainActivity.getString(R.string.unsubscribe)));
                mSolo.clickOnButton(mMainActivity.getString(R.string.unsubscribe));
                mSolo.waitForText(mPackage.getString(R.string.unsubscribe_tip), 1, 3000);
                CMMBUITestUtils.sleepThread(2000);

                // confirm to unsubscribe
                assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
                CMMBUITestUtils.expectOneDialog(mSolo, mPackage);
                mSolo.clickOnButton(mPackage.getString(android.R.string.yes));
                CMMBUITestUtils.sleepThread(20000);
            
                CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
                CMMBUITestUtils.sleepThread(1000);
            
                Log.v(TAG, "Current Activity is Main " + (mMainActivity == mSolo.getCurrentActivity()));
                return;
            }
        
            if (unSubCount > 0){
                // entry unSubscribed tab
                assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
                CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
                mSolo.clickOnText(mMainActivity.getString(R.string.unsubscribed_package));
                CMMBUITestUtils.sleepThread(1000);

                // subscribe
                assertTrue(mSolo.searchButton(mMainActivity.getString(R.string.subscribe)));
                mSolo.clickOnButton(mMainActivity.getString(R.string.subscribe));
                mSolo.waitForText(mPackage.getString(R.string.subscribe_tip), 1, 3000);
                CMMBUITestUtils.sleepThread(2000);
                
                // confirm to unsubscribe
                assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
                CMMBUITestUtils.expectOneDialog(mSolo, mPackage);
                mSolo.clickOnButton(mPackage.getString(android.R.string.yes));
                CMMBUITestUtils.sleepThread(20000);

                CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
                CMMBUITestUtils.sleepThread(1000);
                return;
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        CMMBUITestUtils.sleepThread(2000);
        
        Log.v(TAG, "test02Package >>>>>"); 
    }
    
    public void test03Package() {
        Log.v(TAG, "test03Package <<<<<");
        
        test02Package();

        Log.v(TAG, "test03Package >>>>>"); 
    }

    public void test04Package() {
        Log.v(TAG, "test04Package <<<<<");
        
        if (false == mInMbbmsMode)
            return;
        
        // goto Package Activity
        mPackage = entryPackage(mInst, mSolo);
        CMMBUITestUtils.sleepThread(1000);

        // wait for sync purchase item done and check
        mSolo.waitForText(mPackage.getString(R.string.query_account_info_success), 1, 10000);

        // entry Subscribed tab and click on Content List menu item
        assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        mSolo.clickOnText(mMainActivity.getString(R.string.subscribed_package));
        CMMBUITestUtils.sleepThread(1000);
        mSolo.clickOnMenuItem(mMainActivity.getString(R.string.content_list));

        // entry Content Select Activity
        ActivityMonitor am = new ActivityMonitor(ContentSelectActivity.class.getName(), null, false);
        mInst.addMonitor(am);
        ContentSelectActivity contentSelect = (ContentSelectActivity)mInst.waitForMonitorWithTimeout(am, 3000);

        // Check and goback to package activity
        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        CMMBUITestUtils.sleepThread(500);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(2000);

        // goBack to Main Screen
        assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        mSolo.goBack();

        // goto search and try to subscribe/unsubscribe
        checkSearch();
        checkSearch();            

        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        CMMBUITestUtils.sleepThread(2000);
        
        Log.v(TAG, "test04Package >>>>>"); 
    }

    private void checkSearch() {
        CMMBUITestUtils.sleepThread(2000);
        setActivity(mMainActivity);

        // goto Package Activity
        mPackage = entryPackage(mInst, mSolo);
        CMMBUITestUtils.sleepThread(1000);

        // wait for sync purchase item done and check
        mSolo.waitForText(mPackage.getString(R.string.query_account_info_success), 1, 10000);

        // check and click on search menu item
        assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        mSolo.clickOnMenuItem(mMainActivity.getString(R.string.search));
        CMMBUITestUtils.sleepThread(2000);

        mSolo.enterText(0, "CCTV");
        CMMBUITestUtils.sleepThread(500);
        mSolo.sendKey(KeyEvent.KEYCODE_ENTER);
        CMMBUITestUtils.sleepThread(3000);

        // TODO: check if a new activity is started
        if(mSolo.searchButton(mMainActivity.getString(R.string.subscribe))) {
            mSolo.clickOnButton(mMainActivity.getString(R.string.subscribe));
            mSolo.waitForText(mMainActivity.getString(R.string.subscribe_tip), 1, 3000);
            mSolo.clickOnButton(mMainActivity.getString(android.R.string.yes));
            CMMBUITestUtils.sleepThread(20000);
        } else if (mSolo.searchButton(mMainActivity.getString(R.string.unsubscribe))) {
            mSolo.clickOnButton(mMainActivity.getString(R.string.unsubscribe));
            mSolo.waitForText(mMainActivity.getString(R.string.unsubscribe_tip), 1, 3000);
            mSolo.clickOnButton(mMainActivity.getString(android.R.string.yes));
            CMMBUITestUtils.sleepThread(20000);
        }

        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        CMMBUITestUtils.sleepThread(2000);
    }

    public void test05Package() {
        Log.v(TAG, "test05Package <<<<<");
        
        if (false == mInMbbmsMode)
            return;
        
        // goto Package Activity
        mPackage = entryPackage(mInst, mSolo);
        CMMBUITestUtils.sleepThread(1000);

        // wait for sync purchase item done and check
        mSolo.waitForText(mPackage.getString(R.string.query_account_info_success), 1, 10000);
        
        try {
            // get subscribed items
            ListView mSubscribed = 
                (ListView)CMMBUITestUtils.getField(PackageActivity.class, "mSubscribedListView").get(mPackage);
            int subCount = mSubscribed.getCount() - 1;
            Log.v(TAG, "Subed num = " + subCount);

            // get unSubscribed items
            assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
            CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
            mSolo.clickOnText(mMainActivity.getString(R.string.unsubscribed_package));
            CMMBUITestUtils.sleepThread(1000);
            ListView mUnSubscribed = 
                (ListView)CMMBUITestUtils.getField(PackageActivity.class, "mUnsubscribedListView").get(mPackage);
            int unSubCount = mUnSubscribed.getCount();
            Log.v(TAG, "Un Subed num = " + unSubCount);
        
            if (unSubCount > 0){
                // entry unSubscribed tab
                assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
                CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
                mSolo.clickOnText(mMainActivity.getString(R.string.unsubscribed_package));
                CMMBUITestUtils.sleepThread(1000);

                // subscribe
                assertTrue(mSolo.searchButton(mMainActivity.getString(R.string.subscribe)));
                mSolo.clickOnButton(mMainActivity.getString(R.string.subscribe));
                mSolo.waitForText(mPackage.getString(R.string.subscribe_tip), 1, 3000);
                CMMBUITestUtils.sleepThread(2000);
                
                // confirm to subscribe
                assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
                CMMBUITestUtils.expectOneDialog(mSolo, mPackage);
                mSolo.clickOnButton(mPackage.getString(android.R.string.yes));
                CMMBUITestUtils.sleepThread(20000);
            
                // goto Package Activity
                mPackage = entryPackage(mInst, mSolo);
                CMMBUITestUtils.sleepThread(1000);

                // wait for sync purchase item done and check
                mSolo.waitForText(mPackage.getString(R.string.query_account_info_success), 1, 10000);
            }
        
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // entry subscribed tab
        assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        mSolo.clickOnText(mMainActivity.getString(R.string.subscribed_package));
        CMMBUITestUtils.sleepThread(1000);

        // for entry menu twice, special here
        mSolo.sendKey(KeyEvent.KEYCODE_MENU);
        CMMBUITestUtils.sleepThread(500);
        mSolo.goBack();
        CMMBUITestUtils.sleepThread(1000);

        // unsubscribe all items
        assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        mSolo.clickOnMenuItem(mMainActivity.getString(R.string.unsubscribe_all));
        CMMBUITestUtils.sleepThread(2000);

        // confirm to unsubscribe
        assertTrue(CMMBUITestUtils.isExpectedActivity(mPackage));
        CMMBUITestUtils.expectOneDialog(mSolo, mPackage);
        mSolo.clickOnButton(mPackage.getString(android.R.string.yes));
        mSolo.waitForText(mPackage.getString(R.string.unsubscribe_all_success), 1, 30000);
            
        CMMBUITestUtils.expectNoDialog(mSolo, mPackage);
        CMMBUITestUtils.sleepThread(1000);

        Log.v(TAG, "test05Package >>>>>"); 
    }

    public void test06Package() 
        throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException, InvocationTargetException {
        Log.v(TAG, "test06Package <<<<<");
        
        if (false == mInMbbmsMode)
            return;
        
        // goto Package Activity
        mPackage = entryPackage(mInst, mSolo);
        CMMBUITestUtils.sleepThread(1000);

        // wait for sync purchase item done and check
        mSolo.waitForText(mPackage.getString(R.string.query_account_info_success), 1, 10000);

        Class<?>[] clz = mPackage.getClass().getDeclaredClasses();
        for(int i=0;i<clz.length; i++) {
            if(clz[i].getName().indexOf("PackageAdapter")>=0) {
                Object packageAdapter = FlectUtil.getFiled(mPackage, "mSubscribedAdapter");
                Object[] params = new Object[2];
                params[0] = MBBMSStore.SG.PurchaseData.SUBSCRIPTION_TYPE_ENUM_PURCHASEITEM_MULTI_MONTH;
                params[1] = 5;
                FlectUtil.invoke(packageAdapter, "getPeriod", new Class[]{int.class, int.class}, params);

                params[0]= MBBMSStore.SG.PurchaseData.SUBSCRIPTION_TYPE_ENUM_SERVICE_MONTH;
                FlectUtil.invoke(packageAdapter, "getPeriod", new Class[]{int.class, int.class}, params);

                params[0]=MBBMSStore.SG.PurchaseData.SUBSCRIPTION_TYPE_ENUM_CONTENT_TIMES;
                FlectUtil.invoke(packageAdapter, "getPeriod", new Class[]{int.class, int.class}, params);

                params[0]=1;
                FlectUtil.invoke(packageAdapter, "getPeriod", new Class[]{int.class, int.class}, params);
                
                params[0] = MBBMSStore.SG.PurchaseData.SUBSCRIPTION_TYPE_ENUM_PURCHASEITEM_MULTI_MONTH;
                params[1] = 5.40f;
                FlectUtil.invoke(packageAdapter, "getMoney", new Class[]{int.class, float.class}, params);

                params[0]= MBBMSStore.SG.PurchaseData.SUBSCRIPTION_TYPE_ENUM_SERVICE_MONTH;
                FlectUtil.invoke(packageAdapter, "getMoney", new Class[]{int.class, float.class}, params);

                params[0]=MBBMSStore.SG.PurchaseData.SUBSCRIPTION_TYPE_ENUM_CONTENT_TIMES;
                FlectUtil.invoke(packageAdapter, "getMoney", new Class[]{int.class, float.class}, params);

                params[0]=1;
                FlectUtil.invoke(packageAdapter, "getMoney", new Class[]{int.class, float.class}, params);

                params[0] = "test";
                params[1] = 1;
                FlectUtil.invoke(packageAdapter, "getTip", new Class[]{String.class, int.class}, params);

                params[0] = "test";
                params[1] = 0;
                FlectUtil.invoke(packageAdapter, "getTip", new Class[]{String.class, int.class}, params);

                params = new Object[1];
                params[0] = "000100000000001";
                FlectUtil.invoke(packageAdapter, "getContentNames", new Class[]{String.class}, params);
            }
        }
        Log.v(TAG, "test06Package >>>>>"); 
    }

    private PackageActivity entryPackage(Instrumentation inst, Solo solo) {
        gobackToMain(solo);
        CMMBUITestUtils.expectNoDialog(solo, mMainActivity);
        ActivityMonitor am = new ActivityMonitor(PackageActivity.class.getName(), null, false);
        inst.addMonitor(am);
        solo.clickOnMenuItem(mMainActivity.getString(R.string.package_management));
        PackageActivity pack = (PackageActivity)inst.waitForMonitorWithTimeout(am, 3000);
        assertNotNull(pack);
        setActivity(pack);

        return pack;
    }

    private void gobackToMain(Solo solo) {
        while(false == CMMBUITestUtils.isExpectedActivity(mMainActivity)) {
            CMMBUITestUtils.expectNoDialog(solo, mMainActivity);
            solo.goBack();
            CMMBUITestUtils.sleepThread(1000);
        }
    }
}
