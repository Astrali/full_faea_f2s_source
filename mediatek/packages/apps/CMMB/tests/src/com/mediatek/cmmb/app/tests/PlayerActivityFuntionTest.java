/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.cmmb.app.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;
import com.mediatek.cmmb.app.ChannelListManager;
import com.mediatek.cmmb.app.ChannelListManager.Channel;
import com.mediatek.cmmb.app.MainScreen;
import com.mediatek.cmmb.app.ModeSwitchManager;
import com.mediatek.cmmb.app.PlayerActivity;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class PlayerActivityFuntionTest extends ActivityInstrumentationTestCase2<PlayerActivity> {

    private static final String TAG = "CMMB::PlayerActivityFuntionTest";
    private static final int STATE_OPENED = 2;
    private static final int STATE_STARTED = 3;
    
    private static final int STATE_UPDATING = 1;
    
    private static final int CMMB_MODE_MBBMS = 2;  
    
    private static final int POSITION_GOTO_PROGRAM_LIST = 1;
    private static final int POSITION_GOTO_EM_LIST = 2;
    private static final int POSITION_FULL_SCREEN = 3;
    //private static final int POSITION_SHOW_IMD_AMOUNT = 4;
    
    private PlayerActivity mActivity;
    private MainScreen mMainmenu;
    Instrumentation mInstrumentation;
    private Solo mSolo;
    private ArrayList<Channel> mList; 
    private View mControlPanel;
    
    public PlayerActivityFuntionTest() {
        super("com.mediatek.cmmb.app", PlayerActivity.class);
    }
    
    public void setUp() throws Exception {
        Log.d(TAG,"setUp>>");
        super.setUp();
        mInstrumentation = getInstrumentation();
        mInstrumentation.setInTouchMode(false);         
        setActivityInitialTouchMode(false);          
        mActivity = getMyActivity();
        if (mActivity == null) {
            Log.d(TAG, "no player activity can test");
            return ;
        }
        mActivity.getApplicationContext();
        mSolo = new Solo(getInstrumentation(), getActivity());
        mInstrumentation.waitForIdleSync();
        Thread.sleep(2000);
        Log.d(TAG, "setUp <<");
    }
    
    public PlayerActivity getMyActivity() throws Exception {
        Activity a = null;
        
        ActivityMonitor playerMonitor = new ActivityMonitor(
                "com.mediatek.cmmb.app.PlayerActivity", null, false);
        mInstrumentation.addMonitor(playerMonitor);
        mMainmenu = launchActivity(
                mInstrumentation.getTargetContext().getPackageName(), MainScreen.class, null);
        setActivity(mMainmenu);
        try {
            checkSimModeAndSelect();
        } catch (Exception e) {
            Log.d(TAG, "getMyActivity exception e:" + e.getMessage());
        }
        mInstrumentation.waitForIdleSync();   
        Thread.sleep(300);
        waitAgain();
        
        ListView listView = (ListView) mMainmenu.findViewById(com.mediatek.cmmb.app.R.id.list_all);
        if (listView == null || listView.getCount() <= 0) {
           Log.d(TAG, "set up listView is null");
           return (PlayerActivity)a;
        }
 
       // assertTrue((listView != null) && (listView.getCount() > 0));
        ListAdapter listAdapter = listView.getAdapter();  
        for (int i = 0; i < listView.getCount(); i++) {           
            View view = listAdapter.getView(i, null, listView);
            getChannelListValue();
            ChannelListManager.Channel ch = mList.get(i);
            if (ch.isFree()) {
                Log.d(TAG, "Play free" + ch.toString());
                //sl.clickOnText(channelName);
                for (int j = 0; j <= i; j++) {
                    sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
                }
                sendKeys(KeyEvent.KEYCODE_DPAD_CENTER);
                mInstrumentation.waitForIdleSync();
                a = playerMonitor.waitForActivityWithTimeout(5000);
                if (a == null) {
                   Log.d(TAG, "activity failed");
                 }
                //assertTrue(a != null);  
                setActivity(a);
                break;
            } 
        }
        return (PlayerActivity)a;
    }
    
    public void testCase01openChannel() {
        Log.d(TAG,"testCase01_openChannel");
        
        mInstrumentation.waitForIdleSync();
        try {
            Field fState = PlayerActivity.class.getDeclaredField("mState");
            fState.setAccessible(true);
            int state = (Integer)fState.get(mActivity);
            if (state == STATE_OPENED || state == STATE_STARTED) {
                Log.d(TAG, "state Pass");
            } else {
                Log.d(TAG, "state failed");
            }
            
        } catch (Exception e) {        
            Log.d(TAG, "open channel exception e:" + e.getMessage());
        }    
        Log.d(TAG,"testCase01_openChannel <<");
    }

    public void testCase02playlock() {
        Log.d(TAG,"testCase02_playlock >> ");
        View playListButton = mActivity.findViewById(com.mediatek.cmmb.app.R.id.channel_list);
        showcontrolPanel();
        clickView(playListButton);
        try {
        Thread.sleep(2000);
        } catch (Exception e) {
            Log.d(TAG, "Exception e:" + e.getMessage());
        }
        ListView listView = (ListView) mActivity.findViewById(com.mediatek.cmmb.app.R.id.channel_list_in_player);
        assertTrue((listView != null) && (listView.getCount() > 0));
        ListAdapter listAdapter = listView.getAdapter();
        Log.d(TAG, "fist visible view item :" + listView.getFirstVisiblePosition());
        View view = listAdapter.getView(listView.getFirstVisiblePosition(), null, listView);        
        TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.player_channel_name);
        ImageView encrptedView = (ImageView)view.findViewById(com.mediatek.cmmb.app.R.id.player_encryped_indicator); 
        String channelName = textView.getText().toString();
        Log.d(TAG, "playfromlistname:" + channelName);
        try {  
        Thread.sleep(2000);
        if (encrptedView.getVisibility() == View.VISIBLE) {
            mSolo.clickOnText(channelName);
        } else {
            mSolo.clickOnScreen(50, 50);
        }
        Thread.sleep(5000);
        } catch (Exception e) {
            Log.d(TAG, "Exception e:" + e.getMessage());
        }
        Log.d(TAG,"testCase02_playlock << ");     
    }
    
    public void testCase03subcribe() {
        Log.d(TAG,"testCase03_subcribe >> ");  
        View playListButton = mActivity.findViewById(com.mediatek.cmmb.app.R.id.channel_list);
        showcontrolPanel();
        clickView(playListButton);
        try {
        Thread.sleep(2000);
        } catch (Exception e) {
            Log.d(TAG, "Exception e:" + e.getMessage());
        }
        ListView listView = (ListView) mActivity.findViewById(com.mediatek.cmmb.app.R.id.channel_list_in_player);
        assertTrue((listView != null) && (listView.getCount() > 0));
        ListAdapter listAdapter = listView.getAdapter();
        Log.d(TAG, "fist visible view item :" + listView.getFirstVisiblePosition());        
        View view = listAdapter.getView(listView.getFirstVisiblePosition(), null, listView);        
        TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.player_channel_name);
        ImageView encrptedView = (ImageView)view.findViewById(com.mediatek.cmmb.app.R.id.player_encryped_indicator); 
        String channelName = textView.getText().toString();
        Log.d(TAG, "subcribe:" + channelName);
        try {  
        Thread.sleep(2000);
        if (encrptedView.getVisibility() == View.VISIBLE) {
            mSolo.clickOnText(channelName);
            Thread.sleep(500);
            if (getMode() == CMMB_MODE_MBBMS) {
                Field subdlg = PlayerActivity.class.getDeclaredField("mDlgNeedSubScrib");
                subdlg.setAccessible(true);
                View needSubDlg = (View)subdlg.get(mActivity);
                if (needSubDlg.getVisibility() == View.VISIBLE) {
                    TextView submsg = (TextView)needSubDlg.findViewById(
                            com.mediatek.cmmb.app.R.id.dlg_needsubscrib_message);
                    String msg = submsg.getText().toString();
                    mSolo.clickOnText(msg); //unsub, back
                }
            }
            Thread.sleep(2000);
        } else {
            mSolo.clickOnScreen(50, 50);
        }
        
        } catch (Exception e) {
            Log.d(TAG, "Exception e:" + e.getMessage());
        }
        Log.d(TAG,"testCase03_subcribe << ");  
    }
   /* 
    public void testCase04_EMBlowLevel() { 
        Log.d(TAG,"testCase04_playPrevious >> ");  
        View emb = mActivity.findViewById(com.mediatek.cmmb.app.R.id.unread_eb);
        if (emb.getVisibility() == View.VISIBLE) {
            
        } else {
            Log.d(TAG, " no unread emb")
        }
        Log.d(TAG,"testCase04_playPrevious << ");  
    }
*/
    public void testCase05mute() throws Exception {
        Log.d(TAG, "testCase05_mute >>");
        mInstrumentation.waitForIdleSync();  
        Field fState  = PlayerActivity.class.getDeclaredField("mMuted");
        fState.setAccessible(true); 
        boolean muted = fState.getBoolean(mActivity);
        Log.d(TAG, "muted: " + muted);
        ImageView imgView ;        
        try {
            Field mutebtn = PlayerActivity.class.getDeclaredField("mMuteButton");
            assertNotNull(mutebtn);
            mutebtn.setAccessible(true);
            imgView = (ImageView)mutebtn.get(mActivity);
            showcontrolPanel();
            clickView(imgView);             
            Log.d(TAG, "mute change value:" + (Boolean)fState.get(mActivity));
            Thread.sleep(2000);
            showcontrolPanel();
            clickView(imgView);
        } catch (Exception e) {
            Log.d(TAG, "exception e:" + e.getMessage());
        }
        Thread.sleep(2000);

        Log.d(TAG, "mute change value:" + (Boolean)fState.get(mActivity));
        Log.d(TAG, "testCase05_mute <<");
    }
    
    public void testCase06EMB() {
        Log.d(TAG, "testCase06_EMB >>");
        invokeOptionMenuByOrder(POSITION_GOTO_EM_LIST);
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            Log.d(TAG, "sleep exception e:" + e.getMessage());
        }
        mSolo.goBack();
        mSolo.sleep(2000);
        Log.d(TAG, "testcase go to program list");
        invokeOptionMenuByOrder(POSITION_GOTO_PROGRAM_LIST);
        mSolo.sleep(2000);
        mSolo.goBack();
        Log.d(TAG, "testCase06EMB <<");
    }
    
    public void testCase07PlayfromList() {
        Log.d(TAG, "testCase07_PlayCurrfromList >>");
        View playListButton = mActivity.findViewById(com.mediatek.cmmb.app.R.id.channel_list);
        showcontrolPanel();
        clickView(playListButton);
        try {
        Thread.sleep(2000);
        } catch (Exception e) {
            Log.d(TAG, "Exception e:" + e.getMessage());
        }
        ListView listView = (ListView) mActivity.findViewById(com.mediatek.cmmb.app.R.id.channel_list_in_player);
        assertTrue((listView != null) && (listView.getCount() > 0));
        ListAdapter listAdapter = listView.getAdapter();
        Log.d(TAG, "fist visible view item :" + listView.getFirstVisiblePosition());
        View view = listAdapter.getView(listView.getFirstVisiblePosition(), null, listView);        
        TextView textView = (TextView) view.findViewById(com.mediatek.cmmb.app.R.id.player_channel_name);
        String channelName = textView.getText().toString();
        Log.d(TAG, "playfromlistname:" + channelName);
        try {  
        Thread.sleep(2000);
        mSolo.clickOnText(channelName);
        Thread.sleep(5000);
        } catch (Exception e) {
            Log.d(TAG, "Exception e:" + e.getMessage());
        }
        
        Log.d(TAG, "testCase07_PlayCurrfromList <<");
    }
    
    public void testCase08playStop() {
        Log.d(TAG, "testCase08_playStop >>");
        ImageView imgView; 
        try {
            Field psbtn = PlayerActivity.class.getDeclaredField("mPlayStopButton");
            assertNotNull(psbtn);
            psbtn.setAccessible(true);
            imgView = (ImageView)psbtn.get(mActivity);
            showcontrolPanel();
            clickView(imgView);
        } catch (Exception e) {
            Log.d(TAG, "Exception e:" + e.getMessage());
        }        
        
        try {
        Thread.sleep(5000);
        } catch (Exception e) {
            Log.d(TAG, "exception e:" + e.getMessage());
        }
        Log.d(TAG, "testCase08_playStop <<");
    }
  
    /*
     * (non-Javadoc)
     * @see android.test.ActivityInstrumentationTestCase2#tearDown()
     */
    private void checkSimModeAndSelect() {
        Log.d(TAG, "CheckSimMode >>");  

        try {
            if (getMode() != CMMB_MODE_MBBMS) {
                Log.d(TAG, "show CMMB confirm log");
                mInstrumentation.waitForIdleSync();            
                // click OK button of save recording dialog
                mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
                mInstrumentation.waitForIdleSync();
                return ;  
            }
            
            Field fSelectSimDialog = MainScreen.class.getDeclaredField("mSelectSimDlg");
            if (fSelectSimDialog != null) {
                Log.d(TAG, "show select sim log");
                mInstrumentation.waitForIdleSync();            
                // click OK button of save recording dialog
                mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
                mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
                mInstrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
                mInstrumentation.waitForIdleSync();
                return ;        
            }    
        } catch (NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }

    private int getMode() {
        Log.d(TAG, "GetMode");
        int mode = 0;
        try {
        Field modeTemp = MainScreen.class.getDeclaredField("mModeSwitchManager");
        assertNotNull(modeTemp);
        modeTemp.setAccessible(true);
        mode = ((ModeSwitchManager)modeTemp.get(mMainmenu)).getMode();
        }  catch (Exception e) {
            Log.d(TAG, " error happened e:" + e.getMessage());  
        } 
        Log.d(TAG, "Get Mode :<<" + mode);
        return mode;        
    }
    
    private void getChannelListValue() {
        try {
            Field mF  = mMainmenu.getClass().getDeclaredField("mChannelListManager");       
            assertNotNull(mF);
            mF.setAccessible(true);  
        
            mList = ((ChannelListManager)mF.get(mMainmenu)).getChannelList();
        } catch (Exception e) {
            Log.d(TAG, "getchannellistvalue exception e:" + e.getMessage());
        }
        Log.d(TAG, "getChannelListValue list:" + mList); 
    }
    
    private void showcontrolPanel() {
        Log.d(TAG, "show control panel >>");
        try {
            Field cp = mActivity.getClass().getDeclaredField("mControlPanel");
            assertNotNull(cp);
            cp.setAccessible(true);
       
            mControlPanel = (View)cp.get(mActivity);
        } catch (Exception e) {
            Log.d(TAG, "showcontrol exception e" + e.getMessage());
        }
        if (mControlPanel.getVisibility() == View.VISIBLE) {
            Log.d(TAG, "show control panel >>  >>");
            return ;
        }
        mSolo.clickOnScreen(100, 100);
        mInstrumentation.waitForIdleSync();
       
        try {
            Thread.sleep(300);
        } catch (Exception e) {
            Log.d(TAG, "exception e:" + e.getMessage());
        }
        Log.d(TAG, "show control panel <<");
    }
    
   
    private void clickView(final View view) {
        try {
        runTestOnUiThread(new Runnable() {
            public void run() {                
                Log.d(TAG,"clickView view = " + view);     
                view.performClick();
            }
        });
        } catch (Throwable e) {
            Log.d(TAG, "click view throwable e" + e.getMessage());
        }
        mInstrumentation.waitForIdleSync();
    }   
    
  
    private void invokeOptionMenuByOrder(int order) {
        Log.d(TAG, "invokeOptionMenuByOrder order:" + order);
        switch (order) {
            case POSITION_GOTO_PROGRAM_LIST:
                mInstrumentation.invokeMenuActionSync(mActivity, POSITION_GOTO_EM_LIST, 0);
                break;            
            case POSITION_GOTO_EM_LIST:
                mInstrumentation.invokeMenuActionSync(mActivity, POSITION_GOTO_EM_LIST, 0);
                break;
            case POSITION_FULL_SCREEN:
                mInstrumentation.invokeMenuActionSync(mActivity, POSITION_FULL_SCREEN, 0);
                break;
            default:
                Log.d(TAG,"Strange menu is selected.order = " + order);
                break;
        }
        mInstrumentation.waitForIdleSync();    
    }
    
    private int getChannelListState() throws Exception {
        Field fState = MainScreen.class.getDeclaredField("mState");
        fState.setAccessible(true);
        int state = (Integer)fState.get(mMainmenu);
        Log.d(TAG, "state:" + state);
        return state;
    }
    
    private void waitAgain() throws Exception {
        do {
           Log.d(TAG, "wait again");
        Thread.sleep(5000);
    } while (getChannelListState() == STATE_UPDATING);        
    }
    
    @Override
    public void tearDown() throws Exception {    
        Log.d(TAG,"tearDown >>");
        if (mActivity != null) {
            mActivity.finish();
            mInstrumentation.waitForIdleSync();     
            Thread.sleep(2000);  
            if (mMainmenu != null) {
                mMainmenu.finish();
                mInstrumentation.waitForIdleSync();     
                Thread.sleep(2000);
            }
        }
        super.tearDown();
        Log.d(TAG,"tearDown <<");
    }
    
}
