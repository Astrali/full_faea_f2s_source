
package com.mediatek.bluetooth.hid;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.IBluetoothHid;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.test.ServiceTestCase;
import android.util.Log;

import com.mediatek.bluetooth.BluetoothUnitTestJni;
import com.mediatek.bluetooth.Reflector;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class HidServiceTestCase extends ServiceTestCase<BluetoothHidService> {

    private static final String TAG = "[BT][Hid][HidServiceTestCase]";

    private static final boolean DEBUG = true;

    private boolean mIsBluetoothOn = false;

    private BluetoothUnitTestJni mJniManager = null;

    private Context mTestContext;

    private IBluetoothHid mService;

    private BluetoothHidService mHidService;

    private BluetoothDevice mDevice;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = IBluetoothHid.Stub.asInterface(service);
            printLog("onServiceConnected is called");
        }

        public void onServiceDisconnected(ComponentName className) {
            mService = null;
            printLog("onServiceDisconnected is called");
        }
    };

    @Override
    protected void setUp() throws Exception {
        // TODO Auto-generated method stub
        super.setUp();
        BluetoothHidService.sUtState = true;
        BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
        if (ba != null && !ba.isEnabled()) {
            ba.enable();
            sleep(4000);
        }
        if (ba != null && ba.isEnabled()) {
            mIsBluetoothOn = true;
        }
        mTestContext = this.getContext();
        mJniManager = new BluetoothUnitTestJni();
    }

    @Override
    protected void tearDown() throws Exception {
        // TODO Auto-generated method stub
        mIsBluetoothOn = false;
        super.tearDown();
    }

    public HidServiceTestCase() {
        super(BluetoothHidService.class);
        // TODO Auto-generated constructor stub
    }

    public void test01StartService() {
        if (mIsBluetoothOn) {
            Intent intent = new Intent(this.getContext(), BluetoothHidService.class);
            mTestContext.startService(intent);
            Log.i(TAG, "betty:testcase01 startService success");
            sleep(2000);
            mTestContext.stopService(intent);
            sleep(2000);
        } else {
            Log.i(TAG, "betty:testcase01 BT is off");
        }
    }

    public void test02BindService() {
        String[] jniLogs = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Req;0;false;serverDeactivateReqNative;void;0", "Req;0;false;wakeupListenerNative;void;0",
                "Req;0;false;stopListentoSocketNative;void;0", "Req;0;false;cleanServiceNative;void;0"
        };
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn) {
            Intent tmpIntent = new Intent(IBluetoothHid.class.getName());
            if (mTestContext.bindService(tmpIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                Log.i(TAG, "betty:[test02BindService]bind service success");
            }
            sleep(4000);
            mTestContext.unbindService(mConnection);
            sleep(4000);
        } else {
            Log.i(TAG, "BT is off");
        }
    }

    public void test03Enable1() {
        //case description:1 service enable,2 return success ,
        String jniLogs[] = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Cb;1;false;sendServiceMsg;void;2;0;7C:ED:8D:68:48:E8",
                "Req;0;false;serverDeactivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;2;7C:ED:8D:68:48:E8",
                "Req;0;false;wakeupListenerNative;void;0", "Req;0;false;stopListentoSocketNative;void;0",
                "Req;0;false;cleanServiceNative;void;0"
        };
        printLog("run test03Enable1");
        sleep(100);
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn) {
            Intent tmpIntent = new Intent(this.getContext(), BluetoothHidService.class);
            mTestContext.startService(tmpIntent);
            sleep(2000);
            mTestContext.stopService(tmpIntent);
            sleep(2000);
        } else {
            assertTrue(mIsBluetoothOn);
            Log.d(TAG, "mIsBluetoothOn is false");
        }
    }

    public void test04Enable2() {
        int waitingTime = 4000;
        
         // case description: 1 service enable, 2 return fail,
         
        String jniLogs[] = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;1;7C:ED:8D:68:48:E8",
                "Req;0;false;serverDeactivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;2;7C:ED:8D:68:48:E8",
                "Req;0;false;wakeupListenerNative;void;0", "Req;0;false;stopListentoSocketNative;void;0",
                "Req;0;false;cleanServiceNative;void;0"
        };
        printLog("run test04Enable2");
        sleep(100);
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn) {
            Intent tmpIntent = new Intent(this.getContext(), BluetoothHidService.class);
            mTestContext.startService(tmpIntent);
            sleep(waitingTime);
            mTestContext.stopService(tmpIntent);
            sleep(waitingTime);
        } else {
            assertTrue(mIsBluetoothOn);
            Log.d(TAG, "mIsBluetoothOn is false");
        }
    }

    public void test05Disable1() {
        /*
         * case description:1 service bond, 2 unbind service , 3 disable success
         */
        String jniLogs[] = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;0;7C:ED:8D:68:48:E8",
                "Req;0;false;serverDeactivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;2;7C:ED:8D:68:48:E8",
                "Req;0;false;wakeupListenerNative;void;0", "Req;0;false;stopListentoSocketNative;void;0",
                "Req;0;false;cleanServiceNative;void;0"
        };
        printLog("run test05Disable1");
        sleep(1000);
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn && mService == null) {
            Intent tmpIntent = new Intent(IBluetoothHid.class.getName());
            if (mTestContext.bindService(tmpIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                printLog("test06 bindSerivce Success");
            }
            bindTimeOut();
            mTestContext.unbindService(mConnection);
            printLog("unbind HidService success");
            sleep(2000);
        } else {
            assertTrue(mIsBluetoothOn);
            Log.d(TAG, "mIsBluetoothOn false");
        }
    }

    public void test06Disable2() {
        /*
         * case description:1 service bond,2 unbind service ,3 disable fail
         */
        String jniLogs[] = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;0;7C:ED:8D:68:48:E8",
                "Req;0;false;serverDeactivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;3;7C:ED:8D:68:48:E8",
                "Req;0;false;wakeupListenerNative;void;0", "Req;0;false;stopListentoSocketNative;void;0",
                "Req;0;false;cleanServiceNative;void;0"
        };
        printLog("run test06Disable2");
        sleep(1000);
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn && mService == null) {
            Intent tmpIntent = new Intent(IBluetoothHid.class.getName());
            if (mTestContext.bindService(tmpIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                printLog("test06 bindSerivce Success");
            }
            bindTimeOut();
            mTestContext.unbindService(mConnection);
            printLog("unbind HidService success");
            sleep(2000);
        } else {
            assertTrue(mIsBluetoothOn);
            Log.d(TAG, "mIsBluetoothOn false");
        }
    }

    public void test07Connect1() throws Exception {
        /*
         * case description(from settings): 1 turn on BT, 2 connect Hid device ,
         * 3 connect success,
         */
        String jniLogs[] = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;0;7C:ED:8D:68:48:E8",
                "Req;0;false;serverConnectReqNative;void;1;7C:ED:8D:68:48:E8",
                "Cb;0;false;sendServiceMsg;void;2;4;7C:ED:8D:68:48:E8",
                "Req;0;false;serverDeactivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;2;7C:ED:8D:68:48:E8",
                "Req;0;false;wakeupListenerNative;void;0", "Req;0;false;stopListentoSocketNative;void;0",
                "Req;0;false;cleanServiceNative;void;0"
        };
        printLog("run test07Connect1");
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn && mService == null) {
            Intent tmpIntent = new Intent(IBluetoothHid.class.getName());
            // Class[] argType = new Class[] {
            // BluetoothDevice.class
            // };
            if (mTestContext.bindService(tmpIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                printLog("test07 bindSerivce Success");
            }
            bindTimeOut();
            try {
                constructBluetoothDevice();
                mService.connect(mDevice);
                sleep(2000);
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
            Log.i(TAG, "connect HidService success");
            mTestContext.unbindService(mConnection);
        } else {
            assertTrue(mIsBluetoothOn);
            Log.d(TAG, "mIsBluetoothOn false");
        }
        sleep(2000);
    }

    public void test08Connect2() throws Exception {
        /*
         * case description(from settings): 1 turn on BT, 2 connect Hid device ,
         * 3 connect fail,
         */
        String jniLogs[] = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;0;7C:ED:8D:68:48:E8",
                "Req;0;false;serverConnectReqNative;void;1;7C:ED:8D:68:48:E8",
                "Cb;0;false;sendServiceMsg;void;2;5;7C:ED:8D:68:48:E8",
                "Req;0;false;serverDeactivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;2;7C:ED:8D:68:48:E8",
                "Req;0;false;wakeupListenerNative;void;0", "Req;0;false;stopListentoSocketNative;void;0",
                "Req;0;false;cleanServiceNative;void;0"
        };
        printLog("betty:run test08Connect2");
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn && mService == null) {
            Intent tmpIntent = new Intent(IBluetoothHid.class.getName());
            // Class[] argType = new Class[] {
            // BluetoothDevice.class
            // };
            if (mTestContext.bindService(tmpIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                printLog("testConnect bindSerivce Success");
            }
            bindTimeOut();
            try {
                constructBluetoothDevice();
                mService.connect(mDevice);
                sleep(2000);
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
            printLog("connect HidService success");
            mTestContext.unbindService(mConnection);
        } else {
            assertTrue(mIsBluetoothOn);
            Log.d(TAG, "mIsBluetoothOn false");
        }
        sleep(2000);
    }

    public void test09Disconnect1()throws Exception {
        // int waitingTime = 2000;
        /*
         * case description: 1 connect Hid device success, 2 disconnect Hid
         * device success ,3 btmtk_hidh_handle_disconnect_cnf,
         */
        String jniLogs[] = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;0;7C:ED:8D:68:48:E8",
                "Req;0;false;serverConnectReqNative;void;1;7C:ED:8D:68:48:E8",
                "Cb;0;false;sendServiceMsg;void;2;4;7C:ED:8D:68:48:E8",
                "Req;0;false;serverDisconnectReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;6;7C:ED:8D:68:48:E8"
        };
        printLog("run test09Disconnect1");
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn && mService == null) {
            Intent tmpIntent = new Intent(IBluetoothHid.class.getName());
            if (mTestContext.bindService(tmpIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                printLog("test09Disconnect1 bindSerivce Success");
            }
            bindTimeOut();
            try {
                constructBluetoothDevice();
                mService.connect(mDevice);
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
            printLog("connect HidService success");
            sleep(2000);
            try {
                mService.disconnect(mDevice);
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
            printLog("test09Disconnect1 Disconnect success");
            sleep(2000);
            mTestContext.unbindService(mConnection);
        } else {
            assertTrue(mIsBluetoothOn);
            Log.d(TAG, "mIsBluetoothOn false");
        }
        sleep(2000);
    }

    public void test10Disconnect2()throws Exception {
        /*
         * case description: 1 connect Hid device success, 2 disconnect Hid
         * device fail ,3 btmtk_hidh_handle_disconnect_cnf,
         */
        String jniLogs[] = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;0;7C:ED:8D:68:48:E8",
                "Req;0;false;serverConnectReqNative;void;1;7C:ED:8D:68:48:E8",
                "Cb;0;false;sendServiceMsg;void;2;4;7C:ED:8D:68:48:E8",
                "Req;0;false;serverDisconnectReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;7;7C:ED:8D:68:48:E8"
        };
        printLog("betty:run test10Disconnect2");
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn && mService == null) {
            Intent tmpIntent = new Intent(IBluetoothHid.class.getName());

            if (mTestContext.bindService(tmpIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                printLog("test10Disconnect2 bindSerivce Success");
            }
            bindTimeOut();
            try {
                constructBluetoothDevice();
                mService.connect(mDevice);
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
            printLog("test10Disconnect2 connect HidService success");
            sleep(2000);
            try {
                mService.disconnect(mDevice);
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
            printLog("test10Disconnect2 Disconnect HidService fail");
            sleep(2000);
            mTestContext.unbindService(mConnection);
        } else {
            assertTrue(mIsBluetoothOn);
            Log.d(TAG, "mIsBluetoothOn false");
        }
        sleep(2000);
    }

    public void test11reflectReceiver1() throws Exception {
        printLog("test11_reflectReceiver1");
        BroadcastReceiver receiver;
        Intent tmpIntent = new Intent(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        Bundle bundle = new Bundle();
        constructBluetoothDevice();
        bundle.putParcelable(BluetoothDevice.EXTRA_DEVICE, mDevice);
        try {
            mHidService = new BluetoothHidService();
            receiver = (BroadcastReceiver) Reflector.get(mHidService, "mReceiver");
            tmpIntent.putExtras(bundle);
            receiver.onReceive(mTestContext, tmpIntent);
        } catch (NoSuchFieldException e) {
           e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public void test12reflectReceiver2() throws Exception {
        printLog("test12_reflectReceiver2");
        final int notify = 11;
        Map notifyMap;
        BroadcastReceiver receiver;
        Intent tmpIntent = new Intent(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        Bundle bundle = new Bundle();
        constructBluetoothDevice();
        bundle.putParcelable(BluetoothDevice.EXTRA_DEVICE, mDevice);
        bundle.putInt(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.BOND_NONE);
        try {
            mHidService = new BluetoothHidService();
            receiver = (BroadcastReceiver) Reflector.get(mHidService, "mReceiver");
            notifyMap = (Map) Reflector.get(mHidService, "mNotifyMap");
            notifyMap.put("7C:ED:8D:68:48:E8", notify);
            tmpIntent.putExtras(bundle);
            receiver.onReceive(mTestContext, tmpIntent);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public void test13reflectReceiver3() throws Exception {
        printLog("test13_reflectReceiver3");
        final int notify = 11;
        Map notifyMap;
        Map<String, String> stateMap;
        BroadcastReceiver receiver;
        Intent tmpIntent = new Intent(BluetoothDevice.ACTION_NAME_CHANGED);
        Bundle bundle = new Bundle();
        constructBluetoothDevice();
        bundle.putParcelable(BluetoothDevice.EXTRA_DEVICE, mDevice);
        try {
            mHidService = new BluetoothHidService();
            receiver = (BroadcastReceiver) Reflector.get(mHidService, "mReceiver");
            notifyMap = (Map) Reflector.get(mHidService, "mNotifyMap");
            stateMap = (HashMap<String, String>) Reflector.get(mHidService, "mStateMap");
            notifyMap.put("7C:ED:8D:68:48:E8", notify);
            stateMap.put("7C:ED:8D:68:48:E8", "connected");
            tmpIntent.putExtras(bundle);
            if (mTestContext.bindService(tmpIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                bindTimeOut();
                receiver.onReceive(mTestContext, tmpIntent);
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } finally {
            if (mConnection != null) {
                mTestContext.unbindService(mConnection);
            }
        }
    }

    public void test14getStateDevice() throws Exception {
        /*
         * case description(from settings): 1 turn on BT, 2 connect Hid device ,
         * 3 connect success,
         */
        String jniLogs[] = {
                "Req;0;false;initServiceNative;void;0", "Req;0;false;serverActivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;0;7C:ED:8D:68:48:E8",
                "Req;0;false;serverDeactivateReqNative;void;0",
                "Cb;0;false;sendServiceMsg;void;2;2;7C:ED:8D:68:48:E8",
                "Req;0;false;wakeupListenerNative;void;0", "Req;0;false;stopListentoSocketNative;void;0",
                "Req;0;false;cleanServiceNative;void;0"
        };
        printLog("test14getStateDevice");
        mJniManager.setCaseLog(jniLogs, ";");
        if (mIsBluetoothOn && mService == null) {
            Intent tmpIntent = new Intent(IBluetoothHid.class.getName());
//            Class[] argType = new Class[] {
//                BluetoothDevice.class
//            };
            if (mTestContext.bindService(tmpIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                printLog("test14getStateDevice bindSerivce Success");
            }
            bindTimeOut();
            try {
                constructBluetoothDevice();
                mService.getState(mDevice);
                sleep(1000);
                mService.getCurrentDevices();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            sleep(1000);
            mTestContext.unbindService(mConnection);
        } else {
            assertTrue(mIsBluetoothOn);
            Log.d(TAG, "mIsBluetoothOn false");
        }
    }

    private void bindTimeOut() {
        int timeout = 0;
        while (mService == null) {
            Log.d(TAG, "mService in while is null");
            if (timeout > 100) {
                Log.d(TAG, "timeout is out of bound,then return");
                return;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeout++;
        }
    }

    @SuppressWarnings("unchecked")
    private void constructBluetoothDevice() throws Exception {
        try {
            Constructor con = BluetoothDevice.class.getDeclaredConstructor(String.class);
            Log.i(TAG, "Constructor:" + con);
            con.setAccessible(true);
            mDevice = (BluetoothDevice) con.newInstance("7C:ED:8D:68:48:E8");
            Log.i(TAG, "BluetoothDevice:" + mDevice);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void printLog(String msg) {
        if (DEBUG) {
            Log.d(TAG, msg);
        }
    }
}
