package com.mediatek.camera.ext;

public class Op02CameraExtension extends CameraExtension {
    private static final String TAG = "Op02CameraExtension";
    private static final boolean LOG = true;

    private ISavingPath mSavingPath;
    
    @Override
    public ISavingPath getSavingPath() {
        if (mSavingPath == null) {
            mSavingPath = new MySavingPath();
        }
        return mSavingPath;
    }
    
    private class MySavingPath extends SavingPath {
        @Override
        public String getFilePath(int filetype) {
            switch(filetype) {
            case FILE_TYPE_VIDEO:
                return "/Video";
            default:
                return "/Photo";
            }
        }
    }
}
