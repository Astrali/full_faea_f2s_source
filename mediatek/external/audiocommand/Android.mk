LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := eng

ifeq ($(strip $(MTK_PLATFORM)),MT6589)
  LOCAL_CFLAGS += -DAUDIO_MT6589
endif

LOCAL_SRC_FILES:=       \
	audiocommand.cpp
	
LOCAL_C_INCLUDES=       \
       $(MTK_PATH_SOURCE)/frameworks/base/include/media

       
ifeq ($(strip $(MTK_PLATFORM)),MT6589)
LOCAL_C_INCLUDES+=       \
    $(MTK_PATH_SOURCE)/platform/common/hardware/audio/aud_drv \
    $(MTK_PATH_SOURCE)/platform/common/hardware/audio/include \
    $(MTK_PATH_SOURCE)/platform/common/hardware/audio/speech_driver \
    $(MTK_PATH_PLATFORM)/hardware/audio/aud_drv \
    $(MTK_PATH_PLATFORM)/hardware/audio/include \
    $(MTK_PATH_PLATFORM)/hardware/audio/speech_driver \
    $(MTK_PATH_SOURCE)/external/aee/binary/inc
endif
       

LOCAL_SHARED_LIBRARIES := libcutils libutils libbinder libmedia libaudioflinger

ifeq ($(strip $(MTK_PLATFORM)),MT6589)
LOCAL_SHARED_LIBRARIES += libaudio.primary.default libaed
endif

LOCAL_MODULE:= audiocommand

include $(BUILD_EXECUTABLE)

