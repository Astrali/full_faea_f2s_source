
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := SouGouInput.apk
LOCAL_PACKAGE_NAME := SouGouInput.apk
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := EXECUTABLE
LOCAL_MODULE_PATH := $(TARGET_OUT_APPS)
LOCAL_SRC_FILES := $(LOCAL_MODULE)

include $(BUILD_PREBUILT)

ifeq ($(strip $(SouGouInput)),yes)
	include $(call all-makefiles-under,$(LOCAL_PATH))
endif
