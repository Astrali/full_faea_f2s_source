
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := Security.apk
LOCAL_PACKAGE_NAME := Security.apk
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := EXECUTABLE
LOCAL_MODULE_PATH := $(TARGET_OUT_APPS)
LOCAL_SRC_FILES := $(LOCAL_MODULE)

include $(BUILD_PREBUILT)

ifeq ($(strip $(LbeRootManager)),yes)
	include $(call all-makefiles-under,$(LOCAL_PATH))
endif
