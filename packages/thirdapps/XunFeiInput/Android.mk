
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := XunFeiInput.apk
LOCAL_PACKAGE_NAME := XunFeiInput.apk
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := EXECUTABLE
LOCAL_MODULE_PATH := $(TARGET_OUT_APPS)
LOCAL_SRC_FILES := $(LOCAL_MODULE)

include $(BUILD_PREBUILT)

ifeq ($(strip $(XunFeiInput)),yes)
	include $(call all-makefiles-under,$(LOCAL_PATH))
endif
