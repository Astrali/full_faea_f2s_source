/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.providers.drm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.drm.DrmEvent;
import android.drm.DrmInfo;
import android.drm.DrmManagerClient;
import android.drm.DrmManagerClient.OnEventListener;
import android.drm.DrmStore;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.internal.telephony.Phone;
import com.mediatek.common.featureoption.FeatureOption;

import java.net.InetAddress;
import java.net.UnknownHostException;

// when connection is available, sync secure timer
public class ConnectionChangeReceiver extends BroadcastReceiver {
    private static final String TAG = "DRM/ConnectionChangeReceiver";
    private static final String INVALID_DEVICE_ID = "000000000000000";

    private static Thread sThread = null; // the thread is static so that only one thread is running to do SNTP sync
    private static final String NETWORK_TYPE_MOBILE_NET =
            Phone.FEATURE_ENABLE_NET;

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d(TAG, "onReceive : CONNECTIVITY_CHANGE received.");
        if (FeatureOption.MTK_DRM_APP) {
            DrmManagerClient client = new DrmManagerClient(context);
            client.setOnEventListener(new CheckClockListener(context));

            // dummy data
            byte[] data = new byte[1];
            data[0] = 0;
            DrmInfo info =
                new DrmInfo(DrmStore.DrmInfoType.TYPE_DRM_CHECK_CLOCK,
                            data, DrmStore.DrmObjectMime.MIME_DRM_CONTENT);
            int result = client.processExtraDrmInfo(info);
        }
    }

    private class CheckClockListener implements OnEventListener {
        private Context mContext = null;

        public CheckClockListener(Context context) {
            mContext = context;
        }
        public void onEvent(DrmManagerClient client, DrmEvent event) {
            if (DrmStore.DrmInfoType.TYPE_DRM_CHECK_CLOCK == event.getType()) {
                String status = event.getMessage();
                Log.d(TAG, "CheckClockListener: Answer secure timer status: " + status);

                boolean isValid = status.equals("valid");
                if (isValid) {
                    Log.d(TAG, "CheckClockListener: Secure timer is already valid");
                    return;
                }
                if (null != mContext) {
                    ConnectionChangeReceiver.this.launchSNTP(mContext);
                }
            }
        }
    }

    private class DrmExEventListener implements OnEventListener {
        private Context mContext = null;

        public DrmExEventListener(Context context) {
            mContext = context;
        }

        public void onEvent(DrmManagerClient client, DrmEvent event) {
            switch (event.getType()) {
                case DrmStore.DrmInfoType.TYPE_DRM_LOAD_DEVICE_ID:
                    String status = event.getMessage();
                    Log.d(TAG, "DrmExEventListener: Answer device id: " + status);

                    // get an empty device id: the device id was not saved yet
                    if (status.isEmpty()) {
                        Log.d(TAG, "The device id is empty, try obtain it");
                        status = BootCompletedReceiver.deviceId(mContext);
                        Log.d(TAG, "Obtained device id: " + status);

                        // anyway, we need to save the device id (may be invalid value)
                        // so that the secure timer can be saved
                        // dummy data
                        byte[] data = new byte[1];
                        data[0] = 0;
                        DrmInfo info =
                            new DrmInfo(DrmStore.DrmInfoType.TYPE_DRM_SAVE_DEVICE_ID,
                                    data, DrmStore.DrmObjectMime.MIME_DRM_CONTENT);
                        info.put(DrmStore.DrmInfoType.KEY_DRM_SAVE_DEVICE_ID, status);
                        int result = client.processExtraDrmInfo(info);
                        break;
                    }

                    // we already have a device id: no matter if it's empty or not
                    if (status.equals(INVALID_DEVICE_ID)) {
                        Log.w(TAG, "The device id is an invalid value");
                    }

                    // just launch SNTP
                    if (null != mContext) {
                        ConnectionChangeReceiver.this.launchSimpleThread(mContext, client);
                    }
                    break;

                case DrmStore.DrmInfoType.TYPE_DRM_SAVE_DEVICE_ID:
                    Log.d(TAG, "DrmExEventListener: Device id saved.");
                    if (null != mContext) {
                        ConnectionChangeReceiver.this.launchSimpleThread(mContext, client);
                    }
                    break;

                default:
                    break;
            }
        }
    }

    private void launchSNTP(Context context) {
        if (null == sThread || (null != sThread && !sThread.isAlive())) {
            Log.d(TAG, "SNTP : the thread is not running.");

            // firstly we validate the device id
            DrmManagerClient client = new DrmManagerClient(context);
            client.setOnEventListener(new DrmExEventListener(context));

            // dummy data
            byte[] data = new byte[1];
            data[0] = 0;
            DrmInfo info =
                new DrmInfo(DrmStore.DrmInfoType.TYPE_DRM_LOAD_DEVICE_ID,
                        data, DrmStore.DrmObjectMime.MIME_DRM_CONTENT);
            int result = client.processExtraDrmInfo(info);
        } else {
            Log.d(TAG, "SNTP : the thread is already running.");
        }
    }

    private void launchSimpleThread(final Context context, final DrmManagerClient client) {
        Log.d(TAG, "SNTP : launch the thread.");

        // check if the network is usable
        final ConnectivityManager conManager =
            (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null == conManager) {
            Log.e(TAG, "SNTP : invalid connectivity manager.");
            return;
        }

        NetworkInfo networkInfo = conManager.getActiveNetworkInfo();
        if (null == networkInfo) {
            Log.e(TAG, "SNTP : invalid active network info.");
            return;
        }
        if (!networkInfo.isAvailable()) {
            Log.e(TAG, "SNTP : unavailable active network.");
            return;
        }

        final int type = networkInfo.getType();
        Log.d(TAG, "SNTP : active network type: " + type);

        sThread = new Thread(new Runnable() {
            public void run() {
                Log.d(TAG, "SNTP : the thread launched.");
                int result = checkRouteToHost(conManager, type);
                if (-1 != result) {
                    int oft = Ntp.sync(sHostList[result]);
                    Log.d(TAG, "SNTP: synchronization result, utc time offset: " + oft);

                    // dummy data
                    byte[] data = new byte[1];
                    data[0] = 0;
                    DrmInfo info =
                        new DrmInfo(DrmStore.DrmInfoType.TYPE_DRM_UPDATE_CLOCK,
                                data, DrmStore.DrmObjectMime.MIME_DRM_CONTENT);
                    info.put(DrmStore.DrmInfoType.KEY_DRM_UPDATE_CLOCK,
                            String.valueOf(oft));
                    int infoResult = client.processExtraDrmInfo(info);
                }
            }
        });
        sThread.start();
    }

    // modify these SNTP host servers, for different countries.
    private static String[] sHostList = new String[] {
        "hshh.org",
        "t1.hshh.org",
        "t2.hshh.org",
        "t3.hshh.org",
        "clock.via.net"
    };

    private int checkRouteToHost(ConnectivityManager conManager, int type) {
        Log.v(TAG, "==== check if there's available route to SNTP servers ====");

        int result = -1;
        if (conManager != null) {
            int size = sHostList.length;
            for (int i = 0; i < size; i++) {
                int address = 0;
                try {
                    Log.d(TAG, "get host address by name: [" + sHostList[i] + "].");
                    InetAddress addr = InetAddress.getByName(sHostList[i]);
                    address = ipToInt(addr.getHostAddress());
                } catch (UnknownHostException e) {
                    Log.e(TAG, "caught UnknownHostException");
                    continue;
                }

                Log.d(TAG, "request route for host: [" + sHostList[i] + "].");
                if (conManager.requestRouteToHost(type, address)) {
                    Log.d(TAG, "request route for host success.");
                    result = i;
                    break;
                }
                Log.d(TAG, "request route for host failed.");
            }
        }
        return result;
    }

    private int ipToInt(String ipAddress) {
        if (ipAddress == null) {
            return -1;
        }

        String[] addrArray = ipAddress.split("\\.");
        int size = addrArray.length;
        if (size != 4) {
            return -1;
        }

        int[] addrBytes = new int[size];
        try {
            for (int i = 0; i < size; i++) {
                addrBytes[i] = Integer.parseInt(addrArray[i]);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return -1;
        }

        Log.v(TAG, "ipToInt: a[0] = " + addrBytes[0]
                   + ", a[1] = " + addrBytes[1]
                   + ", a[2] = " + addrBytes[2]
                   + ", a[3] = " + addrBytes[3]);
        return ((addrBytes[3] & 0xff) << 24)
               | ((addrBytes[2] & 0xff) << 16)
               | ((addrBytes[1] & 0xff) << 8)
               | (addrBytes[0] & 0xff);
    }
}
