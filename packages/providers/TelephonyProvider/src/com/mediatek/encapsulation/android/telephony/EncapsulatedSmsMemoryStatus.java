
package com.mediatek.encapsulation.android.telephony;

import android.telephony.SmsMemoryStatus;
import android.os.Parcel;
import android.os.Parcelable;

import com.mediatek.encapsulation.EncapsulationConstant;

public class EncapsulatedSmsMemoryStatus extends SmsMemoryStatus {
    /** M: MTK ADD */
    public int mUsed;
    public int mTotal;

    /** M: MTK ADD */
    public EncapsulatedSmsMemoryStatus() {
        super();
        mUsed = 0;
        mTotal = 0;
    }

    /** M: MTK ADD */
    public EncapsulatedSmsMemoryStatus(int used, int total) {
        super(used, total);
        mUsed = used;
        mTotal = total;
    }

    /** M: MTK ADD */
    public int getUsed() {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return super.getUsed();
        } else {
            return mUsed;
        }
    }

    /** M: MTK ADD */
    public int getTotal() {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return super.getTotal();
        } else {
            return mTotal;
        }
    }

    /** M: MTK ADD */
    public int getUnused() {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return super.getUnused();
        } else {
            return (mTotal - mUsed);
        }
    }

    /** M: MTK ADD */
    public void reset() {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            super.reset();
        } else {
            mUsed = 0;
            mTotal = 0;
        }

    }

    /** M: MTK ADD */
    public int describeContents() {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return super.describeContents();
        } else {
            return 0;
        }

    }

    /** M: MTK ADD */
    public void writeToParcel(Parcel dest, int flags) {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            super.writeToParcel(dest, flags);
        } else {
            dest.writeInt(mUsed);
            dest.writeInt(mTotal);
        }

    }

    /** M: MTK ADD */
    public static final Parcelable.Creator<EncapsulatedSmsMemoryStatus> CREATOR =
                    new Parcelable.Creator<EncapsulatedSmsMemoryStatus>() {
        public EncapsulatedSmsMemoryStatus createFromParcel(Parcel source) {
            int used;
            int total;

            used = source.readInt();
            total = source.readInt();
            return new EncapsulatedSmsMemoryStatus(used, total);
        }

        public EncapsulatedSmsMemoryStatus[] newArray(int size) {
            return new EncapsulatedSmsMemoryStatus[size];
        }
    };

}