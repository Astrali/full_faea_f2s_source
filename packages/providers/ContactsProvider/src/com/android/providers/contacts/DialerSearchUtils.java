package com.android.providers.contacts;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.telephony.PhoneNumberUtils;

public class DialerSearchUtils {
    
    public static String computeNormalizedNumber(String number) {
        String normalizedNumber = null;
        if (number != null) {
            normalizedNumber = PhoneNumberUtils.getStrippedReversed(number);
        }
        return normalizedNumber;
    }
    
    public static String stripSpecialCharInNumberForDialerSearch(String number) {
        if (number == null)
            return null;
        int len = number.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            char c = number.charAt(i);
            if (PhoneNumberUtils.isNonSeparator(c)) {
                sb.append(c);
            } else if (c == ' ' || c == '-' || c == '(' || c == ')') {
                // strip blank and hyphen
            } else {
                /*
                 * Bug fix by Mediatek begin 
                 * CR ID: ALPS00293790 
                 * Description:
                 * Original Code: break;
                 */
                continue;
                /*
                 * Bug fix by Mediatek end
                 */
            }
        }
        return sb.toString();
    }
    
    public static Cursor queryPhoneLookupByNumber(SQLiteDatabase db, ContactsDatabaseHelper dbHelper,
            String number, String[] projection, String selection, String[] selectionArgs,
            String groupBy, String having, String sortOrder, String limit) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String numberE164 = PhoneNumberUtils.formatNumberToE164(number,
                dbHelper.getCurrentCountryIso());
        String normalizedNumber = PhoneNumberUtils.normalizeNumber(number);
        dbHelper.buildPhoneLookupAndContactQuery(qb, normalizedNumber, numberE164);
        qb.setStrict(true);
        boolean foundResult = false;
        Cursor c = qb.query(db, projection, selection, selectionArgs, groupBy, having,
                sortOrder, limit);
        try {
            if (c.getCount() > 0) {
                foundResult = true;
                return c;
            } else {
                qb = new SQLiteQueryBuilder();
                //dbHelper.buildMinimalPhoneLookupAndContactQuery(qb, normalizedNumber);
                // use the raw number instead of the normalized number because
                // phone_number_compare_loose in SQLite works only with non-normalized
                // numbers
                dbHelper.buildFallbackPhoneLookupAndContactQuery(qb, number);
                qb.setStrict(true);
            }
        } finally {
            if (!foundResult) {
                // We'll be returning a different cursor, so close this one.
                c.close();
            }
        }
        return qb.query(db, projection, selection, selectionArgs, groupBy, having,
                sortOrder, limit);
    }
}
