package com.mediatek.providers.contacts;

import android.content.Context;
import android.net.Uri;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.mediatek.providers.contacts.ContactsFeatureConstants.FeatureOption;

public class SimCardUtils {
    public static final String TAG = "ProviderSimCardUtils";

    public interface SimSlot {
        int SLOT_NONE = -1;
        int SLOT_SINGLE = 0;
        int SLOT_ID1 = com.android.internal.telephony.PhoneConstants.GEMINI_SIM_1;
        int SLOT_ID2 = com.android.internal.telephony.PhoneConstants.GEMINI_SIM_2;
    }

    public interface SimType {
        String SIM_TYPE_SIM_TAG = "SIM";
        int SIM_TYPE_SIM = 0;

        String SIM_TYPE_USIM_TAG = "USIM";
        int SIM_TYPE_USIM = 1;

        //UIM
        int SIM_TYPE_UIM = 2;
        String SIM_TYPE_UIM_TAG = "UIM";
        //UIM
    }
    
    public static class SimUri {
        public static final Uri ICCURI = Uri.parse("content://icc/adn/");   
        public static final Uri ICCURI1 = Uri.parse("content://icc/adn1/");
        public static final Uri ICCURI2 = Uri.parse("content://icc/adn2/");
        
        public static final Uri ICCUSIM1URI = Uri.parse("content://icc/pbr");
        public static final Uri ICCUSIMURI = Uri.parse("content://icc/pbr1/");
        public static final Uri ICCUSIM2URI = Uri.parse("content://icc/pbr2/");
        
        public static Uri getSimUri(int slotId) {
            boolean isUsim = isSimUsimType(slotId);
            if (FeatureOption.MTK_GEMINI_SUPPORT) {
                if (slotId == 0) {
                    return isUsim ? ICCUSIM1URI : ICCURI1;
                } else {
                    return isUsim ? ICCUSIM2URI : ICCURI2;
                }
            } else {
                return isUsim ? ICCUSIMURI : ICCURI;
            }
        }
    }
    
    
    public static boolean isSimInserted(int slotId) {
        final ITelephony iTel = ITelephony.Stub.asInterface(ServiceManager
                .getService(Context.TELEPHONY_SERVICE));
        boolean isSimInsert = false;
        try {
            if (iTel != null) {
                if (FeatureOption.MTK_GEMINI_SUPPORT) {
                    isSimInsert = iTel.isSimInsert(slotId);
                } else {
                    isSimInsert = iTel.isSimInsert(0);
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            isSimInsert = false;
        }
        return isSimInsert;
    }

    public static int getSimTypeBySlot(int slotId) {
        final ITelephony iTel = ITelephony.Stub.asInterface(ServiceManager
                .getService(Context.TELEPHONY_SERVICE));
        int simType = -1;
        try {
            if (FeatureOption.MTK_GEMINI_SUPPORT) {
                if (SimType.SIM_TYPE_USIM_TAG.equals(iTel.getIccCardTypeGemini(slotId))) {
                    simType = SimType.SIM_TYPE_USIM;
                } else if (SimType.SIM_TYPE_UIM_TAG.equals(iTel.getIccCardTypeGemini(slotId))) {
                    simType = SimType.SIM_TYPE_UIM;
                } else if (SimType.SIM_TYPE_SIM_TAG.equals(iTel.getIccCardTypeGemini(slotId))) {
                    simType = SimType.SIM_TYPE_SIM;
                }
            } else {
                if (SimType.SIM_TYPE_USIM_TAG.equals(iTel.getIccCardType())) {
                    simType = SimType.SIM_TYPE_USIM;
                } else if (SimType.SIM_TYPE_UIM_TAG.equals(iTel.getIccCardType())) {
                    simType = SimType.SIM_TYPE_UIM;
                } else if (SimType.SIM_TYPE_SIM_TAG.equals(iTel.getIccCardType())) {
                    simType = SimType.SIM_TYPE_SIM;
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "catched exception.");
            e.printStackTrace();
        }
        return simType;
    }
    
    public static boolean isSimUsimType(int slotId) {
        final ITelephony iTel = ITelephony.Stub.asInterface(ServiceManager
                .getService(Context.TELEPHONY_SERVICE));
        boolean isUsim = false;
        try {
            if (FeatureOption.MTK_GEMINI_SUPPORT) {
                if (SimType.SIM_TYPE_USIM_TAG.equals(iTel.getIccCardTypeGemini(slotId))) {
                    isUsim = true;
                }
            } else {
                if (SimType.SIM_TYPE_USIM_TAG.equals(iTel.getIccCardType())) {
                    isUsim = true;
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "catched exception.");
            e.printStackTrace();
        }
        return isUsim;
    }
    
}
