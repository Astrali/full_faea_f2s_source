package com.mediatek.qsb.ext;

import android.content.Context;
import android.app.SearchEngineInfo;

public interface IPreferenceSetting {
    SearchEngineInfo getDefaultSearchEngine(Context context);
}
