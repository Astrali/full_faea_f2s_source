package com.mediatek.browser.ext;

import android.content.Context;

import com.mediatek.pluginmanager.Plugin;
import com.mediatek.pluginmanager.PluginManager;

public class Extensions {
    private static IBrowserSmallFeatureEx sSmallFeaturePlugin = null;
    private static IBrowserDownloadEx sDownloadPlugin = null;
    private static IBrowserProcessNetworkEx sProcessNetworkPlugin = null;
    private static IBrowserSmsHandlerEx sSmsHandlerPlugin = null;

    public static IBrowserSmallFeatureEx getSmallFeaturePlugin(Context context) {
        if (sSmallFeaturePlugin == null) {
            try {
                sSmallFeaturePlugin = (IBrowserSmallFeatureEx)PluginManager.createPluginObject(context,
                        IBrowserSmallFeatureEx.class.getName());
            } catch (Plugin.ObjectCreationException e) {
                sSmallFeaturePlugin = new BrowserSmallFeatureEx(context);
            }
        }
        return sSmallFeaturePlugin;
    }

    public static IBrowserDownloadEx getDownloadPlugin(Context context) {
        if (sDownloadPlugin == null) {
            try {
                sDownloadPlugin = (IBrowserDownloadEx)PluginManager.createPluginObject(context,
                        IBrowserDownloadEx.class.getName());
            } catch (Plugin.ObjectCreationException e) {
                sDownloadPlugin = new BrowserDownloadEx();
            }
        }
        return sDownloadPlugin;
    }

    public static IBrowserProcessNetworkEx getProcessNetworkPlugin(Context context) {
        if (sProcessNetworkPlugin == null) {
            try {
                sProcessNetworkPlugin = (IBrowserProcessNetworkEx)PluginManager.createPluginObject(context,
                        IBrowserProcessNetworkEx.class.getName());
            } catch (Plugin.ObjectCreationException e) {
                sProcessNetworkPlugin = new BrowserProcessNetworkEx();
            }
        }
        return sProcessNetworkPlugin;
    }

    public static IBrowserSmsHandlerEx getSmsHandlerPlugin(Context context) {
        if (sSmsHandlerPlugin == null) {
            try {
                sSmsHandlerPlugin = (IBrowserSmsHandlerEx)PluginManager.createPluginObject(context,
                        IBrowserSmsHandlerEx.class.getName());
            } catch (Plugin.ObjectCreationException e) {
                sSmsHandlerPlugin = new BrowserSmsHandlerEx();
            }
        }
        return sSmsHandlerPlugin;
    }
}