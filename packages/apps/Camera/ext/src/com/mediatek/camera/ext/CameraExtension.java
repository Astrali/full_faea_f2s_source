package com.mediatek.camera.ext;

public class CameraExtension implements ICameraExtension {
    private ISavingPath mSavingPath;
    private IFeatureExtension mFeatureExtension;
    
    public ISavingPath getSavingPath() {
        if (mSavingPath == null) {
            mSavingPath = new SavingPath();
        }
        return mSavingPath;
    }

    public IFeatureExtension getFeatureExtension() {
        if (mFeatureExtension == null) {
            mFeatureExtension = new FeatureExtension();
        }
        return mFeatureExtension;
    }
}
