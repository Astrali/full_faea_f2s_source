package com.mediatek.camera.ext;

public interface ISavingPath {
    int FILE_TYPE_PHOTO = 0; //photo
    int FILE_TYPE_VIDEO = 1; //video
    int FILE_TYPE_PANO = 2; //panorama
    
    String getFilePath(int filetype);
}
