package com.mediatek.camera.ext;

public interface ICameraExtension {
    ISavingPath getSavingPath();
    IFeatureExtension getFeatureExtension();
}
