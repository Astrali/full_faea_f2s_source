/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.Activity;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.preference.CheckBoxPreference;
import android.content.ContentResolver;
import android.provider.Settings;
import android.util.Log;
import com.android.dfish.AWLedControl;

public class BreathLight extends SettingsPreferenceFragment {
	
	private PreferenceScreen mBreathLightScreen;
	private CheckBoxPreference mNofiticationPreference;
	private CheckBoxPreference mLowPowerPreference;
	private CheckBoxPreference mChargingPreference;
	private CheckBoxPreference mPhoneIDLEPreference;
	
	private static final String BREATHLIGHT_DEFAULT = "zzz_breath_light_default";
	private static final String NOTIFICATION = "zzz_notifications";
	private static final String LOW_POWER = "zzz_low_power";
	private static final String CHARGING = "zzz_charging";
	private static final String IPHONE_IDLE = "zzz_phone_idle";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.breath_light);
		
		mBreathLightScreen = (PreferenceScreen) findPreference(BREATHLIGHT_DEFAULT);
		mNofiticationPreference = (CheckBoxPreference) findPreference(NOTIFICATION);
		mLowPowerPreference = (CheckBoxPreference) findPreference(LOW_POWER);
		mChargingPreference = (CheckBoxPreference) findPreference(CHARGING);
		mPhoneIDLEPreference = (CheckBoxPreference) findPreference(IPHONE_IDLE);
		
		//mBreathLightScreen.removePreference(mPhoneIDLEPreference);
    }

	public void onResume(){
		super.onResume();
		updatePreference();
	}
	
	private void updatePreference(){
		/*set notification breath light turn on or off*/
		boolean mNofiticationPreferenceEnabled = Settings.System.getInt(getContentResolver(),
									Settings.System.ZZZ_BREATH_LIGHT_NOTIFY,0) == 1;
		//Log.d("liujie","mNofiticationPreferenceEnabled=>"+mNofiticationPreferenceEnabled);
        if (mNofiticationPreference != null) {
            mNofiticationPreference.setChecked(mNofiticationPreferenceEnabled);    
        }
		
		/*set low power breath light turn on or off*/
		boolean mLowPowerPreferenceEnabled = Settings.System.getInt(getContentResolver(),
									Settings.System.ZZZ_BREATH_LIGHT_LOW_POWER,0) == 1;
		//Log.d("liujie","mLowPowerPreferenceEnabled=>"+mLowPowerPreferenceEnabled);
        if (mLowPowerPreference != null) {
            mLowPowerPreference.setChecked(mLowPowerPreferenceEnabled);    
        }
		
		/*set charging breath light turn on or off*/
		boolean mChargingPreferenceEnabled = Settings.System.getInt(getContentResolver(),
									Settings.System.ZZZ_BREATH_LIGHT_CHARGING,0) == 1;
		//Log.d("liujie","mChargingPreferenceEnabled=>"+mChargingPreferenceEnabled);
        if (mChargingPreference != null) {
            mChargingPreference.setChecked(mChargingPreferenceEnabled);    
        }
		
		/*set phone_idle breath light turn on or off*/
		boolean mPhoneIDLEPreferenceEnable = Settings.System.getInt(getContentResolver(),
									Settings.System.ZZZ_BREATH_LIGHT_PHONE_IDLE,0) == 1;
		//Log.d("liujie","mPhoneIDLEPreferenceEnable=>"+mPhoneIDLEPreferenceEnable);
        if (mPhoneIDLEPreference != null) {
            mPhoneIDLEPreference.setChecked(mPhoneIDLEPreferenceEnable);    
        }
		
	}
	
	@Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
		//Log.d("liujie","==onPreferenceTreeClick==");
        if (mNofiticationPreference == preference) {
            boolean isChecked = ((CheckBoxPreference) preference).isChecked();
            Settings.System.putInt(getContentResolver(), Settings.System.ZZZ_BREATH_LIGHT_NOTIFY,
								   isChecked ? 1 : 0); 
			if(!isChecked)AWLedControl.isAwBlueLedEnable(3,0,0,0);
			return true;
        }else if (mLowPowerPreference == preference) {
            boolean isChecked = ((CheckBoxPreference) preference).isChecked();
            Settings.System.putInt(getContentResolver(), Settings.System.ZZZ_BREATH_LIGHT_LOW_POWER,
								   isChecked ? 1 : 0); 
			return true;
        }else if (mChargingPreference == preference) {
            boolean isChecked = ((CheckBoxPreference) preference).isChecked();
            Settings.System.putInt(getContentResolver(), Settings.System.ZZZ_BREATH_LIGHT_CHARGING,
								   isChecked ? 1 : 0); 
			return true;
        }else if (mPhoneIDLEPreference == preference) {
            boolean isChecked = ((CheckBoxPreference) preference).isChecked();
            Settings.System.putInt(getContentResolver(), Settings.System.ZZZ_BREATH_LIGHT_PHONE_IDLE,
								   isChecked ? 1 : 0); 
			if(isChecked)AWLedControl.isAwStandByEnable(4,1,1,1);
			else AWLedControl.isAwStandByEnable(4,0,0,0);
			return true;
        }  
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
}
