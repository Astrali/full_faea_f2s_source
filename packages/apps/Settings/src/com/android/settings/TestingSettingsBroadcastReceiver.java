package com.android.settings;

import android.provider.Telephony;
import static com.android.internal.telephony.TelephonyIntents.SECRET_CODE_ACTION;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.util.Log;
import android.view.KeyEvent;
import android.content.ComponentName;
import android.net.Uri;


public class TestingSettingsBroadcastReceiver extends BroadcastReceiver {
  
	Uri cyengineerUri = Uri.parse("android_secret_code://112");
	Uri pcbInfoUri = Uri.parse("android_secret_code://2170");
	
    public TestingSettingsBroadcastReceiver() {
    }
    
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(SECRET_CODE_ACTION)) {
			 Uri uri = intent.getData();
			/*
            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setClass(context, TestingSettings.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
			*/
			 Intent i = new Intent(Intent.ACTION_MAIN);
			 i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			if(uri.equals(cyengineerUri)){
				i.setComponent(new ComponentName("com.android.testmode",
												 "com.android.testmode.AllTest"));
				Log.i("SECRET_CODE","Before start InternalEM activity");
				context.startActivity(i);
			
			}else if(uri.equals(pcbInfoUri)){		
                i.setComponent(new ComponentName("com.android.pcbinfo","com.android.pcbinfo.PcbInfoActivity"));
                context.startActivity(i);
            }
		}
	}
}
