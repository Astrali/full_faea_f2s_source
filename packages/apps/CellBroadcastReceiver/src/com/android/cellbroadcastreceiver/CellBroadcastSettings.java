/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.android.cellbroadcastreceiver;

import android.content.res.Resources;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.provider.Settings;

// add for gemini
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.CheckBoxPreference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceManager;
import android.provider.Telephony.SIMInfo;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.common.featureoption.FeatureOption;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import com.mediatek.xlog.Xlog;

/**
 * Settings activity for the cell broadcast receiver.
 */
public class CellBroadcastSettings extends PreferenceActivity {
    public static final String TAG = "CellBroadcastSettings";
    // Preference key for whether to enable emergency notifications (default enabled).
    public static final String KEY_ENABLE_EMERGENCY_ALERTS = "enable_emergency_alerts";

    // Duration of alert sound (in seconds).
    public static final String KEY_ALERT_SOUND_DURATION = "alert_sound_duration";

    // Default alert duration (in seconds).
    public static final String ALERT_SOUND_DEFAULT_DURATION = "4";

    // Speak contents of alert after playing the alert sound.
    public static final String KEY_ENABLE_ALERT_SPEECH = "enable_alert_speech";

    // Preference category for emergency alert and CMAS settings.
    public static final String KEY_CATEGORY_ALERT_SETTINGS = "category_alert_settings";

    // Preference category for ETWS related settings.
    public static final String KEY_CATEGORY_ETWS_SETTINGS = "category_etws_settings";

    // Whether to display CMAS extreme threat notifications (default is enabled).
    public static final String KEY_ENABLE_CMAS_EXTREME_THREAT_ALERTS =
            "enable_cmas_extreme_threat_alerts";

    // Whether to display CMAS severe threat notifications (default is enabled).
    public static final String KEY_ENABLE_CMAS_SEVERE_THREAT_ALERTS =
            "enable_cmas_severe_threat_alerts";

    // Whether to display CMAS amber alert messages (default is enabled).
    public static final String KEY_ENABLE_CMAS_AMBER_ALERTS = "enable_cmas_amber_alerts";

    // Preference category for development settings (enabled by settings developer options toggle).
    public static final String KEY_CATEGORY_DEV_SETTINGS = "category_dev_settings";

    // Whether to display ETWS test messages (default is disabled).
    public static final String KEY_ENABLE_ETWS_TEST_ALERTS = "enable_etws_test_alerts";

    // Whether to display CMAS monthly test messages (default is disabled).
    public static final String KEY_ENABLE_CMAS_TEST_ALERTS = "enable_cmas_test_alerts";

    // Preference category for Brazil specific settings.
    public static final String KEY_CATEGORY_BRAZIL_SETTINGS = "category_brazil_settings";

    // Preference key for whether to enable channel 50 notifications
    // Enabled by default for phones sold in Brazil, otherwise this setting may be hidden.
    public static final String KEY_ENABLE_CHANNEL_50_ALERTS = "enable_channel_50_alerts";

    // cb message filter entry    
    public static final String CELL_BROADCAST = "pref_key_cell_broadcast";    
    public static final String SUB_TITLE_NAME = "sub_title_name";
    private Preference mCBsettingPref;
    private Preference mAlertVolumePref;
    // add for gemini
    private static int mSimId = -1;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // add for gemini
        if (FeatureOption.MTK_GEMINI_SUPPORT == true) {
            addPreferencesFromResource(R.xml.preferences);
            initGeminiPreference(getIntent());
        } else {
            // Display the fragment as the main content.
            getFragmentManager().beginTransaction().replace(android.R.id.content,
                    new CellBroadcastSettingsFragment()).commit();
        }
        // cb message filter entry
        mCBsettingPref = findPreference(CELL_BROADCAST);
        if (mCBsettingPref != null) {
            OnPreferenceClickListener l = new OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Intent it = new Intent();
                    it.setClassName("com.android.phone", "com.mediatek.settings.CellBroadcastActivity");
                    it.setAction(Intent.ACTION_MAIN);
                    SIMInfo info = getSIMInfoGemini(preference.getContext());
                    if (info != null) {
                        it.putExtra(PhoneConstants.GEMINI_SIM_ID_KEY, info.mSlot);
                        it.putExtra(SUB_TITLE_NAME, info.mDisplayName);
                    }
                    startActivity(it);
                    return true;
                }
            };
            mCBsettingPref.setOnPreferenceClickListener(l);
        }
        mAlertVolumePref = findPreference(KEY_ALERT_SOUND_VOLUME);
        if (mAlertVolumePref != null) {
            mAlertVolumePref.setOnPreferenceClickListener(getAlertVolumeListener());
        }
    }

    /**
     * New fragment-style implementation of preferences.
     */
    public static class CellBroadcastSettingsFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);
            PreferenceScreen preferenceScreen = getPreferenceScreen();

            // Handler for settings that require us to reconfigure enabled channels in radio
            Preference.OnPreferenceChangeListener startConfigServiceListener =
                    new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference pref, Object newValue) {
                    if (FeatureOption.MTK_GEMINI_SUPPORT == true) {
                        SIMInfo info = getSIMInfoGemini(pref.getContext());
                        if (info != null){
                            mSimId = (int) info.mSimId;
                            Intent serviceIntent = new Intent(
                                    CellBroadcastConfigService.ACTION_ENABLE_CHANNELS, null,
                                    pref.getContext(), CellBroadcastConfigService.class);
                            serviceIntent.putExtra("sim_id", mSimId);
                            pref.getContext().startService(serviceIntent);
                        }
                    } else {
                        CellBroadcastReceiver.startConfigService(pref.getContext());
                    }
                    return true;
                }
            };

            // Show extra settings when developer options is enabled in settings.
            boolean enableDevSettings = Settings.Secure.getInt(getActivity().getContentResolver(),
                    Settings.Secure.DEVELOPMENT_SETTINGS_ENABLED, 0) != 0;

            Resources res = getResources();
            boolean showEtwsSettings = res.getBoolean(R.bool.show_etws_settings);

            // Emergency alert preference category (general and CMAS preferences).
            PreferenceCategory alertCategory = (PreferenceCategory)
                    findPreference(KEY_CATEGORY_ALERT_SETTINGS);

            // Show alert settings and ETWS categories for ETWS builds and developer mode.
            if (enableDevSettings || showEtwsSettings) {
                // enable/disable all alerts
                Preference enablePwsAlerts = findPreference(KEY_ENABLE_EMERGENCY_ALERTS);
                if (enablePwsAlerts != null) {
                    enablePwsAlerts.setOnPreferenceChangeListener(startConfigServiceListener);
                }

                // alert sound duration
                ListPreference duration = (ListPreference) findPreference(KEY_ALERT_SOUND_DURATION);
                duration.setSummary(duration.getEntry());
                duration.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference pref, Object newValue) {
                        final ListPreference listPref = (ListPreference) pref;
                        final int idx = listPref.findIndexOfValue((String) newValue);
                        listPref.setSummary(listPref.getEntries()[idx]);
                        return true;
                    }
                });
            } else {
                // Remove general emergency alert preference items (not shown for CMAS builds).
                alertCategory.removePreference(findPreference(KEY_ENABLE_EMERGENCY_ALERTS));
                alertCategory.removePreference(findPreference(KEY_ALERT_SOUND_DURATION));
                alertCategory.removePreference(findPreference(KEY_ENABLE_ALERT_SPEECH));
                // Remove ETWS preference category.
                preferenceScreen.removePreference(findPreference(KEY_CATEGORY_ETWS_SETTINGS));
            }

            if (!res.getBoolean(R.bool.show_cmas_settings)) {
                // Remove CMAS preference items in emergency alert category.
                alertCategory.removePreference(
                        findPreference(KEY_ENABLE_CMAS_EXTREME_THREAT_ALERTS));
                alertCategory.removePreference(
                        findPreference(KEY_ENABLE_CMAS_SEVERE_THREAT_ALERTS));
                alertCategory.removePreference(findPreference(KEY_ENABLE_CMAS_AMBER_ALERTS));
            }
            if (!res.getBoolean(R.bool.show_brazil_settings)) {
                preferenceScreen.removePreference(findPreference(KEY_CATEGORY_BRAZIL_SETTINGS));
            }
            if (!enableDevSettings) {
                preferenceScreen.removePreference(findPreference(KEY_CATEGORY_DEV_SETTINGS));
            }

            Preference enableChannel50Alerts = findPreference(KEY_ENABLE_CHANNEL_50_ALERTS);
            if (enableChannel50Alerts != null) {
                enableChannel50Alerts.setOnPreferenceChangeListener(startConfigServiceListener);
            }
        }
    }

    // add for gemini functions
    // the default value here is the same as preferences.xml
    public static final boolean ENABLE_EMERGENCY_ALERTS_DEFAULT = true;
    // alert sound duration default value is exist
    // alert sound volume
    public static final String KEY_ALERT_SOUND_VOLUME = "alert_sound_volume";
    private float mAlertVolume = 1.0f;
    public static final boolean ENABLE_ETWS_TEST_ALERTS_DEFAULT = true;
    public static final boolean ENABLE_ALERT_SPEECH_DEFAULT = true;
    public static final boolean ENABLE_CMAS_EXTREME_THREAT_ALERTS_DEFAULT = true;
    public static final boolean ENABLE_CMAS_SEVERE_THREAT_ALERTS_DEFAULT = true;
    public static final boolean ENABLE_CMAS_AMBER_ALERTS_DEFAULT = false;
    public static final boolean ENABLE_CMAS_TEST_ALERTS_DEFAULT = false;
    public static final boolean ENABLE_CHANNEL_50_ALERTS_DEFAULT = true;

    private void initGeminiPreference(Intent it) {
        mSimId = getIntent().getIntExtra("sim_id", -1);
        Xlog.d(TAG, "mSimId:" + mSimId);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        CheckBoxPreference enableEmergencyAlerts = (CheckBoxPreference) findPreference(KEY_ENABLE_EMERGENCY_ALERTS);
        if (enableEmergencyAlerts != null) {
            String newKey = KEY_ENABLE_EMERGENCY_ALERTS + "_"+ mSimId;
            enableEmergencyAlerts.setKey(newKey);
            enableEmergencyAlerts.setChecked(prefs.getBoolean(newKey, ENABLE_EMERGENCY_ALERTS_DEFAULT));
        }
        
        ListPreference alertSoundDuration = (ListPreference) findPreference(KEY_ALERT_SOUND_DURATION);
        if (alertSoundDuration != null) {
            String newKey = KEY_ALERT_SOUND_DURATION + "_"+ mSimId;
            alertSoundDuration.setKey(newKey);
            alertSoundDuration.setSummary(prefs.getString(newKey, ALERT_SOUND_DEFAULT_DURATION));
            alertSoundDuration.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference pref, Object newValue) {
                    final ListPreference listPref = (ListPreference) pref;
                    final int idx = listPref.findIndexOfValue((String) newValue);
                    listPref.setSummary(listPref.getEntries()[idx]);
                    return true;
                }
            });
        }

        CheckBoxPreference enableAlertSpeech = (CheckBoxPreference) findPreference(KEY_ENABLE_ALERT_SPEECH);
        if (enableAlertSpeech != null) {
            String newKey = KEY_ENABLE_ALERT_SPEECH + "_"+ mSimId;
            enableAlertSpeech.setKey(newKey);
            enableAlertSpeech.setChecked(prefs.getBoolean(newKey, ENABLE_ALERT_SPEECH_DEFAULT));
        }
        
        CheckBoxPreference enableEtwsAlerts = (CheckBoxPreference) findPreference(KEY_ENABLE_ETWS_TEST_ALERTS);
        if (enableEtwsAlerts != null) {
            String newKey = KEY_ENABLE_ETWS_TEST_ALERTS + "_"+ mSimId;
            enableEtwsAlerts.setKey(newKey);
            enableEtwsAlerts.setChecked(prefs.getBoolean(newKey, ENABLE_ETWS_TEST_ALERTS_DEFAULT));
        }

        CheckBoxPreference enableCmasExtremeThreatAlerts = (CheckBoxPreference) findPreference(KEY_ENABLE_CMAS_EXTREME_THREAT_ALERTS);
        if (enableCmasExtremeThreatAlerts != null) {
            String newKey = KEY_ENABLE_CMAS_EXTREME_THREAT_ALERTS + "_"+ mSimId;
            enableCmasExtremeThreatAlerts.setKey(newKey);
            enableCmasExtremeThreatAlerts.setChecked(prefs.getBoolean(newKey, ENABLE_CMAS_EXTREME_THREAT_ALERTS_DEFAULT));
        }

        CheckBoxPreference enableCmasSevereThreatAlerts = (CheckBoxPreference) findPreference(KEY_ENABLE_CMAS_SEVERE_THREAT_ALERTS);
        if (enableCmasSevereThreatAlerts != null) {
            String newKey = KEY_ENABLE_CMAS_SEVERE_THREAT_ALERTS + "_"+ mSimId;
            enableCmasSevereThreatAlerts.setKey(newKey);
            enableCmasSevereThreatAlerts.setChecked(prefs.getBoolean(newKey, ENABLE_CMAS_SEVERE_THREAT_ALERTS_DEFAULT));
        }

        CheckBoxPreference enableCmasAmberAlerts = (CheckBoxPreference) findPreference(KEY_ENABLE_CMAS_AMBER_ALERTS);
        if (enableCmasAmberAlerts != null) {
            String newKey = KEY_ENABLE_CMAS_AMBER_ALERTS + "_"+ mSimId;
            enableCmasAmberAlerts.setKey(newKey);
            enableCmasAmberAlerts.setChecked(prefs.getBoolean(newKey, ENABLE_CMAS_AMBER_ALERTS_DEFAULT));
        }

        CheckBoxPreference enableCmasTestAlerts = (CheckBoxPreference) findPreference(KEY_ENABLE_CMAS_TEST_ALERTS);
        if (enableCmasTestAlerts != null) {
            String newKey = KEY_ENABLE_CMAS_TEST_ALERTS + "_"+ mSimId;
            enableCmasTestAlerts.setKey(newKey);
            enableCmasTestAlerts.setChecked(prefs.getBoolean(newKey, ENABLE_CMAS_TEST_ALERTS_DEFAULT));
        }

        CheckBoxPreference enableChannel50Alerts = (CheckBoxPreference) findPreference(KEY_ENABLE_CHANNEL_50_ALERTS);
        if (enableChannel50Alerts != null) {
            String newKey = KEY_ENABLE_CHANNEL_50_ALERTS + "_"+ mSimId;
            enableChannel50Alerts.setKey(newKey);
            enableChannel50Alerts.setChecked(prefs.getBoolean(newKey, ENABLE_CHANNEL_50_ALERTS_DEFAULT));
        }

        Preference.OnPreferenceChangeListener startConfigServiceListener =
        new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference pref, Object newValue) {
                CellBroadcastReceiver.startConfigServiceGemini(pref.getContext(), mSimId);
                return true;
            }
        };

        if (enableEmergencyAlerts != null) {
            enableEmergencyAlerts.setOnPreferenceChangeListener(startConfigServiceListener);
        }
        if (enableAlertSpeech != null) {
            enableAlertSpeech.setOnPreferenceChangeListener(startConfigServiceListener);
        }

        // update title as sim card name
        SIMInfo simInfo = SIMInfo.getSIMInfoById(this, mSimId);
        if (simInfo != null) {
            setTitle(simInfo.mDisplayName);
        }
    }

    private OnPreferenceClickListener getAlertVolumeListener() {
        OnPreferenceClickListener l = new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(CellBroadcastSettings.this);
                LayoutInflater flater = getLayoutInflater();
                View v = flater.inflate(R.layout.alert_dialog_view, null);
                SeekBar sb = (SeekBar)v.findViewById(R.id.seekbar);
                // set bar's progress                
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CellBroadcastSettings.this);
                float pro = 1.0f;
                if (FeatureOption.MTK_GEMINI_SUPPORT) {
                    pro = prefs.getFloat(KEY_ALERT_SOUND_VOLUME+"_"+mSimId, 1.0f);
                } else {
                    pro =prefs.getFloat(KEY_ALERT_SOUND_VOLUME, 1.0f);
                }
                int progress = (int)(pro*100);
                if (progress < 0) {
                    progress = 0;
                } else if (progress > 100) {
                    progress = 100;
                }
                //Xlog.d(TAG, "open volume setting,progress:"+progress+",pro:"+pro);
                sb.setProgress(progress);
                sb.setOnSeekBarChangeListener(getSeekBarListener());
                dialog.setTitle(R.string.alert_sound_volume)
                .setView(v)
                .setPositiveButton(R.string.button_dismiss, new PositiveButtonListener())
                .setNegativeButton(R.string.button_cancel, new NegativeButtonListener())
                .show();
                return true;
            }
        };
        return l;
    }

    private SeekBar.OnSeekBarChangeListener getSeekBarListener() {
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mAlertVolume = progress/100.0f;
                    Xlog.d(TAG, "volume:"+mAlertVolume);
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // nothing to do
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // make some sample sound
                Intent audioIntent = new Intent(CellBroadcastSettings.this, CellBroadcastAlertAudio.class);
                audioIntent.setAction(CellBroadcastAlertAudio.ACTION_START_ALERT_AUDIO);
                if (FeatureOption.MTK_GEMINI_SUPPORT) {
                    audioIntent.putExtra("sim_id", mSimId);
                }
                audioIntent.putExtra("has_alert_volume", true);
                audioIntent.putExtra("alert_volume", mAlertVolume);
                audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_DURATION_EXTRA, 1);
                startService(audioIntent);
            }
        };
    }

    private class PositiveButtonListener implements OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            //save alert sound volume
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CellBroadcastSettings.this);
            SharedPreferences.Editor editor = prefs.edit();
            if (FeatureOption.MTK_GEMINI_SUPPORT) {
                editor.putFloat(KEY_ALERT_SOUND_VOLUME+"_"+mSimId, mAlertVolume);
            } else {
                editor.putFloat(KEY_ALERT_SOUND_VOLUME, mAlertVolume);
            }
            editor.commit();
        }
    }

    private class NegativeButtonListener implements OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            // cancel
            dialog.dismiss();
        }
    }

    private static SIMInfo getSIMInfoGemini(Context context) {
        SIMInfo info = null;
        if (FeatureOption.MTK_GEMINI_SUPPORT == true) {
            info = SIMInfo.getSIMInfoBySlot(context, PhoneConstants.GEMINI_SIM_1);
            if (info == null) {
                info = SIMInfo.getSIMInfoBySlot(context, PhoneConstants.GEMINI_SIM_2);
            }
        } else {
            info = SIMInfo.getSIMInfoBySlot(context, PhoneConstants.GEMINI_SIM_1);
        }
        return info;
    }
}
