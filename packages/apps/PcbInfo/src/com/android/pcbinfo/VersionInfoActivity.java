package com.android.pcbinfo;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemProperties;
import android.widget.TextView;
import android.provider.Settings.System;

public class VersionInfoActivity extends Activity {
	
	private TextView mTextView01;
	private TextView mTextView02;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.version_info);
		mTextView01 = (TextView)findViewById(R.id.t01);
		mTextView02 = (TextView)findViewById(R.id.t02);
		
		
		//mTextView01.setText("VERSION_INFORMATION = "+System.getString(getContentResolver(),System.VERSION_INFO));
		//mTextView02.setText("BUILD_TIME = "+SystemProperties.get("doublefish.custom.verno","??????")/*.substring(4)*/);
		
    }
	
	public void onStop(){
		super.onStop();
		finish();
	}


}
