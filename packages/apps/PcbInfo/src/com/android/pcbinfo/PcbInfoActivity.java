package com.android.pcbinfo;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemProperties;
import android.widget.TextView;

public class PcbInfoActivity extends Activity {
	/*
	private TextView mTextView01;
	private TextView mTextView02;
	private TextView mTextView03;
	private TextView mTextView04;
	private TextView mTextView05;
	private TextView mTextView06;
	//private TextView mTextView07;
	private TextView mTextView08;
	private TextView mTextView09;
	private TextView mTextView10;
	private TextView mTextView11;
	private TextView mTextView12;
	private TextView mTextView13;
	private TextView mTextView14;
	*/
	private TextView mTextView15;
	private TextView mTextView16;
	private TextView mTextView17;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		/*
		mTextView01 = (TextView)findViewById(R.id.t01);
		mTextView02 = (TextView)findViewById(R.id.t02);
		mTextView03 = (TextView)findViewById(R.id.t03);
		mTextView04 = (TextView)findViewById(R.id.t04);
		mTextView05 = (TextView)findViewById(R.id.t05);
		mTextView06 = (TextView)findViewById(R.id.t06);
		//mTextView07 = (TextView)findViewById(R.id.t07);
		mTextView08 = (TextView)findViewById(R.id.t08);
		mTextView09 = (TextView)findViewById(R.id.t09);
		mTextView10 = (TextView)findViewById(R.id.t10);
		mTextView11 = (TextView)findViewById(R.id.t11);
		mTextView12 = (TextView)findViewById(R.id.t12);
		mTextView13 = (TextView)findViewById(R.id.t13);
		mTextView14 = (TextView)findViewById(R.id.t14);
		
		mTextView01.setText("PCB = " + SystemProperties.get("doublefish.vendor.pcb.name","??????"));
		mTextView02.setText("MTK_MODEM_SUPPORT = "+SystemProperties.get("doublefish.mtk.modem.support","??????"));
		mTextView03.setText("CUSTOM_MODEM = "+SystemProperties.get("doublefish.cust.modem","??????"));
		mTextView04.setText("GEMINI = "+SystemProperties.get("doublefish.gemini","??????"));
		mTextView05.setText("LCM_WIDTH = " + SystemProperties.get("doublefish.lcd.width","??????"));
		mTextView06.setText("LCM_HEIGHT = "+SystemProperties.get("doublefish.lcd.height","??????"));
		//mTextView07.setText(SystemProperties.get("doublefish.imgsensor","??????"));
		mTextView08.setText("TOUCHPANEL = "+SystemProperties.get("doublefish.touchpanel","??????"));
		mTextView09.setText("MAGNETOMETER = "+SystemProperties.get("doublefish.magentometer","??????"));
		mTextView10.setText("ACCELEROMETER = "+SystemProperties.get("doublefish.accelerometer","??????"));
		mTextView11.setText("ALSPS = "+SystemProperties.get("doublefish.alsps","??????"));
		mTextView12.setText("BUILD_VERNO = "+SystemProperties.get("doublefish.mtk.branch","??????"));
		mTextView13.setText("BUILD_TIME = "+SystemProperties.get("doublefish.custom.verno","??????"));
		//mTextView14.setText("VERSION = " + Build.VERSION.RELEASE);
		*/
		mTextView15 = (TextView)findViewById(R.id.t15);
		mTextView16 = (TextView)findViewById(R.id.t16);
		mTextView17 = (TextView)findViewById(R.id.t17);
		
		mTextView15.setText("HARDWARE_VERSION = " + SystemProperties.get("ro.custom.build.project.name","??????"));
		mTextView16.setText("SOFT_VERSION = "+SystemProperties.get("ro.build.display.id","??????"));
		mTextView17.setText("BUILD_TIME = "+SystemProperties.get("ro.custom.build.version","??????"));
    }
	
	public void onStop(){
		super.onStop();
		finish();
	}
}
