/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.phone.vt;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.android.internal.telephony.PhoneConstants;
import com.mediatek.settings.VTAdvancedSetting;
import com.mediatek.settings.VTSettingUtils;

public class VTBackgroundBitmapHandler {

    private static final String LOG_TAG = "VTBackgroundBitmapHandler";

    private Bitmap mBitmap1;
    private Bitmap mBitmap2;

    /**
     * recycle bitmap used for background
     */
    public void recycle() {
        log("recycle()");
        if (null != mBitmap1) {
            mBitmap1.recycle();
        }
        mBitmap1 = null;
        if (null != mBitmap2) {
            mBitmap2.recycle();
        }
        mBitmap2 = null;
    }

    public void recycle(int slot) {
        log("recycle(), slot = " + slot);
        if (PhoneConstants.GEMINI_SIM_1 == slot) {
            if (null != mBitmap1) {
                mBitmap1.recycle();
            }
            mBitmap1 = null;
        } else if (PhoneConstants.GEMINI_SIM_2 == slot) {
            if (null != mBitmap2) {
                mBitmap2.recycle();
            }
            mBitmap2 = null;
        }
    }

    /**
     * force to update bitmap according to setting
     */
    public void forceUpdateBitmapBySetting(int slot) {
        log("forceUpdateBitmapBySetting(), slot = " + slot);
        recycle(slot);
        if (PhoneConstants.GEMINI_SIM_1 == slot) {
           if (VTSettingUtils.getInstance().mPicToReplacePeer
                   .equals(VTAdvancedSetting.SELECT_DEFAULT_PICTURE2)) {
               mBitmap1 = BitmapFactory.decodeFile(VTAdvancedSetting.getPicPathDefault2());
           } else if (VTSettingUtils.getInstance().mPicToReplacePeer
                   .equals(VTAdvancedSetting.SELECT_MY_PICTURE2)) {
               mBitmap1 = BitmapFactory.decodeFile(VTAdvancedSetting.getPicPathUserselect2(slot));
           }
        } else if (PhoneConstants.GEMINI_SIM_2 == slot) {
            if (VTSettingUtils.getInstance().mPicToReplacePeer
                    .equals(VTAdvancedSetting.SELECT_DEFAULT_PICTURE2)) {
                mBitmap2 = BitmapFactory.decodeFile(VTAdvancedSetting.getPicPathDefault2());
            } else if (VTSettingUtils.getInstance().mPicToReplacePeer
                    .equals(VTAdvancedSetting.SELECT_MY_PICTURE2)) {
                mBitmap2 = BitmapFactory.decodeFile(VTAdvancedSetting.getPicPathUserselect2(slot));
            }
        }
    }

    /**
     * update bitmap by setting
     */
    public void updateBitmapBySetting(int slot) {
        log("updateBitmapBySetting(), slot = " + slot);
        if (PhoneConstants.GEMINI_SIM_1 == slot) {
            if (null != mBitmap1) {
                return;
            }
        } else if (PhoneConstants.GEMINI_SIM_2 == slot) {
            if (null != mBitmap2) {
                return;
            }
        } else {
            return;
        }
        forceUpdateBitmapBySetting(slot);
    }

    /**
     * get bitmap stored
     * @return bitmap
     */
    public Bitmap getBitmap(int slot) {
        log("getBitmap(), slot = " + slot);
        if (PhoneConstants.GEMINI_SIM_1 == slot) {
            return mBitmap1;
        } else if (PhoneConstants.GEMINI_SIM_2 == slot) {
            return mBitmap2;
        } else {
            return null;
        }
    }

    private void log(String msg) {
        Log.d(LOG_TAG, msg);
    }
}
