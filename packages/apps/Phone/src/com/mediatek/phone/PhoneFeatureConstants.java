package com.mediatek.phone;

public class PhoneFeatureConstants {

    public static final class FeatureOption {
        public static final boolean MTK_GEMINI_SUPPORT =
            com.mediatek.common.featureoption.FeatureOption.MTK_GEMINI_SUPPORT;
        public static final boolean MTK_VT3G324M_SUPPORT =
            com.mediatek.common.featureoption.FeatureOption.MTK_VT3G324M_SUPPORT;
        public static final boolean MTK_TTY_SUPPORT =
            com.mediatek.common.featureoption.FeatureOption.MTK_TTY_SUPPORT;
        public static final boolean MTK_PHONE_VT_VOICE_ANSWER =
            com.mediatek.common.featureoption.FeatureOption.MTK_PHONE_VT_VOICE_ANSWER;
        public static final boolean MTK_PHONE_VOICE_RECORDING =
            true; //com.mediatek.featureoption.FeatureOption.MTK_PHONE_VOICE_RECORDING;
        public static final boolean MTK_BRAZIL_CUSTOMIZATION_VIVO =
            com.mediatek.common.featureoption.FeatureOption.MTK_BRAZIL_CUSTOMIZATION_VIVO;
        public static final boolean MTK_BRAZIL_CUSTOMIZATION_CLARO =
            com.mediatek.common.featureoption.FeatureOption.MTK_BRAZIL_CUSTOMIZATION_CLARO;
        public static final boolean MTK_GEMINI_3G_SWITCH =
            com.mediatek.common.featureoption.FeatureOption.MTK_GEMINI_3G_SWITCH;
        public static final boolean MTK_DUAL_MIC_SUPPORT =
            com.mediatek.common.featureoption.FeatureOption.MTK_DUAL_MIC_SUPPORT;
        public static final boolean MTK_TB_APP_CALL_FORCE_SPEAKER_ON =
            com.mediatek.common.featureoption.FeatureOption.MTK_TB_APP_CALL_FORCE_SPEAKER_ON;
        public static final boolean MTK_PHONE_NUMBER_GEODESCRIPTION =
            com.mediatek.common.featureoption.FeatureOption.MTK_PHONE_NUMBER_GEODESCRIPTION;
        public static final boolean EVDO_DT_SUPPORT =
            com.mediatek.common.featureoption.FeatureOption.EVDO_DT_SUPPORT;
        public static final boolean MTK_DT_SUPPORT =
            com.mediatek.common.featureoption.FeatureOption.MTK_DT_SUPPORT;
        public static final boolean MTK_ENS_SUPPORT =
            com.mediatek.common.featureoption.FeatureOption.MTK_ENS_SUPPORT;
        public static final boolean MTK_GEMINI_ENHANCEMENT =
            com.mediatek.common.featureoption.FeatureOption.MTK_GEMINI_ENHANCEMENT;
        public static final boolean MTK_AUDIO_PROFILES =
            com.mediatek.common.featureoption.FeatureOption.MTK_AUDIO_PROFILES;
        public static final boolean MTK_FLIGHT_MODE_POWER_OFF_MD = 
            com.mediatek.common.featureoption.FeatureOption.MTK_FLIGHT_MODE_POWER_OFF_MD;
        public static final boolean MTK_VOICE_UI_SUPPORT =
            com.mediatek.common.featureoption.FeatureOption.MTK_VOICE_UI_SUPPORT;
    }

}
