package com.mediatek.calloption;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.sip.SipManager;
import android.provider.Settings;
import android.provider.Telephony.SIMInfo;

import com.mediatek.calloption.SimPickerAdapter.ItemHolder;

import java.util.ArrayList;
import java.util.List;

public abstract class SimPickerDialog {

    public static final int DEFAULT_SIM_NOT_SET = -5;

    private SimPickerDialog() {
    }

    /*public static Dialog create(Context context, String title,
                                DialogInterface.OnClickListener listener,
                                SimPickerAdapter simAdapter, final boolean isInternet,
                                final boolean isMultiSim) {
        return create(context, title, DEFAULT_SIM_NOT_SET, 
                      createItemHolder(context, isInternet), listener, simAdapter);
    }*/

    /*public static Dialog create(Context context, String title, long suggestedSimId,
                                DialogInterface.OnClickListener listener,
                                SimPickerAdapter simAdapter, final boolean isInternet,
                                final boolean isMultiSim) {
        return create(context, title, suggestedSimId, createItemHolder(context, isInternet), listener, simAdapter);
    }*/

    public static Dialog create(Context context, String title, /*long suggestedSimId, List<ItemHolder> items,*/
                                   DialogInterface.OnClickListener listener, SimPickerAdapter simPickerAdpater,
                                   boolean addInternet, boolean only3GItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //simPickerAdpater.init(context, items, suggestedSimId);
        simPickerAdpater.setItems(createItemHolder(context, addInternet, only3GItem));
        builder.setSingleChoiceItems(simPickerAdpater, -1, listener)
               .setTitle(title);
        return builder.create();
    }

    /*protected static List<ItemHolder> createItemHolder(Context context, boolean internet) {
        return createItemHolder(context, null, internet, null);
    }*/

    protected static List<ItemHolder> createItemHolder(Context context,
            /*String phone,*/ boolean addInternet/*, ArrayList<Account> accounts*/, boolean only3GItem) {

        List<SIMInfo> simInfos = SIMInfo.getInsertedSIMList(context);
        ArrayList<ItemHolder> itemHolders = new ArrayList<ItemHolder>();
        ItemHolder temp = null;

        /*if (!TextUtils.isEmpty(phone)) {
            temp = new ItemHolder(phone, SimPickerAdapter.ITEM_TYPE_TEXT);
            itemHolders.add(temp);
        }*/

        int index = 0;
        for (SIMInfo simInfo : simInfos) {
            temp = new ItemHolder(simInfo, SimPickerAdapter.ITEM_TYPE_SIM);
            if (index == 0) {
                if (!only3GItem || (simInfo != null && simInfo.mSlot >= 0 
                                && CallOptionUtils.get3GCapabilitySIMBySolt(simInfo.mSlot))) {
                    itemHolders.add(temp);
                }
            } else {
                int lastPos = itemHolders.size() - 1;
                SIMInfo temInfo = (SIMInfo)itemHolders.get(lastPos).mData;
                if (simInfo.mSlot < temInfo.mSlot) {
                    if (!only3GItem || (simInfo != null && simInfo.mSlot >= 0 
                                    && CallOptionUtils.get3GCapabilitySIMBySolt(simInfo.mSlot))) {
                        itemHolders.add(lastPos, temp);
                    }
                } else {
                    if (!only3GItem || (simInfo != null && simInfo.mSlot >= 0 
                                    && CallOptionUtils.get3GCapabilitySIMBySolt(simInfo.mSlot))) {
                        itemHolders.add(temp);
                    }
                }
            }
            index++;
        }

        int enabled = Settings.System.getInt(context.getContentResolver(),
                                             Settings.System.ENABLE_INTERNET_CALL, 0);
        if (!only3GItem && addInternet && SipManager.isVoipSupported(context) && enabled == 1) {
            temp = new ItemHolder("Internet"/*context.getResources().getText(R.string.internet)*/,
                    SimPickerAdapter.ITEM_TYPE_INTERNET);
            itemHolders.add(temp);
        }
        
        /*if (accounts != null) {
            for (Account account : accounts) {
                temp = new ItemHolder(account, SimPickerAdapter.ITEM_TYPE_ACCOUNT);
                itemHolders.add(temp);
            }
        }*/
        return itemHolders;
    }

    protected abstract SimPickerAdapter getSimPickerAdapter(Context context,
                                                            List<ItemHolder> items,
                                                            long suggestedSimId);
}
