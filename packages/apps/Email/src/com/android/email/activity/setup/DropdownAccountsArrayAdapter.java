package com.android.email.activity.setup;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

/**
 * An adapter used by EmailAccountAutoCompleteTextView, extending {@link ArrayAdapter},
 * this class add account username when publish filter results.
 *
 */
public class DropdownAccountsArrayAdapter extends ArrayAdapter<String> {
    private List<String> mObjects;
    DropdownAccountsArrayFilter mFilter;
    // A copy of the original mObjects array, initialized from and then used instead as soon as
    // the mFilter ArrayFilter is used. mObjects will then only contain the filtered values.
    private ArrayList<String> mOriginalValues;
    private final Object mLock = new Object();
    LayoutInflater mInflater;
    int mResourceId;
    String mUserName;
    public DropdownAccountsArrayAdapter(Context context,
            int textViewResourceId, ArrayList<String> objects) {
        super(context, textViewResourceId, objects);
        mInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        mResourceId = textViewResourceId;
        mObjects = objects;
    }

    public void setUserName(String name) {
        mUserName = name;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setData(ArrayList<String> objects) {
        mObjects = objects;
    }

    @Override
    public int getCount() {
        return mObjects.size();
    }

    @Override
    public String getItem(int position) {
        return mObjects.get(position);
    }

    @Override
    public int getPosition(String item) {
        return mObjects.indexOf(item);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new DropdownAccountsArrayFilter();
        }
        return mFilter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        TextView text;

        if (convertView == null) {
            view = mInflater.inflate(mResourceId, parent, false);
        } else {
            view = convertView;
        }
        text = (TextView) view;
        String item = getItem(position);
        // implement special ellipsis (only add ellipsis in the username end).
        text.setEllipsize(null);
        TextPaint paint = text.getPaint();
        int textWidth = (int) paint.measureText(item);
        // For match the UI spec, limit the view's width
        int viewWidth = parent.getWidth() - 50;
        if (textWidth > viewWidth) {
            String[] parts = item.split("@");
            int domainWidth = (int) paint.measureText("@" + parts[1]);
            CharSequence ellipsizedUserName = TextUtils.ellipsize(parts[0],
                    text.getPaint(), viewWidth - domainWidth,
                    TruncateAt.END);
            item = ellipsizedUserName + "@" + parts[1];
        }

        text.setText(item);

        return view;
    }

    /**
     * <p>An array filter constrains the content of the array adapter with
     * a prefix. Each item that does not start with the supplied prefix
     * is removed from the list.</p>
     */
    private class DropdownAccountsArrayFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                synchronized (mLock) {
                    mOriginalValues = new ArrayList<String>(mObjects);
                }
            }

            if (prefix == null || prefix.length() == 0) {
                ArrayList<String> list;
                synchronized (mLock) {
                    list = new ArrayList<String>(mOriginalValues);
                }
                results.values = list;
                results.count = list.size();
            } else {
                String prefixString = prefix.toString().toLowerCase();

                ArrayList<String> values;
                synchronized (mLock) {
                    values = new ArrayList<String>(mOriginalValues);
                }

                final int count = values.size();
                final ArrayList<String> newValues = new ArrayList<String>();

                for (int i = 0; i < count; i++) {
                    final String value = values.get(i);
                    final String valueText = value.toString().toLowerCase();

                    // First match against the whole, non-splitted value
                    if (valueText.startsWith(prefixString)) {
                        newValues.add(value);
                    } else {
                        final String[] words = valueText.split(" ");
                        final int wordCount = words.length;

                        // Start at index 0, in case valueText starts with space(s)
                        for (int k = 0; k < wordCount; k++) {
                            if (words[k].startsWith(prefixString)) {
                                newValues.add(value);
                                break;
                            }
                        }
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mObjects = (List<String>) results.values;
            List<String> tmp = new ArrayList<String>();
            for(String s: mObjects) {
                tmp.add(mUserName + "@" + s);
            }
            mObjects = tmp;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
