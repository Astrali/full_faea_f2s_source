package com.android.email.activity.setup;

import android.content.Context;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * M: An adapter used by EmailAccountAutoCompleteTextView, extending
 * {@link ArrayAdapter}, this class add email address when publish filter
 * results.
 *
 */
public class DropdownAddressArrayAdapter extends ArrayAdapter<String> {

    LayoutInflater mInflater;
    int mResourceId;

    public DropdownAddressArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        TextView text;
        if (convertView == null) {
            view = mInflater.inflate(mResourceId, parent, false);
        } else {
            view = convertView;
        }
        text = (TextView) view;
        String item = getItem(position);
        // implement special ellipsis (only add ellipsis in the username end).
        text.setEllipsize(null);
        TextPaint paint = text.getPaint();
        int textWidth = (int) paint.measureText(item);
        // For match the UI spec, limit the view's width
        int viewWidth = parent.getWidth() - 50;
        if (textWidth > viewWidth) {
            String[] parts = item.split("@");
            int domainWidth = (int) paint.measureText("@" + parts[1]);
            CharSequence ellipsizedUserName = TextUtils.ellipsize(parts[0], text.getPaint(),
                    viewWidth - domainWidth, TruncateAt.END);
            item = ellipsizedUserName + "@" + parts[1];
        }

        text.setText(item);

        return view;
    }
}
