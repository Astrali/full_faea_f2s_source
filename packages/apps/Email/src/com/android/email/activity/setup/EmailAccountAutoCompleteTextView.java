package com.android.email.activity.setup;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.util.AttributeSet;
import android.widget.BaseAdapter;
import android.widget.ListPopupWindow;
import android.widget.MultiAutoCompleteTextView;
import com.android.email.R;

import com.android.emailcommon.Logging;
import com.android.emailcommon.provider.EmailContent.Message;
import com.android.emailcommon.utility.EmailAsyncTask;
import com.android.emailcommon.utility.TextUtilities;
import com.mediatek.email.provider.history.EmailAddress;

/** An editable text view, extending {@link MultiAutoCompleteTextView}, that
 *  can filter data by EmailAccountTokenizer (separate string by '@'), and auto-fill
 *  EditText when the filter count was only one.
 */
public class EmailAccountAutoCompleteTextView extends MultiAutoCompleteTextView {
    private boolean mIsPerformCompletion = false;
    private boolean mIMEFullScreenMode = false;
    // Record the last input text length, and don't do the auto-replace
    // if the user was deleting chars.
    private int mLastHostNameLength = 0;

    ListPopupWindow mPopup;
    EmailAccountTokenizer mTokenizer;

    public EmailAccountAutoCompleteTextView(Context context) {
        super(context);
    }

    public EmailAccountAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setThreshold(1);
        mTokenizer = new EmailAccountTokenizer();
        setTokenizer(mTokenizer);
        // Disable auto-replace on only one matched item when IME was in fullscreen mode
        //, cause the IME's own bug.
    /*  Configuration cf = context.getResources().getConfiguration();
        if (cf.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mIMEFullScreenMode = true;
        } else {
            mIMEFullScreenMode = false;
        }*/
    }

    public EmailAccountAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setPerfomCompletion(boolean performCompletion){
        mIsPerformCompletion = performCompletion;
    }

    @Override
    protected void replaceText(CharSequence text) {
        clearComposingText();

        if (mIsPerformCompletion) {
            int end = getSelectionEnd();
            // false, not trigger filter once more.
            setText(text, false);
            Editable editable = getText();
            Selection.setSelection(editable, end, editable.length());
        } else {
            setText(text);
            // make sure we keep the caret at the end of the text view
            Editable spannable = getText();
            Selection.setSelection(spannable, spannable.length());
            int hostNameLength = getEditHostNameLength();
            mLastHostNameLength = hostNameLength;
        }
    }

    @Override
    public void onFilterComplete(int count) {
        super.onFilterComplete(count);
        int hostNameLength = getEditHostNameLength();
        if (hostNameLength > mLastHostNameLength) {
            replacePartTextIfNeed();
        }
        mLastHostNameLength = hostNameLength;
    }

    /* 
     * Obtain the last edited text to avoid miss the mLastHostNameLength
     * due to screen orientation.
     */
    @Override
    public void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);
        mLastHostNameLength = getEditHostNameLength();
    }

    private int getEditHostNameLength() {
        String text = getText().toString();
        return text.length();
    }

    public void replacePartTextIfNeed() {
        BaseAdapter adapter = (BaseAdapter) getAdapter();
        if (adapter.getCount() == 0) {
            return;
        }
        String text = getText().toString();
        CharSequence replaceText = convertSelectionToString(adapter.getItem(0));
        if (replaceText.equals(text)) {
            dismissDropDown();
            return;
        }
        int itemCount = adapter.getCount();
        // if the matched account was only one, we auto fill the EditText with it.
        // Don't replace text when the IME was in full screen mode.
        if (itemCount == 1 && mIMEFullScreenMode == false) {
            setPerfomCompletion(true);
            replaceText(replaceText);
            setPerfomCompletion(false);
            dismissDropDown();
        }
    }

    /**
     * This email account Tokenizer can be used for lists where the items are
     * separated by '@' and one or more spaces.
     */
    public static class EmailAccountTokenizer implements Tokenizer {
        public int findTokenStart(CharSequence text, int cursor) {
            int i = 0;

            while (i < cursor && text.charAt(i) != '@') {
                i++;
            }
            // Don't perform filtering when there was no '@' input.
            if (i == cursor) {
                return cursor;
            }
            return i + 1;
        }

        public int findTokenEnd(CharSequence text, int cursor) {
            int i = cursor;
            int len = text.length();

            while (i < len) {
                if (text.charAt(i) == '@') {
                    return i;
                } else {
                    i++;
                }
            }
            return len;
        }

        public CharSequence terminateToken(CharSequence text) {
            // Highlight the replacing text part.
            SpannableString highlightSpan = new SpannableString(text);
            highlightSpan.setSpan(new BackgroundColorSpan(TextUtilities.HIGHLIGHT_COLOR_INT), 0,
                    highlightSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return highlightSpan;
        }
    }
}
