package com.android.testmode;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.net.Uri;
import android.view.WindowManager;
import android.view.KeyEvent;
import android.util.Log;

public class KeyTest extends Activity implements View.OnClickListener {

	private Button mButton;
	private TextView mTv;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_HOMEKEY_DISPATCHED);
        setContentView(R.layout.key_layout);
		mButton = (Button)findViewById(R.id.button_finish);
		mTv = (TextView)findViewById(R.id.key_value);
		mButton.setOnClickListener(this);
	}
	
	public void onAttachedToWindow() {
        // TODO Auto-generated method stub
        //this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
        super.onAttachedToWindow();
    }

	public boolean onKeyDown(int keyCode, KeyEvent event){
		Log.d("WindowManager","keycode="+keyCode);
		if(keyCode == KeyEvent.KEYCODE_BACK){
			mTv.setText("BACK");
		}else if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
			mTv.setText("VOL_DOWN");
		}else if(keyCode == KeyEvent.KEYCODE_VOLUME_UP){
			mTv.setText("VOL_UP");
		}else if(keyCode == KeyEvent.KEYCODE_HOME){
			mTv.setText("HOME");
		}else if(keyCode == KeyEvent.KEYCODE_MENU){
			mTv.setText("MENU");
		}/*else if(keyCode == KeyEvent.KEYCODE_SILENT_DOWN){
			mTv.setText("SILENT_DOWN");
		}else if(keyCode == KeyEvent.KEYCODE_SILENT_UP){
			mTv.setText("SILENT_UP");
		}else if(keyCode == KeyEvent.KEYCODE_SILENT){
			mTv.setText("SILENT");
		}*/
		
		//KEYCODE_SILENT_DOWN
		//KEYCODE_SILENT_UP
		//KEYCODE_SILENT
		return true;
	}

	public boolean onKeyUP(int keyCode, KeyEvent event){
		return true;
	}
	
	public void onClick(View v){
		finish();
	}
}
