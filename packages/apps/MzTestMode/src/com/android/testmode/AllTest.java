package com.android.testmode;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.util.Log;
import com.mediatek.common.featureoption.FeatureOption;
//yx@201209
import android.content.Context;
import android.content.res.Resources;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.content.ContentResolver;
import android.provider.Settings.System;

public class AllTest extends Activity implements OnClickListener{
	
	//0
	private Button mButtonMainCamera;
	private Button mButtonSubCamera;
	private Button mButtonFlashLight;
	//1
	private Button mButtonAudio;
	private Button mButtonSpeaker;
	private Button mButtonReceiver;
	
	//2
	private Button mButtonWifi;
	private Button mButtonBT;
	private Button mButtonGPS;

	//3
	private Button mButtonGsensor;
	private Button mButtonMsensor;
	private Button mButtonAsensor;

	//4
	private Button mButtonPsensor;
	private Button mButtonMemory;
	private Button mButtonFM;

	//5
	private Button mButtonPower;
	private Button mButtonBattery;
	private Button mButtonCPU;

	//6
	private Button mButtonSD;
	private Button mButtonKey;
	private Button mButtonDial;

	//7
	private Button mButtonVibrate;
	private Button mButtonTP;
	private Button mButtonLCD;
	
	//8
	private Button mButtonLED;
	private Button mButtonGyro;
	private Button mButtonHeadSet;
	
	
	//9
	private Button mButtonReset;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //0
        mButtonMainCamera = (Button)findViewById(R.id.button_main_camera);
        mButtonSubCamera = (Button)findViewById(R.id.button_sub_camera);
        mButtonFlashLight = (Button)findViewById(R.id.button_flashlight);
        //1
        mButtonAudio = (Button)findViewById(R.id.button_audio);
        mButtonSpeaker = (Button)findViewById(R.id.button_speaker);
        mButtonReceiver = (Button)findViewById(R.id.button_receiver);
        //2
        mButtonWifi = (Button)findViewById(R.id.button_wifi);
        mButtonBT = (Button)findViewById(R.id.button_bluetooth);
		mButtonGPS = (Button)findViewById(R.id.button_gps);
		
		/* +yx@20120903 WIFI */
		if (!FeatureOption.MTK_GPS_SUPPORT) {
			mButtonGPS.setVisibility(View.GONE);
		}

        //3
        mButtonGsensor = (Button)findViewById(R.id.button_gsensor);
        mButtonMsensor = (Button)findViewById(R.id.button_msensor);
		mButtonAsensor = (Button)findViewById(R.id.button_asensor);
		/* +yx@201210 环境光 */
		/*
		if(System.getString(getContentResolver(),System.VERSION_INFO).indexOf("NOTYPELIGHT") != -1){
			mButtonAsensor.setVisibility(View.GONE);
		}
		*/
		//4
        mButtonPsensor = (Button)findViewById(R.id.button_psensor);
        mButtonMemory = (Button)findViewById(R.id.button_memory);
		mButtonFM = (Button)findViewById(R.id.button_fm);

		//5
        mButtonPower = (Button)findViewById(R.id.button_power);
        mButtonBattery = (Button)findViewById(R.id.button_battery);
		mButtonCPU = (Button)findViewById(R.id.button_cpu);	
		//6
		mButtonSD = (Button)findViewById(R.id.button_sd);
		mButtonKey = (Button)findViewById(R.id.button_key);
		mButtonDial = (Button)findViewById(R.id.button_dial);
	
		//7
		mButtonVibrate = (Button)findViewById(R.id.button_vibrate);
		mButtonTP = (Button)findViewById(R.id.button_tp);
		mButtonLCD = (Button)findViewById(R.id.button_lcd);
	
		//8
		mButtonLED = (Button)findViewById(R.id.button_led);
		mButtonGyro	= (Button)findViewById(R.id.button_gyro);	
		mButtonHeadSet = (Button)findViewById(R.id.button_headset);	
		
		//9
		mButtonReset = (Button)findViewById(R.id.button_reset);
		
        initAllButtons();
    }
    
	private boolean getStatus(View v){
		String sp = this.getSharedPreferences("save",Context.MODE_PRIVATE).getString(Integer.toString(v.getId()),"0");
		if(sp.equals("1")){
			return true;
		} else{
			return false;
		}
	}
	protected void onResume() {
		super.onResume();
		Drawable mbg = getResources().getDrawable(R.drawable.btn_default_pressed);
		if(getStatus(mButtonMainCamera))//0
		mButtonMainCamera.setBackgroundDrawable(mbg);
		if(getStatus(mButtonSubCamera))
		mButtonSubCamera.setBackgroundDrawable(mbg);
		if(getStatus(mButtonFlashLight))
		mButtonFlashLight.setBackgroundDrawable(mbg);
		
		if(getStatus(mButtonAudio))//1
		mButtonAudio.setBackgroundDrawable(mbg);
		if(getStatus(mButtonSpeaker))
		mButtonSpeaker.setBackgroundDrawable(mbg);
		if(getStatus(mButtonReceiver))
		mButtonReceiver.setBackgroundDrawable(mbg);
		
		if(getStatus(mButtonWifi))//2
		mButtonWifi.setBackgroundDrawable(mbg);
		if(getStatus(mButtonBT))
		mButtonBT.setBackgroundDrawable(mbg);
		if(getStatus(mButtonGPS))
		mButtonGPS.setBackgroundDrawable(mbg);
		
		if(getStatus(mButtonGsensor))//3
		mButtonGsensor.setBackgroundDrawable(mbg);
		if(getStatus(mButtonMsensor))
		mButtonMsensor.setBackgroundDrawable(mbg);
		if(getStatus(mButtonAsensor))
		mButtonAsensor.setBackgroundDrawable(mbg);
		
		if(getStatus(mButtonPsensor))//4
		mButtonPsensor.setBackgroundDrawable(mbg);
		if(getStatus(mButtonMemory))
		mButtonMemory.setBackgroundDrawable(mbg);
		if(getStatus(mButtonFM))
		mButtonFM.setBackgroundDrawable(mbg);
		
		if(getStatus(mButtonPower))//5
		mButtonPower.setBackgroundDrawable(mbg);
		if(getStatus(mButtonBattery))
		mButtonBattery.setBackgroundDrawable(mbg);
		if(getStatus(mButtonCPU))
		mButtonCPU.setBackgroundDrawable(mbg);
		
		if(getStatus(mButtonSD))//6
		mButtonSD.setBackgroundDrawable(mbg);
		if(getStatus(mButtonKey))
		mButtonKey.setBackgroundDrawable(mbg);
		if(getStatus(mButtonDial))
		mButtonDial.setBackgroundDrawable(mbg);
		
		if(getStatus(mButtonVibrate))//7
		mButtonVibrate.setBackgroundDrawable(mbg);
		if(getStatus(mButtonTP))
		mButtonTP.setBackgroundDrawable(mbg);
		if(getStatus(mButtonLCD))
		mButtonLCD.setBackgroundDrawable(mbg);
	
		if(getStatus(mButtonLED))//8
		mButtonLED.setBackgroundDrawable(mbg);
		if(getStatus(mButtonGyro))
		mButtonGyro.setBackgroundDrawable(mbg);
		if(getStatus(mButtonHeadSet))
	    mButtonHeadSet.setBackgroundDrawable(mbg);
    }
	/*
	private void reset(){
		//0
		mButtonMainCamera.setBackgroundDrawable(null);
		
		mButtonSubCamera.setBackgroundDrawable(mbg);
		mButtonFlashLight.setBackgroundDrawable(mbg);
		
		//1
		mButtonAudio.setBackgroundDrawable(mbg);
		mButtonSpeaker.setBackgroundDrawable(mbg);
		mButtonReceiver.setBackgroundDrawable(mbg);
		
		//2
		mButtonWifi.setBackgroundDrawable(mbg);
		mButtonBT.setBackgroundDrawable(mbg);
		mButtonGPS.setBackgroundDrawable(mbg);
		
		//3
		mButtonGsensor.setBackgroundDrawable(mbg);
		mButtonMsensor.setBackgroundDrawable(mbg);
		mButtonAsensor.setBackgroundDrawable(mbg);
		
		//4
		mButtonPsensor.setBackgroundDrawable(mbg);
		mButtonMemory.setBackgroundDrawable(mbg);
		mButtonFM.setBackgroundDrawable(mbg);
		
		//5
		mButtonPower.setBackgroundDrawable(mbg);
		mButtonBattery.setBackgroundDrawable(mbg);
		mButtonCPU.setBackgroundDrawable(mbg);
		
		//6
		mButtonSD.setBackgroundDrawable(mbg);
		mButtonKey.setBackgroundDrawable(mbg);
		mButtonDial.setBackgroundDrawable(mbg);
		
		//7
		mButtonVibrate.setBackgroundDrawable(mbg);
		mButtonTP.setBackgroundDrawable(mbg);
		mButtonLCD.setBackgroundDrawable(mbg);
		
		//8
		mButtonLED.setBackgroundDrawable(mbg);
		mButtonGyro.setBackgroundDrawable(mbg);
		
	}
	*/
    private void initAllButtons(){
    	
    	//0
    	mButtonMainCamera.setOnClickListener(this);
    	mButtonSubCamera.setOnClickListener(this);
    	mButtonFlashLight.setOnClickListener(this);
    	//1
    	mButtonAudio.setOnClickListener(this);
    	mButtonSpeaker.setOnClickListener(this);
        mButtonReceiver.setOnClickListener(this);
    	
    	//2
    	mButtonWifi.setOnClickListener(this);
    	mButtonBT.setOnClickListener(this);
		mButtonGPS.setOnClickListener(this);

		//3
        mButtonGsensor.setOnClickListener(this);
        mButtonMsensor.setOnClickListener(this);
		mButtonAsensor.setOnClickListener(this);

		//4
        mButtonPsensor.setOnClickListener(this);
        mButtonMemory.setOnClickListener(this);
		mButtonFM.setOnClickListener(this);

		//5
        mButtonPower.setOnClickListener(this);
        mButtonBattery.setOnClickListener(this);
		mButtonCPU.setOnClickListener(this);

		//6
		mButtonSD.setOnClickListener(this);
		mButtonKey.setOnClickListener(this);
		mButtonDial.setOnClickListener(this);
	
		//7
		mButtonVibrate.setOnClickListener(this);
		mButtonTP.setOnClickListener(this);
		mButtonLCD.setOnClickListener(this);
		
		//8
		mButtonLED.setOnClickListener(this);
		mButtonGyro.setOnClickListener(this);
		mButtonHeadSet.setOnClickListener(this);
		
		//9
		mButtonReset.setOnClickListener(this);
    }
    public void onClick(View v){
    	int viewId = v.getId();
		this.getSharedPreferences("save",Context.MODE_PRIVATE).edit().putString(Integer.toString(viewId),"1").commit();
		
    	switch(viewId){
    	//0
    	case R.id.button_main_camera:
    	{
    		Intent intent = new Intent("android.intent.action.maincameratest");
    		intent.putExtra("frontOrBack", 0);
    		startActivity(intent);
    	}
    	break;
    		
    	case R.id.button_flashlight:
    	{
    		Intent intent = new Intent("android.intent.action.flashlighttest");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_sub_camera:
    	{
    		Intent intent = new Intent("android.intent.action.maincameratest");
    		intent.putExtra("frontOrBack", 1);
    		startActivity(intent);
    	}
    	break;
    	//1
    	case R.id.button_audio:
    	{
    		Intent intent = new Intent("android.intent.action.cyaudiotest");
    		startActivity(intent);
    	}
    	break;
    	
    	
    	case R.id.button_speaker:
    	{
    		Intent intent = new Intent("android.intent.action.speakertest");
    		startActivity(intent);
    	}
    	break;

    	case R.id.button_receiver:
    	{
    		Intent intent = new Intent("android.intent.action.receivertest");
    		startActivity(intent);
    	}
    	break;

    	
    	//2
    	case R.id.button_wifi:
    	{
    		Intent intent = new Intent();
    		//intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.wifi.WiFi");
    		intent.setClassName("com.android.settings", "com.android.settings.wifi.WifiSettings");
    		//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_bluetooth:
    	{
    		Intent intent = new Intent("android.settings.BLUETOOTH_SETTINGS");
			//intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.bluetooth.BTList");
			//intent.setClassName("com.android.settings", "com.android.settings.bluetooth.BluetoothSettings");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_gps:
    	{
    		Intent intent = new Intent();
			intent.setClassName("com.mediatek.ygps", "com.mediatek.ygps.YgpsActivity");
    		startActivity(intent);
    	}
    	break;

		//3
		case R.id.button_gsensor:
    	{
    		Intent intent = new Intent();
    		intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.GSensorTest");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_msensor:
    	{
    		Intent intent = new Intent();
			intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.MSensorTest");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_asensor:
    	{
    		Intent intent = new Intent();
			intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.ASensorTest");
    		startActivity(intent);
    	}
    	break;

		//4
		case R.id.button_psensor:
    	{
    		Intent intent = new Intent();
    		intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.PSensorTest");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_memory:
    	{
    		Intent intent = new Intent();
			intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.memory.Memory");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_fm:
    	{
    		Intent intent = new Intent();
			intent.setClassName("com.mediatek.FMRadio", "com.mediatek.FMRadio.FMRadioActivity");
    		startActivity(intent);
    	}
    	break;
		
		//5
		case R.id.button_power:
    	{
    		Intent intent = new Intent();
    		intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.power.Power");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_battery:
    	{
    		Intent intent = new Intent("android.intent.action.batterytest");
			//intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.memory.Memory");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_cpu:
    	{
    		Intent intent = new Intent();
			intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.cpufreq.Cpufreq");
    		startActivity(intent);
    	}
    	break;

		//6
		case R.id.button_sd:
    	{
    		Intent intent = new Intent("android.intent.action.sdtest");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_key:
    	{
    		Intent intent = new Intent("android.intent.action.keytest");
			//intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.memory.Memory");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_dial:
    	{
    		Intent intent = new Intent("android.intent.action.dialtest");
    		startActivity(intent);
    	}
    	break;

		//7
		case R.id.button_vibrate:
    	{
    		Intent intent = new Intent("android.intent.action.vibratetest");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_tp:
    	{
    		Intent intent = new Intent();
			intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.touchscreen.TouchScreenList");
    		startActivity(intent);
    	}
    	break;
    	
    	case R.id.button_lcd:
    	{
    		Intent intent = new Intent("android.intent.action.lcdtest");
    		startActivity(intent);
    	}
    	break;
		
		//8
		case R.id.button_led:
		{
			Intent intent = new Intent("android.intent.action.ledtest");
    		startActivity(intent);
		}
		break;
		
		case R.id.button_gyro:
		{
			Intent intent = new Intent();
			intent.setClassName("com.mediatek.engineermode", "com.mediatek.engineermode.GYROSensorTest");
    		startActivity(intent);
		}
		break;
		
		case R.id.button_headset:
    	{
    		Intent intent = new Intent("android.intent.action.headsettest");
    		startActivity(intent);
    	}
    	break;
		
		//9
		case R.id.button_reset:
		{
			this.getSharedPreferences("save",Context.MODE_PRIVATE).edit().clear().commit();
			finish();
		}
		
    	default:
    		break;
    	}
    }
}
