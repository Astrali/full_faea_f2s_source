package com.android.testmode;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.Environment;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import android.os.StatFs;
import java.util.List;
import java.lang.Integer;
import android.widget.TextView;
import android.widget.LinearLayout;
 
public class SDTest extends Activity{

	private TextView mSdAllView;
	private TextView mSdRestView;
	private TextView mSdMountView;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_layout);
		mSdAllView = (TextView)findViewById(R.id.sd_all);
		mSdRestView = (TextView)findViewById(R.id.sd_available);
		mSdMountView = (TextView)findViewById(R.id.sd_mount);
		
		String status = Environment.getExternalStorageState();
        String readOnly = "";
        //if (status.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            //status = Environment.MEDIA_MOUNTED;
            //readOnly = mRes.getString(R.string.read_only);
        //}
 
        if (status.equals(Environment.MEDIA_MOUNTED)) {
			
            try {
                File path = Environment.getExternalStorageDirectory();
                StatFs stat = new StatFs(path.getPath());
                long blockSize = stat.getBlockSize();
                long totalBlocks = stat.getBlockCount();
                long availableBlocks = stat.getAvailableBlocks();
                
                mSdAllView.setText(formatSize(totalBlocks * blockSize));
                mSdRestView.setText(formatSize(availableBlocks * blockSize) + readOnly);

            } catch (IllegalArgumentException e) {
                status = Environment.MEDIA_REMOVED;
            }
            
        } else {
        	mSdMountView.setText(R.string.sd_not_mounted);
        }

	}

	public void onResume() {
        super.onResume();
    }
	
    private String formatSize(long size) {
        return Formatter.formatFileSize(this, size);
    }
}
