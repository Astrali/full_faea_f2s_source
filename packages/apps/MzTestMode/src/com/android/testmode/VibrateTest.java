package com.android.testmode;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.os.Vibrator;
import android.view.WindowManager;
import java.lang.Runnable;
import android.os.Handler;
import android.content.Context;
//import android.content.Intent;
//import android.net.Uri;

public class VibrateTest extends Activity implements View.OnClickListener{

	private Button mButtonShort;
	private Button mButtonMiddle;
	private Button mButtonLong;
	private Button mButtonLoop;
	private Button mButtonStop;
	private Vibrator mVb;
	private StopVb mStopVb;
	private Handler mHandler;
	
	public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                setContentView(R.layout.vibrate_layout);
		mButtonShort = (Button)findViewById(R.id.vibrate_button_short);
		mButtonMiddle = (Button)findViewById(R.id.vibrate_button_middle);
		mButtonLong = (Button)findViewById(R.id.vibrate_button_long);
		mButtonLoop = (Button)findViewById(R.id.vibrate_button_loop);
		mButtonStop = (Button)findViewById(R.id.vibrate_button_stop);

		mButtonShort.setOnClickListener(this);
		mButtonMiddle.setOnClickListener(this);
		mButtonLong.setOnClickListener(this);
		mButtonLoop.setOnClickListener(this);
		mButtonStop.setOnClickListener(this);
		
		//mVb = new Vibrator();
		mVb = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		mStopVb = new StopVb();
		mHandler = new Handler();
	}

	public void onClick(View v){
		int vId = v.getId();
		switch(vId){
		case R.id.vibrate_button_short:
			mVb.vibrate(150);
			break;
		case R.id.vibrate_button_middle:
			mVb.vibrate(2000);
			break;
		case R.id.vibrate_button_long:
			mVb.vibrate(new long []{0,2000},0);
			break;
		case R.id.vibrate_button_loop:
			mHandler.removeCallbacks(mStopVb);
			mVb.vibrate(new long []{1000,2000},0);
			mHandler.postDelayed(mStopVb, 72*3600*1000);
			break;
		case R.id.vibrate_button_stop:
			mHandler.removeCallbacks(mStopVb);
			mVb.cancel();
			break;
		}
	}
	
	public void onPause(){
		super.onPause();
		mHandler.removeCallbacks(mStopVb);
		mVb.cancel();
	}
	class StopVb implements Runnable{
		public void run(){
			mVb.cancel();
		}
	}
}
