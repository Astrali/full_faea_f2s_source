package com.android.testmode;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.content.Context;
import com.android.dfish.AWLedControl;

public class LEDTest extends Activity implements View.OnClickListener{

	private Button mRedButton;
	private Button mGreenButton;
	private Button mBlueButton;
	private Button mWhiteButton;
	
	private int status = 0;
	private int level = 1;
	
	private static int NONE_STATUS = 0;
	private static int RED_STATUS = 1;
	private static int GREEN_STATUS = 2;
	private static int BULE_STATUS = 3;
	private static int WHITE_STATUS = 4;

	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.led_layout);
		mRedButton = (Button)findViewById(R.id.red_button);
		mRedButton.setOnClickListener(this);
		mGreenButton = (Button)findViewById(R.id.green_button);
		mGreenButton.setOnClickListener(this);
		mBlueButton = (Button)findViewById(R.id.blue_button);
		mBlueButton.setOnClickListener(this);
		mWhiteButton = (Button)findViewById(R.id.white_button);
		mWhiteButton.setOnClickListener(this);
		
		
	}

	public void onClick(View v){
		int viewId = v.getId();
		//closeAwLED();
    	switch(viewId){
			case R.id.red_button:
			{
				if(status != RED_STATUS || level > 3) level = 1;
				AWLedControl.isAwRedLedEnable(0,1,0,level);
				level++;
				status = RED_STATUS;
			}
				break;
			case R.id.green_button:
			{
				if(status != GREEN_STATUS || level > 3) level = 1;
				AWLedControl.isAwGreenLedEnable(0,1,0,level);
				level++;
				status = GREEN_STATUS;
			}
				break;
			case R.id.blue_button:
			{
				if(status != BULE_STATUS || level > 3) level = 1;
				AWLedControl.isAwBlueLedEnable(0,1,0,level);
				level++;
				status = BULE_STATUS;
			}
				break;
			case R.id.white_button:
			{
				if(status != WHITE_STATUS || level > 3) level = 1;
				AWLedControl.isAwWhiteEnable(0,1,0,level);
				level++;
				status = WHITE_STATUS;
			}
				break;
		}
	}
	
	private void closeAwLED(){
		AWLedControl.isAwRedLedEnable(0,0,0,1);
		AWLedControl.isAwGreenLedEnable(0,0,0,1);
		AWLedControl.isAwBlueLedEnable(0,0,0,1);
		AWLedControl.isAwWhiteEnable(0,0,0,1);
	}
	
	public void onPause(){
		super.onPause();
		closeAwLED();
	}
	

}
