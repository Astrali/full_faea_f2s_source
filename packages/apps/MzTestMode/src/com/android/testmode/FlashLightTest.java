package com.android.testmode;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class FlashLightTest extends Activity implements View.OnClickListener{
	
	private Button bt_on_off = null;
	private Camera mCamera = null;
	private Parameters mParameters = null;
	private boolean mOn = true;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("double fish","onCreate");
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
				//WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.flashlight_layout);
		mCamera = Camera.open();
		mParameters = mCamera.getParameters();
		bt_on_off = (Button) findViewById(R.id.flash_on_off);
		bt_on_off.setOnClickListener(this);
	}

	public void onClick(View v) {
		if (mOn) {
			bt_on_off.setText(R.string.string_off);
			mParameters.setFlashMode(Parameters.FLASH_MODE_TORCH);// ����
			mCamera.setParameters(mParameters);
			mOn = false;
		} else {
			bt_on_off.setText(R.string.string_on);
			mParameters.setFlashMode(Parameters.FLASH_MODE_OFF);// �ر�
			mCamera.setParameters(mParameters);
			mOn = true;
		}
	}
	protected void onStop() {
		super.onStop();
		//Log.d("double fish","onStop");
		if(mCamera != null){
			mCamera.release();
		}
		mCamera = null;
		finish();
		//parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
		//camera.setParameters(parameters);
		//camera.release();
	}

}
