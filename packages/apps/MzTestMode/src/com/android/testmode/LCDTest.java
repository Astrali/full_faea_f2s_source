package com.android.testmode;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.SeekBar;
import android.content.Intent;
import android.os.RemoteException;
import android.os.PowerManager;
import android.os.ServiceManager;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.content.Context;
//import android.net.Uri;

public class LCDTest extends Activity implements View.OnClickListener,SeekBar.OnSeekBarChangeListener{

	private Button mButton;
	private int mOldBrightness;
	private SeekBar mSbar;
	private static final int MINIMUM_BACKLIGHT = android.os.PowerManager.BRIGHTNESS_OFF + 30;
	private static final int MAXIMUM_BACKLIGHT = android.os.PowerManager.BRIGHTNESS_ON;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lcd_layout);
		mButton = (Button)findViewById(R.id.lcd_button_rgb);
		mSbar = (SeekBar)findViewById(R.id.skbLcd);
		mButton.setOnClickListener(this);
		mSbar.setMax(MAXIMUM_BACKLIGHT - MINIMUM_BACKLIGHT);
		mSbar.setOnSeekBarChangeListener(this);
		
	}

	public void onClick(View v){
		Intent intent = new Intent("android.intent.action.lcdrgbtest");
		startActivity(intent);
	}

	public void onProgressChanged(SeekBar seekBar, int progress,
            boolean fromTouch) {
        setBrightness(progress + MINIMUM_BACKLIGHT);
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
        // NA
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        // NA
    }

    private void setBrightness(int brightness) {
        //try {
           PowerManager power = (PowerManager) getSystemService(Context.POWER_SERVICE);
            if (power != null) {
                power.setBacklightBrightness(brightness);
            }
       // } catch (RemoteException doe) {
            
        //}        
    }

	public void onResume(){
		super.onResume();
		updateSeekBarBySetting();
	}
	
	public void onPause(){
		super.onPause();
		setBrightness(mOldBrightness);
	}
	private void updateSeekBarBySetting(){            	
        try {
            mOldBrightness = Settings.System.getInt(this.getContentResolver(), 
                Settings.System.SCREEN_BRIGHTNESS);
        } catch (SettingNotFoundException snfe) {
            mOldBrightness = MAXIMUM_BACKLIGHT;
        }
        mSbar.setProgress(mOldBrightness - MINIMUM_BACKLIGHT);        
    }

}
