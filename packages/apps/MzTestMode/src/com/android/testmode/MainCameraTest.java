package com.android.testmode;

import android.app.Activity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.Window;
import android.view.WindowManager;
import android.view.SurfaceView;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import java.io.IOException;
import android.content.Context;
import android.widget.FrameLayout;
import android.util.Log;

import java.util.List;
import android.hardware.Camera.Size;
import android.view.Display;

import android.view.MotionEvent;

public class MainCameraTest extends Activity{
	
	private CameraPreview mPreview;
	private Camera mCamera; 
	
	private boolean mPreviewRunning = false;
	private int mWhitch;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		mWhitch = getIntent().getIntExtra("frontOrBack", 0);
		

		setContentView(R.layout.camera_surface);

        mPreview = new CameraPreview(this);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);       

	}
	
	    public void onPause(){
		super.onPause();
        if(mCamera!=null){
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
        finish();
	}
	
	public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback { 
	    private SurfaceHolder mHolder; 
	    //private Camera mCamera; 

	    public CameraPreview(Context context) { 
	        super(context); 

	        mHolder = getHolder(); 
	        mHolder.addCallback(this); 
	        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); 
	    } 

	    public void surfaceCreated(SurfaceHolder holder) { 
	        try { 
	            mCamera = Camera.open(mWhitch);
			/*
		    if(mWhitch == 1)		
	            mCamera.setDisplayOrientation(180);
            else
                mCamera.setDisplayOrientation(0);
            */    
	            mCamera.setPreviewDisplay(holder); 
	            mCamera.startPreview(); 
	        } catch (IOException e) { 
	        } 
	    } 
	 

	    public void surfaceDestroyed(SurfaceHolder holder) { 
				if(mCamera!=null){
	    		mCamera.stopPreview();
	    		mCamera.release();
	    		mCamera = null;
                }
	    	} 

	 
	    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) { 
	        if (mHolder.getSurface() == null){ 
	          return; 
	        } 

	        try { 
	            mCamera.stopPreview(); 
	        } catch (Exception e){ 
	        } 
	 
	        try { 
				Camera.Parameters p = mCamera.getParameters();
				Size size = p.getPictureSize();
				List<Size> sizes = p.getSupportedPreviewSizes();
				Size optimalSize = getOptimalPreviewSize(sizes, (double)size.width/size.height);
				p.setPreviewSize(optimalSize.width, optimalSize.height);
				mCamera.setParameters(p);
	            mCamera.setPreviewDisplay(mHolder); 	            
	            mCamera.startPreview(); 
	        } catch (Exception e){ 
	        } 
	    } 
	}
	
	public boolean onTouchEvent(MotionEvent event){
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			mCamera.autoFocus(null);
		}
		return true;
	}
	
	private Size getOptimalPreviewSize(List<Size> sizes, double targetRatio) {
        final double ASPECT_TOLERANCE = 0.05;
        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        // Because of bugs of overlay and layout, we sometimes will try to
        // layout the viewfinder in the portrait orientation and thus get the
        // wrong size of mSurfaceView. When we change the preview size, the
        // new overlay will be created before the old one closed, which causes
        // an exception. For now, just get the screen size

        Display display = getWindowManager().getDefaultDisplay();
        int targetHeight = Math.min(display.getHeight(), display.getWidth());

        if (targetHeight <= 0) {
            // We don't know the size of SurefaceView, use screen height
            WindowManager windowManager = (WindowManager)
                    getSystemService(Context.WINDOW_SERVICE);
            targetHeight = windowManager.getDefaultDisplay().getHeight();
        }

        // try to find a size larger but closet to the desired preview size
        for (Size size : sizes) {
            if (targetHeight > size.height) continue;
            
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // not found, apply origional policy.
        if (optimalSize == null) {

            // Try to find an size match aspect ratio and size
            for (Size size : sizes) {
                double ratio = (double) size.width / size.height;
                if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            //Log.v(TAG, "No preview size match the aspect ratio");
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }
	
}
