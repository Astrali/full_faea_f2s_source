package com.android.testmode;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;
import android.widget.Button; 
import android.content.Context;
import android.content.IntentFilter;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.util.Log;

public class SpeakerTest extends Activity implements View.OnClickListener{

	private Button mButtonStart;
	private Button mButtonStop;
	private TextView mTv;
	
	private MediaPlayer mMediaPlayer;

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            int state = intent.getIntExtra("state",0);
			if(state == 0){
				mTv.setText(R.string.headset_no_plug);
			}else if(state == 1){
				mTv.setText(R.string.headset_plug);
			}
        }
    };
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speaker_layout);
		mButtonStart = (Button)findViewById(R.id.button_start);
		mButtonStop = (Button)findViewById(R.id.button_stop);
		mTv = (TextView)findViewById(R.id.headset_plug);
		mButtonStart.setOnClickListener(this);
		mButtonStop.setOnClickListener(this);
		AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
		/* +yx@20120405设置初使音量为最大 */
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);
		if(audioManager.isWiredHeadsetOn()){
			mTv.setText(R.string.headset_plug);
		}else{
			mTv.setText(R.string.headset_no_plug);
		}
		//int mode = audioManager.getMode();
		//audioManager.setSpeakerphoneOn(true);
		//Log.d("double fish","mode = "+mode);
 		//audioManager.setMode(AudioManager.MODE_IN_CALL);// 把模式调成听筒放音模式
		//mMediaPlayer = MediaPlayer.create(this,R.raw.test);
		//mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_HEADSET_PLUG);
		registerReceiver(mReceiver, filter);
	}
	
	public void onPause(){
		super.onPause();
		unregisterReceiver(mReceiver);
		if(mMediaPlayer != null && mMediaPlayer.isPlaying()){
			mMediaPlayer.stop();
		}	
		mMediaPlayer = null;
		finish();
	}
	
	public void onClick(View v){
		int id = v.getId();
		if(id == R.id.button_start){
			if(mMediaPlayer != null && mMediaPlayer.isPlaying())
				return;
			mMediaPlayer = MediaPlayer.create(this,R.raw.test);
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			/* +yx@20120405设置媒体循环播放 */
			mMediaPlayer.setLooping(true);
			mMediaPlayer.start();
		}else{
			if(mMediaPlayer != null){
				if(mMediaPlayer.isPlaying()){
					mMediaPlayer.stop();
				}
			}
		}
	}


		
}
