package com.android.testmode;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.SeekBar;
import android.widget.LinearLayout;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
//import android.content.Intent;
//import android.net.Uri;

public class LCDrgbTest extends Activity{

	private static final int EVENT_TICK = 1;
	private LinearLayout mLl;
	private int[] mColor = {Color.RED,Color.GREEN,Color.BLUE,Color.WHITE};
	private int mWhich = 0;
	
	private Handler mHandler = new Handler(){
		
		public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_TICK:
					if(mWhich > 3){
						mWhich = 0;
					}
					mLl.setBackgroundColor(mColor[mWhich]);
					mWhich++;
                    sendEmptyMessageDelayed(EVENT_TICK, 2000);
                    break;
            }
        }
	};
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lcd_rgb_layout);
		mLl = (LinearLayout)findViewById(R.id.background);
	}

	public void onResume(){
		super.onResume();
		mHandler.sendEmptyMessageDelayed(EVENT_TICK, 2000);
	}
}
