package com.android.testmode;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.content.Intent;
import android.net.Uri;

public class DialTest extends Activity implements View.OnClickListener{

	private Button mButton;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dial_layout);
		mButton = (Button)findViewById(R.id.dial_button);
		mButton.setOnClickListener(this);
	}

	public void onClick(View v){
		String number = "112";
		Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
		startActivity(intent);
	}
}
