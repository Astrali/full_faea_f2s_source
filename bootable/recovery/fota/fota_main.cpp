#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <linux/input.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/reboot.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "bootloader.h"
#include "common.h"
#include "cutils/properties.h"
#include "install.h"
#include "minui/minui.h"
#include "minzip/DirUtil.h"
#include "roots.h"
#include "recovery_ui.h"


extern bool fota_process(void);

int main(int argc, char **argv)
{
    int  ret = EXIT_SUCCESS;

    time_t start = time(NULL);


    // If these fail, there's not really anywhere to complain...
    //freopen(TEMPORARY_LOG_FILE, "a", stdout); setbuf(stdout, NULL);
    //freopen(TEMPORARY_LOG_FILE, "a", stderr); setbuf(stderr, NULL);

    //fprintf(stderr, "Starting recovery on %s", ctime(&start));

    //ui_init();

    load_volume_table2();

    ////if (INSTALL_SUCCESS != install_fota_delta_package("/data"))  {
    ret = install_fota_delta_package(argv[1]);
    if (INSTALL_SUCCESS != ret)  {
        fprintf(stderr, "Upgrade fail : 0x%X (%d)", ret, ret);
    }

    if (argc == 3)  {
        struct bootloader_message boot;
        memset(&boot, 0, sizeof(boot));
        strlcpy(boot.command, "boot-recovery", sizeof(boot.command));
        strlcpy(boot.recovery, "recovery\n", sizeof(boot.recovery));
        strlcat(boot.recovery, "--fota_delta_path=", sizeof(boot.recovery));
        strlcat(boot.recovery, argv[2], sizeof(boot.recovery));
        set_bootloader_message(&boot);
        sync();
    }

    return ret;
}

void write_all_log(void)
{
    return;
}
