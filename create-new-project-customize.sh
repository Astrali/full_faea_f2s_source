#!/bin/bash

 if [ $# -ne 6 ] ; then
	echo "ERROR : You must use this batch like this : ./create-cust-files.sh project-name modem gemini customer-name customer-mobile-name logo-dir-name"
	echo "      P1       project name : e.g. e903 e906 e908 ..."
	echo "      P2       g/w = gprs or wcdma"
	echo "      P3       gemini or not : gemini = d, single = s"
	echo "      P4       custom name : e.g. lifan , lenovo ..."
	echo "      P5       mobile name : e.g. U880, A6 ..."
	echo "      P6       logo source dir : e.g. qhd ..."
	echo "      Those 5 paramters will comb to a customize folder name"
	echo "All customized folder name as follow :"
	ls customize/
	exit
fi


# $1 - project name : e.g. e903, e906
# $2 - gprs or wcdma
# $3 - gemini or not : gemini = d, single = s
# $4 - customer name : e.g. lifan
# $5 - customer project name : e.g. a6


cust_dir=customize/$1-$2-$3-$4-$5
our_proj=$1
cust_logo_dir=$1

if [ ! -d mediatek/config/$our_proj ] ; then
	echo "Project $our_proj doesnot exist!!!"
	exit
fi

if [ -d $cust_dir ] ; then
	echo "ERROR : $cust_dir already exist !!!" 
	exit
else
	echo "Create $cust_dir !!!" 
	mkdir $cust_dir
	mkdir $cust_dir/animation
	mkdir $cust_dir/lk-logo
fi


echo "-> [MAKEFILE]"
# target makefile
echo "-> cp build/target/product/$our_proj.mk $cust_dir/$our_proj.mk.txt"
cp build/target/product/$our_proj.mk $cust_dir/$our_proj.mk.txt

# build makefile
echo "-> cp mediatek/config/$our_proj/ProjectConfig.mk $cust_dir/ProjectConfig.mk.txt"
cp mediatek/config/$our_proj/ProjectConfig.mk $cust_dir/ProjectConfig.mk.txt 

# kenel config
cp -av cp mediatek/config/$our_proj/autoconfig/kconfig/project $cust_dir/kernel_config.txt

# logo 
echo "-> [LOGO]"

echo "-> cp -r mediatek/custom/common/lk/logo/$6 $cust_dir/lk-logo"
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_01.bmp $cust_dir/lk-logo/${our_proj}_bat_10_01.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_02.bmp $cust_dir/lk-logo/${our_proj}_bat_10_02.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_03.bmp $cust_dir/lk-logo/${our_proj}_bat_10_03.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_04.bmp $cust_dir/lk-logo/${our_proj}_bat_10_04.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_05.bmp $cust_dir/lk-logo/${our_proj}_bat_10_05.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_06.bmp $cust_dir/lk-logo/${our_proj}_bat_10_06.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_07.bmp $cust_dir/lk-logo/${our_proj}_bat_10_07.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_08.bmp $cust_dir/lk-logo/${our_proj}_bat_10_08.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_09.bmp $cust_dir/lk-logo/${our_proj}_bat_10_09.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_100.bmp $cust_dir/lk-logo/${our_proj}_bat_100.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_10_10.bmp $cust_dir/lk-logo/${our_proj}_bat_10_10.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_01.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_01.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_02.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_02.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_03.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_03.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_04.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_04.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_05.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_05.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_06.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_06.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_07.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_07.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_08.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_08.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_09.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_09.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_animation_10.bmp $cust_dir/lk-logo/${our_proj}_bat_animation_10.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_bg.bmp $cust_dir/lk-logo/${our_proj}_bat_bg.bmp
cp mediatek/custom/common/lk/logo/$6/$6_bat_img.bmp $cust_dir/lk-logo/${our_proj}_bat_img.bmp
cp mediatek/custom/common/lk/logo/$6/$6_battery.bmp $cust_dir/lk-logo/${our_proj}_battery.bmp
cp mediatek/custom/common/lk/logo/$6/$6_charger_ov.bmp $cust_dir/lk-logo/${our_proj}_charger_ov.bmp
cp mediatek/custom/common/lk/logo/$6/$6_kernel.bmp $cust_dir/lk-logo/${our_proj}_kernel.bmp
cp mediatek/custom/common/lk/logo/$6/$6_low_battery.bmp $cust_dir/lk-logo/${our_proj}_low_battery.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_0.bmp $cust_dir/lk-logo/${our_proj}_num_0.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_1.bmp $cust_dir/lk-logo/${our_proj}_num_1.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_2.bmp $cust_dir/lk-logo/${our_proj}_num_2.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_3.bmp $cust_dir/lk-logo/${our_proj}_num_3.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_4.bmp $cust_dir/lk-logo/${our_proj}_num_4.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_5.bmp $cust_dir/lk-logo/${our_proj}_num_5.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_6.bmp $cust_dir/lk-logo/${our_proj}_num_6.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_7.bmp $cust_dir/lk-logo/${our_proj}_num_7.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_8.bmp $cust_dir/lk-logo/${our_proj}_num_8.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_9.bmp $cust_dir/lk-logo/${our_proj}_num_9.bmp
cp mediatek/custom/common/lk/logo/$6/$6_num_percent.bmp $cust_dir/lk-logo/${our_proj}_num_percent.bmp
cp mediatek/custom/common/lk/logo/$6/$6_uboot.bmp $cust_dir/lk-logo/${our_proj}_uboot.bmp
echo "-> cp mediatek/custom/$our_proj/lk/inc/cust_display.h $cust_dir/cust_display.h"
cp mediatek/custom/common/lk/include/target/cust_display.h $cust_dir/lk-cust_display.h 


#memory device
echo "->[MCP]"
cp -v mediatek/custom/$our_proj/preloader/inc/custom_MemoryDevice.h $cust_dir/

# volume config
#echo "-> [AUDIO PARAM AND SYSTEM VOLUME LEVEL]"
#echo "-> mediatek/custom/common/cgen/inc/audio_volume_custom_default.h $cust_dir/audio_volume_custom_default.h "
#cp mediatek/custom/common/cgen/inc/audio_volume_custom_default.h $cust_dir/audio_volume_custom_default.h 
#echo "-> cp frameworks/base/media/java/android/media/AudioManager.java  $cust_dir/AudioManager.java  "
#cp frameworks/base/media/java/android/media/AudioManager.java  $cust_dir/AudioManager.java 
#echo "-> cp frameworks/base/media/java/android/media/AudioService.java $cust_dir/AudioService.java "
#cp frameworks/base/media/java/android/media/AudioService.java $cust_dir/AudioService.java 

# codegen.dws
#echo "-> [CODEGEN]"
#cp -av mediatek/custom/$our_proj/kernel/dct/dct/codegen.dws $cust_dir/codegen.dws

# default setting value
echo "-> [DEFAULT SETTING]"
echo "-> cp frameworks/base/packages/SettingsProvider/res/values/defaults.xml $cust_dir/defaults.xml"
cp frameworks/base/packages/SettingsProvider/res/values/defaults.xml $cust_dir/defaults.xml 

# default launcher2 layout
#echo "-> [LAUNCHER LAYOUT]"
#echo "-> cp packages/apps/Launcher2/res/xml/default_workspace.xml $cust_dir/default_workspace.xml"
#cp packages/apps/Launcher2/res/xml/default_workspace.xml $cust_dir/default_workspace.xml

# default wallpaper
#echo "-> [DEFALUT WALLPAPER]"
#echo "-> cp frameworks/base/core/res/res/drawable/default_wallpaper.jpg $cust_dir/default_wallpaper.jpg"
#cp frameworks/base/core/res/res/drawable/default_wallpaper.jpg $cust_dir/default_wallpaper.jpg

# app icon

echo "!!! DONE !!! NOW, you must config all those files by your project !!!!!!!!!! And then commit all !!! "
