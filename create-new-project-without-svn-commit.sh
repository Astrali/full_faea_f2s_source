#!/bin/bash

if [ $# -ne 2 ] ; then
	echo "ERROR : commind like this ./create-new-project.sh cenon73_gb e908 "
else
	echo "->Create build/target/product/$2.mk"
	cp build/target/product/$1.mk build/target/product/$2.mk

#	echo "->Create vendor/mediatek/$2"
#	cp -r vendor/mediatek/$1 vendor/mediatek/$2
#	mv vendor/mediatek/$2/artifacts/out/target/product/$1 vendor/mediatek/$2/artifacts/out/target/product/$2
#	find vendor/mediatek/$2/ -name .svn | xargs rm -rf

	echo "->Create mediatek/config/$2"
	cp -r mediatek/config/$1 mediatek/config/$2
	find mediatek/config/$2/ -name .svn | xargs rm -rf

	echo "->Create bootable/lk"
	cp -r bootable/bootloader/lk/build-$1 bootable/bootloader/lk/build-$2
	mv bootable/bootloader/lk/build-$2/custom/$1 bootable/bootloader/lk/build-$2/custom/$2
	cp -av bootable/bootloader/lk/project/$1.mk bootable/bootloader/lk/project/$2.mk
	find bootable/bootloader/lk/build-$2/ -name .svn | xargs rm -rf

	echo "->Create mediatek/custom/$2"
	cp -r mediatek/custom/$1 mediatek/custom/$2
#	mv mediatek/custom/$2/uboot/inc/configs/$1.h mediatek/custom/$2/uboot/inc/configs/$2.h
	find mediatek/custom/$2/ -name .svn | xargs rm -rf

	echo "Create project $2 DONE!!!"
fi

