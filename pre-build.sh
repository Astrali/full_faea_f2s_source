#!/bin/bash

 if [ $# -ne 5 ] ; then
	echo "ERROR : You must use this batch like this : ./pre-build.sh project-name modem gemini customer-name customer-mobile-name"
	echo "      P1       project name : e.g. e903 e906 e908 ..."
	echo "      P2       g/w = gprs or wcdma"
	echo "      P3       gemini or not : gemini = d, single = s"
	echo "      P4       custom name : e.g. lifan , lenovo ..."
	echo "      P5       mobile name : e.g. U880, A6 ..."
	echo "      Those 5 paramters will comb to a customize folder name"
	echo "All customized folder name as follow :"
	ls customize/
	exit 1
fi


# $1 - project name : e.g. e903, e906
# $2 - gprs or wcdma
# $3 - gemini or not : gemini = d, single = s
# $4 - customer name : e.g. lifan
# $5 - customer project name : e.g. a6

cust_dir=customize/$1-$2-$3-$4-$5
our_proj=$1
cust_proj=$5
cust_logo_dir=$1

now_year=`date +%Y`
now_month=`date +%-m`
now_day=`date +%-d`
now_time=`date +%Y-%m-%d-%H-%M-%S`
if [ "$2" != "w" ] ; then
	build_ver=`date +V2G.%y.%m.%d`
else
	build_ver=`date +V3G.%y.%m.%d`
fi

if [ ! -d mediatek/config/$our_proj ] ; then
	echo "Project $our_proj doesnot exist!!!"
	exit 1
fi

if [ ! -d $cust_dir ] ; then
	echo "Custom folder $cust_dir doesnot exist!!!"
	exit 1
fi


#cat $cust_dir/customize_info.txt >> build/tools/buildinfo.sh
if [ -f $cust_dir/buildinfo.sh ] ; then
	cp -av $cust_dir/buildinfo.sh build/tools/buildinfo.sh
fi

echo "-> [Create VENDOR] vendor/mediatek/$1"
rm -rf vendor/mediatek/$1
cp -r vendor/mediatek/lenovo89_wet_jb2 vendor/mediatek/$1
mv vendor/mediatek/$1/artifacts/out/target/product/lenovo89_wet_jb2 vendor/mediatek/$1/artifacts/out/target/product/$1
find vendor/mediatek/$1/ -name .svn | xargs rm -rf

rm -rf mediatek/$1
cp -r mediatek/lenovo89_wet_jb2 mediatek/$1
mv mediatek/$1/artifacts/out/target/product/lenovo89_wet_jb2 mediatek/$1/artifacts/out/target/product/$1
find mediatek/$1/ -name .svn | xargs rm -rf

echo "-> [BUILD VERNO]"
# generate build ver mk 
if [ -f $cust_dir/temp.mk ] ; then
	rm $cust_dir/temp.mk
fi


echo "-> [KERNEL CONFIG]"
if [ -d $cust_dir/kernel_config.txt ] ; then
	cp -av cp  $cust_dir/kernel_config.txt  mediatek/config/$our_proj/autoconfig/kconfig/project
fi

echo "-> [DEFAULT RTC]"
# generate build time as rtc default time
if [ -f $cust_dir/cust_rtc.h ] ; then
	rm $cust_dir/cust_rtc.h 
fi
touch $cust_dir/cust_rtc.h 
if [ -f $cust_dir/rtc-mt6589.h ] ; then
	rm $cust_dir/rtc-mt6589.h
fi
touch $cust_dir/rtc-mt6589.h

# rtc-mt6589.h
echo "-> Generate $cust_dir/rtc-mt6589.h"
echo "/* This file auto gen by pre-build.sh */" >> $cust_dir/rtc-mt6589.h
echo "#ifndef _RTC_MT6589_H_" >> $cust_dir/rtc-mt6589.h
echo "#define _RTC_MT6589_H_" >> $cust_dir/rtc-mt6589.h
echo "#define RTC_YES		1" >> $cust_dir/rtc-mt6589.h
echo "#define RTC_NO		0" >> $cust_dir/rtc-mt6589.h
echo "#define RTC_OVER_TIME_RESET	RTC_YES" >> $cust_dir/rtc-mt6589.h
echo "#define RTC_DEFAULT_YEA		$now_year" >> $cust_dir/rtc-mt6589.h
echo "#define RTC_DEFAULT_MTH		$now_month" >> $cust_dir/rtc-mt6589.h
echo "#define RTC_DEFAULT_DOM		$now_day" >> $cust_dir/rtc-mt6589.h
echo "#endif /* _RTC_MT6589_H_ */" >> $cust_dir/rtc-mt6589.h

# cust_rtc.h
echo "-> Generate $cust_dir/cust_rtc.h"
echo "/* This file auto gen by pre-build.sh */" >> $cust_dir/cust_rtc.h
echo "#ifndef __CUST_RTC_H__" >> $cust_dir/cust_rtc.h
echo "#define __CUST_RTC_H__" >> $cust_dir/cust_rtc.h
echo "#define RTC_DEFAULT_YEA		$now_year" >> $cust_dir/cust_rtc.h
echo "#define RTC_DEFAULT_MTH		$now_month" >> $cust_dir/cust_rtc.h
echo "#define RTC_DEFAULT_DOM		$now_day" >> $cust_dir/cust_rtc.h
echo "#endif /* __CUST_RTC_H__ */" >> $cust_dir/cust_rtc.h

echo "-> mv $cust_dir/rtc-mt6589.h mediatek/custom/$our_proj/kernel/rtc/rtc/rtc-mt6589.h"
mv $cust_dir/rtc-mt6589.h mediatek/custom/$our_proj/kernel/rtc/rtc/rtc-mt.h
echo "-> mv $cust_dir/cust_rtc.h mediatek/custom/$our_proj/preloader/inc/cust_rtc.h"
mv $cust_dir/cust_rtc.h mediatek/custom/$our_proj/preloader/inc/cust_rtc.h

echo "-> [MAKEFILE]"
# target makefile
echo "-> cp $cust_dir/$our_proj.mk.txt build/target/product/$our_proj.mk"
cp $cust_dir/$our_proj.mk.txt build/target/product/$our_proj.mk

# build makefile
echo "-> cp $cust_dir/ProjectConfig.mk.txt mediatek/config/$our_proj/ProjectConfig.mk"
cp $cust_dir/ProjectConfig.mk.txt mediatek/config/$our_proj/ProjectConfig.mk

# add custom build verno 
echo "CUSTOM_BUILD_VERNO = $build_ver" >>  mediatek/config/$our_proj/ProjectConfig.mk


# lk logo 
echo "-> [LOGO]"
echo "-> cp -r $cust_dir/lk-logo mediatek/custom/common/lk/logo/$cust_logo_dir"
if [ -d mediatek/custom/common/lk/logo/$cust_logo_dir ] ; then
	rm -rf mediatek/custom/common/lk/logo/$cust_logo_dir 
fi
cp -r $cust_dir/lk-logo mediatek/custom/common/lk/logo/$cust_logo_dir
echo "-> cp $cust_dir/lk-cust_display.h mediatek/custom/$our_proj/lk/inc/cust_display.h"
cp $cust_dir/lk-cust_display.h mediatek/custom/common/lk/include/target/cust_display.h

#animation
echo "-> [ANIMATION]"
echo "->  cp $cust_dir/animation/Android.mk.txt frameworks/base/data/sounds/Android.mk"
rm -rf frameworks/base/data/animation
cp -r $cust_dir/animation frameworks/base/data/
mv frameworks/base/data/animation/Android.mk.txt frameworks/base/data/animation/Android.mk

# codegen.dws
if [ -f $cust_dir/codegen.dws ] ; then
	cp -av $cust_dir/codegen.dws mediatek/custom/${our_proj}/kernel/dct/dct/codegen.dws
else
	cp -av mediatek/custom/${our_proj}/kernel/dct/dct/codegen.dws mediatek/custom/${our_proj}/kernel/dct/dct/codegen.dws.bak.${now_time}
	svn revert mediatek/custom/${our_proj}/kernel/dct/dct/codegen.dws
fi

#mcp
echo "-> [MCP]"
cp -av $cust_dir/custom_MemoryDevice.h mediatek/custom/$our_proj/preloader/inc/custom_MemoryDevice.h

#ptgen
echo "-> [PTGEN]"
cp -av $cust_dir/partition_table_MT6589.xls mediatek/build/tools/ptgen/MT6589/partition_table_MT6589.xls

# default setting value
echo "-> [DEFAULT SETTING]"
echo "-> cp $cust_dir/defaults.xml frameworks/base/packages/SettingsProvider/res/values/defaults.xml"
cp $cust_dir/defaults.xml frameworks/base/packages/SettingsProvider/res/values/defaults.xml

#third app
echo "->[Third APK]"
rm -rf third_apk
cp -avr $cust_dir/third_apk third_apk
find third_apk -name .svn | xargs rm -rf

# default wallpaper
#echo "-> [DEFALUT WALLPAPER]"
cp -av $cust_dir/default_wallpaper.jpg frameworks/base/core/res/res/drawable-nodpi/default_wallpaper.jpg

# custom.info(bluetooth,wifi hotspot)
cp -av $cust_dir/custom.conf mediatek/config/common/custom.conf

echo "!!! DONE !!! NOW, you can build $1 for $4($5) by : ./mk $1 new "
